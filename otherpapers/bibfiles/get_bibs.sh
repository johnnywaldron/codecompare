#!/bin/bash

# PURPOSE: get the DB&LP .bib files for the given list of conferences

CONFERENCES="iticse sigcse"
YEARS="2018 2017 2016 2015 2014"

for CONF in ${CONFERENCES}; do
    for YEAR in ${YEARS}; do
        echo ${CONF} ${YEAR}
        OUTFILE=${CONF}${YEAR}.bib
        wget "https://dblp.uni-trier.de/search/publ/api?q=toc%3Adb%2Fconf%2F${CONF}/${CONF}${YEAR}.bht%3A&h=1000&format=bib1&rd=1a" -O ${OUTFILE}
    done
done


