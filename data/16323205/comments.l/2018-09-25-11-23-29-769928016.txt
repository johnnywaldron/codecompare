/*
  CS3071  Assignment 2 comments.l 
  Matthew Ng ngm1@tcd.ie 16323205
*/
%{
// nothing here
%}

%%

"{".*"}"	      { /*nothing*/ }
"**".*/\n       { /* ignore */ }
\n              { printf("%s", yytext); }
.		            { printf("%s", yytext); }

%%

int main()
{
  yylex();
  return 0;
}
