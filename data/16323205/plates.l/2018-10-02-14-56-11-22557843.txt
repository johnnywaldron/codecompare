/*
  CS3071  Assignment 3 plates.l 
  Matthew Ng ngm1@tcd.ie 16323205
*/
%{
const int CURRENT_YEAR = 18;
const int MIN_PLATE_YEAR = 87;
const int SPEC_CHANGE_YEAR = 14;
const char* currentCounties[26] = 
            { "C", "CE", "CN", "CW", "D", "DL", "G", "KE", "KK", "KY",
              "L", "LD", "LH", "LM", "LS", "MH", "MN", "MO", "OY", "RN",
              "SO", "T", "W", "WH", "WX", "WW" };
const char* prevCounties[29] = 
            { "C", "CE", "CN", "CW", "D", "DL", "G", "KE", "KK", "KY",
              "L", "LK", "LD", "LH", "LM", "LS", "MH", "MN", "MO", "OY", 
              "RN", "SO", "TN", "TS", "W", "WD", "WH", "WX", "WW" };
void checkReg(char* plateNo);
%}

%%
[0-9]{2}(-[A-Z]{1,2})(-[0-9]{6})       { checkReg(yytext); }

[0-9]{2}[12](-[A-Z]{1,2})(-[0-9]{6})   { checkReg(yytext); }

[ \t\n]                                  { /*ignore*/ }
.	                                     { printf("INVALID\n"); }

%%

int main()
{
  yylex();
  return 0;
}

bool contains(char** array, char* string)
{
  for (int i = 0; i < sizeof(array); i++)
    if (strcmp(array[i], string) == 0)
      return true;
  return false
}

void checkReg(char* plateNo)
{
  int year = atoi(strtok(plateNo, "-"));
  char* county = strtok(null, "-");
  if ( year / 100 > 0)
    year /= 10;
      
  if ((year >= 0 && year <= CURRENT_YEAR)
    if (year >= SPEC_YEAR_CHANGE)
      if (contains(currentCounties, county)) {
        printf("%d\n", CURRENT_YEAR - year);
        return;
      }
    else
      if(contains(prevCounties, county)) {
        printf("%d\n", CURRENT_YEAR - year);
        return;
      }  
  
  if (year >= MIN_PLATE_YEAR && year <= 99))
    if (contains(prevCounties, county)) {
      printf("%d\n", (2000+CURRENT_YEAR) - (1900+year));
      return;  
    }
    
  printf("INVALID\n");    
}

