%{
# include "roman.tab.h"
void yyerror();
%}

%%
[ \t\r\f\v]*	{}
I 				{ return ONE; }
V  				{ return FIVE; }
X 				{ return TEN; }
L   			{ return FIFTY; }
C 				{ return ONEHUNDRED; }
D 				{ return FIVEHUNDRED; }
M 				{ return ONETHOUSAND; }
\n   			{ return EOL; }
.				{ yyerror(); }
%%			