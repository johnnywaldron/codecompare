%{
#include <stdlib.h>
#include <stdio.h>
#include "romcalc.tab.h"
int yylex();
void yyerror(char* s);
void printResult(int x);
%}

%token EOL ADD SUB MUL DIV ZERO ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND

%%
calclist: /* NOTHING */
 | calclist exp EOL { ($2 == 0) ? printf("Z\n") : printResult($2); }
 ;

exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: number
 | '{' exp '}' { $$ = $2; }
 ;

number: thousands lessThanThousand {$$ = $1 + $2; }
| lessThanThousand {$$ = $1;}
| ZERO {$$ = $1;}
;

thousands: THOUSAND {$$ = $1;}
| thousands THOUSAND {$$ = $1 + $2;}
;
lessThanThousand: nineHundreds {$$ = $1;}
| lessThanNineHundred {$$ = $1;}
;

nineHundreds: HUNDRED THOUSAND {$$ = $2 - $1;}
| HUNDRED THOUSAND lessThanHundred {$$ = $2 - $1 + $3;}
;

lessThanNineHundred: FIVEHUNDRED lessThanFiveHundred {$$ = $1 + $2;}
| FIVEHUNDRED {$$ = $1;}
| lessThanFiveHundred {$$ = $1;}
;

lessThanFiveHundred: fourHundreds {$$ = $1;}
| lessThanFourHundred {$$ = $1;}
;

fourHundreds: HUNDRED FIVEHUNDRED {$$ = $2 - $1;}
| HUNDRED FIVEHUNDRED lessThanHundred {$$ = $2 - $1 + $3;}
;

lessThanFourHundred: hundreds lessThanHundred {$$ = $2 + $1;}
| hundreds {$$ = $1;}
| lessThanHundred {$$ = $1;}
;

hundreds: HUNDRED {$$ = $1;}
| HUNDRED HUNDRED {$$ = $1 + $2;}
| HUNDRED HUNDRED HUNDRED {$$ = $1 + $2 + $3;}
;

lessThanHundred: nineties {$$ = $1;}
| lessThanNinety {$$ = $1;}
;

nineties: TEN HUNDRED {$$ = $2 - $1;}
| TEN HUNDRED lessThanTen {$$ = $2 - $1 + $3;}
;

lessThanNinety: FIFTY lessThanforty {$$ = $2 + $1;}
| FIFTY {$$ = $1;}
| lessThanforty {$$ = $1;}
| forties {$$ = $1;}
;

forties: TEN FIFTY {$$ = $2 - $1;}
| TEN FIFTY lessThanTen {$$ = $2 - $1 + $3;}
;

lessThanforty: tens lessThanTen {$$ = $2 + $1;}
| tens {$$ = $1;}
| lessThanTen {$$ = $1;}
;

tens: TEN {$$ = $1;}
| TEN TEN {$$ = $1 +$2;}
| TEN TEN TEN {$$ = $1 + $2 + $3;}
;

lessThanTen: ONE TEN {$$ = $2 - $1;}
| FIVE {$$ = $1;}
| FIVE units {$$ = $2 + $1;}
| ONE FIVE {$$ = $2 - $1;}
| units {$$ = $1;}
;

units: ONE {$$ = $1;}
| ONE ONE {$$ = $1 + $2;}
| ONE ONE ONE {$$ = $1 + $2 + $3;}
;
%%
int main()
{
    yyparse();
    return 0;
}
void printResult(int x)
{
    if(x < 0)
    {
        printf("-");
        printResult(x * -1);
        return;
    }
    if(x >= 1000)
    {
        printf("%s", "M");
        printResult(x - 1000);
        return;
    }
    else if(x >= 900)
    {
        printf("%s", "CM");
        printResult(x - 900);
        return;
    }
    else if(x >= 500)
    {
        printf("%s", "D");
        printResult(x - 500);
        return;
    }
    else if(x >= 400)
    {
        printf("%s", "CD");
        printResult(x - 400);
        return;
    }
    else if(x >= 100)
    {
        printf("%s", "C");
        printResult(x - 100);
        return;
    }
    else if(x >= 90)
    {
        printf("%s", "XC");
        printResult(x - 90);
        return;
    }
    else if(x >= 50)
    {
        printf("%s", "L");
        printResult(x - 50);
        return;
    }
    else if(x >= 40)
    {
        printf("%s", "XL");
        printResult(x - 40);
        return;
    }
    else if(x >= 10)
    {
        printf("%s", "X");
        printResult(x - 10);
        return;
    }
    else if(x >= 9)
    {
        printf("%s", "IX");
        printResult(x - 9);
        return;
    }
    else if(x >= 5)
    {
        printf("%s", "V");
        printResult(x - 5);
        return;
    }
    else if(x >= 4)
    {
        printf("%s", "IV");
        printResult(x - 4);
        return;
    }
    else if(x >= 1)
    {
        printf("%s", "I");
        printResult(x - 1);
        return;
    }
    else if(x == 0)
    {
        printf("\n");
        return;
    }
}
void yyerror(char *s)
{
fprintf(stderr, "%s\n", s);
}
