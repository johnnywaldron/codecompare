%{
	int currentYear = 2018;
	int valid = 0;
	int year = 0;
%}

%%
[0-9]"-"C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|W|WD"-"[0-9]{1,6}	{valid = 1}
"-"[0-9]{1,6}		{ }
[0-9]{2,3}	{year = atoi(yytext); if (year>99){year = year/10;}  if (year<87){year += 2000;}else{year+=1900;} ; year = currentYear-year;  }
C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|W|WD		{}
"-"
[ \t]
\n

%%

int main()
{
  yylex();
  if (valid == 1){printf ("%d\n",year);}
	else printf ("INVALID") ; 
  return 0;
}
