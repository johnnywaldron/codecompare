%{

%}

%%

[0-9]{2,3}	{int year = atoi(yytext); printf ("%d",year) ; }
C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|W|WD		{ printf("%s",yytext) ; }
[0-9]{1,6}		{ int sequence = atoi(yytext); printf ("%d",sequence) ;}

%%

int main()
{
  yylex();
  
  return 0;
}
