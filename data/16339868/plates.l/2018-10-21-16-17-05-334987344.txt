%{

%}

OLDCOUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NEWCOUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
NEWYEAR	[0-9]{2}[12]
OLDYEAR	[0-9]{2}
SEQUENCENO [0-9]{1,6}[ \t\n]+
%%
{OLDYEAR}-{OLDCOUNTY}-{SEQUENCENO}	{ char ystr[2]; strncpy(ystr, yytext, 2); 						 
 					int y = atoi(ystr); if(y<=13){y+=2000;} 									 
 					else{y+=1900; } printf("%d\n", 2018-y); }
{NEWYEAR}-{NEWCOUNTY}-{SEQUENCENO}	{ char ystr[2]; strncpy(ystr, yytext, 2); 
 					int y = atoi(ystr); printf("%d\n", 18-y); 
 					}
.+[\t\n]			{ printf("INVALID"); }
\n				{ }

%%
int main()
{
	yylex();
	return 0;
}
