%{
#include "romcalc.tab.h"
#include <stdio.h>
%}

%%
"+" {return ADD;}
"-" {return SUB;}
"/" {return DIV;}
"*" {return MUL;}
"{" {return OP;}
"}" {return CP;}
Z {return ZERO;}
I {return ONE;}
V {return FIVE;}
X {return TEN;}
L {return FIFTY;}
C {return HUNDRED;}
D {return FIVEHUNDRED;}
M {return THOUSAND;}
\n  {return EOL}
[ \t]*  {}
. {yyerror();}
%%
