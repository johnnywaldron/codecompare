%{
#include "romcalc.tab.h"
#include <stdio.h>
%}
%%
"+" {return ADD;}
"-" {return SUB;}
"/" {return DIV;}
"*" {return MUL;}
"{" {return yytext[0];}
"}" {return yytext[0];}
Z {yylval = 0; return ZERO;}
I {yylval = 1; return ONE;}
V {yylval = 5; return FIVE;}
X {yylval = 10; return TEN;}
L {yylval = 50; return FIFTY;}
C {yylval = 100; return HUNDRED;}
D {yylval = 500; return FIVEHUNDRED;}
M {yylval = 1000; return THOUSAND;}
\n  {return EOL;}
[ \t]*  {}
. {printf("syntax error\n"); exit(0);}
%%
