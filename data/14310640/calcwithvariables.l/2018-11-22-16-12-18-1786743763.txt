%{

#include "calcwithvariables.tab.h"
%}

%option noyywrap nodefault yylineno

%%
":=" {return ASSIGN;}
"print" { return PRINT; }

[a-z] { yylval.s = lookup(yytext); return NAME; }

[0-9] { yylval.d = atoi(yytext); return NUMBER; }

"-" |
"+" |
"*" |
"/" |
";"		{return yytext[0];}

"//".*  
[ \t]
"\n"
.	{ printf("syntax error\n"); exit(0);}
%%