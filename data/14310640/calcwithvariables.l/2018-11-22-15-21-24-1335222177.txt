%{
typedef int YYSTYPE;
#include "calcwithvariables.tab.h"
%}

%option noyywrap nodefault yylineno

%%
[a-z] { yylval.s = lookup(yytext); return NAME; }

[0-9] { yylval.d = atoi(yytext); return NUMBER; }

"-" |
"+" |
"*" |
"/" |
";"		{return yytext[0];}

":=" {return ASSIGN;}
"print" { return PRINT; }

"//".*  
[ \t]   { /* ignore white space */ }
.	{ printf("syntax error\n"); exit(0);}
%%