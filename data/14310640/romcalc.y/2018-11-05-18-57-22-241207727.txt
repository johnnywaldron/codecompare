%{
typedef int YYSTYPE;
#  include <stdio.h>
#  include <stdlib.h>
#  include <stdarg.h>
#  include <string.h>

/* interface to the lexer */
extern int yylineno; /* from lexer */
void yyerror(char *s, ...);
int yyparse();
int yylex();

char *romans[13] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
int integers[13] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};

struct ast {
  int nodetype;
  struct ast *l;
  struct ast *r;
};

struct numval {
  int nodetype;	
  int number;
};

struct ast *newast(int nodetype, struct ast *l, struct ast *r);
struct ast *newnum(int d);
int eval(struct ast *);
void treefree(struct ast *);
void toStr(char *src,int val);
%}


/* declare tokens */
%token IV IX XL XC CD CM
%token EOL

%%

calclist: /* nothing */
| calclist exp EOL {
	 char *rom =.malloc(sizeof(char) * 1000);
	 toStr(rom,eval($2));
     printf("%s\n", rom);
     treefree($2);
 }

exp: factor
 | exp '+' factor { $$ = newast('+', $1,$3); }
 | exp '-' factor { $$ = newast('-', $1,$3);}
 ;

factor: term
 | factor '*' term { $$ = newast('*', $1,$3); }
 | factor '/' term { $$ = newast('/', $1,$3); }
 ;

term: roman   { $$ = newnum($1); }
 | '{' exp '}' { $$ = $2; }
 | '-' term    { $$ = newast('M', $2, NULL); }
 ;

roman: IV		{$$ = 4;}
| IX			{$$ = 9;}
| XL			{$$ = 40;}
| XC 			{$$ = 90;}
| CD 			{$$ = 400;}
| CM 			{$$ = 900;}
| 'I' 	 		{$$ = 1;}
| 'V'			{$$ = 5;}
| 'X'			{$$ = 10;}
| 'L'			{$$ = 50;}
| 'C' 			{$$ = 100;}
| 'D' 			{$$ = 500;}
| 'M' 			{$$ = 1000;}
| roman roman	{$$ = $1 + $2;}


%%

void yyerror(char *s, ...)
{
  va_list ap;
  va_start(ap, s);

  fprintf(stderr, "%d: error: ", yylineno);
  vfprintf(stderr, s, ap);
  fprintf(stderr, "\n");
}

struct ast *
newnum(int d)
{
  struct numval *a = malloc(sizeof(struct numval));
  
  if(!a) {
    yyerror("out of space");
    exit(0);
  }
  a->nodetype = 'K';
  a->number = d;
  return (struct ast *)a;
}

struct ast *
newast(int nodetype, struct ast *l, struct ast *r)
{
  struct ast *a = malloc(sizeof(struct ast));
  
  if(!a) {
    yyerror("out of space");
    exit(0);
  }
  a->nodetype = nodetype;
  a->l = l;
  a->r = r;
  return a;
}

int
eval(struct ast *a)
{
  int v;

  switch(a->nodetype) {
  case 'K': v = ((struct numval *)a)->number; break;

  case '+': v = eval(a->l) + eval(a->r); break;
  case '-': v = eval(a->l) - eval(a->r); break;
  case '*': v = eval(a->l) * eval(a->r); break;
  case '/': v = eval(a->l) / eval(a->r); break;
  case '|': v = eval(a->l); if(v < 0) v = -v; break;
  case 'M': v = -eval(a->l); break;
  default: printf("internal error: bad node %c\n", a->nodetype);
  }
  return v;
}

void
treefree(struct ast *a)
{
  switch(a->nodetype) {

    /* two subtrees */
  case '+':
  case '-':
  case '*':
  case '/':
    treefree(a->r);

    /* one subtree */
  case '|':
  case 'M':
    treefree(a->l);

    /* no subtree */
  case 'K':
    free(a);
    break;

  default: printf("internal error: free bad node %c\n", a->nodetype);
  }
}

void
toStr(char *src, int val){
	if (val == 0){
		strcpy(src, "Z");
	}

	int i;
	for (i = 0; i < 13; i++){
		while (val >= integers[i]){
			strcat(src, romans[i]);
			val -= integers[i];
		}
	}
}

int main()
{
  yyparse();
  return 0;
}
