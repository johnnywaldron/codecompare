%{
int nums = 0;
%}
%option noyywrap
%%

[0-9]+	{ if (atoi(yytext) % 2 == 0) {nums++;}}
\n 	{ nums++;}

%%

int main()
{
  yylex();
  printf("%d¥n", nums);
  return 0;
}
