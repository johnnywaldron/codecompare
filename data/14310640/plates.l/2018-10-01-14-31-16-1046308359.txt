%{
int current_year = 2018;
%}
%option noyywrap
%%

[0-9]{2}[1-2]\-[A-Z]{1,2}\-[0-9]{1,6}[ \t|\n]+	{ printf("%d\n", current_year - (2000 + atoi(yytext)/10));}

%%

int main()
{
  yylex();
  return 0;
}
