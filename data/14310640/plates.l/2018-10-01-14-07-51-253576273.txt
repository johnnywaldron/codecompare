%{
int current_year = 2018;
%}
%option noyywrap
%%

[0-9]{3}\-[A-Z]{1,2}\-[0-9]{1,6}[ \t|\n]+ {printf("hello");}
\n 	{ printf("\n");}
. 	{ printf("%s", yytext);}

%%

int main()
{
  yylex();
  return 0;
}
