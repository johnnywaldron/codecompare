%{
int current_year = 2018;
%}
%option noyywrap
%%

(1[3-8][1-2])\-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)\-[0-9]{1,6}[ \t|\n]+	{ printf("%d\n", current_year - (2000 + atoi(yytext)/10));}
(0[0-9]|1[0-2]|8[7-9]|9[0-9])\-(LK|TN|TS|WD|C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|W|WH|WX|WW)\-[0-9]{1,6}[ \t|\n]+	{ int y = atoi(yytext);
											  					  int x = 0;
											 					  if(y <= 12){
											  					  	x = 2000+y;
											  					  	printf("%d\n", current_year - x);
											  					  }else if (y >= 87){
											  					  	x = 1900+y;
											  						printf("%d\n", current_year - x);
											  					  }
																}
^[ \t]*\n  {printf("INVALID\n");}
.	{}
%%

int main()
{
  yylex();
  return 0;
}
