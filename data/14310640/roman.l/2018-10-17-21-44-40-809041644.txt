%{
typedef int YYSTYPE;
#include "roman.tab.h"
%}

%option noyywrap nodefault yylineno

%%
IV		{return IV;}
IX		{return IX;}
XL		{return XL;}
XC		{return XC;}
CD		{return CD;}
CM		{return CM;}

I |
V |
X |
L |
C |
D |
M		{ return yytext[0]; }
\n      { return EOL; }
"//".*  
[ \t]   { /* ignore white space */ }
.	{ printf("syntax error\n"); exit(0);}
%%