%{
typedef int YYSTYPE;
#include "romcalc.tab.h"
%}

%option noyywrap nodefault yylineno

%%
IIII+ |
VV+ |
XXXXX+ |
LL+ |
CCCCC+ |
DD+ 	{printf("syntax error\n"); exit(0);}

IV		{return IV;}
IX		{return IX;}
XL		{return XL;}
XC		{return XC;}
CD		{return CD;}
CM		{return CM;}

I |
V |
X |
L |
C |
D |
M		{ return yytext[0]; }

"-" |
"+" |
"*" |
"/" |
"{" |
"}"		{return yytext[0];}

\n      { return EOL; }
"//".*  
[ \t]   { /* ignore white space */ }
.	{ printf("syntax error\n"); exit(0);}
%%