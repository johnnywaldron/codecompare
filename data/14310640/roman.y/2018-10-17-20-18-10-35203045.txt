%{
#  include <stdio.h>
#  include <stdlib.h>
#  include <stdarg.h>
/* interface to the lexer */
extern int yylineno; /* from lexer */
void yyerror(char *s, ...);
int yyparse();
int yylex();

int sum = 0;
%}


/* declare tokens */
%token IV IX XL XC CD CM ERROR
%token EOL

%type <a> term

%%

conv: /* nothing */
| conv roman EOL{printf("%d\n", sum); sum =0;}
| conv EOL
| ERROR {return -1;}

roman: term		
| term roman 

term: IV		{sum += 4;}
| IX			{sum += 9;}
| XL			{sum += 40;}
| XC 			{sum += 90;}
| CD 			{sum += 400;}
| CM 			{sum += 900;}
| 'I' 	 		{sum += 1;}
| 'V'			{sum += 5;}
| 'X'			{sum += 10;}
| 'L'			{sum += 50;}
| 'C' 			{sum += 100;}
| 'D' 			{sum += 500;}
| 'M' 			{sum += 1000;}


%%

void yyerror(char *s, ...)
{
  va_list ap;
  va_start(ap, s);

  fprintf(stderr, "%d: error: ", yylineno);
  vfprintf(stderr, s, ap);
  fprintf(stderr, "\n");
}

int main()
{
  yyparse();
  return 0;
}
