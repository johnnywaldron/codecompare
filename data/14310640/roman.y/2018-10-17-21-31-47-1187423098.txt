%{
typedef int YYSTYPE;
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))
#  include <stdio.h>
#  include <stdlib.h>
#  include <stdarg.h>
/* interface to the lexer */
extern int yylineno; /* from lexer */
void yyerror(char *s, ...);
int yyparse();
int yylex();

struct val{
	int v;
	struct val *n;
};
struct val *s1,*s2;
int sum = 0;
%}


/* declare tokens */
%token IV IX XL XC CD CM
%token EOL

%type <a> term

%%

conv: /* nothing */
| conv roman EOL{ s2->v=sum; struct val *t= malloc(sizeof(struct val)); s2->n=t; s2=t; sum =0;}
| conv EOL	{while (s1->v > 0){printf("%d\n", s1->v);s1 = s1->n;}}

roman: term		
| term roman 

term: IV		{sum += 4;}
| IX			{sum += 9;}
| XL			{sum += 40;}
| XC 			{sum += 90;}
| CD 			{sum += 400;}
| CM 			{sum += 900;}
| 'I' 	 		{sum += 1;}
| 'V'			{sum += 5;}
| 'X'			{sum += 10;}
| 'L'			{sum += 50;}
| 'C' 			{sum += 100;}
| 'D' 			{sum += 500;}
| 'M' 			{sum += 1000;}


%%

void yyerror(char *s, ...)
{
  va_list ap;
  va_start(ap, s);

  fprintf(stderr, "%d: error: ", yylineno);
  vfprintf(stderr, s, ap);
  fprintf(stderr, "\n");
}

int main()
{
  s1,s2 = malloc(sizeof(struct val));
  yyparse();
  return 0;
}
