%{
int star_comment = 0;
int bracket_comment = 0;
%}
%option noyywrap
%%

[\t]*\*{2}	{ 
				if (star_comment == 0 && bracket_comment == 0){
					star_comment = 1;
				}
			}
\n 			{
				if (star_comment == 1){
					star_comment = 0;
					printf("\n");
				}else if (bracket_comment == 0){
					printf("\n");
				}
			}
.			{
				if (star_comment == 0 && bracket_comment == 0){
					printf("%s", yytext);
				}
			}

%%

int main()
{
  yylex();
  return 0;
}
