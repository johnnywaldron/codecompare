%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
void yyerror(char *s);
%}

/* declare tokens */
%token ONE FIVE TEN FIFTY ONEHUNDRED FIVEHUNDRED ONETHOUSAND
%token EOL

%%

expr: /* nothing */
 | expr value EOL { printf("%d\n", $2); }
 ; 

value: term 
 | value term { if ($2 <= $1)
                {
                    $$ = $1 + $2;
                }
                else if ($2 > $1 &&( ($2 == 5 && $1 == 1)||($2 == 10 && $1 == 1) || ($2 == 50 && $1 == 10) || ($2 == 100 && $1 == 10) || ($2 == 500 && $1 == 100) || ($2 == 1000 && $1 == 100))) 
                {
                    $$ = $2 - $1;
                }
              }
 ;

term: ONE { $$ = $1; }
 | FIVE { $$ = $1; }
 | TEN { $$ = $1; }
 | FIFTY { $$ = $1; }
 | ONEHUNDRED  { $$ = $1; }
 | FIVEHUNDRED { $$ = $1; }
 | ONETHOUSAND { $$ = $1; }
 ;
 
%% 
  
int main()
{
  //printf("> "); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("%s", s);
  exit(0);
}








