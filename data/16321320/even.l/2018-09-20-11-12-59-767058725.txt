%{
int countEven = 0;
int currNum = 0;
%}

%%
[0-9]+  { currNum = atoi(yytext); countEven += (currNum % 2 == 0) ? 1:0; }
.       { ; }
%%

int main(){
        yylex();
        printf("%d", countEven);
        return 0;
}
