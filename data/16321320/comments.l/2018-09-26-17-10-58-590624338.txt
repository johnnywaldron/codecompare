%{

%}

%%
\"(.)*\"                        {printf("%s", yytext);}
"**"(.)*                        {}
\{([^\}])*\}                    {}
\{([^}]|\n)*                    {printf("syntax error\n"); return 1;}
([^{]|\n)*/\}                   {printf("%s syntax error\n", yytext); return 1;}
%%

int main(){
  yylex();
  return 0;
}

