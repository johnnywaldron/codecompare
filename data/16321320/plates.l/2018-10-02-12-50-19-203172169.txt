%{
#define CURR_YEAR 2018
int year;
int diff;
%}

%%
[0-9][0-9][0-9]/"-"("C"|"CE"|"CN"|"CW"|"D"|"DL"|"G"|"KE"|"KK"|"KY"|"L"|"LD"|"LH"|"LM"|"LS"|"MH"|"MN"|"MO"|"OY"|"RN"|"SO"|"T"|"W"|"WH"|"WX"|"WW")"-"[0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]? {
  year = atoi(yytext);
  year = (year / 10) + 2000;
  diff = CURR_YEAR - year;
  printf("%d\n", diff);
}
[0-9][0-9]/"-"("C"|"CE"|"CN"|"CW"|"D"|"DL"|"G"|"KE"|"KK"|"KY"|"L"|"LD"|"LH"|"LK"|"LM"|"LS"|"MH"|"MN"|"MO"|"OY"|"RN"|"SO"|"TN"|"TS"|"W"|"WD"|"WH"|"WX"|"WW")"-"[0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]? {
  year = atoi(yytext);
  if(year <= 99){
    year = year + 1900;
  } else {
    year = year + 2000;
  }
  diff = CURR_YEAR - year;
  printf("%d\n", diff);
}
[\t\n]  {}
. {}
%%

int main(){
  yylex();
  return 0;
}

