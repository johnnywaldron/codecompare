%{
#include <stdio.h>
%}

%%

"**".*	{}

"}"		{ printf ("Error: syntax error.\n");
          break;
         }



"{"     { /* ignore multiline comments. */
           register int c;

           for (;;)
           {
             while ((c = input ()) != '}' && c != EOF)
               ; /* eat up text of comment. */

             if (c == '}')
             {
                break; /* found the end. */
             }

             if (c == EOF)
             {
               printf ("Error: EOF in comment.\n");
               break;
             }
           }
         }


%%

main(){
	yylex();
	return 0;
}



