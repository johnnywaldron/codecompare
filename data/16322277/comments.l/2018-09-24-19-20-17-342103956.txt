%{
#include <stdio.h>
static void single_line_comment(void);
%}

%%

"**"	{single_line_comment();}

%%

main(){
	yylex();
	return 0;
}

static void single_line_comment(){
	char c;
	while((c = input()) != '\n'){
		putchar(c);
	}
	putchar('\n');
}

