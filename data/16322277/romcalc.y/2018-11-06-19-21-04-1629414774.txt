%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  int yylex();
  int yyparse();
  void yyerror(char *s);
  void decToRoman();
%}

%token NUM
%token ADD SUB MUL DIV ABS LEFTBRACKET RIGHTBRACKET
%token EOL
%output = "romcalc.tab.c"

%%

calclist:
| calclist exp EOL { decToRoman($2); }
;

exp: factor
| exp NUM { $$ = $$ + $2; }
| exp ADD factor { $$ = $1 + $3; }
| exp SUB factor { $$ = $1 - $3; }
;

factor: bracket
| factor NUM { $$ = $$ + $2; }
| factor MUL bracket { $$ = $1 * $3; }
| factor DIV bracket { $$ = $1 / $3; }
;

bracket: value
| LEFTBRACKET exp RIGHTBRACKET { $$ = $2; }
;

value: NUM
 | value NUM   { $$ = $$ + $2;  }
 ;

%%


int main()
{
  yyparse();
	return 0;
}

void yyerror(char *s)
{
  printf("error: %s\n", s);
}

void decToRoman(int num)  
{    
	if(num == 0) {
     	printf("Z\n");
     	return;
     }

	int flag = 0;
	if(num < 0){
		num = -1*num;
		flag = 1;
	}

     char m[16][16] = {"", "M", "MM", "MMM", "MMMM", "MMMMM", "MMMMMM", "MMMMMMM", "MMMMMMMM", "MMMMMMMMM", "MMMMMMMMMM", "MMMMMMMMMMM", "MMMMMMMMMMMM", "MMMMMMMMMMMMM", "MMMMMMMMMMMMMM", "MMMMMMMMMMMMMMM"};

     char c[10][5] = {"", "C", "CC", "CCC", "CD", "D",
                        "DC", "DCC", "DCCC", "CM"}; 
     char x[10][5] = {"", "X", "XX", "XXX", "XL", "L",  
                        "LX", "LXX", "LXXX", "XC"}; 
     char i[10][5] = {"", "I", "II", "III", "IV", "V",  
                        "VI", "VII", "VIII", "IX"};
          
     // Converting to roman
     char *thousands = m[num/1000];
     char *hundereds = c[(num%1000)/100];
     char *tens =  x[(num%100)/10];
     char *ones = i[num%10];


     char ans[100] = { 0 };
     strcat(ans,thousands); 
     strcat(ans,hundereds);
     strcat(ans,tens);
     strcat(ans,ones);
     
   
     if(flag == 0){
     	printf("%s\n", ans);
     	return;
     }
     if(flag == 1){
     	printf("-%s\n", ans);
     	return;
     }


     printf("Error");
} 



