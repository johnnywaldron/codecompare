%{
int count = 0;
%}
%%
[0-9]*[02468]	{ count++; }
[a-zA-Z]+	{}
\n		{}
.		{}
%%
int main()
{
	yylex();
	printf("%d\n", count);
	return 0;
}

