%{

%}

countiesPresent (C)|(CE)|(CN)|(CW)|(D)|(DL)|(G)|(KE)|(KK)|(KY)|(L)|(LD)|(LH)|(LM)|(LS)|(MH)|(MN)|(MO)|(OY)|(RN)|(SO)|(T)|(W)|(WH)|(WX)|(WW)
countiesPre2013 (C)|(CE)|(CN)|(CW)|(D)|(DL)|(G)|(KE)|(KK)|(KY)|(L)|(LD)|(LH)|(LM)|(LS)|(MH)|(MN)|(MO)|(OY)|(RN)|(SO)|(T)|(W)|(WH)|(WX)|(WW)|(LK)|(TN)|(TS)|(T)|(WD)
/* any 2 numbers followed by a 1 or 2 = new license */
yearPresent [0-9]{2}[12]
/* any two numbers = old license */
yearPre2013 [0-9]{2}
/*any 1 too 6 numbers followed any white space */
numbers [0-9]{1,6}[ \t\n\r\f\v]+

%%

{yearPresent}-{countiesPresent}-{numbers} {	char year[2];
						/*copy year , ignore trailing white space*/
						strncpy(year,yytext,2);
						int numYear = atoi(year);
						/*years since registration = - 18 */
						numYear = 18 - numYear;
						printf("%d\n",numYear);
					  }
{yearPre2013}-{countiesPre2013}-{numbers} {	char year [2];
						strncpy(year,yytext,2);
						int numYear = atoi(year);
						/*same as present years except if num year > 12 = bought in 1980's +*/
						if(numYear <= 12){
							numYear = numYear + 2000;
						}else{
							numYear = numYear + 1900;
						}
						/*years since registration = - 2018 */
						numYear = 2018 - numYear;
						printf("%d\n", numYear);
					  }
\n					  {}

%%

int main(){
	yylex();
	return 0;
}
