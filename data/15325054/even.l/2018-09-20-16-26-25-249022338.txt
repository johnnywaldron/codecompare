/* Assignment 1 : Cormac O'Dwyer : 15325054 */

%{
int evenNum = 0;
%}

%%
[0-9]+ { int i = atoi(yytext); if((i%2) == 0) evenNum++;}
\n {}
.  {}
%%

int main(){
	yylex();
	printf("%d", evenNum);
	return 0;
}
