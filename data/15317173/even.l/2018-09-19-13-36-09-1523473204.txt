%{
	int numOfEven = 0;
%}

%%

[0-9]+[02468][^0-9] {numOfEven++;}

%%

int main()
{
	yylex();
	printf("%d\n", numOfEven);
	return 0;
}
