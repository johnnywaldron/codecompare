%{
#include "roman.h"
	void yyerror(char *s);
	int yylval;
%}

%%

"I" { return ONE; }
"V" { return FIVE; }
"X" { return TEN; }
"L" { return FIFTY; }
"C" { return HUNDRED; }
"D" { return FIVE_HUNDRED; }
"M" { return THOUSAND; }

%%



