%{
	int count = 0; 
%}


%%

[a-zA-z0-9]*	{ if(count == 0) printf("%s", yytext); }	//any word or digit
\n				{ if(count == 0) printf("%s", yytext); }	//new line
"**".*			{ /* Print nothing here*/ }				//For ** comments
"{"				{ count++; }
"}"				{ count--; }


%%

int main()
{
	yylex();
	if(count != 0) {
		printf("syntax error\n");
	}
	return 0;
}


