%{
	int syntaxError = 0;
	int skip = 0; 
%}

%%

[**].*			{ printf("here");}	//For ** comments
[a-zA-z].+		{ if(skip == 0) {printf("%s", yytext);} }

%%

int main()
{
	yylex();
	printf("syntaxError: %d   skip: %d", syntaxError, skip);
	if(syntaxError == 1 || skip == 1) {
		printf("syntax error\n");
	}
	return 0;
}


