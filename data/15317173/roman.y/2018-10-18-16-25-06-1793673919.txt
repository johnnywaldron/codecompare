%{
#include <stdio.h>
//#include <stdlib.h>
int yylex();
//int yyparse();
void yyerror(char *s);
%}

%token ONE FIVE TEN FIFTY HUNDRED FIVE_HUNDRED THOUSAND 
%token ABS EOL

%%
roman_numeral: //nothing
 | roman_numeral numeral EOL { printf("= %d\n> ", $2); }
 ;

numeral: term
 | numeral term { $$ = $1 + $2;}
 ;


term:
 | ONE  		{ $$ = 1; }
 | FIVE 		{ $$ = 5; }
 | TEN			{ $$ = 10; }
 | FIFTY 		{ $$ = 50; }
 | HUNDRED		{ $$ = 100; }
 | FIVE_HUNDRED	{ $$ = 500; }
 | THOUSAND		{ $$ = 1000; }
 ;

%%

int main()
{
  printf("> "); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
	fprintf(stderr, "error: %s\n", s);
}
