%{
#include <string.h>
int chars = 0;
%}

%%

[0-9][02468]|[02468]	{chars += strlen(yytext); }
[13579]
\n

%%

int yywrap()
{

}

int main()
{
  yylex();
  printf("%8d\n",chars);
  return 0;
}
