/* Solution by Eoin Dowling - 16327673 - dowlineo@tcd.ie */

/* Declarations */
%{
int integerCount = 0;
int evenCount = 0;
/* for debugging purposes */
int regex1 = 0;
int regex2 = 0;
int regex3 = 0;
int regex4 = 0;
%}

%%
[[:digit:]]*[02468] {evenCount++;}	/*  Handle Even integers  */
[[:digit:]]+	; /* ignore any Odd integers */ 
[[:space:]]+	; /* ignore what we would consider 'whitespace' */
[^[:space:]]+	; /* ignore anything else we might come across */
%%
int main()
{
    yylex();
    printf("%d\n", evenCount);
    return 0;
}
