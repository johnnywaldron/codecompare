%{
int comm1 = 0;
int comm2 = 0;
int str = 0;
%}

%%
["]	{if (str==1){str=0;}else{str=1;}printf(yytext);}
"**"	{if (str==1){printf(yytext);}else{comm1 = 1;}}
\n	{comm1 = 0; if (comm2==0){printf("\n");}}
"{"	{if (str==1){printf(yytext);}else if (comm1==0){comm2=1;}}
"}"	{if (str==1){printf(yytext);}else if (comm2==0){printf("syntax error\n");}else{comm2=0;}}
.	{if (comm1==0 && comm2==0){printf(yytext);}}
%%

int main()
{
	yylex();
	if (comm2==1)
	{
		printf("syntax error\n");
	}
	return 0;
}
