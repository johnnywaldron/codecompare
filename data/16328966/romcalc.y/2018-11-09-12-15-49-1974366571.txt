%{
#  include <stdio.h>
int yylex();
void yyerror();
%}

%token I V X L C D M ADD SUB MUL DIV EOL

%%
calclist:
| calclist exp EOL { decimal2roman($2); printf("\n") }
;

exp: factor
| factor ADD factor { $$ = $1 + $3; }
| factor SUB factor { $$ = $1 - $3; }
;

factor: thousand
| thousand MUL thousand { $$ = $1 * $3; }
| thousand DIV thousand { $$ = $1 / $3; }
;

thousand:
| M hundred { $$ = 1000 + $2; }
| M M hundred { $$ = 2000 + $3; }
| M M M hundred { $$ = 3000 + $4; }
| hundred { $$ = $1; }
;

hundred:
| C ten { $$ = 100 + $2; }
| C C ten { $$ = 200 + $3; }
| C C C ten { $$ = 300 + $4; }
| C D ten { $$ = 400 + $3; }
| D ten { $$ = 500 + $2; }
| D C ten { $$ = 600 + $3; }
| D C C ten { $$ = 700 + $4; }
| D C C C ten { $$ = 800 + $5; }
| C M ten { $$ = 900 + $3; }
| ten { $$ = $1; }
;

ten:
| X one { $$ = 10 + $2; }
| X X one { $$ = 20 + $3; }
| X X X one { $$ = 30 + $4; }
| X L one { $$ = 40 + $3; }
| L one { $$ = 50 + $2; }
| L X one { $$ = 60 + $3; }
| L X X one { $$ = 70 + $4; }
| L X X X one { $$ = 80 + $5; }
| X C one { $$ = 90 + $3; }
| one { $$ = $1; }
;

one:  { $$ = 0; }
| I { $$ = 1; }
| I I { $$ = 2; }
| I I I { $$ = 3; }
| I V { $$ = 4; }
| V { $$ = 5; }
| V I { $$ = 6; }
| V I I { $$ = 7; }
| V I I I { $$ = 8; }
| I X { $$ = 9; }
;
%%

int main()
{
  yyparse();
  return 0;
}

void decimal2roman(int num){
    int decimal[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
    char *symbol[] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
    int i = 0;

    if ( num == 0 )
    {
      printf("Z");
      return;
    }

    while(num){
        while(num/decimal[i]){
            printf("%s",symbol[i]);
            num -= decimal[i];
        }
        i++;
    }
}

void yyerror()
{
  printf("syntax error\n");
  exit(0);
}
