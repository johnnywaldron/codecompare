%{
# include "romcalc.tab.h"
void yyerror();
%}

int value;

%%
"+"   { return ADD; }
"-"   { return SUB; }
"*"   { return MUL; }
"/"   { return DIV; }
"I"   { return I; }
"V"   { return V; }
"X"   { return X; }
"L"   { return L; }
"C"   { return C; }
"D"   { return D; }
"M"   { return M; }
\n    { return EOL; }
[ \t] {}
.     { yyerror(); }
%%
