%{
int valid = 0;
int year = 0;
%}

YEAR1 [8][7-9]|[90][0-9]|[1][12]|13[1|2]
YEAR2 1[4-8][1|2]
NUMBER [0-9]{1,6}
COUNTY1 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|W|WH|WX|WW|LK|TN|TS|WD
COUNTY2 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|LK|TN|TS|WD

%%
{YEAR1}-{COUNTY1}-{NUMBER}\n	{valid = 1; yyless(2); year = atoi(yytext); return 1;}
{YEAR2}-{COUNTY2}-{NUMBER}\n	{valid = 1; yyless(2); year = atoi(yytext); return 1;}
\n	{return 1;}
[ \t] 	{}
.+	{}
%% 

int main()
{
	while (yylex())
	{
		if (valid == 0)
		{
			printf("INVALID\n");
		}
		else
		{
			if ( year < 86 )
			{
				printf("%d\n", 18-year);
			}
			else
			{
				printf("%d\n", 118-year);
			}
			valid = 0;
		}
	}
	
	return 0;
}

