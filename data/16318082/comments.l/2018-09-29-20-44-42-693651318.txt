%{
#include <string.h>
int bracketCount = 0;
int leftBracket = 0;
int rightBracket = 0;
%}

%%

[a-zA-Z]+     	  {if(bracketCount==0) printf("%s", yytext);}
\".*\"		  {printf("%s", yytext);}
\n	          {if(bracketCount==0) printf("%s", yytext);}
"**"[^"].*        {/* do nothing*/}	
"{" 	 	  {bracketCount = 1; leftBracket++;}
"}"	          {bracketCount = 0; rightBracket++;}
.	          {if(bracketCount==0) printf("%s", yytext);}

%%

int main()
{

yylex();

if((leftBracket == 0) && (rightBracket > 0)  ){

printf("syntax error\n");

return 0;

}


if(bracketCount != 0){

printf("syntax error\n");

}


return 0;
}
