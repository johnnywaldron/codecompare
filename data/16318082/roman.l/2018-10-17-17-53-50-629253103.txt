/* */
%{
  #include <stdio.h>
  void yyerror(char *s);
  int yylex();
%}
  

%%
  int NUMBER = 0;

[I]	 NUMBER += 1;
[IV] NUMBER += 4;
[V]	 NUMBER += 5;
[IX] NUMBER += 9;
[X]	 NUMBER += 10;
[XL] NUMBER += 40;
[L]	 NUMBER += 50;
[XC] NUMBER += 90;
[C]	 NUMBER += 100;
[CD] NUMBER += 400;
[D]	 NUMBER += 500;
[CM] NUMBER += 900;
[M]	 NUMBER += 1000;

[ \s\t\r]	|
\n	{printf("%d\n",NUMBER); NUMBER=0;}



.* 	  { yyerror("syntax error\n"); }
%%

