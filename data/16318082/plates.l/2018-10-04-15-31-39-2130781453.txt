%{
#include <string.h>

int currentYear = 18;
int checkYear;
int yearsOld;
int before2000 = 100;
int oldCar; 
int valid = 0; 

%}

YEAR ^\t*[0-9]{2}[1|2]*[-][C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|WH|WX|WW|L|LK|TN|TS|T|W|WD][-][0-9]{1,6}




%%

{YEAR} {	
		
		checkYear=atoi(yytext); 
		if(checkYear > 100){
		checkYear = checkYear / 10;
		}
		if(checkYear > currentYear){
		   oldCar = before2000 -checkYear; 
		   yearsOld = oldCar+currentYear; 
		   printf("%d", yearsOld);
		   } 
		else{yearsOld =currentYear-checkYear; printf("%d", yearsOld);}
}




^\n	   {}

.	   {printf("INVALID");}

%%

int main()
{
  yylex();
  return 0;
}

