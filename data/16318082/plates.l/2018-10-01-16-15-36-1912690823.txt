%{
#include <string.h>

int currentYear = 18;
int checkYear;
int yearsOld;

%}

%%

^\t*[0-9]{2}  {checkYear=atoi(yytext); yearsOld =currentYear-checkYear; printf("%d", yearsOld);}
^\n	   {}
[\t]       {}
.	   {}

%%

int main()
{
  yylex();
  return 0;
}

