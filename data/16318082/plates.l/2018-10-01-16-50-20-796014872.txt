%{
#include <string.h>

int currentYear = 18;
int checkYear;
int yearsOld;
int before2000 = 100;
int oldCar; 

%}

%%

^\t*[0-9]{2}  {checkYear=atoi(yytext); if(checkYear > currentYear){oldCar = before2000 -checkYear; yearsOld = oldCar+currentYear; printf("INVALID\n");} else{yearsOld =currentYear-checkYear; printf("%d", yearsOld);}}
^\n	   {}
.	   {}

%%

int main()
{
  yylex();
  return 0;
}

