%{
#include <string.h>

int currentYear = 17;
int checkYear;
int yearsOld;
int before2000 = 100;
int oldCar; 

%}

%%

^\t*[0-9]{2}  {checkYear=atoi(yytext); if(checkYear > currentYear){oldCar = before2000 -checkYear; yearsOld = oldCar+currentYear; printf("%d", yearsOld-1);} else{yearsOld =currentYear-checkYear; printf("%d", yearsOld);}}

^\n	   {}
[\t]       {}
.	   {}

%%

int main()
{
  yylex();
  return 0;
}

