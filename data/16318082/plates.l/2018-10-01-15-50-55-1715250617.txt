%{
#include <string.h>

int currentYear = 18;
int checkYear;
int yearsOld;

%}

%%

^[0-9]{2}  {checkYear=atoi(yytext); currentYear - checkYear = yearsOld; printf("%s", yearsOld);}
.	    {}

%%

int main()
{
  yylex();
  return 0;
}

