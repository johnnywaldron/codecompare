%{
#include <string.h>

int currentYear = 18;
int checkYear;
int yearsOld;

%}

%%

^[0-9]{2}  {checkYear=atoi(yytext); yearsOld =currentYear-checkYear; printf("%d", yearsOld);}
[ \s\t\r]* {}
.	   {}

%%

int main()
{
  yylex();
  return 0;
}

