%{
int i;
int result = 0;
int j;
int fix = 0;
%}

%%

[0-9]+	{i=atoi(yytext); if(i%2==0){result++;}}
\n      {j=atoi(yytext); {fix++;}}
. 	{j=atoi(yytext); {fix++;}}

%%

int main()
{
  yylex();
  printf("%d\n",result);
	return 0;
}

