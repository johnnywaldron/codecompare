    /* calculator */
%{
    #include "calcwithvariables.tab.h"
 void yyerror(char *s);
int yylex();


%}
EXP	([Ee][-+]?[0-9]+)
%option noyywrap
%%

"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"|"     { return ABS; }
":="    {return ASSIGNMENT;}
[0-9]+	{ yylval = atoi(yytext); return NUMBER; }


"print" {return PRINT; }

[a-z]  { yylval = *yytext -'a'; return VARIABLE; }

[ \t \r \n]      ;   { /* ignore white space */ }

";" { return EOL; }
[Z] {return ERROR;}



%%
