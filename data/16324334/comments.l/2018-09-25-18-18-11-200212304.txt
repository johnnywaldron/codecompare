%{
int inComment = 0;
%}

%%

"{" {inComment=1;}
"}" {if(inComment==1) {inComment=0;} else {printf("syntax error\n");}}
"**".*	{}
\n		{}
.		{if (inComment == 0) {printf("%s", yytext);} }

%%

int main()
{
  yylex();
  if (inComment == 1) { printf("syntax error\n"); }
  return 0;
}
