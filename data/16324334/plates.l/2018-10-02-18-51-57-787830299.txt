%{

%}

DIGIT [0-9]
BIANUAL [1-2]
COUNTYNEW C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTYOLD C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WH|WX|WW|WD
PLATE [0-9]|[0-9][0-9]|[0-9][0-9][0-9]|[0-9][0-9][0-9][0-9]|[0-9][0-9][0-9][0-9][0-9]|[0-9][0-9][0-9][0-9][0-9][0-9]

%%

{DIGIT}{DIGIT}{BIANUAL}(-){COUNTYNEW}(-){PLATE}		{printf("Valid\n");
														char* yearN = yytext;
														char destN[1];
														strncpy(destN, yearN, 2);
														printf(destN);}
														
{DIGIT}{DIGIT}(-){COUNTYOLD}(-){PLATE}				{printf("Valid\n");
														char* yearO = yytext;
														char destO[1];
														strncpy(destO, yearO, 2);
														printf(destO);}

\n		{}
. 		{}


%%

int main()
{
  yylex();
  return 0;
}
