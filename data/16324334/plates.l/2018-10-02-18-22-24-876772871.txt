%{

%}

DIGIT [0-9]
BIANUAL [1-2]
COUNTYNEW [C,CE,CN,CW,D,DL,G,KE,KK,KY,L,LD,LH,LM,LS,MH,MN,MO,OY,RN,SO,T,W,WH,WX,WW]
COUNTYOLD [C,CE,CN,CW,D,DL,G,KE,KK,KY,L,LK,LD,LH,LM,LS,MH,MN,MO,OY,RN,SO,TN,TS,W,WH,WX,WW,WD]
PLATE {DIGIT}{DIGIT}{DIGIT}{DIGIT}{DIGIT}

%%

{DIGIT}{DIGIT}{BIANUAL}[-].[^-]		{printf("Valid\n");}

\n	   {}


%%

int main()
{
  yylex();
  return 0;
}
