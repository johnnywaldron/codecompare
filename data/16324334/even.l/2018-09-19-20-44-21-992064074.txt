
%{
int numCount = 0;
%}

%%
[0-9]+ {i=atoi(yytext); if(i%2==0) numCount++; } 
%%

int main()
{
  yylex();
  printf("%8d", numCount);
  return 0;
}

