%{
    #include <stdio.h>
    #include <math.h>
    void yyerror(char *);
    int yylex(void);
    int symbols[26];
	int hasError = 0;
%}

%output "calcwithvariables.tab.c"
%token NUMBER VARIABLE EOL ASSIGNMENT PRINT
%token ADD SUB MUL DIV ABS

%%
calclist: /* nothing */
 | ERROR {hasError = 1; printf("syntax error\n", s); return 0;}
 | calclist PRINT exp EOL { if (hasError == 0) printf("%d\n", $3); else return 0;}
 | calclist exp EOL {}
 ; 

exp: factor 
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term 
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: NUMBER 
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 | VARIABLE {$$ = symbols[$1]; }
 | VARIABLE ASSIGNMENT exp {symbols[$1] = $3; }
 ;
%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("syntax error\n", s);
  hasError = 1;
}

