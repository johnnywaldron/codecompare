%{ 
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
%}
%output "roman.tab.c"
%token ONE FIVE TEN FIFTY ONEH FIVEH THOUSAND NEWLINE
%token NEXT
%%
calclist:
	|simp {printf("%d", $$);}
	;
simp:  	eval {$$ = $1;}
	|simp eval {$$ = $1 + $2;}
	;
eval:   ONE {$$ = 1;}
	|ONE FIVE {$$ = 4;}
	|FIVE {$$ = 5;}
	|FIVE ONE {$$ = 6;}
	|ONE TEN {$$ =  9;}
	|TEN {$$ =  10;}
	|TEN FIFTY {$$ = 40;}
	|FIFTY {$$ = 50;}
	|FIFTY TEN {$$ = 60;}
	|TEN ONEH {$$ = 90;}
	|ONEH {$$ = 100;}
	|ONEH FIVEH {$$ = 400;}
	|FIVEH {$$ = 500;}
	|FIVEH ONEH {$$ = 600;}
	|ONEH THOUSAND {$$ = 900;}
	|THOUSAND {$$ = 1000;}
	;
%%
void yyerror(char *s)
{

}


int
main()
{

//  yydebug = $1 + 1;
  yyparse();
  return 0;
}

