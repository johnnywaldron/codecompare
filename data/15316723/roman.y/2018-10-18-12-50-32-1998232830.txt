%{
  #  include <stdio.h>
  #  include <stdlib.h>
  int yylex();
  int results[100];
  int index = 0;
  void yyerror(char *s);
  
%}

%output "roman.tab.c"
%token EOL
%token INT
%token ZERO
%token MINUS
%token I
%token V
%token X
%token L
%token C
%token D
%token M
%token ERR
%%

romanparse:
| romanparse expr EOL {
			results[index] = $2;
		    }
input: /* parser entry */
  maybe_minus INT  { *numeral_parse_result = $1 * $2; }
| maybe_minus ZERO { *numeral_parse_result = 0; }
| maybe_minus expr { *numeral_parse_result = $1 * $2; }
;

maybe_minus:
  %empty { $$ = 1; }
| MINUS;

expr:
  max_c M max_m { $$ = $2 - $1 + $3; }
| max_c D max_c { $$ = $2 - $1 + $3; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_m:
  %empty { $$ = 0; }
| max_c M max_m { $$ = $2 - $1 + $3; }
| max_c D max_c { $$ = $2 - $1 + $3; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_c:
  %empty { $$ = 0; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_x:
  %empty { $$ = 0; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_i:
  %empty { $$ = 0; }
|       I max_i { $$ = $1 + $2; }
;


%%

void yyerror(char *s)
{
  printf("%s\n", s);
}

int main()
{
  
  yyparse();
  int i;
  for(i=0; i<index; i++)
	printf("%d",results[index]);
	return 0;
}
