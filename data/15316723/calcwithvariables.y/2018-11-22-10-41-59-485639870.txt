%{
# include <stdio.h>
# include <string.h>
# include <stdlib.h>


int variables['z'-'a'];
int symbols['z'-'a'];
int valid(char ch);
int get(char ch);
void put(char ch, int val);
void yyerror(char* s);
int yylex();
%}

%token ASSIGN 
%token ADD 
%token SUB 
%token MUL 
%token DIV
%token PRINT
%token VAR
%token NUM
%token EOL

%%

calclist:
 | calclist statement EOL
 ;

statement: VAR ASSIGN expr  { put($1, $3); }
 | PRINT expr               { printf("%d\n", $2); }
 ;

expr: expr ADD factor      { $$ = $1 + $3; }
 | expr SUB factor         { $$ = $1 - $3; }
 | factor  
 ;
 
factor: factor MUL num      { $$ = $1 * $3; }
 | factor DIV num           { $$ = $1 / $3; }
 | num
 ;

num: SUB num                { $$ = -$2; }
 | VAR                   { 
                                if (valid($1))
                                  $$ = get($1);
                                else
                                  yyerror("syntax error");
                            }
 | NUM
 ;



%%
int main()
{ 
  yyparse();
  return 0;
}
int valid(char ch)
{
  int index = ch - 'z'; 
  if( index <26 && index>0){
	return 1;
  }
  return 0;
}

void put(char ch, int val)
{
  int index = ch - 'z';
  symbols[index] = val;
  variables[index] = 1;
}

int get(char ch)
{
  int index = ch - 'z';
  return symbols[index];
}



void yyerror(char* s)
{
  printf("%s\n", s);
  exit(0);
}


