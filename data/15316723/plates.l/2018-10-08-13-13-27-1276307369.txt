%{
#include <stdio.h>
#include <stdlib.h>

%}

whitespace	[\n\t\r\v\f ]

plate_new	  	([01][0-9][12])

plate_old		([0189][0-9])

county_new (C)|(CE)|(CN)|(CW)|(D)|(DL)|(G)|(KE)|(KK)|(KY)|(L)|(LD)|(LH)|(LM)|(LS)|(MH)|(MN)|(MO)|(OY)|(RN)|(SO)|(T)|(W)|(WH)|(WH)|(WX)|(WW)

county_old (C)|(CE)|(CN)|(CW)|(D)|(DL)|(G)|(KE)|(KK)|(KY)|(L)|(LK)|(LD)|(LH)|(LM)|(LS)|(MH)|(MN)|(MO)|(OY)|(RN)|(SO)|(TS)|(TN)|(W)|(WH)|(WH)|(WX)|(WW)

%%

{plate_new}-{county_new}-[0-9]{1,6}{whitespace}+	{years(yytext);}

{plate_old}-{county_old}-[0-9]{1,6}{whitespace}+	{years(yytext);}

{whitespace} {}

.*{whitespace}		{printf("INVALID\n");}

"\n"+	{}

%%

int years(char ch[]){

    char year[2];

    /*2 digit years : 18 - YY*/
    if(yytext[0] > '2'){
  	   year[0] = yytext[0];
  	   year[1] = yytext[1];
	   int years = 18-atoi(year);
  	   printf("%d\n", years);
    }
    /*3 digit years: 118 - YY(disregard digit 3)*/
    else if(yytext[0] > '7'){
  	   year[0] = yytext[0];
  	   year[1] = yytext[1];
  	   int years = 118-atoi(year);
  	   printf("%d\n", years);
    }
}

int main(){
	yylex();
	return 0;
}
