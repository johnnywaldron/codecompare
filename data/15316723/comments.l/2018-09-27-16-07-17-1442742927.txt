%{
	#include <stdio.h>
	int ch;
%}

%%
"**".*	//regular expressin to catch anything after "**" and recognise it as a comment 
		{
			printf("single line comment");
		}



 //error if a "}" is found before a "{"
"}"	{  
		printf ("syntax error\n");
		return 0;
	}

 //"{" and "}" are used for multi-line comments. The program should go until it finds 
"{" { 
		int true =1;
		while(true)
		{
			//run through until enf of comment or EOF
			ch = input();
			while ((ch != '}' && ch != EOF)
			; 

			//EOF before comment ends 
			if (ch == EOF)	
			{
				printf ("syntax error\n");
				break;
			}

			//found "}"
			if (ch == '}') break;
		}

["][^"]*["]	//regular expression to catch anything in between ".." and print it 
		{
			printf("%s",yytext);
		}
}
%%

int main(){
	yylex();
	return 0;
}

