%{
int evens = 0;
%}

%%

[02468]+	{ evens++; }

%%

int main()
{
  yylex();
  printf("%d", evens);
  return 0;
}
