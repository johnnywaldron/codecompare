%{
int evens = 0;
%}

%%

[02468]+	{ evens++; }
[13579]+	{	}

%%

int main()
{
  yylex();
  printf("%d", evens);
  return 0;
}
