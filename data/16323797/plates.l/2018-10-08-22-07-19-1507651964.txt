/* Insert variables here */
%{
  int new_base = 18;

  int old_base = 2018;
%}

/* Define Regex here */
PRE_2013 (8{1}[7-9]{1})|(9{1}[0-9]{1})|(1{1}[0-2]{1})
POST_2013 (1[3-8]{1}[1-2]{1})
COUNTIES_PRE_2013 (-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|W|WH|WX|WW)-)
COUNTIES_POST_2013 (-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)-)
NUMBER ([0-9]{1,6})

OLD_REG ({PRE_2013}{COUNTIES_PRE_2013}{NUMBER})
NEW_REG ({POST_2013}{COUNTIES_POST_2013}{NUMBER})
%%

[ \t\n]*{OLD_REG}[ \t\n]* {
    char year[2];
    // copy the year to a string
    strncpy(year, yytext, 2);
    // convert year string to int
    int years = atoi(year);

    // Any reg with a year > 12 will be before the 2013 changes
    // and therefore is from 1900 on.
    // Any reg <= 12 is from the 2000s
    if(years > 12)
    {
      years = years + 1900;
    }
    else
    {
      years = years + 2000;
    }

    // Age calculated by subtracting years from this year
    int age = old_base-years;
    printf("%d\n", age);
  }

[ \t\n]*{NEW_REG}[ \t\n]* {
    char year[2];
    // copy part of reg with year disregarding the 3 digit 
    strncpy(year, yytext, 2);

    // convert year string to int 
    int years = atoi(year);
  
    int age = new_base-years;
    printf("%d\n", age);
  }

[ \t\n]*.+[ \t\n]* {
    // if anything other than a valid reg is input
    printf("INVALID\n");
  }

%%

int main()
{
  yylex();
  return 0;
}

