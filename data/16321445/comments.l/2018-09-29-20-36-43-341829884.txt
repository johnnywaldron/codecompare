%{
#include <stdio.h>
#include <stdlib.h>


/*var result:String;*/
int brackets = 0;
%}

%x SHORT_COMMENT
%x IN_COMMENT
%x QUOTE
%x ERROR
%%

<INITIAL>
{
\"				{printf("\""); BEGIN(QUOTE);}
"{"             {brackets++; BEGIN(IN_COMMENT);}
"}"				{printf("syntax error\n"); BEGIN(ERROR);}
"**"			{BEGIN(SHORT_COMMENT);}
.				{printf("%s", yytext);}
}
<SHORT_COMMENT>
{
\n				{printf("\n");BEGIN(INITIAL);}
.				{}
}
<IN_COMMENT>{

"}"    			{brackets--; BEGIN(INITIAL);}

\n				{}
.				{}
				

}
<QUOTE>
{
\"				{printf("\"");BEGIN(INITIAL);}
.				{printf("%s", yytext);}
}

<ERROR>
{
\n				{}
.				{}
}

%%

int main()
{
	yylex();
	/*printf("%d\n",result);*/
	/*printf("%s", result);*/
	if(brackets != 0)
		{printf("syntax error\n");}
	return 0;
}
