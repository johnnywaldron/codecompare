%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

var result:String = NULL;
int brackets = 0;
%}

%x SHORT_COMMENT
%x IN_COMMENT
%x QUOTE
%x ERROR
%%

<INITIAL>
{
\"				{BEGIN(QUOTE);}
"{"             {brackets++; BEGIN(IN_COMMENT);}
"}"				{result+= "syntax error\n"; BEGIN(ERROR);}
"**"			{BEGIN(SHORT_COMMENT);}
.				{result += yytext;}
}
<SHORT_COMMENT>
{
\n				{BEGIN(INITIAL);}
.				{}
}
<IN_COMMENT>{

"}"    			{BEGIN(INITIAL);} 																/*{brackets--; if(brackets >= 0) BEGIN(INITIAL);}*/
[^*\n]+   		// eat comment in chunks
"*"       		// eat the lone star
\n        		yylineno++;
\0				{result +="syntax error\n";BEGIN(ERROR);}

}
<QUOTE>
{
\"				{BEGIN(INITIAL);}
.				{result += yytext;}
}

<ERROR>
{
.				{}
}

%%

int main()
{
	yylex();
	/*printf("%d\n",result);*/
	printf("%s", result);
	return 0;
}
