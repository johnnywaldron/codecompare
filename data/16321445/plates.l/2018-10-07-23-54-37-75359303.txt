%{
#include <stdio.h>
#include <stdlib.h>
int year = 0;
int i = 0;
%}

%x CHECK1
%x CHECK2
%x CHECKCOUNTY
%x CHECKNUMBER
%x ERROR
%%

<INITIAL>
{
[0-9]+						{year = atoi(yytext);BEGIN(CHECK1);}
\n						{/*printf("\n");*/}
.
}

<CHECK1>
{
"-"					{i = 0; int j = year; if(j>=1000){printf("%s\n","INVALID");BEGIN(ERROR);} else{if(j > 130) j = j/10; if(j<100 && j <= 18) j = j+100; i = 118-j; /*printf("%d\n",i);*/BEGIN(CHECKCOUNTY);}}
\n					{printf("%s\n","INVALID1");BEGIN(ERROR);}
.					{printf("%s%s%s\n","INVALID2 2",yytext,"2");BEGIN(ERROR);}

}

<CHECKCOUNTY>
{

["C"]|["CE"]|["CN"]|["CW"]|["D"]|["DL"]|["G"]|["KE"]|["KK"]|["KY"]|["L"]|["LD"]|["LH"]|["LM"]|["LS"]|["MH"]|["MN"]|["MO"]|["OY"]|["R"/"N"]|["SO"]|["W"]|["WH"]|["WX"]|["WW"]	{/*printf("%d%s",i,"Valid ");*/BEGIN(CHECK2);}
["LK"]|["T"/"N"]|["T"/"S"]|["WD"]			{if(i > 5 ) BEGIN(CHECK2); else BEGIN(ERROR);}
["T"]					{if(i <= 5 ) BEGIN(CHECK2); else BEGIN(ERROR);}
\n						{printf("%s\n","INVALID3");BEGIN(ERROR);}
.						{printf("%s\n","INVALID4");BEGIN(ERROR);}
}

<CHECK2>
{
"-"					{BEGIN(CHECKNUMBER);}
\n					{printf("%s\n","INVALID8");BEGIN(ERROR);}
.					{/*printf("%s%s%s\n","INVALID9 8",yytext,"2");BEGIN(ERROR);*/}

}

<CHECKNUMBER>
{
[0-9]+						{int j = atoi(yytext); if(j <= 999999 && j > 0) {/*i = 0; int j = year; if(j>=1000)BEGIN(ERROR); if(j > 130) j = j/10; if(j<100 && j <= 18) j = j+100; i = 118-j;*/ printf("%d\n",i);BEGIN(INITIAL);} else BEGIN(ERROR);}
\n 						{printf("%s\n","INVALID6");BEGIN(ERROR);}
. 						{printf("%s\n","INVALID7");BEGIN(ERROR);}
}

<ERROR>
{
\n					{BEGIN(INITIAL);}
.
		
}
	


%%

int main()
{
	yylex();

	return 0;
}
