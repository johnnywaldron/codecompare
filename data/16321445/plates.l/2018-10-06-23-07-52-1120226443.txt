%{
#include <stdio.h>
#include <stdlib.h>
int year = 0;
int i = 0;
%}

NUMBER  [0-9]{1,[6]}
COUNTY1  "C"|"CE"|"CN"|"CW"|"D"|"DL"|"G"|"KE"|"KK"|"KY"|"L"|"LD"|"LH"|"LM"|"LS"|"MH"|"MN"|"MO"|"OY"|"RN"|"SO"|"T"|"W"|"WH"|"WX"|"WW"
COUNTY2	 "LK"|"TN"|"TS"|"WD"																		/* make T invaild*/
/*YEAR  [0-9]+*/

%x CHECK
%x CHECKCOUNTY
%x CHECKNUMBER
%x ERROR
%%

<INITIAL>
{
[0-9]+						{year = atoi(yytext);BEGIN(CHECK);}
\n						{printf("\n");}
.
}

<CHECK>
{
"-"					{i = 0; int j = year; if(j>=1000)BEGIN(ERROR); if(j > 130) j = j/10; /*if(j<100) j = j+100;*/ i = 118-j; printf("%d%s",i," ");BEGIN(CHECKCOUNTY);}
\n					{BEGIN(ERROR);}
.					{BEGIN(ERROR);}

}

<CHECKCOUNTY>
{
["C"|"CE"|"CN"|"CW"|"D"|"DL"|"G"|"KE"|"KK"|"KY"|"L"|"LD"|"LH"|"LM"|"LS"|"MH"|"MN"|"MO"|"OY"|"RN"|"SO"|"W"|"WH"|"WX"|"WW"]["-"]	{BEGIN(CHECKNUMBER);}
["LK"|"TN"|"TS"|"WD"]["-"]			{if(i > 5 ) BEGIN(CHECKNUMBER); else BEGIN(ERROR);}
["T"]["-"]					{if(i <= 5 ) BEGIN(CHECKNUMBER); else BEGIN(ERROR);}
\n						{BEGIN(ERROR);}
.						{BEGIN(ERROR);}
}

<CHECKNUMBER>
{
[0-9]+						{int j = atoi(yytext); if(j <= 999999 && j > 0) {printf("%d\n",i);BEGIN(INITIAL);} else BEGIN(ERROR);}
\n {BEGIN(ERROR);}
. {BEGIN(ERROR);}
}

<ERROR>
{
\n					{BEGIN(INITIAL);}
.
		
}
	


%%

int main()
{
	yylex();

	return 0;
}
