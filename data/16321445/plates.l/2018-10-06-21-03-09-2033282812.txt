%{
#include <stdio.h>
#include <stdlib.h>
int year = 0;
%}

NUMBER  [0-9]{1,[6]}
COUNTY1  "C"|"CE"|"CN"|"CW"|"D"|"DL"|"G"|"KE"|"KK"|"KY"|"L"|"LD"|"LH"|"LM"|"LS"|"MH"|"MN"|"MO"|"OY"|"RN"|"SO"|"T"|"W"|"WH"|"WX"|"WW"
COUNTY2	 "LK"|"TN"|"TS"|"WD"																		/* make T invaild*/
YEAR  [0-9][0-9][0-9]?

%x YEAR
%x ERROR
%%

<INITIAL>
{
[YEAR]						{int year = atoi(yytext);printf("%d\n",year);BEGIN(YEAR);}
\n
.
}

<YEAR>
{
["-"][COUNTY1]["-"][NUMBER]		{int i = 0; int j = year; if(j > 130) j = j/10; if(j<100) j = j+100; i = 118-j; printf("%d\n",i);}
["-"][COUNTY2]["-"][NUMBER]		{}
\n					{BEGIN(INITIAL);}
.

}

<ERROR>
{
.
}
	


%%

int main()
{
	yylex();

	return 0;
}
