%{
#include "romcalc.tab.h"
void yyerror(char *s);
%}

%%

I                           {yylval = 1; return I;}
V                           {yylval = 5; return V;}
X                           {yylval = 10; return X;}
L                           {yylval = 50; return L;}
C                           {yylval = 100; return C;}
D                           {yylval = 500; return D;}
M                           {yylval = 1000; return M;}

IV                          {yylval = 4; return IV;}
IX                          {yylval = 9; return IX;}
XL                          {yylval = 40; return XL;}
XC                          {yylval = 90; return XC;}
CD                          {yylval = 400; return CD;}
CM                          {yylval = 900; return CM;}

"+"                        	{yylval = 1; return ADD;}
"-"	                        {yylval = 1; return SUB;}
"*"	                        {yylval = 1; return MUL;}
"/"	                        {yylval = 1; return DIV;}
"{"                         {return OP;}
"}"                         {return CP;}

\n                          {return EOL;}
.                           {yyerror("syntax error");exit(0);}
%%
