%{
#  include <stdio.h>
#  include <stdlib.h>

int yylex();
void yyerror(char *s);
%}

/* declare tokens */
%token EOL
%token I IV V IX X XL L XC C CD D CM M

%%

calclist: /* nothing */
| calclist EOL                 { printf("%d\n",$2);}
;

numeral: roman {$$ = $1;}                 {if(2*$1>$2*10)
                                            {$$ = $1+$2;}
                                           else{yyerror("syntax error");}
                                         }

roman: I | V | X | L | C | D | M {$$ = $1;}
;
%%

int main()
{
  printf("");
  yyparse();
  return 0;

}


void yyerror(char *s){
  fprintf(stderr, "%s\n", s);
  exit(0);
}
