%{
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>
int yylex();
void yyerror(char *s);
int array[26] = {};
%}

%token EOL
%token ADD MUL SUB DIV ASSIGN VARIABLE NUM PRINT SEMICOLON
%%

input:
 | input VARIABLE ASSIGN exp SEMICOLON {array[($2)-49] = $4;}
 | input PRINT VARIABLE SEMICOLON {printf("%d\n",array[($3)-49]);}

exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: NUM
 | SUB NUM {$$ = 0 - $2;}
 | VARIABLE {$$ = array[($1)-49];}
;


%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(0);
}
