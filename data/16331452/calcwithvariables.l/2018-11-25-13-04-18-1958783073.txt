%{
#include "calcwithvariables.tab.h"
void yyerror(char *s);
%}

%%
"+"                        	 {return ADD;}
"-"	                         {return SUB;}
"*"	                         {return MUL;}
"/"	                         {return DIV;}
"print"                      {return PRINT;}
":="                         {return ASSIGN;}
";"                          {return SEMICOLON;}
[a-z]{1}                     {yylval = yytext[0] - '0'; return VARIABLE;}
[0-9]+                       {yylval = atoi(yytext); return NUM;}

\n                           {}
[ \t]                        {}
.                            {yyerror("syntax error");}
%%
