%{
#include "calcwithvariables.tab.h"
void yyerror(char *s);
%}

%%
"+"                        	{return ADD;}
"-"	                        {return SUB;}
"*"	                        {return MUL;}
"/"	                        {return DIV;}
"print"                     {return PRINT;}
":="                        {return ASSIGN;}
";"                         {return SEMICOLON;}

\n                          {return EOL;}
.                           {yyerror("syntax error");exit(0);}
%%
