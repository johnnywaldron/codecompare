%{
#  include <math.h>
#  include <string.h>
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
void yyerror(char *s);
char* concatenateStrings(const char *string1, const char *string2);
void convertToRomanNumeral(char* rom, int num);
%}

/* declare tokens */
%token ADD SUB MUL DIV
%token OB CB
%token EOL
%token I IV IX V X XL L XC C CD D CM M
%%

calclist: /* nothing */
 | calclist exp EOL { char* ans; convertToRomanNumeral(ans, $2);}
 ;

exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: number
 | OB exp CB   {$$=$2;}
 ;

number: sub-roman {$$ = $1;}
 | roman {$$ = $1;}
 | roman number {if($1==$2 && ($1 == 5 || $1 == 50 || $1 == 500)) {yyerror("syntax error");}else {$$ = $1 + $2;}}
 | sub-roman number {if(2*$1>$2*10){$$ = $1 + $2;} else {yyerror("syntax error");}}
;

roman: I | V | X | L | C | D | M {$$ = $1;}
;

sub-roman: IV | IX | XL | XC | CD | CM {$$ = $1;}
;
%%

int main()
{
  printf("");
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(0);
}

char* concatenateStrings(const char *string1, const char *string2)
{
  char *answer = malloc(strlen(string1) + strlen(string2) + 1);
  strcpy(answer, string1);
  strcpy(answer, string2);
  return answer;
}

void convertToRomanNumeral(char* rom, int num)
{
  char* sign;
  char* returnNumeral = "";
  int index = 0;
  char* numerals [] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
  int decimalVals [] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};

  if(num<0)
  {
    num = num*-1;
    sign = "-";
  }
  else
  {
    sign = "";
  }

  if(num == 0)
  {
    printf("Z");
  }

  while(index < (sizeof(decimalVals)/4))
  {
    while(num>=decimalVals[index])
    {
      num = num - decimalVals[index];
      if(sizeof(rom>0))
      {
        returnNumeral = concatenateStrings(returnNumeral, numerals[index]);
      }
      else
      {
        returnNumeral = concatenateStrings(returnNumeral, numerals[index]);
      }
    }
    index = index + 1;
  }
  printf("%s%s\n", sign, returnNumeral);
}
