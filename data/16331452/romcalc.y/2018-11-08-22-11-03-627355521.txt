%{
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>
int yylex();
void yyerror(char *s);
void convertToRomanNumeral(int num, char* rom);
char* concatenateStrings(const char *string1, const char *string2);
%}

%token EOL
%token I IV IX V X XL L XC C CD D CM M ADD MUL SUB DIV OP CP
%%


calclist: /* nothing */
 | calclist exp EOL { char* answer ;convertToRomanNumeral($2,answer);}
 ;

exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: number
| OP exp CP   {$$=$2;}
;

number:subRoman {$$ = $1;}
| roman {$$ = $1;}
| roman  number{if($1==$2 && ($1==5 || $1==50 || $1==500)){yyerror("syntax error");}else {$$ = $1+$2;}}
| subRoman number  {if(2*$1>$2*10){$$ = $1+$2;}else{yyerror("syntax error");}}
;

subRoman: IV | IX | XL | XC | CD | CM {$$ = $1;}
;
roman:  I  | V  | X | L | C | D| M {$$ = $1;}
;
%%

int main()
{
  printf("");
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(0);
}

void convertToRomanNumeral(int num,char* rom){

    char* sign;
    if(num<0){
        num = num*-1;
        sign = "-";
    }
    else
    {
        sign = "";
    }

    char* numerals [] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","I"};
    int decimalVals [] = {1000,900,500,400,100,90,50,40,10,9,5,1};

    int index = 0;
    char* returnNumeral = "";
    if(num==0)
    {
      printf("Z");
    }
    while(index<(sizeof(decimalVals)/4))
    {
        while(num>=decimalVals[index])
        {
            num = num -  decimalVals[index];
            if(sizeof(returnNumeral>0))
            {
                returnNumeral =concatenateStrings(returnNumeral,numerals[index]);
            }
            else
            {
                returnNumeral = concatenateStrings(returnNumeral,numerals[index]);
            }

        }
        index = index+1;
    }

    printf("%s%s\n",sign,returnNumeral);
}

char* concatenateStrings(const char *string1, const char *string2)
{
    char *ans = malloc(strlen(string1) + strlen(string2) + 1);
    strcpy(ans, string1);
    strcat(ans, string2);
    return ans;
}
