%{
int current_year = 2018;
int year = 0;
int prefix = 0;
int year_Made = 0;
int year_Since = 0;
%}

OLD_YEAR (8[7-9]{1})|(9[0-9]{1})|(0[0-9]{1})|(1[0-2]{1})
YEAR_13  13[1-2]
NEW_YEAR 1[4-8][1-2]
OLD_COUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NEW_COUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
SERIAL_NUMBER [0-9]{1,6}

%%
{OLD_YEAR}{1}\-{OLD_COUNTY}{1}\-{SERIAL_NUMBER}{1}(" "|\t|\n)+   {char years[] = {' ', ' '};
                                                                  strncpy(years, yytext, 2);
                                                                  year = atoi(years);
                                                                  if(year < 100 && year > 87){prefix = 19;}
                                                                  else if(year>=00 && year <= 18){prefix = 20;}
                                                                  year_Made = (prefix * 100) + year;
                                                                  year_Since = current_year - year_Made;
                                                                  printf("%d\n", year_Since);
                                                                  }

{YEAR_13}{1}\-{OLD_COUNTY}{1}\-{SERIAL_NUMBER}{1}(" "|\t|\n)+     {char years[] = {' ', ' '};
                                                                   strncpy(years, yytext, 2);
                                                                   year = atoi(years);
                                                                   if(year < 100 && year > 87){prefix = 19;}
                                                                   else if(year>=00 && year <= 18){prefix = 20;}
                                                                   year_Made = (prefix * 100) + year;
                                                                   year_Since = current_year - year_Made;
                                                                   printf("%d\n", year_Since);
                                                                   }

{NEW_YEAR}{1}\-{NEW_COUNTY}{1}\-{SERIAL_NUMBER}{1}(" "|\t|\n)+    {char years[] = {' ', ' '};
                                                                   strncpy(years, yytext, 2);
                                                                   year = atoi(years);
                                                                   if(year < 100 && year > 87){prefix = 19;}
                                                                   else if(year>=00 && year <= 18){prefix = 20;}
                                                                   year_Made = (prefix * 100) + year;
                                                                   year_Since = current_year - year_Made;
                                                                   printf("%d\n", year_Since);
                                                                   }

{OLD_YEAR}{1}\-{NEW_COUNTY}{1}\-{SERIAL_NUMBER}{1}(" "|\t|\n)+        {printf("INVALID\n");}
{YEAR_13}{1}\-{NEW_COUNTY}{1}\-{SERIAL_NUMBER}{1}(" "|\t|\n)+         {printf("INVALID\n");}
{NEW_YEAR}{1}\-{OLD_COUNTY}{1}\-{SERIAL_NUMBER}{1}(" "|\t|\n)+        {printf("INVALID\n");}

%%

int main()
{
yylex();
return 0;
}

