%{

%}

OLD_YEAR (8[7-9]{1}|9[0-9]{1}|0[0-9]{1}|1[0-2]{1}
NEW_YEAR 1[3-8][1-2]
OLD_COUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NEW_COUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
SERIAL_NUMBER [0-9]{1-6}

%%
{OLD_YEAR}{OLD_COUNTY}{SERIAL_NUMBER}              {printf("VALID\n");}

%%

int main()
{
yylex();
return 0;
}

