%{
int comment = 0;
%}

%%
\n			{ printf("\n");}
[/A-z+0-9+/]+		{ printf(yytext);}
"**".*			{}
[{]([^*]|[\n])*[}]	{comment = 1;}
%%	

int main(){
yylex();
return 0;
}
