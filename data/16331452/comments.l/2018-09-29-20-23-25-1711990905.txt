%{

char output[4000];
int current = 0;
int comment = 0;

%}

%%


"**".*											{comment = 1;}
[{+][}*]										{comment = 1;}
[{]+(([0-9\.\nA-z.\40\t\:\;\=\,\(\){}]+)[}]+)	{comment = 1;}
["]([^*]|[\n])*["]								{comment = 0;printf(yytext);}


%%

int main()
{
yylex();
return 0;
}
