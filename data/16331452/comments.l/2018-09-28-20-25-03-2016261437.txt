%{
int comment = 0;
%}

%%
\n			{ printf("\n"); }
[A-z+0-9+]+		{ printf(yytext); }
[^**].*			{}
[{](.|\n)+*[}]		{comment = 1;}
[{]			{if(comment == 0){printf("syntax error\n");}}
[}]			{if(comment == 0){printf("syntax error\n");}}
%%	

int main(){
yylex();
printf(yytext);
return 0;
}
