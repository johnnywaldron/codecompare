%{
int comment = 0;
%}

%%
\n			{ printf("\n"); }
[/A-z+0-9+/]+		{ printf(yytext);}
"**".*			{}
[{]("")*[}]		{ comment = 1; }
[{](.|\n)+*[}]+		{ comment = 1; }
%%	

int main(){
yylex();
printf(yytext);
return 0;
}
