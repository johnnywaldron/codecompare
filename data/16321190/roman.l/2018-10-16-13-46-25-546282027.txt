%{
# include "roman.tab.h"
void yyerror(char *s);
int yylex();
int yyparse();
%}

%%

[I] {return UNIT;}
[V] {return FIVE;}
[X] {return TEN;}
[L] {return FIFTY;}
[C] {return HUNDRED;}
[D] {return FIVEHUNDRED;}
[M] {return THOUSAND;}
\n  { return EOL; }
.	{ yyerror("ddddsorMddd\n"); }

%%
