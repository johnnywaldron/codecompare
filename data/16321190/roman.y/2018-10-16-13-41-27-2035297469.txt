%{
# include <stdio.h>
# include <stdlib.h>
int yyparse();
int yyerror();
int yylex();
%}
%token THOUSAND HUNDRED TEN UNIT FIVE FIFTY FIVEHUNDRED
%token EOL
%%

number:
| number thousands hundreds tens units EOL {printf("%d\n", ($2 + $3 + $4 + $5));}
;



hundreds: consechundreds
| prehundreds
| FIVEHUNDRED consechundreds {$$ = 500 + $2;}
;

prehundreds: HUNDRED FIVEHUNDRED {$$ = 400;}
| HUNDRED THOUSAND {$$ = 900;}
;

consechundreds: {$$ = 0;}
| HUNDRED {$$ = 100;}
| HUNDRED HUNDRED {$$ = 200;}
| HUNDRED HUNDRED HUNDRED {$$ = 300;}
;

thousands: {$$ = 0;}
| THOUSAND thousands {$$ = $2 + 1000; }
;

tens: consectens
| pretens
| FIFTY consectens {$$ = 50 + $2;}
;

consectens: {$$ = 0;}
| TEN {$$ = 10;}
| TEN TEN {$$ = 20;}
| TEN TEN TEN {$$ = 30;}
;

pretens: TEN FIFTY {$$ = 40;}
| TEN HUNDRED {$$ = 90;}
;

units: consecunits
| preunits
| FIVE consecunits {$$ = 5 + $2;}
;

consecunits: {$$ = 0;}
| UNIT {$$ = 1;}
| UNIT UNIT {$$ = 2;}
| UNIT UNIT UNIT {$$ = 3;}
;

preunits: UNIT FIVE {$$ = 4;}
| UNIT TEN {$$ = 9;}
;

%%
int main(int argc, char **argv) {
  yyparse();
  return 0;
}

yyerror(char *s)
{
  printf("%s", s);
}
