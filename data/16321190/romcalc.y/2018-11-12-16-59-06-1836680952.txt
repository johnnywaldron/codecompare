%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yyparse();
void yyerror();
int yylex();
char* convertNumerals();
int number = 0;
int total = 0;
char* result;
%}

%token UNIT FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND SYMBOL NUM EOL

%%
calclist: 
| calclist expr EOL		{
							total = $2;
							
							if(total < 0){
								total = total*-1;
  								result = convertNumerals(total);
								char* tempMem;
								tempMem = malloc(100);
								strcpy(tempMem, "-");
								strcat(tempMem, result);
								printf("%s\n", tempMem);
								free(tempMem);
							}
							else if(total == 0){
  								printf("Z\n");
							}
  						else{
							result = convertNumerals(total); 								
							printf("%s\n",result);}
						}
						
| calclist EOL			{ yyerror("syntax error"); }
;

expr: factor
| expr '+' factor {$$ = $1 + $3;}
| expr '-' factor {$$ = $1 - $3;}
;

factor: term
| factor '*' term {$$ = $1 * $3;}
| factor '/' term {$$ = $1 / $3;}
;

term: number
| '{' expr '}' {$$ = $2;}
| '-' term     {$$ = -1 * $2;}
;


number:
| number thousands hundreds tens units EOL {
          int temp = $2 + $3 + $4 + $5;
          if(temp != 0) {
            printf("%d\n", temp);
          }
        }
;



hundreds: consechundreds
| prehundreds
| FIVEHUNDRED consechundreds {$$ = 500 + $2;}
;

prehundreds: HUNDRED FIVEHUNDRED {$$ = 400;}
| HUNDRED THOUSAND {$$ = 900;}
;

consechundreds: {$$ = 0;}
| HUNDRED {$$ = 100;}
| HUNDRED HUNDRED {$$ = 200;}
| HUNDRED HUNDRED HUNDRED {$$ = 300;}
;

thousands: {$$ = 0;}
| THOUSAND thousands {$$ = $2 + 1000; }
;

tens: consectens
| pretens
| FIFTY consectens {$$ = 50 + $2;}
;

consectens: {$$ = 0;}
| TEN {$$ = 10;}
| TEN TEN {$$ = 20;}
| TEN TEN TEN {$$ = 30;}
;

pretens: TEN FIFTY {$$ = 40;}
| TEN HUNDRED {$$ = 90;}
;

units: consecunits
| preunits
| FIVE consecunits {$$ = 5 + $2;}
;

consecunits: {$$ = 0;}
| UNIT {$$ = 1;}
| UNIT UNIT {$$ = 2;}
| UNIT UNIT UNIT {$$ = 3;}
;

preunits: UNIT FIVE {$$ = 4;}
| UNIT TEN {$$ = 9;}

%%


/* This method was taken from:
 https://stackoverflow.com/questions/23269143/c-program-that-converts-numbers-to-roman-numerals
*/
char* convertNumerals(int num){
	    int del[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1}; // Key value in Roman counting
	    char * sym[] = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" }; //Symbols for key values
	    char* res;
	    res = malloc(64);
	    int i = 0;                   //
	    while(num){                 //while input number is not zero
	        while(num/del[i]){      //while a number contains the largest key value possible
	            strcat(res, sym[i]); //append the symbol for this key value to res string
	            num -= del[i];       //subtract the key value from number
	        }
	        i++;                     //proceed to the next key value
	    }
	    return res;
}


void yyerror(char *s) {
	printf("%s\n", s);
	exit(EXIT_SUCCESS);
}

int main(){
	yyparse();
	return 0;
}

