
rex1   "\"".*\n"\""
rex2   [**](.*)
rex3     /\{([^{}]+)\}\g
rexerr1  [0-9A-z \t\n]"}"
rexerr2   [0-9A-z \t\n]"}"
ddd 	[a-z]
%%


{rex1}	{printf("LOP");}
{rex2}   {printf("");}
{rex3}   {printf("");}
{rexerr1}     {printf("syntax error");
                          exit(EXIT_SUCCESS);}
{rexerr2}        {printf("syntax error");
                        exit(EXIT_SUCCESS);}


%%

int main()
{
  yylex();
	return 0;
}
