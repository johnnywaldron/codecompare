
rex1   "\"".*\n"\""
rex2   [**](.*)
rex3     "{"[0-9A-z \t\n]"}"[^\"]
rexerr1  "{"[0-9A-z \t\n]`EOF
rexerr2   [0-9A-z \t\n]"}"
ddd 	[a-z]
%%


{rex1}	{printf("LOP");}
{rex2}   {printf("\b");}
{rex3}   {printf("ITU");}
{rexerr1}     {printf("syntax error");
                          exit(EXIT_SUCCESS);}
{rexerr2}        {printf("syntax error");
                        exit(EXIT_SUCCESS);}


%%

int main()
{
  yylex();
	return 0;
}
