
rex2   [**]{2}(.*)
rex3    \{([^}]+)\}
rex4	"{}"
rex5	\".*\"	
rexerr1  [0-9A-z \t\n]"}"
rexerr2   [0-9A-z \t\n]"}"
%%



{rex2}   {printf("");}
{rex3}   {printf("");}
{rex4}	{printf("");}
{rex5}	{printf(yytext);}
{rexerr1}     {printf("syntax error");
                          exit(EXIT_SUCCESS);}
{rexerr2}        {printf("syntax error");
                        exit(EXIT_SUCCESS);}


%%

int main()
{
  yylex();
	return 0;
}
