%{
int integers = 0;
int evenInts = 0;	

%}

%%

[0-9]+ {integers =atoi(yytext); if(integers % 2 == 0){evenInts++;} }
.|\n {}

%%

int main()
{
	yylex();
	printf("%d\n", evenInts);
	return 0;

}
