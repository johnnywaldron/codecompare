%{
# include "romcalc.tab.h"
void yyerror(char *s);
%}

%%
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"|" { return ABS; }

"I" {return I; }
"V" {return V; }
"X" {return X; }
"L" {return L; }
"C" {return C; }
"D" {return D; }
"M" {return M; }
"{" {return OPEN; }
"}" {return CLOSE; }

\n      { return EOL; }
[ \t]   { /* ignore white space */ }
.	{ yyerror("syntax error"); return -1;}
%%

