%{
%}

%%

1(31|32|41|42|51|52|61|62|71|72|81|82)               // take in 3 digit year
[87-99]                                              //search from year 87 to 99
[0-12]                                              //search from year 0 to 12
%%

int main()
{
  printf("INVALID\n");
  yylex();
  return 0;
}
