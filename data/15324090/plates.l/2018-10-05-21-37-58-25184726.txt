%{
  int year = 0;
  char num [2];
%}

PREVYEARS (([8][7-9])|([9|0]|[0-9])|([1][0-2]))[-]
RECENTYEARS ([1][3-8][1|2])[-]
COUNTIESPREV (C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW)[-]
COUNTIESPRIOR(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)[-]
NUMBERS [0-9]{1,6}[ \tab\n]

%%
{PREVYEARS}{COUNTIESPREV}{NUMBERS}      {num[0] = yytext[0]; num[1] = yytext[1]; if (num[0]=='1'){ 
                                             year = 2000 + atoi(num);} else {year = 1900 + atoi(num);}
                                             printf("%d\n",2018-year);} 

{RECENTYEARS}{COUNTIESPRIOR}{NUMBERS}   {num[0] = yytext[0]; num[1] = yytext[1]; year = 2000 + atoi(num);
                                            printf ("%d\n", 2018-year);;} 
[ \tab\n]+
[.]*                                    {printf("INVALID\n");}

{PREVYEARS}[(A-Z*)|(a-s)*][-]{NUMBERS}      {printf("INVALID\n");}
{RECENTYEARS}[(A-Z*)|(a-z)*][-]{NUMBERS}    {printf("INVALID\n");}

{PREVYEARS}{COUNTIESPREV}[0-9]{7}+          {printf("INVALID\n");}
{RECENTYEARS}{COUNTIESPRIOR}[0-9]{7}+       {printf("INVALID\n");}

^[0-9]{4}[-]                                {printf("INVALID\n");}
(A-Z)*|(a-z)*                               {printf("INVALID\n");}
%%

int main()
{
  yylex();
  return 0;
}
