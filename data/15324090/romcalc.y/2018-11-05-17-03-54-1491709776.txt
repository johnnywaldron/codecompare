%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();

void print_roman(int a)
{
    int i = 0;
    char* sign = "";
    if(a==0){
        printf("Z");}
    else if(a<0){
        sign="-";
        a*=-1;}
    char M[1000];
    strcpy (M,"");
    for(i = 0; i<a/1000; i++)
        strcat(M,"M");
    char* C[] = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
    char* X[] = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
    char* I[] = {"","I","II","III","IV","V","VI","VII","VIII","IX"};
    printf("%s%s%s%s%s\n",sign,M,C[(a%1000)/100],X[(a%100)/10],I[(a%10)]);
}
%}



/* declare tokens */
%output "romcalc.tab.c";
%token EOL
%token ONE
%token FIVE
%token TEN
%token FIFTY
%token HUNDRED
%token FIVEHUNDRED
%token THOUSAND
%token ERROR
%left MINUS ADD
%left DIVIDE MULTIPLY
%left LEFTBRACKET RIGHTBRACKET 
%%

fullnum:                        
    | fullnum expression EOL             {print_roman($2);}
    | ERROR                             {printf("syntax error\n"); return 0;}
    ;

expression: thousands
    | LEFTBRACKET expression RIGHTBRACKET    {$$= $2;}
    | expression DIVIDE expression           {$$ = $1/$3;}
    | expression MULTIPLY expression         {$$ = $1*$3;}
    | expression MINUS expression            {$$ = $1-$3;}
    | expression ADD expression              {$$ = $1+$3;}
    ;


thousands: fivehundreds                            
    |THOUSAND fivehundreds                                                  {$$=1000+$2;}
    |THOUSAND THOUSAND fivehundreds                                         {$$=2000+$3;}               
    |THOUSAND THOUSAND THOUSAND fivehundreds                                {$$=3000+$4;}
    |THOUSAND THOUSAND THOUSAND THOUSAND fivehundreds                       {$$=4000+$5;}   
    |THOUSAND THOUSAND THOUSAND THOUSAND THOUSAND fivehundreds              {$$=5000+$6;} 
    |THOUSAND THOUSAND THOUSAND THOUSAND THOUSAND THOUSAND fivehundreds    {$$=6000+$7;}



fivehundreds: hundreds                              
    |HUNDRED FIVEHUNDRED fifties                    {$$=400+$3;}
    |FIVEHUNDRED hundreds                           {$$=500+$2;}                
    |HUNDRED THOUSAND fifties                       {$$=900+$3;}
;


 hundreds: fifties                                 
    |HUNDRED fifties                                {$$=100+$2;}
    |HUNDRED HUNDRED fifties                        {$$=200+$3;}
    |HUNDRED HUNDRED HUNDRED fifties                {$$=300+$4;}
;

fifties: tens                   
    |TEN FIFTY fives                                {$$=40+$3;}
    |FIFTY tens                                     {$$=50+$2;}                                    
    |TEN HUNDRED fives                              {$$=90+$3;}
;

tens: fives                     
    | TEN fives                                     {$$=10+$2;}
    | TEN TEN fives                                 {$$=20+$3;}
    | TEN TEN TEN fives                             {$$=30+$4;}
    ;

fives: ones                    
    | ONE FIVE                                      {$$=4;}
    | FIVE ones                                     {$$=5+$2;}
    | ONE TEN                                       {$$=9;}
;

ones:                                               {$$=0;} 
    | ONE                                           {$$=1;}
    | ONE ONE                                       {$$=2;}
    | ONE ONE ONE                                   {$$=3;}
;

%%

int main()
{
    yyparse();
    return 0;
}

void yyerror(char *s)
{
    printf("syntax error\n");
}
