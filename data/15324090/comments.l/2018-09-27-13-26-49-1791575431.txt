%{
%}

%%

[\*]{2}[^\n]*
[{][^}]*[}]
[{][^}]*      {printf("syntax error\n"); return 0;}
%%

int main()
{
  yylex();
  return 0;
}
