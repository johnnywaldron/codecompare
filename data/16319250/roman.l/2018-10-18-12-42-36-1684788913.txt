%{
# include "roman.tab.h"

void yyerror(char *s);
int yylex();
int total = 0;

%}

%%

"I"  {  total += 1; return ONE; }
"IV" {  total += 4; return FOUR; }
"V"  { total += 5; return FIVE; }
"IX" { total += 9; return NINE; }
"X"  { total += 10; return TEN; }
"XL" { total += 40; return FORTY; }
"L"  { total += 50; return FIFTY; }
"XC" { total += 90; return NINETY; }
"C"  { total += 100; return HUNDR; }
"CD" { total += 400; return FOURHUNDR; }
"D"  { total += 500; return FIVEHUNDR; }
"CM" { total += 900; return NINEHUNDR; }
"M"  { total += 1000; return THOUSAND; }
"MM" { total += 2000; return TWOTHOUSAND;}
\n   { return EOL; }
.	 { yyerror("syntax error"); }
%%
