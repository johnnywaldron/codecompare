%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
int print =1;
int val;
int result;
int total =0;
int var = 0;
int array [26];
%}
%output "calcwithvariables.tab.c"

%token EOL ADD SUB MUL DIV VAR DIGIT EOE
%%

calc: /* nothing */ {}
| calc expr EOE { printf($2); }
;

expr: factor
 | expr ADD factor { $$ = $1 + $3;  }
 | expr SUB factor { $$ = $1 - $3;  }
;
factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
;
term: DIGIT {$$ = total; total = 0;}
 | VAR {printf(yytext); var = atoi(yytext); var = var-97; $$ = array[var];}
;


;
%%
void yyerror(char *s)
{
  if(print  == 1)
  {
    printf("syntax error\n");
    print = 0;
  }
}


int
main()
{
//  yydebug = 1;
    yyparse();
    return 0;
}
