%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
int print =1;
extern int total;
extern int i;
int array [26];
%}
%output "calcwithvariables.tab.c"

%token ADD SUB MUL DIV VAR DIGIT EOE ASSIGN PRINT EOL
%%

calc: /* nothing */ {}
| calc expression EOE EOL{}
| calc print EOE EOL
;
expression: VAR ASSIGN expr {array[i] = $3;}
;

expr: factor
 | expr ADD factor { $$ = $1 + $3;  }
 | expr SUB factor { $$ = $1 - $3;  }
;
factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
;
term: DIGIT {$$ = total;}
 | VAR {$$ = array[i];}
;

print: PRINT VAR {printf("%d", array[i]);}
;

;
%%
void yyerror(char *s)
{
  if(print  == 1)
  {
    printf("syntax error\n");
    print = 0;
  }
}


int main()
{
//  yydebug = 1;
    yyparse();
    return 0;
}
