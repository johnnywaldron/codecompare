%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
int print =1;
int val;
int result;
extern int total;
%}
%output "romcalc.tab.c"

%token EOL ONE FOUR FIVE NINE TEN FORTY FIFTY NINETY HUNDR FOURHUNDR FIVEHUNDR NINEHUNDR THOUSAND ADD SUB MUL DIV OC CC
%%

number: /* nothing */ {}
| number valid EOL { total = 0; }
;

valid : roman {dec2romanstr(total); val = total; total = 0;}| roman {val = total; total = 0;} expr

expr : multiply | multiply expr | divide | divide expr | add | add expr | subtract | subtract expr

multiply : MUL roman {result = val*total;if(result == 0) printf("Z"); else dec2romanstr(result);}

divide : DIV roman {result = val/total; if(result == 0) printf("Z"); else dec2romanstr(result);}

add : ADD roman {result = val+total; if(result == 0) printf("Z"); else dec2romanstr(result);}

subtract : SUB roman {result = val-total; if(result == 0) printf("Z"); else dec2romanstr(result);}

rome : one| FIVE one | FIVE | TEN | FOUR | NINE
;
one: ONE | ONE two
;
two : ONE | ONE three
;
three : ONE
;

rome1 : ten | FIFTY ten | FIFTY | HUNDR | FORTY | NINETY
;
ten: TEN | TEN twenty
;
twenty : TEN | TEN thirty
;
thirty : TEN
;

rome2 : hundred | FIVEHUNDR hundred | FIVEHUNDR | FOURHUNDR | NINEHUNDR
;
hundred: HUNDR | HUNDR two-hundred
;
two-hundred : HUNDR | HUNDR three-hundred
;
three-hundred : HUNDR
;

rome3 : THOUSAND rome3 | THOUSAND


roman : rome2 | rome1 | rome | rome2 rome1 | rome2 rome | rome2 rome1 rome | rome1 rome | rome3 rome2 | rome3 rome1 | rome3 rome | rome3 rome2 rome1 | rome3 rome2 rome | rome3 rome2 rome1 rome | rome3 rome1 rome
;
%%
void yyerror(char *s)
{
  if(print  == 1)
  {
    printf("syntax error\n");
    print = 0;
  }
}

void dec2romanstr(int num){
    int del[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1}; // Key value in Roman counting
    char * sym[] = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" }; //Symbols for key values
    char res[64] = "\0";         //result string
    int i = 0;                   //
    while (num){                 //while input number is not zero
        while (num/del[i]){      //while a number contains the largest key value possible
            strcat(res, sym[i]); //append the symbol for this key value to res string
            num -= del[i];       //subtract the key value from number
        }
        i++;                     //proceed to the next key value
    }
    puts(res);
}

int
main()
{
//  yydebug = 1;
    yyparse();
    return 0;
}
