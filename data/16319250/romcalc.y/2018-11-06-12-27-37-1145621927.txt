%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
int print =1;
int val;
int result;
extern int total;
%}
%output "romcalc.tab.c"

%token EOL ONE FOUR FIVE NINE TEN FORTY FIFTY NINETY HUNDR FOURHUNDR FIVEHUNDR NINEHUNDR THOUSAND ADD SUB MUL DIV OC CC
%%

number: /* nothing */ {}
| number valid EOL { total = 0; }
;

valid : roman {romanConvert(total); val = total; total = 0;}| roman {val = total; total = 0;} expr

expr : multiply | multiply expr | divide | divide expr | add | add expr | subtract | subtract expr

multiply : MUL roman {result = val*total;if(result == 0) printf("Z"); else romanConvert(result);}

divide : DIV roman {result = val/total; if(result == 0) printf("Z"); else romanConvert(result);}

add : ADD roman {result = val+total; if(result == 0) printf("Z"); else romanConvert(result);}

subtract : SUB roman {result = val-total; if(result == 0) printf("Z"); else romanConvert(result);}

rome : one| FIVE one | FIVE | TEN | FOUR | NINE
;
one: ONE | ONE two
;
two : ONE | ONE three
;
three : ONE
;

rome1 : ten | FIFTY ten | FIFTY | HUNDR | FORTY | NINETY
;
ten: TEN | TEN twenty
;
twenty : TEN | TEN thirty
;
thirty : TEN
;

rome2 : hundred | FIVEHUNDR hundred | FIVEHUNDR | FOURHUNDR | NINEHUNDR
;
hundred: HUNDR | HUNDR two-hundred
;
two-hundred : HUNDR | HUNDR three-hundred
;
three-hundred : HUNDR
;

rome3 : THOUSAND rome3 | THOUSAND


roman : rome2 | rome1 | rome | rome2 rome1 | rome2 rome | rome2 rome1 rome | rome1 rome | rome3 rome2 | rome3 rome1 | rome3 rome | rome3 rome2 rome1 | rome3 rome2 rome | rome3 rome2 rome1 rome | rome3 rome1 rome
;
%%
void yyerror(char *s)
{
  if(print  == 1)
  {
    printf("syntax error\n");
    print = 0;
  }
}

void predigit(char num1, char num2);
void postdigit(char c, int n);

char romanval[1000];
int i = 0;
int romanConvert(int number)
{
    int j;
    if (number <= 0)
    {
        printf("Invalid number");
        return 0;
    }
    while (number != 0)
    {
        if (number >= 1000)
        {
            postdigit('M', number / 1000);
            number = number - (number / 1000) * 1000;
        }
        else if (number >= 500)
        {
            if (number < (500 + 4 * 100))
            {
                postdigit('D', number / 500);
                number = number - (number / 500) * 500;
            }
            else
            {
                predigit('C','M');
                number = number - (1000-100);
            }
        }
        else if (number >= 100)
        {
            if (number < (100 + 3 * 100))
            {
                postdigit('C', number / 100);
                number = number - (number / 100) * 100;
            }
            else
            {
                predigit('L', 'D');
                number = number - (500 - 100);
            }
        }
        else if (number >= 50 )
        {
            if (number < (50 + 4 * 10))
            {
                postdigit('L', number / 50);
                number = number - (number / 50) * 50;
            }
            else
            {
                predigit('X','C');
                number = number - (100-10);
            }
        }
        else if (number >= 10)
        {
            if (number < (10 + 3 * 10))
            {
                postdigit('X', number / 10);
                number = number - (number / 10) * 10;
            }
            else
            {
                predigit('X','L');
                number = number - (50 - 10);
            }
        }
        else if (number >= 5)
        {
            if (number < (5 + 4 * 1))
            {
                postdigit('V', number / 5);
                number = number - (number / 5) * 5;
            }
            else
            {
                predigit('I', 'X');
                number = number - (10 - 1);
            }
        }
        else if (number >= 1)
        {
            if (number < 4)
            {
                postdigit('I', number / 1);
                number = number - (number / 1) * 1;
            }
            else
            {
                predigit('I', 'V');
                number = number - (5 - 1);
            }
        }
    }
    for(j = 0; j < i; j++)
        printf("%c", romanval[j]);
    printf("\n");
    memset(&romanval[0], 0, sizeof(romanval));
    return 0;
}

void predigit(char num1, char num2)
{
    romanval[i++] = num1;
    romanval[i++] = num2;
}

void postdigit(char c, int n)
{
    int j;
    for (j = 0; j < n; j++)
        romanval[i++] = c;
}

int
main()
{
//  yydebug = 1;
    yyparse();
    return 0;
}
