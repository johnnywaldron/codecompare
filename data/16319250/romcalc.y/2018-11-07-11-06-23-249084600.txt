%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
int print =1;
int val;
int result;
int total =0;
%}
%output "romcalc.tab.c"

%token EOL ONE FOUR FIVE NINE TEN FORTY FIFTY NINETY HUNDR FOURHUNDR FIVEHUNDR NINEHUNDR THOUSAND ADD SUB MUL DIV OC CC
%%

number: /* nothing */ {}
| number expr EOL { total = 0; }
| roman EOL {dec2romanstr(total); total =0;}
;

//valid : roman {dec2romanstr(total);} | roman expr

expr: factor
 | expr ADD factor { $$ = $1 + $3; if($$ < 0){ $$ = $$ *-1; printf("-");} dec2romanstr($$);}
 | expr SUB factor { $$ = $1 - $3; if($$ < 0){ $$ = $$ *-1; printf("-");}  dec2romanstr($$);}
;
factor: term {$$ = total; total = 0;}
 | factor MUL term { $$ = $1 * $3; if($$ < 0){ $$ = $$ *-1; printf("-");}  dec2romanstr($$);}
 | factor DIV term { $$ = $1 / $3; if($$ < 0){ $$ = $$ *-1; printf("-");}  dec2romanstr($$);}
;
term: roman {$$ = total;}
 | OC expr CC { $$ = $2; dec2romanstr($$);}
;

rome : one | FIVE {total += 5;} one | FIVE {total += 5;} | TEN {total += 10;} | FOUR {total += 4;} | NINE {total += 9;}
;
one: ONE {total += 1;}| ONE two
;
two : ONE {total += 2;}| ONE three
;
three : ONE {total += 3;}
;

rome1 : ten | FIFTY {total += 50;} ten | FIFTY {total += 50;} | HUNDR {total += 100;} | FORTY {total += 40;} | NINETY {total += 90;}
;
ten: TEN {total += 10;} | TEN twenty
;
twenty : TEN {total += 20;}| TEN thirty
;
thirty : TEN {total += 30;}
;

rome2 : hundred | FIVEHUNDR {total += 500;} hundred | FIVEHUNDR {total += 500;} | FOURHUNDR {total += 400;} | NINEHUNDR {total += 900;}
;
hundred: HUNDR {total += 100;} | HUNDR two-hundred
;
two-hundred : HUNDR {total += 200;} | HUNDR three-hundred
;
three-hundred : HUNDR {total += 300;}
;

rome3 : THOUSAND {total += 1000;} rome3 | THOUSAND {total += 1000;}


roman : rome2 | rome1 | rome | rome2 rome1 | rome2 rome | rome2 rome1 rome | rome1 rome | rome3 rome2 | rome3 rome1 | rome3 rome | rome3 rome2 rome1 | rome3 rome2 rome | rome3 rome2 rome1 rome | rome3 rome1 rome
;
%%
void yyerror(char *s)
{
  if(print  == 1)
  {
    printf("syntax error\n");
    //print = 0;
  }
}

void dec2romanstr(int num){
    if(num == 0)
    {
        printf("Z\n");
    }
    else
    {
        int del[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1}; // Key value in Roman counting
        char * sym[] = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" }; //Symbols for key values
        char res[100] = "\0";         //result string
        int i = 0;                   //
        while (num){                 //while input number is not zero
            while (num/del[i]){      //while a number contains the largest key value possible
                strcat(res, sym[i]); //append the symbol for this key value to res string
                num -= del[i];       //subtract the key value from number
            }
            i++;                     //proceed to the next key value
        }
        printf("%s\n", res);
    }
}

int
main()
{
//  yydebug = 1;
    yyparse();
    return 0;
}
