%{
# include "romcalc.tab.h"

void yyerror(char *s);
int yylex();

%}

%%

"I"  { return ONE; }
"IV" {return FOUR; }
"V"  { return FIVE; }
"IX" {  return NINE; }
"X"  { return TEN; }
"XL" { return FORTY; }
"L"  { return FIFTY; }
"XC" { return NINETY; }
"C"  { return HUNDR; }
"CD" { return FOURHUNDR; }
"D"  { return FIVEHUNDR; }
"CM" { return NINEHUNDR; }
"M"  { return THOUSAND; }
"+"  {return ADD;}
"-"  {return SUB;}
"*"  {return MUL;}
"/"  {return DIV;}
"{"  {return OC;}
"}"  {return CC;}
\n   { return EOL; }
.	 { yyerror("syntax error"); }
%%
