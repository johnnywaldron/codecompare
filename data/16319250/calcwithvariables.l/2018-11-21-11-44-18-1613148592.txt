%{
# include "calcwithvariables.tab.h"

void yyerror(char *s);
int yylex();
int i = 0;
int total =0;
%}

%%
"print" {return PRINT;}
[a-z] {i = (int)(*yytext-'a'); printf("%d", i); return VAR;}
[0-9] {total = atoi(yytext); return DIGIT;}
"+"  {return ADD;}
"-"  {return SUB;}
"*"  {return MUL;}
"/"  {return DIV;}
":="  {return ASSIGN;}
";"  {return EOE;}
\n   {return EOL;}
.	 { yyerror("syntax error"); }
%%
