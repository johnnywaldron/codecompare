%{
# include "calcwithvariables.tab.h"

void yyerror(char *s);
int yylex();

%}

%%

[a-z] {yylval.s = yytext-'a'; return VAR;}
[0-9] {yylval.d = atoi(yytext); return DIGIT;}
"+"  {return ADD;}
"-"  {return SUB;}
"*"  {return MUL;}
"/"  {return DIV;}
";"  {return EOE;}
\n   {return EOL;}
.	 { yyerror("syntax error"); }
%%
