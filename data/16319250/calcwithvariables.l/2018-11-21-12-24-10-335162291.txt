%{
# include "calcwithvariables.tab.h"

void yyerror(char *s);
int yylex();
int i = 0;
int total =0;
%}

%%
"print" {return PRINT;}
[a-z] {return VAR;}
[0-9] {total = atoi(yytext); return DIGIT;}
"+"  {return ADD;}
"-"  {return SUB;}
"*"  {return MUL;}
"/"  {return DIV;}
":="  {return ASSIGN;}
";"  {return EOE;}
[ \s\t\r\n] {}
.	 { yyerror("syntax error"); }
%%
