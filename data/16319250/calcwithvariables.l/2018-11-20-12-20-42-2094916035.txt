%{
# include "calcwithvariables.tab.h"

void yyerror(char *s);
int yylex();

%}

%%

[a-z] {return VAR;}
[0-9] {return DIGIT;}
"+"  {return ADD;}
"-"  {return SUB;}
"*"  {return MUL;}
"/"  {return DIV;}
";"  {return EOE;}
\n   {return EOL;}
.	 { yyerror("syntax error"); }
%%
