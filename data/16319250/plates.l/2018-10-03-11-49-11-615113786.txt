%{
int chars = 0;
int words = 0;
int lines = 0;
int count = 0;
int  validYear = 0;
int  validCounty =0;
%}

%%
[\r\t] 	{}
[0-9]*	{if(validCounty !=1 && validYear !=1)((if(yytext.length == 2)(printf(18 - atoi(yytext))))); validYear = 1;}
[a-zA-Z]+	{validCounty = 1;}
[\n] 	{validCounty =0; validYear = 0; printf("\n");}
.		{}

%%

int main()
{
  yylex();
  return 0;
}
