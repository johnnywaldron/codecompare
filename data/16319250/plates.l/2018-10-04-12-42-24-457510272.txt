%{
int year;
int difference;
int valid = 0;
%}
COUNTYNEW   C|CE|CN|CW|D|DL|G|KE|KK|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|T|SO|W|WH|WX|WW
COUNTYOLD   C|CE|CN|CW|D|DL|G|KE|KK|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|W|WH|WX|WW|TN|TS|WD
YEAROLD     [90][0-9]|[8][7-9]|[1][0-2]|[1][3][1-2]
YEARNEW     [1][4-8][1-2]
DIGITS      [0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]?
%%
[' '\r\t] 	{}
({YEAROLD})([-])({COUNTYOLD})([-])({DIGITS}) {valid = 1; year = atoi(strtok(yytext, "-")); if(year>= 100){year = year/10;} if(year > 18) {difference = 118 - year;} else {difference = 18 - year;} printf("%d\n", difference);}

({YEARNEW})([-])({COUNTYNEW})([-])({DIGITS}) {valid = 1; year = atoi(strtok(yytext, "-"));year = year/10; difference = 18 - year; printf("%d\n", difference);}

[\n] 	{}
.		{if(valid == 0){printf("INVALID\n");}}

%%

int main()
{
  yylex();
  if(valid ==0)
  {
    printf("INVALID");
  }
  return 0;
}

