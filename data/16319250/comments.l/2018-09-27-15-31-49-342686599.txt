%{
int open = 0;
int error = 0;
int quote =0;
%}



%%
"{"		{open = 1;} 
"}"		{{if (open > 0) {open = 0;}
    		else {error = 1;}	}}
L?\"(\\.|[^\\"])*\" {if(open ==0 && error ==0) {printf("%s", yytext);} }
"**".* 	{}
\n			{if(open ==0 && error ==0) {printf("%s", yytext);}}
.			{if(open ==0 && error ==0) {printf("%s", yytext);}}
%%

int main()
{
  	yylex();
	if(error ==1)
	{
		printf("syntax error\n");
	}
  	return 0;
}	
