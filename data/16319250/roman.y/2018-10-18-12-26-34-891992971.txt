%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
extern int total;
%}
%output "roman.tab.c"

%token EOL ONE FOUR FIVE NINE TEN FORTY FIFTY NINETY HUNDR FOURHUNDR FIVEHUNDR NINEHUNDR THOUSAND
%%

calclist: /* nothing */ {}
| calclist roman EOL { printf("%d\n", total); }
;


expr : one| FIVE one | FIVE | TEN | FOUR | NINE
;
one: ONE | ONE two
;
two : ONE | ONE three
;
three : ONE
;

expr1 : ten | FIFTY ten | FIFTY | HUNDR | FORTY | NINETY
;
ten: TEN | TEN twenty
;
twenty : TEN | TEN thirty
;
thirty : TEN
;

expr2 : hundred | FIVEHUNDR hundred | FIVEHUNDR | FOURHUNDR | NINEHUNDR
;
hundred: HUNDR | HUNDR two-hundred
;
two-hundred : HUNDR | HUNDR three-hundred
;
three-hundred : HUNDR
;

roman : expr2 | expr1 | expr | expr2 expr1 | expr2 expr | expr2 expr1 expr | expr1 expr | THOUSAND expr2 | THOUSAND expr1 | THOUSAND expr | THOUSAND expr2 expr1 | THOUSAND expr2 expr | THOUSAND expr2 expr1 expr | THOUSAND expr1 expr
;
%%
void yyerror(char *s)
{
  printf("error: %s\n", s);
}


int
main()
{
//  yydebug = 1;
  yyparse();
  return 0;
}
