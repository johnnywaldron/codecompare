%{
	# include "romcalc.tab.h"
	int yyparse();
%}

%%

"(" {}
")" {}
"+"	{return ADD;}
"-"	{return SUB;}
"*"	{return MUL;}
"/"	{return DIV;}
I	{return I;}
V	{return V;}
X	{return X;}
L	{return L;}
C	{return C;}
D	{return D;}
M	{return M;}
\n	{return EOL;}
.	{printf("syntax error");}
exit(0);
%%

int main (void) {
	yyparse();
    return 0;
}
