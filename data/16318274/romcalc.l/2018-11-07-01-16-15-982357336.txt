%{
	# include "romcalc.tab.h"
	int yyparse();
%}

%%
"[a-z]*"  {return ERROR; printf("syntax error");}
"{" {return OP;}
"}" {return CP;}
"*"	{return MUL;}
"/"	{return DIV;}
"+" {return ADD;}
"-" {return SUB;}
I	{return I;}
V	{return V;}
X	{return X;}
L	{return L;}
C	{return C;}
D	{return D;}
M	{return M;}
\n	{return EOL;}
.	{printf("syntax error");}
exit(0);
%%

int main (void) {
	yyparse();
    return 0;
}
