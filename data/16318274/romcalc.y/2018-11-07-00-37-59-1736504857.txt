%{
# include <stdio.h>
# include <math.h>
# include <string.h>
#define NAME_MAX_SIZE 256
#define NUM_MAX_SIZE 512
int yylex();
int yyparse();
char* r_unparse(int decimal);
void yyerror (char const *s) {
   fprintf (stderr, "%s\n", s);
 }
%}

%output "romcalc.tab.c"

%left '-' '+'
%left '*' '/'


/* its token time */
%token I V X L C D M
%token ADD SUB MUL DIV
%token EOL
// {int decimal = atoi(yytext()); printf("%s\n", r_unparse(decimal));}
//{ printf("%s\n", r_unparse(atoi($1))); }
%%
 calclist: /* nothing */ 	{}
 | calclist exp EOL 		{ printf("%s\n", r_unparse($2)); }
 ;

 exp: romanNum
 |  exp ADD romanNum { $$ = $1 + $3;}
 |  exp SUB romanNum { $$ = $1 - $3;}
 |  exp MUL romanNum { $$ = $1 * $3;}
 |  exp DIV romanNum { $$ = $1 / $3;}
 |'{' exp '}'				{$$ = $2;}
 ;

 romanNum: thousand hundo ten digit	{$$ = $1 + $2 + $3 + $4;}
 |hundo ten digit			{$$ = $1 + $2 + $3;}
 |ten digit					{$$ = $1 + $2;}
 |digit						{$$ = $1;}
 ;

thousand: {$$ = 0;}
 |M {$$ = 1000;}
 |M M {$$ = 2000;}
 |M M M {$$ = 3000;}
 ;

hundo: {$$ = 0;}
 |shundo {$$ = $1;}
 |C D		{$$ = 400;}
 |D shundo	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;

shundo:	{$$ = 0;}
 |C {$$ = 100;}
 |C C		{$$ = 200;}
 |C C C		{$$ = 300;}
 ;

ten: {$$ = 0;}
 |sten {$$ = $1;}
 |X L		{$$ = 40;}
 |L sten	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;

sten: 	{$$ = 0;}
 |X {$$ = 10;}
 |X X		{$$ = 20;}
 |X X X		{$$ = 30;}
 ;

digit:	{$$ = 0;}
 |sdigit {$$ = $1;}
 |I V		{$$ = 4;}
 |V sdigit	{$$ = 5 + $2;}
 |I X		{$$ = 9;}
 ;

sdigit:	{$$ = 0;}
 |I 		{$$ = 1;}
 |I I		{$$ = 2;}
 |I I I		{$$ = 3;}
 ;


%%


char* r_unparse(int decimal) {
	char *buf = malloc(sizeof(char)*NUM_MAX_SIZE);
	int i, idx = 0;
	if(decimal==0)
	{
		buf[idx] ='Z';
	}
	else
	{
		if(decimal < 0)
		{
			buf[idx] = '-';
			idx++;
			decimal = decimal*-1;
		}
		int m = decimal / 1000;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx)
			buf[idx] = 'M';
		decimal -= m * 1000;
		m = decimal / 900;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx++] = 'C';
			buf[idx] = 'M';
		}
		decimal -= m * 900;
		m = decimal / 500;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx] = 'D';
		}
		decimal -= m * 500;
		m = decimal / 400;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx++] = 'C';
			buf[idx] = 'D';
		}
		decimal -= m * 400;
		m = decimal / 100;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx] = 'C';
		}
		decimal -= m * 100;
		m = decimal / 90;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx++] = 'X';
			buf[idx] = 'C';
		}
		decimal -= m * 90;
		m = decimal / 50;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx] = 'L';
		}
		decimal -= m * 50;
		m = decimal / 40;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx++] = 'X';
			buf[idx] = 'L';
		}
		decimal -= m * 40;
		m = decimal / 10;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx] = 'X';
		}
		decimal -= m * 10;
		m = decimal / 9;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx++] = 'I';
			buf[idx] = 'X';
		}
		decimal -= m * 9;
		m = decimal / 5;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx] = 'V';
		}
		decimal -= m * 5;
		m = decimal / 4;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx++] = 'I';
			buf[idx] = 'V';
		}
		decimal -= m * 4;
		m = decimal;
		for (i = 0; i < m && idx < NUM_MAX_SIZE; ++i, ++idx) {
			buf[idx] = 'I';
		}
	}
	return buf;
}