%{
#include <stdio.h>

void yyerror(char *);
int yylex(void);
int sym[26];
%}

%output "calcwithvariables.tab.c"

%token INTEGER VARIABLE ASSIGNMENT
%left '+' '-' '*' '/'

%%
program:
program statement '\n'
| /* NULL */
;

statement:
expression { printf("%d\n", $1); }
| VARIABLE ASSIGNMENT expression { sym[$1] = $3; }
;

expression:
INTEGER
| VARIABLE { $$ = sym[$1]; }
| '-' expression { $$ = -$2; }
| expression '+' expression { $$ = $1 + $3; }
| expression '-' expression { $$ = $1 - $3; }
| expression '*' expression { $$ = $1 * $3; }
| expression '/' expression { $$ = $1 / $3; }
| '{' expression '}' { $$ = $2; }
;

%%
void yyerror(char *s) 
{
 fprintf(stderr, "%s\n", s);
}
int main(void) {
 yyparse();
 return 0;
}