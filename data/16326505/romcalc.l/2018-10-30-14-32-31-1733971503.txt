/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-1.l,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

%{
# include "romcalc.tab.h"
void yyerror();
%}

%%
[ \t\r\f\v]*                  {}
"+"	                          { return ADD; }
"-"	                          { return SUB; }
"*"	                          { return MUL; }
"/"	                          { return DIV; }
"{"                           { return OP; }
"}"                           { return CP; }
I                             {return ONE;}
V                             {return FIVE;}
X                             {return TEN;}
L                             {return FIFTY;}
C                             {return HUNDRED;}
D                             {return FIVEHUNDRED;}
M                             {return THOUSAND;}
\n                            {return EOL;}
.                             {yyerror();}

%%


