%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
%}

/* declare tokens */
%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND
%token EOL
%%
calclist: /* nothing */
 | calclist exp EOL { printf("= %d\n> ", $2); }
 ; 

exp: num 
 | exp num { $$ = $1 + $2; }
 | num exp { $$ = $2 - $1; }
 ;

num: ONE 
 | FIVE 
 | TEN
 | FIFTY
 | HUNDRED
 | FIVEHUNDRED
 | THOUSAND
 ;


%%
int main()
{
  printf("> "); 
  yyparse();
  return 0;
}
