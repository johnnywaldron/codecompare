%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
int yyparse();
void yyerror(char *s);
%}

/* declare tokens */
%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND
%token EOL
%%
calclist: /* nothing */
 | calclist exp EOL { printf("%d\n", $2); }
 ; 

exp: giantnum 
 | giantnum largenum {$$ = $1 + $2;}
 | giantnum largenum mednum {$$ = $1 + $2 + $3;}
 | giantnum largenum mednum smallnum {$$ = $1 + $2 + $3 + $4;}
 | giantnum mednum {$$ = $1 + $2;}
 | giantnum mednum smallnum {$$ = $1 + $2 + $3;}
 | giantnum smallnum {$$ = $1 + $2;}
 | largenum
 | largenum mednum {$$ = $1 + $2;}
 | largenum mednum smallnum {$$ = $1 + $2 + $3;}
 | largenum smallnum {$$ = $1 + $2;}
 | mednum
 | mednum smallnum {$$ = $1 + $2;}
 | smallnum
 ;
 
smallnum: ONE { $$ =1;}
 | ONE ONE { $$ =2;}  
 | ONE ONE ONE { $$ = 3;}
 | ONE FIVE {$$ = 4;}
 | FIVE { $$ =5;}
 | FIVE ONE { $$ =6;}
 | FIVE ONE ONE{ $$ =7;}
 | FIVE ONE ONE ONE { $$ =8;}
 | ONE TEN {$$ = 9;}
 ;
 
mednum: TEN { $$ =10;}
 | TEN TEN { $$ =20;}
 | TEN TEN TEN { $$ =30;}
 | TEN FIFTY {$$ = 40;}
 | FIFTY { $$ =50;}
 | FIFTY TEN { $$ =60;}
 | FIFTY TEN TEN { $$ =70;}
 | FIFTY TEN TEN TEN { $$ =80;}
 | TEN HUNDRED {$$ = 90;}
 ;
 
largenum: HUNDRED { $$ =100;}
 | HUNDRED HUNDRED { $$ =200;}
 | HUNDRED HUNDRED HUNDRED { $$ =300;}
 | HUNDRED FIVEHUNDRED { $$ = 400;}
 | FIVEHUNDRED { $$ =500;}
 | FIVEHUNDRED HUNDRED { $$=600;}
 | FIVEHUNDRED HUNDRED HUNDRED { $$=700;}
 | FIVEHUNDRED HUNDRED HUNDRED HUNDRED { $$=800;}
 | HUNDRED THOUSAND { $$ = 900;}
 ;

giantnum: THOUSAND { $$ =1000;}
 | THOUSAND THOUSAND { $$ =2000;}
 | THOUSAND THOUSAND THOUSAND { $$ =3000;}
 ;


%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("syntax error\n");
  exit(0);
}

