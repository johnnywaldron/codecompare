%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
int yyparse();
void yyerror(char *s);
%}

/* declare tokens */
%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND
%token EOL
%%
calclist: /* nothing */
 | calclist exp EOL { printf("%d\n", $2); }
 ; 

exp: giantnum 
 | giantnum exp  { $$ = $1 + $2; }
 | exp2
 ;

exp2: largernum
 | largernum exp3 {$$ = $1 + $2; }
 | exp3
 ;
 
exp3: largenum
 | largenum exp4 {$$ = $1 + $2; }
 | exp4
 ;
 
exp4: biggernum
 | biggernum exp5 {$$ = $1 + $2; }
 | exp5
 ;
 
exp5: bignum
 | bignum exp6 {$$ = $1 + $2; }
 | exp6
 ;
 
exp6: mednum
 | mednum smallnum {$$ = $1 + $2; }
 | smallnum
 ;
 
smallnum: ONE { $$ =1;}
 | ONE ONE { $$ =2;}  
 | ONE ONE ONE { $$ = 3;}
 ;
 
mednum: FIVE { $$ =5;}
 | ONE FIVE {$$ = 4;}
 ;
 
bignum: TEN { $$ =10;}
 | ONE TEN {$$ = 9;}
 | TEN TEN { $$ =20;}
 | TEN TEN TEN { $$ =30;}
 ;
 
biggernum: FIFTY { $$ =50;}
 | TEN FIFTY {$$ = 40;}
 ;
 
largenum: HUNDRED { $$ =100;}
 | TEN HUNDRED {$$ = 90;}
 | HUNDRED HUNDRED { $$ =200;}
 | HUNDRED HUNDRED HUNDRED { $$ =300;}
 ;
 
largernum: FIVEHUNDRED { $$ =500;}
 | HUNDRED FIVEHUNDRED { $$ = 400;}
 ;
 
giantnum: THOUSAND { $$ =1000;}
 | HUNDRED THOUSAND {$$ = 900;}
 | THOUSAND THOUSAND { $$ =2000;}
 | THOUSAND THOUSAND THOUSAND { $$ =3000;}
 ;


%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}

