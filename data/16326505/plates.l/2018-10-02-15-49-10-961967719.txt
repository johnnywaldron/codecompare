
%{
int year = 0;
char date[3];
%}

%%
[0-9][0-9][12]-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)-[0-9]{1,6}           { 
    date[0] = yytext[0];
    date[1] = yytext[1];
    date[2] = '\0';
    year = atoi(date);
    if(year <= 18)
      printf("%d\n",18-year);
    else
      printf("%d\n",2018 - 1900 - year);
  }

[0-9][0-9]-(C|CE|CN|CW|D|DL|G|KE|KK|KY|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|L|LK|W|WD|WH|WX|WW)-[0-9]{1,6}  { 
    date[0] = yytext[0];
    date[1] = yytext[1];
    date[2] = '\0';
    year = atoi(date);
    if(year <= 18)
      printf("%d\n",18-year);
    else
      printf("%d\n", year);

  }
[0-9][0-9][12]-[(C)(CE)(CN)(CW)(D)(DL)(G)(KE)(KK)(KY)(L)(LD)(LH)(LM)(LS)(MH)(MN)(MO)(OY)(RN)(SO)(T)(W)(WH)(WX)(WW)]     {printf("%s\n",yytext);}
.                   {printf("INVALID\n");}
[ \t\n\r\f\v]+                  {}

%%

int main()
{
  
  yylex();
  return 0;
}

