%{
int count = 0;
%}

%%
[0-9]*[02468]     { count++;}
[0-9]*[13579]     {/*so that odd numbers are ignored*/}
.                   {}
\n                  {}

%%

int main()
{
  yylex();
  printf("%d", count);
  return 0;
}
