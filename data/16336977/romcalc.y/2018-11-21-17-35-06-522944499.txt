%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int yylex();
void yyerror(char* s);

#define MAX_SIZE 255

char PARSED[MAX_SIZE];
void convertToRoman(int, char*);

%}

%left '*' '/'
%left '-' '+'

%token I V X L C D M EOL FAIL
%token ADD SUB MUL DIV OP CP

%%


list: /* nothing */
  | list expr EOL {
    int val = $2;

    if (val == 0) {
      printf("Z");
    }

    if (val < 0) {
      val *= -1;
      printf("-");
    }
    convertToRoman(val, PARSED);
    printf("%s\n", PARSED);

  }
  | FAIL EOL { yyerror("syntax error"); }
  ;

expr: factors
  | expr ADD factors { $$ = $1 + $3; }
  | expr SUB factors { $$ = $1 - $3; }
  ;

factors: numbers
  | factors MUL numbers { $$ = $1 * $3; }
  | factors DIV numbers { $$ = $1 / $3; }
  ;

numbers: thousands hundredths tenths units { $$ = $1 + $2 + $3 + $4; }
  units { $$ = $1; }
  | tenths units { $$ = $1 + $2; }
  | hundredths tenths units { $$ = $1 + $2 + $3;  }
  | OP expr CP { $$ = $2; }
  ;

thousands: M { $$ = 1000; }
  | M M { $$ = 2000; }
  | M M M { $$ = 3000; }
  ;

hundredths: { $$ = 0; }
  | D | hundreds { $$ = $1; }
  | C D { $$ = $2 - $1; }
  | C M { $$ = $2 - $1; }
  | D hundreds { $$ = $2 + $1; }
  ;

hundreds: C { $$ = 100; }
  | C C { $$ = 200;  }
  | C C C { $$ = 300; }
  ;

tenths: { $$ = 0; }
  | L | tens { $$ = $1; }
  | X L { $$ = $2 - $1; }
  | X C { $$ = $2 - $1; }
  | L tens { $$ = $1 + $2; }
  ;

tens: X { $$ = 10; }
  | X X { $$ = 20; }
  | X X X { $$ = 30; }
  ;

units : { $$ = 0; }
  | V | ones { $$ = $1; }
  | I V { $$ = $2 - $1; }
  | I X { $$ = $2 - $1; }
  | V ones { $$ = $1 + $2; }
  ;

ones:
  I { $$ = 1; }
  | I I   { $$ = 2; }
  | I I I  { $$ = 3; }
  ;

%%

int main() {
  yyparse();
  return 0;
}

void yyerror(char* s) {
  fprintf(stderr, "%s\n", s);
  exit(0);
}

void convertToRoman (int val, char *res) {
    char *huns[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    int   size[] = { 0,   1,    2,     3,    2,   1,    2,     3,      4,    2};

    //  Add 'M' until we drop below 1000.

    while (val >= 1000) {
        *res++ = 'M';
        val -= 1000;
    }

    // Add each of the correct elements, adjusting as we go.

    strcpy (res, huns[val/100]); res += size[val/100]; val = val % 100;
    strcpy (res, tens[val/10]);  res += size[val/10];  val = val % 10;
    strcpy (res, ones[val]);     res += size[val];

    // Finish string off.

    *res = '\0';
}
