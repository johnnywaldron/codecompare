%{

#include <stdio.h>
#include <stdlib.h>
int yylex();
void yyerror(char* s);

%}


%token I V X L C D M EOL FAIL

%%

list: /* nothing */
  | list numbers EOL { printf("%d\n", $2); }
  | FAIL EOL { printf("syntFFF\n"); }
  ;

numbers:
  units { $$ = $1; }
  | tenths units { $$ = $1 + $2; }
  | hundredths tenths units { $$ = $1 + $2 + $3;  }
  | thousands hundredths tenths units { $$ = $1 + $2 + $3 + $4; }
  ;

thousands: M { $$ = 1000; }
  | M M { $$ = 2000; }
  | M M M { $$ = 3000; }
  ;

hundredths: { $$ = 0; }
  | D | hundreds { $$ = $1; }
  | C D { $$ = $2 - $1; }
  | C M { $$ = $2 - $1; }
  ;

hundreds: C { $$ = 100; }
  | C C { $$ = 200;  }
  | C C C { $$ = 300; }
  ;

tenths: { $$ = 0; }
  | L | tens { $$ = $1; }
  | X L { $$ = $2 - $1; }
  | X C { $$ = $2 - $1; }
  ;

tens: X { $$ = 10; }
  | X X { $$ = 20; }
  | X X X { $$ = 30; }
  ;

units : { $$ = 0; }
  | V | ones { $$ = $1; }
  | I V { $$ = $2 - $1; }
  | I X { $$ = $2 - $1; }
  | V ones { $$ = $1 + $2; }
  ;

ones:
  I { $$ = 1; }
  | I I   { $$ = 2; }
  | I I I  { $$ = 3; }
  ;

%%

int main() {
  yyparse();
  return 0;
}

void yyerror(char* s) {
  fprintf(stderr, "%s\n", s);
  exit(1);
}
