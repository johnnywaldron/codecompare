%{
#include <math.h>
#include <string.h>

int validCounty(char* st, int yearF){

  char str[] = "    ";
  int offset = 0;
  while(st[offset]!='-'){
    str[offset] = st[offset];
    offset++;
  }
  str[offset] = 0;

  if(!strcmp(str,"C"))
    return 1;
  else if(!strcmp(str,"CE"))
    return 1;
  else if(!strcmp(str,"CN"))
    return 1;
  else if(!strcmp(str,"CW"))
    return 1;
  else if(!strcmp(str,"D"))
    return 1;
  else if(!strcmp(str,"DL"))
    return 1;
  else if(!strcmp(str,"G"))
    return 1; 
  else if(!strcmp(str,"KE"))
    return 1;
  else if(!strcmp(str,"KK"))
    return 1;
  else if(!strcmp(str,"KY"))
    return 1;
  else if((!strcmp(str,"L")&&yearF)||(!strcmp(str,"LK")&&!yearF))
    return 1;
  else if(!strcmp(str,"LD"))
    return 1;
  else if(!strcmp(str,"LH"))
    return 1; 
  else if(!strcmp(str,"LM"))
    return 1;
  else if(!strcmp(str,"LS"))
    return 1;
  else if(!strcmp(str,"MH"))
    return 1;            
  else if(!strcmp(str,"MO"))
    return 1;
  else if(!strcmp(str,"OY"))
    return 1;
  else if(!strcmp(str,"RN"))
    return 1;
  else if(!strcmp(str,"SO"))
    return 1;
  else if((!strcmp(str,"T")&&yearF)||(!strcmp(str,"TN")&&!yearF)||(!strcmp(str,"TS")&&!yearF))
    return 1;
  else if(!strcmp(str,"W")||(!strcmp(str,"WD")&&!yearF))
    return 1;
  else if(!strcmp(str,"WH"))
    return 1;
  else if(!strcmp(str,"WX"))
    return 1;
  else if(!strcmp(str,"WW"))
    return 1;         
  return 0;
}

int difference2(int year){
  if(year<13)
    year+=100;
  return 118-year;  
}

int difference3(int year){
  return 18 - year;
}

%}

%%
([0-9]{2}[-][A-Z]{1,2}[-][0-9]{1,6})*                   {yytext[2] = 0;int y = atoi(yytext); if((y<13&&y>=0)||(y<100&&y>=87)){if(validCounty(yytext+3,0)){printf("%d\n",difference2(y));}else{printf("INVALID\n");}}else{printf("INVALID\n");}}
([0-9]{3}[-][A-Z]{1,2}[-][0-9]{1,6})*                   {yytext[2] = 0;int y = atoi(yytext); if(y>=13&&y<=18){if(validCounty(yytext+4,1)){printf("%d\n",difference3(y));}else{printf("INVALID\n");}}else{printf("INVALID\n");}}
[\ \t\n\r\f\v]*                                         {}
[A-z]*                                                  {}
[0-9]*                                                  {}
[-]()                                                   {}   
%%

int main()
{
    yylex();
    return 0;
}

