/*Svetlana Cvetic, cvetics, 16317724 */

%{
int evens = 0;
#include <math.h>
%}

NUMBER  [0-9]+
%%
{NUMBER}        {register int c;
                 c = NUMBER;
                 if(c%2 == 0) {printf(NUMBER);}}

%%

int main()
{
  yylex();
  printf("%d\n", evens);
  return 0;
}

