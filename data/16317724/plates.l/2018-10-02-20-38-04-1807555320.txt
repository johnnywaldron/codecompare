/*Svetlana Cvetic, cvetics, 16317724 */

%{
#include <string.h>
#include <stdio.h>
int current_year = 2018;
char countiesNew1[] = "CDGLTW";
char countiesNew2[] = "CECNCWDLKEKKKYLDLHLMLSMHMNMOOYRNSOWHWXWW";
char countiesOld1[] = "CDGLW";
char countiesOld2[] = "CECNCWDLKEKKKYLDLHLMLSMHMNMOOYRNSOWHWXWWLKTNTSWD";
%}

%%
[8][7-9][-][A-Z][-][0-9]{1,6}		{char letter = yytext[3]; int i; int t = 0;  for(i=0; i < 6; i++) {if(letter==countiesOld1[i]) {t = 1;}} if(t==1){ char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (1900 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}
[8][7-9][-][A-Z][A-Z][-][0-9]{1,6}	{char letter1 = yytext[3]; char letter2 = yytext[4]; int t = 0; int i; for(i = 0; i < 39; i += 2) {if(letter1==countiesOld2[i]&&letter2==countiesOld2[i+1]) {t = 1;}} if(t==1) {char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (1900 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}

[9|0][0-9][-][A-Z][-][0-9]{1,6}		{char letter = yytext[3]; int t = 0; int i; for(i = 0; i < 6; i++) {if(letter==countiesOld1[i]) {t = 1;}} if(t==1){ char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int offset = 2000; if(num1==9){offset = 1900;} int year = current_year - (offset + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}
[9|0][0-9][-][A-Z][A-Z][-][0-9]{1,6}	{char letter1 = yytext[3]; char letter2 = yytext[4]; int t = 0; int i; for(i = 0; i < 39; i += 2) {if(letter1==countiesOld2[i]&&letter2==countiesOld2[i+1]) {t = 1;}} if(t==1) {char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int offset = 2000; if(num1==9){offset = 1900;} int year = current_year - (offset + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}

[1][0|1|2][-][A-Z][-][0-9]{1,6}		{char letter = yytext[3]; int t = 0; int i; for(i = 0; i < 6; i++) {if(letter==countiesOld1[i]) {t = 1;}} if(t==1){ char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}
[1][0|1|2][-][A-Z][A-Z][-][0-9]{1,6}	{char letter1 = yytext[3]; char letter2 = yytext[4]; int t = 0; int i; for(i = 0; i < 39; i += 2) {if(letter1==countiesOld2[i]&&letter2==countiesOld2[i+1]) {t = 1;}} if(t==1) {char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}

[1][3][1][-][A-Z][-][0-9]{1,6}		{char letter = yytext[4]; int t = 0; int i; for(i = 0; i < 6; i++) {if(letter==countiesOld1[i]) {t = 1;}} if(t==1){ char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}
[1][3][1][-][A-Z][A-Z][-][0-9]{1,6}	{char letter1 = yytext[4]; char letter2 = yytext[5]; int t = 0; int i; for(i = 0; i < 39; i += 2) {if(letter1==countiesOld2[i]&&letter2==countiesOld2[i+1]) {t = 1;}} if(t==1) {char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}

[1][4-8][1][-][A-Z][-][0-9]{1,6}		{char letter = yytext[4]; int t = 0;  int i; for(i = 0; i < 6; i++) {if(letter==countiesNew1[i]) {t = 1;}} if(t==1){ char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}
[1][4-8][1][-][A-Z][A-Z][-][0-9]{1,6}	{char letter1 = yytext[4]; char letter2 = yytext[5]; int t = 0; int i; for(i = 0; i < 39; i += 2) {if(letter1==countiesNew2[i]&&letter2==countiesNew2[i+1]) {t = 1;}} if(t==1) {char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}

[1][3][2][-][A-Z][-][0-9]{1,6}		{char letter = yytext[4]; int t = 0; 
 int i; for(i = 0; i < 6; i++) {if(letter==countiesOld1[i]) {t = 1;}} if(t==1){ char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}
[1][3][2][-][A-Z][A-Z][-][0-9]{1,6}	{char letter1 = yytext[4]; char letter2 = yytext[5]; int t = 0; int i; for(i = 0; i < 39; i += 2) {if(letter1==countiesOld2[i]&&letter2==countiesOld2[i+1]) {t = 1;}} if(t==1) {char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}

[1][4-8][2][-][A-Z][-][0-9]{1,6}		{char letter = yytext[4]; int t = 0; int i; for(i = 0; i < 6; i++) {if(letter==countiesNew1[i]) {t = 1;}} if(t==1){ char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}
[1][4-8][2][-][A-Z][A-Z}][-][0-9]{1,6}	{char letter1 = yytext[4]; char letter2 = yytext[5]; int t = 0; int i; for(i = 0; i < 39; i += 2) {if(letter1==countiesNew2[i]&&letter2==countiesNew2[i+1]) {t = 1;}} if(t==1) {char txt1 = yytext[0]; char txt2 = yytext[1]; int num1 = txt1 - '0'; int num2 = txt2 - '0'; int year = current_year - (2000 + (num1*10+num2)); printf("%d\n", year);} else {printf("INVALID\n");}}

.+	{printf("INVALID\n");}
\n	/*empty*/
%%

int main()
{
  yylex();
  return 0;
}
