%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void yyerror(char *s);
int yylex();
int yyparse();

void convertToRoman (int val, char *res) {
    char *huns[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    int   size[] = { 0,   1,    2,     3,    2,   1,    2,     3,      4,    2};

    //  Add 'M' until we drop below 1000.

    while (val >= 1000) {
        *res++ = 'M';
        val -= 1000;
    }

    // Add each of the correct elements, adjusting as we go.
    strcpy (res, "");
    strcpy (res, huns[val/100]); res += size[val/100]; val = val % 100;
    strcpy (res, tens[val/10]);  res += size[val/10];  val = val % 10;
    strcpy (res, ones[val]);     res += size[val];

    *res = '\0';
}
%}
%output "romcalc.tab.c"

%token I V X L C D M
%left ADD SUB
%left MUL DIV
%token OP CL
%token EOL LETTER
%token ERR

%%

program:
	| program expression EOL {char res[1000]; if($2==0){printf("Z\n\0");} else {if($2<0) {int i = $2*-1; convertToRoman(i, res);printf("-%s\n", res);} else{convertToRoman($2, res); printf("%s\n", res);}}}
	| ERR {printf("syntax error\n"); return 0;}
;


expression : Thousando
	  | expression ADD expression	{ $$ = $1 + $3; }
	  | expression SUB expression	{ $$ = $1 - $3; }
	  | expression MUL expression	{ $$ = $1 * $3; }
	  | expression DIV expression	{ $$ = $1 / $3; }
	  | OP expression CL			{ $$ = $2; }
;

Thousando : FiveHundo
	| M FiveHundo		{$$=1000+$2;}
	| M M FiveHundo		{$$=2000+$3;}
	| M M M FiveHundo	{$$=3000+$4;}
;

FiveHundo : Hundo
	| D Hundo 	{$$=500+$2;}
	| C D Fifties	{$$=400+$3;}
	| C M Fifties 	{$$=900+$3;}
;

Hundo: Fifties
	  | C Fifties     {$$=100+$2;}
	  | C C Fifties   {$$=200+$3;}
	  | C C C Fifties {$$=300+$4;}
;

Fifties: Tens
	| X C	Fives	{$$=90+$3;}
	| L	Tens	{$$=50+$2;}
	| X L Fives	{$$=40+$3;}
;

Tens: Fives
	| X X Fives	{$$=20+$3;}
	| X X X Fives	{$$=30+$4;}
	| X Fives	{$$=10+$2;}
;

Fives: Ones
	| I X		{$$=9;}
	| I V		{$$=4;}
	| V Ones	{$$=5+$2;}
;

Ones: {$$=0;}
	| I 		{$$=1;}
	| I I		{$$=2;}
	| I I I		{$$=3;}
;

%%
int main()
{
	yyparse();
	return 0;
}

void yyerror(char *s)
{
	printf("syntax error\n");
  exit(0);
}
