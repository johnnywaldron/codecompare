%{
  #include "romcalc.tab.h"
%}

%%
"I" {return I;}
"V" {return V;}
"X" {return X;}
"L" {return L;}
"C" {return C;}
"D" {return D;}
"M" {return M;}
"*" {return MUL;}
"+" {return ADD;}
"-" {return SUB;}
"/" {return DIV;}
"{" {return OP;}
"}" {return CL;}
\n  {return EOL;}
.   {return ERR;}
%%

