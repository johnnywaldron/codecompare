/*Svetlana Cvetic, cvetics, 16317724 */

%{
#include <math.h>
#include <stdio.h>
%}

%%
[{][^}]+[^}]    {printf("syntax error\n"); return 0;}
[{][^}]+[}]     /*empty*/
[**][^/n]+*[/n]	/*empty*/
%%

int main()
{
  yylex();
        return 0;
}

