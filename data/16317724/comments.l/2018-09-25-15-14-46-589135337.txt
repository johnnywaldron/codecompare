/*Svetlana Cvetic, cvetics, 16317724 */

%{
#include <math.h>
#include <stdio.h>
%}

%%
["][^"]+["]      {printf(yytext);}
[{][^}]+[}]     /*empty*/
[{][^}]+[^}]    {printf("syntax error\n"); return 0;}
\[**][^\n]+      /*empty*/
%%

int main()
{
  yylex();
  return 0;
}

