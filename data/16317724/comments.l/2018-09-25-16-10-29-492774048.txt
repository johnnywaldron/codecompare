/*Svetlana Cvetic, cvetics, 16317724 */

%{
#include <math.h>
#include <stdio.h>
%}

%%
["][^"]+["]     {printf(yytext);}
[{][^}]+[}]     /*empty*/
[{][^}]+[^}]    {printf("syntax error\n"); return 0;}
[\*]{2}[^\n]+   /*empty*/
[}]             {printf("syntax error\n"); return 0;}
%%

int main()
{
  yylex();
  return 0;
}

