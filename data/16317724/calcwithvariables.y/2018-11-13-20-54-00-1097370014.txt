%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void yyerror(char *s);
int yylex();
int yyparse();
int val[] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

void assignLetter(int num, char let){
int index = (int)let - 97;
val[index] = num;
}

int returnNum(char let){
int index = (int)let - 97;
return val[index];
}
%}

%output "calcwithvariables.tab.c"

%union{char let; int num;};

%left ADD SUB
%left MUL DIV
%token PRINT ASSIGN NUMBER EOL LETTER EMPTY
%token ERR

%%

program:
	| program expr EOL {$<num>$ = $<num>2;}
;

expr: NUMBER 		{$<num>$ = $<num>1;}
	| LETTER ASSIGN expr{int num = $<num>3; char let = $<let>1; assignLetter(num,let); $<num>$ = num;}
	| LETTER		{char let = $<let>1; int num = returnNum(let); $<num>$ = num;}
	|expr ADD expr	{$<num>$ = $<num>1 + $<num>3;}
	|expr SUB expr	{$<num>$ = $<num>1 - $<num>3;}
	|expr MUL expr	{$<num>$ = $<num>1 * $<num>3;}
	|expr DIV expr	{$<num>$ = $<num>1 / $<num>3;}
	|PRINT expr	{int num = $<num>2; printf("%d\n", num);}
;

%%
int main()
{
	yyparse();
	return 0;
}

void yyerror(char *s)
{
	fprintf(stderr, "syntax error\n", s);
	exit(0);
}

