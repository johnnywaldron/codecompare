%{
#include "romcalc.tab.h"
%}

%%
"I"  { return I; }
"V"  { return V; }
"X"  { return X; }
"L"  { return L; }
"C"  { return C; }
"D"  { return D; }
"M"  { return M; }
"+"  { return ADD; }
"-"  { return SUB; }
"*"  { return MUL; }
"/"  { return DIV; }
"\n" { return EOL; }

. { return ERROR; }

%%
