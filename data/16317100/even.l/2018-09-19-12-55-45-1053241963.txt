%{
int evenNums = 0;
int num = atoi(yytext);
%}

%%

[0-9]+	{ if(num % 2 == 0) {evenNums++} }

%%

int main()
{
  yylex();
  printf("%8d\n", evenNums);
  return 0;
}

