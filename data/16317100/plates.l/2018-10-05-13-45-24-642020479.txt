%top{
             #include <stdint.h>
             #include <inttypes.h>
}

%{
  number  [0-9]{1,6}
  VALID_YEAR  [8][7-9]|[9][0-9]|[0][0-9]|[1][0-9]
  SIMPLE_COUNTY C|CE|CN|CW|D|DL|G|KE|KK|L|LD|LH|LM|LS|MH|MO|OY|RN|SO|W|WH|WX|WW
  OLD_COUNTY  LK|TN|TS|WD
  NEW_COUNTY  T
%}

%%

[0-9]{1,6}[-][C|CE|CN|CW|D|DL|G|KE|KK|L|LD|LH|LM|LS|MH|MO|OY|RN|SO|W|WH|WX|WW]
              [-][8][7-9]|[9][0-9]|[0][0-9]|[1][0-9] {printf("Valid\n"); return 0;}

%%
int main()
{
  yylex();
  return 0;
}

