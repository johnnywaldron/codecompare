%{
#include <stdio.h>
#include <stdlib.h>

void yyerror(char *s);
extern int yylex();
extern int yyparse();

%}

%union {
  int num;
}

%token I
%token V
%token X
%token L
%token C
%token D
%token M
%token EOL
%token error
%type <num> Mterm Dterm Cterm Lterm Xterm Vterm Iterm

%%

printNum:
    | printNum Mterm EOL {printf("%d\n", $2);}
    | error     {printf("The bitch failed LOOOOLL");}
    ;
Mterm: Dterm
    | M Dterm     { $$ = 1000 + $2;}
    | M M Dterm   { $$ = 2000 + $3; }
    | M M M Dterm { $$ = 3000 + $4; }
    ;

Dterm: Cterm
    | C D Cterm       { $$ = 400 + $3; }
    | C M Cterm       { $$ = 900 + $3; }
    | D Cterm         { $$ = 500 + $2; }
    ;

Cterm: Lterm
    | C Lterm       { $$ = 100 + $2; }
    | C C Lterm     { $$ = 200 + $3; }
    | C C C Lterm   { $$ = 300 + $4; }
    ;

Lterm: Xterm
    | X C Xterm     { $$ = 90 + $3; }
    | X L Xterm     { $$ = 40 + $3; }
    | L Xterm       { $$ = 50 + $2;}
    ;

Xterm: Vterm
    | X Vterm     { $$ = 10 + $2; }
    | X X Vterm   { $$ = 20 + $3; }
    | X X X Vterm { $$ = 30 + $4; }
    ;

Vterm: Iterm
    | I X       { $$ = 9; }
    | I V       { $$ = 4; }
    | V Iterm   { $$ = 5 + $2; }
    ;

Iterm:          { $$ = 0; }
    | I         { $$ = 1; }
    | I I       { $$ = 2; }
    | I I I     { $$ = 3; }
    ;

%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("Error: %s\n", s);
  exit(0);
}
