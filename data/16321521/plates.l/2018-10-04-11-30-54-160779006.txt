%{

%}

COUNT87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
COUNT14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW

YR87-99 [8|9][0-9]
YR00-12 [0][0-9]|[1][0-2]
YR13 [1][3][1-2]
YR14-18 [1][4-8][1-2]

NUMBER [1-9][0-9]{0,5}

%%

{YR87-99}\-{COUNT87}\-{NUMBER} {
  char c[2] = {yytext[0], yytext[1]};
  int i = atoi(c);
  i = (100-i) + 18;
  printf("%d", i);
}

{YR00-12}\-{COUNT87}\-{NUMBER} {
  char c[2] = {yytext[0], yytext[1]};
  int i = atoi(c);
  i = 18-i;
  printf("%d", i);
}

{YR13}\-{COUNT87}\-{NUMBER} {
  char c[2] = {yytext[0], yytext[1]};
  int i = atoi(c);
  printf("%d", 18-i);
}

{YR14-18}\-{COUNT14}\-{NUMBER} {
  char c[2] = {yytext[0], yytext[1]};
  int i = atoi(c);
  printf("%d", 18-i);
}


[\t\r" "] {}
[^\t^\n^\r^" "] {printf("INVALID\n");}


%%

int main(){
  yylex();
  return 0;
}

