%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
%}
%output "roman.tab.c"

%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND EOL
%%

calclist: /* nothing */ {}
| calclist thousands fivehundreds hundreds fiftys tens fives ones EOL { printf("%d\n", $2 + $3 + $4 + $5 + $6 + $7 + $8);  }
;

ones: {$$ = 0;}
  | ONE {$$ = 1;}
  | ONE ONE {$$ = 2;}
  | ONE ONE ONE {$$ = 3;}
  | ONE FIVE {$$ = 4;}
  | ONE TEN {$$ = 9;}
  ;

fives: {$$ = 0;}
  | FIVE {$$ = 5;}
  //| ONE FIVE {$$ = 4;}
  ;

tens: {$$ = 0;}
  | TEN {$$ = 10;}
  | TEN TEN {$$ = 20;}
  | TEN TEN TEN {$$ = 30;}
  | TEN FIFTY {$$ = 40;}
  | TEN HUNDRED {$$ = 90;}
  //| ONE TEN {$$ = 9;}
  //| TEN ONE TEN {$$ = 19;}
  //| TEN TEN ONE TEN {$$ = 29;}
  ;

fiftys: {$$ = 0;}
  | FIFTY {$$ = 50;}
  //| TEN FIFTY {$$ = 40;}
  ;

hundreds: {$$ = 0;}
  | HUNDRED {$$ = 100;}
  | HUNDRED HUNDRED {$$ = 200;}
  | HUNDRED HUNDRED HUNDRED {$$ = 300;}
  | HUNDRED FIVEHUNDRED {$$ = 400;}
  | HUNDRED THOUSAND {$$ = 900;}
  //| TEN HUNDRED {$$ = 90;}
  //| HUNDRED TEN HUNDRED {$$ = 190;}
  //| HUNDRED HUNDRED TEN HUNDRED {$$ = 290;}
  ;

fivehundreds: {$$ = 0;}
  | FIVEHUNDRED {$$ = 500;}
  //| HUNDRED FIVEHUNDRED {$$ = 400;}
  ;

thousands: {$$ = 0;}
  | THOUSAND {$$ = 1000;}
  | THOUSAND THOUSAND {$$ = 2000;}
  | THOUSAND THOUSAND THOUSAND {$$ = 3000;}
  //| HUNDRED THOUSAND {$$ = 900;}
  //| THOUSAND HUNDRED THOUSAND {$$ = 1900;}
  //| THOUSAND THOUSAND HUNDRED THOUSAND {$$ = 2900;}
  ;

%%
void yyerror(char *s)
{
  printf("%s\n", s);
  exit(0);
}


int
main()
{
//  yydebug = 1;
  yyparse();
  return 0;
}
