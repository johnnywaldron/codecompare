%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
%}
%output "romcalc.tab.c"

%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND PLUS MINUS MUL DIV LEFT RIGHT EOL
%left PLUS MINUS
%left MUL DIV
%%

calclist: /* nothing */ {}
| calclist exp EOL {toRoman($2);}
;

num: thousands fivehundreds hundreds fiftys tens fives ones {$$ = $1 + $2 + $3 + $4 + $5 + $6 + $7;}
;

exp: num {$$ = $1;}
  | exp PLUS exp {$$ = $1 + $3;}
  | exp MINUS exp {$$ = $1 - $3;}
  | exp MUL exp {$$ = $1 * $3;}
  | exp DIV exp {$$ = $1 / $3;}
  | LEFT exp RIGHT {$$ = $2;}
  ;


ones: {$$ = 0;}
  | ONE {$$ = 1;}
  | ONE ONE {$$ = 2;}
  | ONE ONE ONE {$$ = 3;}
  | ONE FIVE {$$ = 4;}
  | ONE TEN {$$ = 9;}
  ;

fives: {$$ = 0;}
  | FIVE {$$ = 5;}
  ;

tens: {$$ = 0;}
  | TEN {$$ = 10;}
  | TEN TEN {$$ = 20;}
  | TEN TEN TEN {$$ = 30;}
  | TEN FIFTY {$$ = 40;}
  | TEN HUNDRED {$$ = 90;}
  ;

fiftys: {$$ = 0;}
  | FIFTY {$$ = 50;}
  ;

hundreds: {$$ = 0;}
  | HUNDRED {$$ = 100;}
  | HUNDRED HUNDRED {$$ = 200;}
  | HUNDRED HUNDRED HUNDRED {$$ = 300;}
  | HUNDRED FIVEHUNDRED {$$ = 400;}
  | HUNDRED THOUSAND {$$ = 900;}
  ;

fivehundreds: {$$ = 0;}
  | FIVEHUNDRED {$$ = 500;}
  ;

thousands: {$$ = 0;}
  | THOUSAND thousands {$$ = 1000 + $2;}
  ;

%%
void yyerror(char *s)
{
  printf("%s\n", s);
  exit(0);
}

int toRoman(int dec){
  int n = dec;
  if(dec>0){
    while(n>0){
      if(n>=1000){
        printf("%c", 'M');
        n -= 1000;
      }
      else if(n>=500){
        printf("%c", 'D');
        n -= 500;
      }
      else if(n>=100){
        printf("%c", 'C');
        n -= 100;
      }
      else if(n>=50){
        printf("%c", 'L');
        n -= 50;
      }
      else if(n>=10){
        printf("%c", 'X');
        n-= 10;
      }
      else if(n>=5){
        printf("%c", 'V');
        n -= 5;
      }
      else{
        printf("%c", 'I');
        n -= 1;
      }
    }
  }
  else if(dec<0){
    printf("-");
    while(n<0){
      if(n>=1000){
        printf("%c", 'M');
        n += 1000;
      }
      else if(n>=500){
        printf("%c", 'D');
        n += 500;
      }
      else if(n>=100){
        printf("%c", 'C');
        n += 100;
      }
      else if(n>=50){
        printf("%c", 'L');
        n += 50;
      }
      else if(n>=10){
        printf("%c", 'X');
        n += 10;
      }
      else if(n>=5){
        printf("%c", 'V');
        n += 5;
      }
      else{
        printf("%c", 'I');
        n += 1;
      }
    }
  }
  else{
    printf("Z");
  }
  printf("\n");

  return 0;
}


int
main()
{
//  yydebug = 1;
  yyparse();
  return 0;
}
