%{
#  include <stdio.h>
#  include <stdlib.h>

void yyerror(char *s);
int yylex();
int yyparse();
void putValu(char c, int n);
int getValu(char c);
int checkDefined(char c);
%}
%output "calcwithvariables.tab.c"

%token VARIABLE NUMBER PRINT ASSIGN ENDSTAT PLUS MINUS MUL DIV EOL
%left PLUS MINUS
%left MUL DIV
%%

calclist: /* nothing */ {}
| calclist expr EOL {/*printf("%d\n", $2);*/}
| calclist printVar ENDSTAT EOL {}
;

expr: VARIABLE ASSIGN exp ENDSTAT {putValu($1, $3);}
;

num: NUMBER
| MINUS NUMBER {$$ = $2 * -1;}

exp: num {$$ = $1;}
  | VARIABLE {int i = getValu($1); $$ = i;}
  | exp PLUS exp {$$ = $1 + $3;}
  | exp MINUS exp {$$ = $1 - $3;}
  | exp MUL exp {$$ = $1 * $3;}
  | exp DIV exp {$$ = $1 / $3;}
  ;

printVar: PRINT VARIABLE { if(checkDefined($2) == 1){
                              printf("%d\n", getValu($2));
                            }
                            else yyerror("syntax error");
                         }
;

%%
void yyerror(char *s)
{
  printf("%s\n", s);
  exit(0);
}

int vals[26];
int isDefined[26];

void putValu(char c, int n){
  int i = c - 97;
  vals[i] = n;
  isDefined[i] = 1;
}

int getValu(char c){
  int i = c - 97;
  return vals[i];
}

int checkDefined(char c){
  return isDefined[c-97];
}

int
main()
{
//  yydebug = 1;
  for(int i=0; i<sizeof(isDefined); i++){
    isDefined[i] = 0;
  }

  yyparse();
  return 0;
}
