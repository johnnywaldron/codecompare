%{
    int isValidYear(int year);
    int isValidCombination(int year, char *county);

    const int currentYear = 2018;
    const char *delimeter = "-";
    int yearVal;
    int numberVal;
    char *countyVal;

%}

YEAR                        [0-9]{2,3}
COUNTY                      C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LK|LM|LS|MH|MN|MO|OY|RN|SO|T|TN|TS|W|WD|WH|WX|WW
NUMBER                      [0-9]{1,6}

%%

{YEAR}-{COUNTY}-{NUMBER}[ \t]*  {
                                    yearVal = atoi(strtok(yytext, delimeter)); 
                                    if(strlen(yytext) == 2)
                                    {
                                        yytext += 3;
                                    }
                                    else
                                    {
                                        yytext += 4;
                                    }
                                    countyVal = strtok(yytext, delimeter);
                                    if(strlen(yytext) == 1)
                                    {
                                        yytext += 2;
                                    }
                                    else
                                    {
                                        yytext += 3;
                                    }
                                    numberVal = atoi(yytext);
                                    if(!isValidYear(yearVal) || !isValidCombination(yearVal, countyVal))
                                    {
                                        printf("INVALID\n");
                                    }
                                    else
                                    {
                                        if(yearVal > 100)
                                        {
                                            yearVal /= 10;
                                        }
                                        if(yearVal >= 87 && yearVal <= 99)
                                        {
                                            yearVal += 1900;
                                        }
                                        else if(yearVal >= 00 && yearVal <= 18)
                                        {
                                            yearVal += 2000;
                                        }
                                        printf("%d", currentYear - yearVal);
                                    }
                                }
.*                              {printf("INVALID\n");}

%%

int main()
{
    yylex();
    return 0;
}

int isValidYear(int year)
{
    if(year >= 87 && year <= 99 || year >= 0 && year <= 12)
    {
        return 1;
    }
    else if(year == 131 || year == 132 || year == 141 || year == 142
            || year == 151 || year == 152 || year == 161 || year == 162
            || year == 171 || year == 172 || year == 181 || year == 182)
    {
        return 1;
    }
    else
    {
        return 0;    
    }
}

int isValidCombination(int year, char *county)
{
    if((year >= 87 && year <= 99 || year >= 00 && year <= 12) 
        && !strcmp(county, "T"))
    {
        return 0;
    }
    else if(year > 100 && (!strcmp(county, "LK") || !strcmp(county, "TN") 
            || !strcmp(county, "TS") || !strcmp(county, "WD")))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
