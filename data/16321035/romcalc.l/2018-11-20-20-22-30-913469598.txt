%{
	#include "romcalc.tab.h"
	int yyparse();
%}

%%
"{" {return OP;}
"}" {return CP;}
"*"	{return MUL;}
"/"	{return DIV;}
"+" {return ADD;}
"-" {return SUB;}
I	{return I;}
V	{return V;}
X	{return X;}
L	{return L;}
C	{return C;}
D	{return D;}
M	{return M;}
\n	{return EOL;}
.	{return ERROR;}
exit(0);
%%
