%{
    #include <stdio.h>
    int yylex(void);
    int sym[26];

    void yyerror(char *);
%}

%token NUM VAR
%token ASSIGN PRINT
%token EOL ERROR
%left '*' '/'
%left '-' '+'

%%
calclist: /* nothing */ {}
|calclist exp EOL       {}
|ERROR EOL              {printf("syntax error\n");}
;

exp: factor         {}
|PRINT factor       {printf("%d\n", $2);}
|VAR ASSIGN factor  {sym[$1] = $3; }
;

factor: NUM
|VAR                {$$ = sym[$1]; }
|'-' factor         {$$ = -$2; }
|factor '+' factor  {$$ = $1 + $3; }
|factor '-' factor  {$$ = $1 - $3; }
|factor '*' factor  {$$ = $1 * $3; }
|factor '/' factor  {$$ = $1 / $3; }
|'{' factor '}'     {$$ = $2; }
;

%%
void yyerror(char *s) 
{
    fprintf(stderr, "%s\n", s);
}

int main(void) 
{
    yyparse();
    return 0;
}