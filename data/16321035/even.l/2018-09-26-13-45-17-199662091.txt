%{

int count = 0;

%}

%%

[0-9]*[0|2|4|6|8] {count++;}
[0-9]*[1|3|5|7|9] {}

%%
 
int main()
{
  yylex();
  printf("%d\n", count);
  return 0;
}
