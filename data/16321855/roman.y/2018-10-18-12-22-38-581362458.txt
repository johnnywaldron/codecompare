%{ 
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
%}
%output "roman.tab.c"

%token I IV V IX X XL L XC C CD D CM M EOL
%%

roman: /*nothing*/ {} | expr EOL { printf("Input conforms to grammar\n"); };

expr : M expr100 expr10 expr1 | M expr100 expr10 | M expr100 expr1 | M expr100 | M expr10 expr1 | M expr10 | M expr1 | M | expr100 expr10 expr1 | expr100 expr10 | expr100 expr1 | expr100 | expr10 expr1 | expr10 | expr1
 ;

expr1: one | IV | V | V one | IX ;
 
one: I | I two ;
 
two: I | I three ;

three: I ;
 
expr10: ten | XL | L | L ten | XC ;
 
ten: X | X twenty ;
 
twenty: X | X thirty ;

thirty: X ;
 
expr100: hundred | CD | D | D hundred | CM ;
 
hundred: C | C twohund ;
 
twohund: C | C threehund ;

threehund: C ;
 
%%
void yyerror(char *s)
{
  printf("error: %s\n", s);
}


int
main()
{
//  yydebug = 1;
  yyparse();
  return 0;
}

