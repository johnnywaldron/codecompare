%{ 
#  include <stdio.h>
#  include <stdlib.h>
#  include <string.h>
void yyerror(char *s);
int intToRoman(int num);
int yylex();
int yyparse();
int cond = 0;
int number;
int result;
%}
%output "romcalc.tab.c"

%token I IV V IX X XL L XC C CD D CM M
%token EOL OB CB ADD SUB MUL DIV
%%

roman: /* nothing */ {}
| roman exprb EOL { intToRoman(result); }
;

exprb: expr			{ result = $1; }
	| OB expr CB	{ result = $2; }
;

expr: exp						{ $$ = $1; }
	| OB expr CB ADD OB expr CB	{ $$ = $2 + $6; }
	| OB expr CB SUB OB expr CB	{ $$ = $2 - $6; }
	| OB expr CB MUL OB expr CB	{ $$ = $2 * $6; }
	| OB expr CB DIV OB expr CB	{ $$ = $2 / $6; }
;

exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term	   { $$ = number; number = 0; }
 | factor MUL term { $$ = $1 * number; number = 0; }
 | factor DIV term { $$ = $1 / number; number = 0; }
;

term: single | double | triple | quadruple
;

single: one
	| IV	{ number += 4; }
	| V		{ number += 5; }
	| V one	{ number += 5; }
	| IX	{ number += 9; }
;
 
one: I { number += 1; } | I two { number += 1; }
;
 
two: I { number += 1; } | I three { number += 1; }
;

three: I { number += 1; }
;

double: term10 | term10 single
;

term10: ten
	| XL	{ number += 40; }
	| L		{ number += 50; }
	| L ten	{ number += 50; }
	| XC	{ number += 90; }
;
 
ten: X { number += 10; } | X twenty { number += 10; }
;
 
twenty: X { number += 10; } | X thirty { number += 10; }
;

thirty: X { number += 10; }
;

triple: term100 | term100 double| term100 single
;
 
term100: hundred
	| CD		{ number += 400; }
	| D			{ number += 500; }
	| D hundred	{ number += 500; }
	| CM		{ number += 900; }
;
 
hundred: C { number += 100; } | C twohund { number += 100; }
;
 
twohund: C { number += 100; } | C threehund { number += 100; }
;

threehund: C { number += 100; }
;

quadruple: term1000 | term1000 triple | term1000 double | term1000 single
;

term1000: M term1000 { number += 1000; } | M { number += 1000; }
;
 
%%
void yyerror(char *s)
{
	if(cond == 0)
	{
  		printf("syntax error\n");
  		cond = 1;
  	}
}

// Function to calculate roman equivalent 
int intToRoman(int num)  
{    
	 if (num == 0)
	 {
     	printf("Z\n");
     	return 1;
     }
     if (num < 0)
     {
     	printf("-");
     	num = num * - 1;
     }
     // storing roman values of digits from 0-9  
     // when placed at different places
     char* c[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}; 
     char* x[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}; 
     char* i[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}; 
     
     // Converting to roman
     while (num >= 1000)
     {
     	printf("M");
     	num -= 1000;
     }
     
     printf("%s", c[(num%1000)/100]); 
     printf("%s", x[(num%100)/10]);
     printf("%s", i[num%10]);
     printf("\n");
     return 1;
} 

int
main()
{
//	yydebug = 1;
  	yyparse();
  	return 0;
}

