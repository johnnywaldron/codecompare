%{
int isComment = 0;
//int isQuote = 0;
%}

%%
"**".*			{ /*do nothing*/ }
[{]				{ isComment = 1; }
[}]				{ isComment = 0; }
[.\n]			{ if (isComment = 0) {printf("%s", yytext);} }



%%

int main()
{
  yylex();
  return 0;
}
