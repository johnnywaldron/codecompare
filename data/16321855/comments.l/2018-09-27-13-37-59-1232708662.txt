%{
int isComment = 0;
int isQuote = 0;
%}

%%
"**".*			{ if (isQuote == 0){} else{ printf("%s", yytext); } }
["]				{ printf("%s", yytext);	if (isQuote == 0) { isQuote == 1; }
										else { isQuote = 0; } }
"{"				{ if (isQuote == 0) { isComment = 1; } else {printf("%s", yytext);}}
"}"				{ if (isQuote == 0) {	if (isComment == 1) {isComment = 0;}
										else { printf("syntax error\n"); } }
					else {printf("%s", yytext);} }
\n				{ if (isComment == 0) {printf("%s", yytext);} }
.				{ if (isComment == 0) {printf("%s", yytext);} }


%%

int main()
{
  yylex();
  return 0;
}
