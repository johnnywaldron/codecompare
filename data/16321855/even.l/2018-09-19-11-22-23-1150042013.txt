%{
int evens = 0;
int i;
%}

%%

[0-9]+	{ i = atoi(yytext); if(i % 2 == 0) evens++;  }

%%

int main()
{
  yylex();
  printf("%8d\n", evens);
  return 0;
}
