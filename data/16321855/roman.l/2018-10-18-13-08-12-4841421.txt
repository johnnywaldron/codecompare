%{
# include "roman.tab.h"

void yyerror(char *s);
int yylex();
int number = 0;


%}

%%
"I" { number += 1; return I; }
"IV" { number += 4; return IV; }
"V" { number += 5; return V; }
"IX"  { number += 9; return IX; }
"X" { number += 10; return X; }
"XL" { number += 40; return XL; }
"L" { number += 50; return L; }
"XC" { number += 90; return XC; }
"C" { number += 100; return C; }
"CD" { number += 400; return CD; }
"D" { number += 500; return D; }
"CM" { number += 900; return CM; }
"M" { number += 1000; return M; }
\n	{ return EOL; }
.	{ yyerror("Mystery character\n"); }
%%

