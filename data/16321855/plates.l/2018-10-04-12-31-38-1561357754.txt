%{
int isYear = 1;
int valid = 0;
int number;
int year;
int time;
%}

YEAR87 [90][0-9]|[8][7-9]|[1][0-2]
YEAR13 [1][3-8][1|2]
COUNTY13 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTY87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LK|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NUMBER [0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]?

%%

[({YEAR87|YEAR13})[-]({COUNTY87}|{COUNTY13})[-]{NUMBER}] { printf("wahey %s", yytext); valid = 1;}

[' '\r\t] {}


%%

int main()
{
  yylex();
  printf("%d", valid);
  return 0;
}
