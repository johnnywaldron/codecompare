%{
int isYear = 1;
int valid = 0;
int number;
int year;
int time;
%}

%%

([90][0-9]|[8][7-9]|[1][0-2]|[1][3-8][1|2])([-])([A-Z][A-Z]?)([-])([0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]?) {int year = atoi(strtok(yytext, "-")); printf("%d", year); valid = 1; }

[' '\r\t] {}

%%

int main()
{
  yylex();
  printf("%d", valid);
  return 0;
}
