%{
int validYear = 0;
int validCounty = 0;
int valid = 0;
int year;
%}

%%

[\n\r\t] {}
[0-9]+ { valid = 1; printf("%s\n", yytext); }

%%

int main()
{
  yylex();
  if (valid == 0) { printf("INVALID\n"); }
  return 0;
}
