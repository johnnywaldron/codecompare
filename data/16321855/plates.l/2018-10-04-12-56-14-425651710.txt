%{
int isYear = 1;
int valid = 0;
int number;
int year;
int time;
%}

YEAR87 [90][0-9]|[8][7-9]|[1][0-2]
YEAR13 [1][3-8][1|2]
COUNTY13 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTY87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LK|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NUMBER [0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]?([\n]|[' '])

%%

({YEAR87})([-])({COUNTY87})([-])({NUMBER}) { int year = atoi(strtok(yytext, "-")); time = 18 - year; if (time < 0) { time += 100; } printf("%d\n", time); }
({YEAR13})([-])({COUNTY13})([-])({NUMBER}) { int year = atoi(strtok(yytext, "-")); if (year >= 100) { year = year/10; } time = 18 - year; if (time < 0) { time += 100; } printf("%d\n", time);  }

[' '\r\t\n] {}
. {printf("INVALID\n");}


%%

int main()
{
  yylex();
  return 0;
}
