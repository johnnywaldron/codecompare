%{
int validYear = 0;
int validCounty = 0;
int isYear = 1;
int valid = 0;
int year;
%}

%%

[\n] {isYear = 1; }
[' '\r\t] {}
[0-9]+ { valid = 1; if (isYear == 1) {year = atoi(yytext); if (year >= 100) { year = year / 10; } 				printf("%d\n", year); } }
[-] { isYear = 0;}
[A-Z]+ {}

%%

int main()
{
  yylex();
  if (valid == 0) { printf("INVALID\n"); }
  return 0;
}
