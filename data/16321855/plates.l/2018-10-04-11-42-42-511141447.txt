%{
int isYear = 1;
int valid = 0;
int number;
int year;
int time;
%}

YEAR [90][0-9]|[8][7-9]|[1][0-2]|[1][3-8][1|2]
COUNTY13 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTY87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LK|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW

%%

([90][0-9]|[8][7-9]|[1][0-2]|[1][3-8][1|2])([-])(COUNTY13|COUNTY87)([-])([0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]?) {int year = atoi(strtok(yytext, "-")); if (year >= 100) {year = year/10;} time = 18 - year; if (time < 0) { time += 100;} printf("%s\n", yytext);}

[' '\r\t\n] {}


%%

int main()
{
  yylex();
  return 0;
}
