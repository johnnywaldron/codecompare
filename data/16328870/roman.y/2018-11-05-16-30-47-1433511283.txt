%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yylex();
void yyerror(char *s);
char message[1000];
%}
/* declare tokens */
%token EOL
%token NUMBER

%%
calculate:
| calculate exp EOL {strcat(message,$2 + "\n");}  
;

exp: NUMBER
| NUMBER exp {$$ = $1 +$2;}
;
%%

int main(int argc, char **argv)
{
  strcpy(message,"");
  yyparse();
  printf("%s",message);
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr,"%s\n",s);
}
