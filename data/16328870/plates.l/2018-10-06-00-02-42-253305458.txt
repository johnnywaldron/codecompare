%{
%}
YYY [1][3-8][12]
YY  [0189][0-9]
C14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
C87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|TN|TS|W|WD|WH|WX|WW
SS  [0-9]{1,6}
%%
{YY}[-]{C87}[-]{SS}[\n\t\s ]*  { if(yytext[3]=='T'){
                             printf("INVALID\n");
			   }
                           else{
                           char year[4];
                           if(yytext[0]=='0'||yytext[0]=='1'){
                            strcpy(year,"20");
                           }
                           else{
                            strcpy(year,"19");
                           }
                           strncat(year,yytext,2);
                           printf("%d\n",2018-atoi(year));
			   }}
{YYY}[-]{C14}[-]{SS}[\n\t\s ]* {char year[4]={"20"};
                            strncat(year,yytext,2);
                            printf("%d\n",2018-atoi(year));}
.*                          {printf("INVALID");}
%%

int main()
{
   yylex();
   return 0;
}
