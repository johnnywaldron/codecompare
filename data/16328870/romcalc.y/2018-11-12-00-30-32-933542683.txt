%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int yylex();
char* convert(int s);
void yyerror(char *s);
%}

/* declare tokens */
%token EOL
%token NUMBER
%token ADD SUB DIV MUL OPEN CLOSE

%%

calculate:
  | calculate exp EOL { printf("%s\n",convert($2)); }
	;

exp: factor
| exp ADD factor {$$ = $1 + $3;}
| exp SUB factor {$$ = $1 - $3;}
;

factor: roman
| roman MUL exp { $$ = $1 * $3; }
| roman DIV exp { $$ = $1 / $3; }
;

roman: NUMBER
| NUMBER roman { if($1+$2>1000 && $1<1000) yyerror("syntax error");
	               $$ = $1 +$2; }
| OPEN exp CLOSE   {$$ = $2;}
;

/*exp: factor
| exp ADD factor { $$ = $1 + $3; }
| exp SUB factor { $$ = $1 - $3; }
;

factor: term
| factor MUL term { $$ = $1 * $3; }
| factor DIV term { $$ = $1 / $3; }
;

term: roman
;*/
%%

int main(int argc, char **argv)
{
   yyparse();
   return 0;
}

char* convert(int s){
	 char *result= malloc (sizeof (char) * 1000);

	 int negative = 0;
	 if (s<0){
		 negative = 1;
		 s = 0-s;
		 strcpy(result, "-");
	 }
	 else{
		 strcpy(result, "");
	 }
   if(s==0){
		 return "Z";
	 }

	 while(s>=1000){
		 s = s-1000;
		 strcat(result,"M");
	 }

	 while(s>=500){
		 if(s==900){
			 strcat(result,"CM");
			 s=s-900;
		 }else{
			 strcat(result,"D");
			 s = s-500;
		 }

	 }

	 while(s>=100){
		 if(s==400){
			 strcat(result,"CD");
			 s=s-400;
		 }else{
			 strcat(result,"C");
			  s = s-100;
		 }

	 }

	 while(s>=50){
		 if(s==90){
			 strcat(result,"XC");
			 s=s-90;
		 }else{
			 strcat(result,"L");
			 s = s-50;
		 }
	 }

	 while(s>=10){
		 if(s==40){
			 s=s-40;
			 strcat(result,"XL");
		 }else{
			 strcat(result,"X");
			  s = s-10;
		 }

	 }

	 while(s>=5){
		 if(s==9){
			 s = s-9;
			 strcat(result,"IX");
		 }else{
			 strcat(result,"V");
			  s = s-5;
		 }

	 }

	 while(s!=0){
	  if(s==4){
			s=s-4;
			strcat(result,"IV");
		}else{
			strcat(result,"I");
				s = s-1;
		}
	 }
	 return result;
}

void yyerror(char *s)
{
  printf("%s\n",s);
  exit(0);
}
