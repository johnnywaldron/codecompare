%{
char *output = malloc(1024*sizeof(char));
int syntax_error = 0;
%}

%%

^["][\*\*].["]$		{if(!syntax_error) {
			strcat(output,yytext);}}
^["][\{][^]*[\}]["]$    {if(!syntax_error) {
			 strcat(output,yytext);}}
[\{][^\}]*$		{if(!syntax_error){
			 syntax_error = 1;
			 strcat(output,"syntax error\n");}}
^[^\{]*[\}]		{if(!syntax_error){
			 syntax_error = 1;
			 strcat(output,"syntax error\n");}}
[\*\*].*                {}
[\{][^]*[\}]		{}
\n			{if(!syntax_error) {
			strcat(output,yytext);}}
. 			{if(!syntax_error) {
			 strcat(output,yytext);}} 

%%
		
int main()
{
  yylex();
  printf("%d\n",output);
  return 0;
}
