%{
 
%}

%%
["].*["]	        {printf("%s",yytext);}
	
[\{][^\}]*$		{char temp[strlen(yytext)];
		         strncpy(temp,yytext, strlen(yytext-1));
			 printf("%s",temp);
			 printf("%s","syntax error\n");
			 exit(0);}

^[^\{]*[\}]		{char temp[strlen(yytext)];
		         strncpy(temp,yytext, strlen(yytext-1));
			 printf("%s",temp);
			 printf("%s","syntax error\n");
			 exit(0);}	
(\*\*).*                {}
(\{[^\}]*?\})    	{}

%%
		
int main()
{
   yylex();
   return 0;
}
