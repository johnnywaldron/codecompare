%{
	#include "roman.tab.h"
	void yyerror(char *s);
%}


%%

IV {yylval = 4; return IV;}
IX {yylval = 9; return IX;}
XL {yylval = 40; return XL;}
XC {yylval = 90; return XC;}
CD {yylval = 400; return CD;}
CM {yylval = 900; return CM;}
I {yylval = 1; return I;}
V {yylval = 5; return V;}
X {yylval = 10; return X;}
L {yylval = 50; return L;}
C {yylval = 100; return C;}
D {yylval = 500; return D;}
M {yylval = 1000; return M;}
\n {return EOL;}
. {yyerror("syntax error\n");}

%%