%{
#include <stdio.h>
int yylex();
void yyerror(char *s);
}%


%token ONE FOUR FIVE NINE TEN FORTY FIFTY NINETY HUNDRED FOURHUN FIVEHUN NINEHUN THOUSAND
%token EOL


%%

print:
| print term EOL { printf("= %d\n> ", $2); }
;

term: THOUSAND
| term {$$ = $$ + 1000}
;
term: NINEHUN
| term {$$ = $$ + 900}
;
term: FIVEHUN
| term {$$ = $$ + 500}
;
term: FOURHUN
| term {$$ = $$ + 400}
;
term: HUNDRED
| term {$$ = $$ + 100}
;

term: NINETY
| term {$$ = $$ + 90}
;

term: FIFTY
| term {$$ = $$ + 50}
;

term: FORTY
| term {$$ = $$ + 40}
;

term: TEN
| term {$$ = $$ + 10}
;

term: NINE
| term {$$ = $$ + 9}
;

term: FIVE
| term {$$ = $$ + 5}
;

term: FOUR
| term {$$ = $$ + 4}
;

term: ONE
| term {$$ = $$ + 1}
;

%%
int main()
{
yyparse();
return 0;
}

void yyerror(char *s){
 fprintf(stderr, "syntax error\n");
}






