%{
	#include "roman.tab.h"
	void yyerror(char *s);
}%


%%
"I" {return ONE;}
"IV"{return FOUR;}
"V"{return FIVE;}
"IX"{return NINE;}
"X"{return TEN;}
"XL"{return FORTY;}
"L"{return FIFTY;}
"XC"{return NINETY;}
"C"{return HUNDRED;}
"CD"{return FOURHUN;}
"D"{return FIVEHUN;}
"CM"{return NINEHUN;}
"M"{return THOUSAND;}
\n {return EOL;}
. {yyerror();}

%%