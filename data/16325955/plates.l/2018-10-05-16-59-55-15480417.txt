%{
NUMBER [0-9]
COUNTY C|CE|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|T|WD
YEAR [0-9]
int values[200];
int i = 0;


%}


%%
9[1-9](C|CE|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|T|WD)[1-9]{1,6} 		{	values[i] = 1990 + atoi(yytext[1]); i++;	}
8[1-9](C|CE|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|T|WD)[1-9]{1,6} 		{	values[i] = 1980 + atoi(yytext[1]); i++;	}
0[1-9](C|CE|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|T|WD)[1-9]{1,6}		{	values[i] = 2000 + atoi(yytext[1]); i++;	}
1[1-4][1,2](C|CE|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|T|WD)[1-9]{1,6}	{	values[i] = 2010 + atoi(yytext[1]); i++;	}
\n 									{	values[i] = -1; i++;						}
.*									{	values[i] = -2;	i++;						}





%%

int main(){
	yylex();
	int n = 0;
	int c = 0;
	while(values[n] != NULL){
		if (values[n] == -1)
			printf("\n");
		else if (values[n] == -2)
			printf("INVALID");
		else {
			c = 2018-values[n];
			printf(%d, c);
		
	return 0;
}
