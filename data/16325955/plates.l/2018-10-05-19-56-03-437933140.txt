%{
int values[200];
char i = 0;



%}

COUNTY C|CE|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|T|T|WD

%%
(9[1-9]"-"{COUNTY}"-"[0-9]{1,6}(\r\n|\r|\n))		{	values[i] = (1990 + yytext[1])-48; i++; 	}
(8[1-9]"-"{COUNTY}"-"[0-9]{1,6}(\r\n|\r|\n))		{	 values[i] = (1980 + yytext[1])-48; i++;	}
(0[1-9]"-"{COUNTY}"-"[0-9]{1,6}(\r\n|\r|\n))		{	 values[i] = (2000 + yytext[1])-48; i++;	}
(1[0-2]"-"{COUNTY}"-"[0-9]{1,6}(\r\n|\r|\n))		{	 values[i] = (2010 + yytext[1])-48; i++;	}
(1[3-8][1,2]"-"{COUNTY}"-"[0-9]{1,6}(\r\n|\r|\n))	{	 values[i] = (2010 + yytext[1])-48; i++;	}
\n 										{	values[i] = -1; i++;						}
.*										{	values[i] = -2;	i++;				}
									

%%

int main(){
	yylex();
	int n = 0;
	int c = 0;
	int j = 0;
	while(j != i){
		if (values[j]==-1)
			printf("\n");
		else if (values[j] == -2)
			printf("INVALID\n");
		else {
			c = 2018-(values[j]);
			printf("%d\n", c);
		}
		j++;
	}
	return 0;
}
