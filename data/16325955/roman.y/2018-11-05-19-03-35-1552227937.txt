%{
#include <stdio.h>
#include <stdlib.h>
int yylex();
void yyerror(char *s);
int fault = 0;
%}


%token I IV V IX X XL L XC C CD D CM M
%token EOL


%%

calclist: /* nothing */
| calclist exp EOL {
 if (fault == 0)
 printf("%d\n", $2);
 }
 ;

exp: reducedNumber {$$ = $1;} | number {$$ = $1;} | exp number {$$ = $1 + $2;} | exp reducedNumber {if ($1 < (5*$2)){yyerror("syntax error\n"}else $$ = $1 + $2;}
;
reducedNumber: IV | IX | XL | XC | CD | CM {$$ = $1;}
;
number: I | V | X | L | C | D | M {$$ = $1;}
;
%%
int main()
{
yyparse();
return 0;
}

void yyerror(char *s){
 fprintf(stderr, "syntax error\n");
 fault = 1;
 exit(0);
}







