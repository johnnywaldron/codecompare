%{
#include <stdio.h>
#include <stdlib.h>
int yylex();
void yyerror(char *s);
%}

%union{
	struct ast *a;
	int x;
}

%type <a> exp factor term id



%token I IV V IX X XL L XC C CD D CM M
%token EOL
%type<x> number reducedNumber


%%

calclist: /* nothing */
| calclist exp EOL {
 printf("%d\n", $2);
 }
 ;

 
exp: factor
 | exp '+' factor 	{ $$ = newast('+', $1, $3); }
 | exp '-' factor	{ $$ = newast('-', $1, $3); }
 ;

factor: term
 | factor '*' term  { $$ = newast('*', $1, $3); }
 | factor '/' term  { $$ = newast('/', $1, $3); }
 ;
 
term: id       
 | '|' term  	    { $$ = newast('|', $2, NULL); }
 | '(' exp ')' 	    { $$ = $2; }
 | '-' term         { $$ = newast('M', $2, NULL); }
 ;
 
id: /* nothing */
 | reducedNumber    {$$ = $1;} 
 | number           {$$ = $1;} 
 | number id        {$$ = newast('+', $1, $2);} 
 | reducedNumber id {
					if ($1 < (5*$2)){
						yyerror("syntax error\n");
					}else $$ = newast('+', $1, $2);}
;
reducedNumber: IV | IX | XL | XC | CD | CM {$$ = $1;}
;
number: I | V | X | L | C | D | M {$$ = $1;}
;



%%
int main()
{
yyparse();
return 0;
}

void yyerror(char *s){
 fprintf(stderr, "syntax error\n");
 exit(0);
 
}







