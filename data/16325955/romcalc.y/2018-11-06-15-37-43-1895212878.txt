%{
#include <stdio.h>
#include <stdlib.h>
int yylex();
void yyerror(char *s);
void output(int x);
%}



%token I IV V IX X XL L XC C CD D CM M
%token EOL



%%

calclist: /* nothing */
| calclist exp EOL {

 output($2);
 }
 ;

 
exp: factor
 | exp '+' factor 	{ $$ = $1 + $3; }
 | exp '-' factor	{ $$ = $1 - $3; }
 ;

factor: term
 | factor '*' term  { $$ = $1 * $3; }
 | factor '/' term  { $$ = $1 / $3; }
 ;
 
term: id       
 | '{' exp '}' 	    { $$ = $2; }
 | '-' term         { $$ = -$2; }
 ;
 
id:
 reducedNumber      {$$ = $1;} 
 | number           {$$ = $1;} 
 | number id        {$$ = $1 + $2;} 
 | reducedNumber id {
					if ($1 < (5*$2)){
						yyerror("syntax error\n");
					}else $$ = $1 + $2;}
;
reducedNumber: IV | IX | XL | XC | CD | CM {$$ = $1;}
;
number: I | V | X | L | C | D | M {$$ = $1;}
;



%%
int main()
{
yyparse();
return 0;
}

void yyerror(char *s){
 fprintf(stderr, "syntax error\n");
 exit(0);
 
}



void output(int x){
	int decimal[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1}; 
    char *symbol[] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};  
    int i = 0;
	if (x==0) printf("%c",'Z');
    while(x){ 
        while(x/decimal[i]){  
            printf("%s",symbol[i]);    
            x -= decimal[i];  
        }
        i++;   
    }
	printf("\n");
}













