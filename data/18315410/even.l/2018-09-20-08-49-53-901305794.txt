%{
int number = 0;
%}

numbersequence [0-9]*

%%

{numbersequence}[0|2|4|6|8]  { number++; }
[\n\r]                       {}
.                            {}

%%

int main()
{
    yylex();
    printf("%d\n", number);
    return 0;
}
