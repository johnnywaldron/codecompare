%{

%}

%%

[{].*[}] {  }
[{](^|\n*.*)+[}] {  }
["].*["] { printf(yytext); } //string
[*]{2}.*    {  }

%%

int main()
{
  yylex();
  return 0;
}
