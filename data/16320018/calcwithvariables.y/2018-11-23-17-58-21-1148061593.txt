
/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.y,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

/* simplest version of calculator */

%{
#  include <stdio.h>
int yylex();
int arr[26] ={0};
void yyerror(char *s);
//void printArr(int *s,int size);
%}

/* declare tokens */

%token NUMBER ASSIGN VAR
%token ADD SUB MUL DIV ABS
%token EOL
%%
calclist: /* nothing */
 | calclist exp EOL { printf("= %d\n> ", $2); }
 ; 


exp: factor 
 | VAR ASSIGN exp {int index  = $1;arr[index] = $3);}
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;




factor: term 
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: NUMBER 
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 | VAR {$$ = arr[$1];}
 ;
%%
int main()
{
  printf("> "); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}


// void printArr(int arr[26], int size)
// {
//   printf("\n");
//   for(int i = 0;i< size/4;i++)
//   {
//     printf("%d,",arr[i]);
  
//   }
//   printf("\n");
// }

//printArr(arr, sizeof(arr)

