%{

char year[4];
int currentIndex = 0;
int currentYear = 2018;
%}

%%



[1][3-9][1-2][-]["C"|"D"|"G"|"L"|"T"|"W"][-][0-9]*	{if(strlen(yytext)<13){strncpy(year,yytext,2);printf("%d\n",18-atoi(year));}else{printf("INVALID\n");}}
[1][3-9][1-2][-](("CE")|("CN")|("CW")|("DL")|("KE")|("KK")|("KY")|("LD")|("LH")|("LM")|("LS")|("MH")|("MN")|("MO")|("OY")|("RN")|("WH")|("WX")|("WW"))+[-][0-9]*		{if(strlen(yytext)<14){strncpy(year,yytext,2);printf("%d\n",18-atoi(year));}}

[3-9][0-9][-]["C"|"D"|"G"|"L"|"W"][-][0-9]*	{
													if(strlen(yytext)<12){
														strncpy(year,yytext,2);
														if(atoi(year)<20){
															strncpy(year,"20",2);
															strncpy(&year[2],yytext,2);
															printf("%d\n",currentYear-atoi(year));
														}
														else
														{
															strncpy(year,"19",2);
															strncpy(&year[2],yytext,2);
															printf("%d\n",currentYear-atoi(year));
														}
													}
												}
[3-9][0-9][-](("CE")|("CN")|("CW")|("DL")|("KE")|("KK")|("KY")|("LD")|("LH")|("LM")|("LK")|("LS")|("MH")|("MN")|("MO")|("OY")|("RN")|("TN")|("TS")|("WH")|("WX")|("WW")|("WD"))+[-][0-9]*		{
																																																	if(strlen(yytext)<13){
																																																		strncpy(year,yytext,2);
																																																			if(atoi(year)<20){
																																																				strncpy(year,"20",2);
																																																				strncpy(&year[2],yytext,2);
																																																				printf("%d\n",currentYear-atoi(year));
																																																			}
																																																			else
																																																			{
																																																				strncpy(year,"19",2);
																																																				strncpy(&year[2],yytext,2);
																																																				printf("%d\n",currentYear-atoi(year));
																																																			}
																																																		}
																																																	}

[ \t\n\r]		{}
[a-zA-Z0-9]+	{printf("INVALID\n");}

%%

int main()
{
  yylex();
  return 0;
}


