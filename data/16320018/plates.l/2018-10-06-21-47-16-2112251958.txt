%{


int currentIndex = 0;
int currentYear = 2018;
void printYearsSince(char* yytext)
{
	char year[4];
	char LastTwoDigits[2];
	strncpy(LastTwoDigits,yytext,2);
	if(atoi(LastTwoDigits)<20){
		strncpy(year,"20",2);
		strncpy(&year[2],yytext,2);
		printf("%d\n",currentYear-atoi(year));
	}
	else
	{
		strncpy(year,"19",2);
		strncpy(&year[2],yytext,2);
		printf("%d\n",currentYear-atoi(year));
	}
}

%}



%%



[1][3-9][1-2][-]["C"|"D"|"G"|"L"|"T"|"W"][-][0-9]*	{if(strlen(yytext)<13){printYearsSince(yytext);}else{printf("INVALID\n");}}
[1][3-9][1-2][-](("CE")|("CN")|("CW")|("DL")|("KE")|("KK")|("KY")|("LD")|("LH")|("LM")|("LS")|("MH")|("MN")|("MO")|("OY")|("RN")|("WH")|("WX")|("WW"))+[-][0-9]*		{if(strlen(yytext)<14){printYearsSince(yytext);}else{printf("INVALID\n");}}

[0-9][0-9][-]["C"|"D"|"G"|"L"|"W"][-][0-9]*	{if(strlen(yytext)<12){printYearsSince(yytext);}else{printf("INVALID\n");}}
[0-9][0-9][-](("CE")|("CN")|("CW")|("DL")|("KE")|("KK")|("KY")|("LD")|("LH")|("LM")|("LK")|("LS")|("MH")|("MN")|("MO")|("OY")|("RN")|("TN")|("TS")|("WH")|("WX")|("WW")|("WD"))+[-][0-9]*		{
																																																	if(strlen(yytext)<13){
																																																		printYearsSince(yytext);
																																																		}
																																																	else{printf("INVALID\n");}
																																																	}

[ \t\n\r]		{}
[a-zA-Z0-9\-.]+	{printf("INVALID\n");}

%%

int main()
{
  yylex();
  return 0;
}


