/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.y,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

/* simplest version of calculator */

%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
void yyerror(char *s);



%}
/*subtractiveNum: numeral number ;*/
/* declare tokens */
%token NUMBER
%token ADD SUB MUL DIV ABS
%token EOL
%token I V X L C D M IV IX XL XC CD CM 
%%
calclist: /* nothing */
 | calclist number EOL { printf("%d\n", $2); }
 ; 


number: subtractiveNum {$$ = $1;}  |numeral {$$ = $1;}  | numeral  number{$$ = $1+$2;} | subtractiveNum number  {if(2*$1>$2*10){$$ = $1+$2;}else{yyerror("syntax error ");}}
;


subtractiveNum: IV | IX | XL | XC | CD | CM {$$ = $1;}
;
numeral:  I  | V  | X | L | C | D| M {$$ = $1;}
;




%%
int main()
{
  
  printf(""); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(0);
}






