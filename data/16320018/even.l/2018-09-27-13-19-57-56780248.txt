%{
int isComment = 0;
char output[5000];
int currentIndex = 0;
int partOfString = 0;
int oneLineComment = 0;
%}

%%
<<EOF>>		{ if(isComment==1){strncpy(&output[currentIndex],"\ninvalid Syntax!!\n",25);}return 0;}
["]			{ partOfString = !partOfString;output[currentIndex] = yytext[0];currentIndex = currentIndex+1;}
"**"		{ if(!isComment&&!partOfString){oneLineComment = 1;}else{if(partOfString){strncpy(&output[currentIndex],"**",2);currentIndex = currentIndex+2;}}}	
"{"			{ if(!oneLineComment&&!partOfString){isComment= 1;}else if(partOfString){output[currentIndex] = yytext[0];currentIndex = currentIndex+1;}}
"}"			{ if(isComment){isComment = 0;}else if(partOfString){output[currentIndex] = yytext[0];currentIndex = currentIndex+1;}}
[a-zA-Z]	{ if(!isComment && !oneLineComment){output[currentIndex] = yytext[0];currentIndex = currentIndex+1;}}
\n			{ if(!isComment && !oneLineComment){output[currentIndex] = yytext[0];currentIndex++;}else if(oneLineComment==1){oneLineComment = 0;output[currentIndex] = '\n'}}
.			{ if(!isComment && !oneLineComment){output[currentIndex] = yytext[0];currentIndex++;}}

%%

int main()
{
  yylex();
  
  printf("%s\n",output );
  return 0;
}
