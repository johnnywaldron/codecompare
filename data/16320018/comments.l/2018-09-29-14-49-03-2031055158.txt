%{

char output[5000];
int currentIndex = 0;

%}

%%
"*"												{output[currentIndex] = yytext[0];currentIndex++;}
"\+"											{output[currentIndex] = yytext[0];currentIndex++;}	
["](([A-z.\40{}*]+))["]							{strncpy(&output[currentIndex],yytext,(int)strlen(yytext));currentIndex+=(int)strlen(yytext);}
[\*][\*][A-z.\40]+/[\n]							{}
[{]+(([0-9\.\nA-z.\40\t\:\;\=\,\(\){}]+)[}]+)	{}
([{]+(([\nA-z.\40\t\+\:]+)[^}]))|[}]			{strncpy(&output[currentIndex],"syntax error\n",50);return 0;}
([\nA-z.]|.)									{output[currentIndex] = yytext[0];currentIndex++;}
%%

int main()
{
  yylex();
  
  printf("%s",output );
  return 0;
}
