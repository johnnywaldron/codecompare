/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.y,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

/* simplest version of calculator */

%{
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h> 
int yylex();
void yyerror(char *s);
void toRomanStr(int num, char* ans);
char* concat(const char *s1, const char *s2);


%}
/*subtractiveNum: numeral number ;*/
/* declare tokens */


%token EOL
%token I V X L C D M IV IX XL XC CD CM PP  ADD MULTIPLY SUBTRACT DIVIDE OP CP NEG
%%


calclist: /* nothing */
 | calclist exp EOL { char* output ;toRomanStr($2,output);}
 ; 


exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUBTRACT factor { $$ = $1 - $3; }
 ;

factor: term 
 | factor MULTIPLY term { $$ = $1 * $3; }
 | factor DIVIDE term { $$ = $1 / $3; }
 ;

term: number
| OP exp CP   {$$=$2;}
;

/*
expression: OP expression CP {$$ = $2;}
| number{$$ = $1;} 
| addSign expression{$$ = $2*-1;}

| number mulSign expression {if($2>0){$$ = $1*$3;}else{$$ = $1/$3;} }
| number addSign expression {$$ = $1+$3*$2;}

;
*/

number:subtractiveNum {$$ = $1;}  
| numeral {$$ = $1;}  
| numeral  number{if($1!=$2){$$ = $1+$2;}else {yyerror("syntax error");}} 
| subtractiveNum number  {if(2*$1>$2*10){$$ = $1+$2;}else{yyerror("syntax error");}}
;


subtractiveNum: IV | IX | XL | XC | CD | CM {$$ = $1;}
;
numeral:  I  | V  | X | L | C | D| M {$$ = $1;}
;
/*
addSign: ADD {$$ = 1;}
|SUBTRACT {$$ = -1;}
;
mulSign: MULTIPLY {$$ = 1;}
|DIVIDE { $$ = -1 ;}
;
*/


%%
int main()
{
  
  printf(""); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{

  fprintf(stderr, "%s\n", s);
  exit(0);
}


void toRomanStr(int num,char* ans){

    char* sign;
    if(num<0){
        num = num*-1;
        sign = "-";
    }
    else
    {
        sign = "";
    }
 
    int nums [] = {1000,900,500,400,100,90,50,40,10,9,5,1};
    char* symb [] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","I"};

    char* str = "";
    int arrayIndex = 0;
    if(num==0){printf("Z");}
    while(arrayIndex<(sizeof(nums)/4))
    {
       
        while(num>=nums[arrayIndex]){
            num = num -  nums[arrayIndex];
            if(sizeof(str>0)){
                //strcat(str, symb[arrayIndex]);
                str =concat(str,symb[arrayIndex]);
            }
            else
            {
                //strcpy(str,symb[arrayIndex]);
                str = concat(str,symb[arrayIndex]);
            }
         
        }
        arrayIndex = arrayIndex+1;
    }

    printf("%s%s\n",sign,str);
}


char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1) + strlen(s2) + 1); 
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}
