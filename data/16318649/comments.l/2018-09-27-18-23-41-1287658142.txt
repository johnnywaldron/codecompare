/* code written by Sarah Phillips, Student Number 16318649*/
/*comment remover*/
%{
  char error []= "syntax error\n";
%}

%option noyywrap

%%
\*{2}.*    {}                /* single line comment beginning with ** */
["]+.*["]+                {printf("%s",yytext);}  /*"{} | **"*/
[{]*[^}]                 {printf("%s", error); return 0;}
[^{]*[}]                 {printf("%s", error); return 0;}
[{][^}]*[}]*      {}        /* {multiline comment } */
%%

int main( )
{
  yylex();
  return 0;
}

