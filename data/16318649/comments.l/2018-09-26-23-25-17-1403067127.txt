/* code written by Sarah Phillips, Student Number 16318649*/

/*comment remover*/
%{
     char code [] = "";
%}

%%
[.]*/s     { code+=yytext;}
\*{2}.*   { code=+"";}        /* single line comment beginning with */
{{1}.*}{1}    {code=+"";}      /*{comment}*/
"{1}{+.*}+"{1}   {code=+yytext;}       / *"{}" */ 
"{1}\*{2}"{1}   {code=+yytext;}       /* "**" */

%%

int main()
{
  yylex();
  printf("%s\n", code);
  return 0;
}

