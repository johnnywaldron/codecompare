/*
  Sarah Phillips 16318649
*/
%{
#include "romcalc.tab.h"
void yyerror(char *s);
%}
%%
"I" { return I; }
"V" { return V; }
"X" { return X; }
"L" { return L; }
"C" { return C; }
"D" { return D; }
"M" { return M; }

"+" { return ADD; }
"-" { return SUB; }
"*" { return MUL; }
"/" { return DIV; }
"{" { return OB; }
"}" { return CB; }

\n  { return EOL; }
.	{ yyerror("syntax error"); }
%%
