%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
int yyparse();
void yyerror(char *s);

int mtotal, ctotal, xtotal, itotal, total;
%}

/* declare tokens */
%token I V X L C D M
%token EOL
%%

calclist: /* nothing */
 | calclist num EOL {printf("%d\n", total);}
 ;

/*1111 1110 1101 1100 1011 1010 1001 1000 111 110 101 100 11 10 01 00 */
num: MTotal CTotal XTotal ITotal 	{total = mtotal + ctotal + xtotal + itotal;}
 | MTotal CTotal XTotal			{total = mtotal + ctotal + xtotal;}
 | MTotal CTotal ITotal			{total = mtotal + ctotal + itotal;}
 | MTotal CTotal 			{total = mtotal + ctotal;}
 | MTotal XTotal ITotal			{total = mtotal + xtotal + itotal;}
 | MTotal XTotal 			{total = mtotal + xtotal;}
 | MTotal ITotal			{total = mtotal + itotal;}
 | MTotal 				{total = mtotal;}
 | CTotal XTotal ITotal 		{total = ctotal + xtotal + itotal;}
 | CTotal XTotal 			{total = ctotal + xtotal;}
 | CTotal ITotal 			{total = ctotal + itotal;}
 | CTotal				{total = ctotal;}
 | XTotal ITotal 			{total = xtotal + itotal;}
 | XTotal 		 		{total = xtotal;}
 | ITotal 		 		{total = itotal;}
 ;

ITotal: I 	{itotal = 1;}
 | I I		{itotal = 2;}
 | I I I	{itotal = 3;}
 | I V		{itotal = 4;}
 | V		{itotal = 5;}
 | V I		{itotal = 6;}
 | V I I	{itotal = 7;}
 | V I I I 	{itotal = 8;}
 | I X		{itotal = 9;}
 ;

XTotal: X 	{xtotal = 10;}
 | X X		{xtotal = 20;}
 | X X X	{xtotal = 30;}
 | X L		{xtotal = 40;}
 | L		{xtotal = 50;}
 | L X		{xtotal = 60;}
 | L X X	{xtotal = 70;}
 | L X X X 	{xtotal = 80;}
 | X C		{xtotal = 90;}
 ;

CTotal: C 	{ctotal = 100;}
 | C C		{ctotal = 200;}
 | C C C	{ctotal = 300;}
 | C D		{ctotal = 400;}
 | D		{ctotal = 500;}
 | D C		{ctotal = 600;}
 | D C C	{ctotal = 700;}
 | D C C C 	{ctotal = 800;}
 | C M		{ctotal = 900;}
 ;

MTotal: M 	{mtotal = 1000;}
 | M M		{mtotal = 2000;}
 | M M M	{mtotal = 3000;}
 ;

%%

void yyerror(char *s)
{
  printf("%s", s);
  exit(0);
}

int 
main()
{
  yyparse();
}



