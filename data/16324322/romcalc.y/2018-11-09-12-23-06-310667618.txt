%{
#  include <stdio.h>
#  include <stdlib.h>
#  include <string.h>

int yylex();
void yyerror(char *s);

char* convert_to_roman(int n);
char* get_M(int n);
char* get_C(int n);
char* get_X(int n);
char* get_I(int n);

%}

/* declare tokens */
%token I V X L C D M
%token EOL
%token ADD SUB MUL DIV OPEN CLOSE
%%

calclist: /* nothing */
 | calclist exp EOL {printf("%s\n", convert_to_roman($2));}
 ;

/* operations */
exp: factor
 | exp ADD factor	{$$ = $1 + $3;}
 | exp SUB factor	{$$ = $1 - $3;}
;

factor: bracket
 | factor MUL bracket	{$$ = $1 * $3;}
 | factor DIV bracket	{$$ = $1 / $3;}
;

bracket: num
 | OPEN exp CLOSE	{$$ = $2;}
;

/*1111 1110 1101 1100 1011 1010 1001 1000 111 110 101 100 11 10 01 00 */
num: MTotal CTotal XTotal ITotal 	{$$ = $1 + $2 + $3 + $4;}
 | MTotal CTotal XTotal			{$$ = $1 + $2 + $3;}
 | MTotal CTotal ITotal			{$$ = $1 + $2 + $3;}
 | MTotal CTotal 			{$$ = $1 + $2;}
 | MTotal XTotal ITotal			{$$ = $1 + $2 + $3;}
 | MTotal XTotal 			{$$ = $1 + $2;}
 | MTotal ITotal			{$$ = $1 + $2;}
 | MTotal 				{$$ = $1;}
 | CTotal XTotal ITotal 		{$$ = $1 + $2 + $3;}
 | CTotal XTotal 			{$$ = $1 + $2;}
 | CTotal ITotal 			{$$ = $1 + $2;}
 | CTotal				{$$ = $1;}
 | XTotal ITotal 			{$$ = $1 + $2;}
 | XTotal 		 		{$$ = $1;}
 | ITotal 		 		{$$ = $1;}
 ;

MTotal: M 				{$$ = 1000;}
 | M M					{$$ = 2000;}
 | M M M				{$$ = 3000;}
 ;

CTotal: C 				{$$ = 100;}
 | C C					{$$ = 200;}
 | C C C				{$$ = 300;}
 | C D					{$$ = 400;}
 | D					{$$ = 500;}
 | D C					{$$ = 600;}
 | D C C				{$$ = 700;}
 | D C C C 				{$$ = 800;}
 | C M					{$$ = 900;}
 ;

XTotal: X 				{$$ = 10;}
 | X X					{$$ = 20;}
 | X X X				{$$ = 30;}
 | X L					{$$ = 40;}
 | L					{$$ = 50;}
 | L X					{$$ = 60;}
 | L X X				{$$ = 70;}
 | L X X X 				{$$ = 80;}
 | X C					{$$ = 90;}
 ;

ITotal: I 				{$$ = 1;}
 | I I					{$$ = 2;}
 | I I I				{$$ = 3;}
 | I V					{$$ = 4;}
 | V					{$$ = 5;}
 | V I					{$$ = 6;}
 | V I I				{$$ = 7;}
 | V I I I 				{$$ = 8;}
 | I X					{$$ = 9;}
 ;

%%

char* convert_to_roman(int n) 
{
	char *roman;
	int val;
	int pos = -n;
  	if (n == 0) return "Z";
	else if (n < 0) {
		if (pos >= 1000) {
			val = pos/1000;
			strncat(roman, get_M(val), strlen(get_M(val)));
			pos = pos%1000;
		}
		if (pos >= 100) {
			val = pos/100;
			strncat(roman, get_C(val), strlen(get_C(val)));
			pos = pos%100;
		}
		if (pos >= 10) {
			val = pos/10;
			strncat(roman, get_X(val), strlen(get_X(val)));
			pos = pos%10;
		}
		if (pos > 0) {
			strncat(roman, get_I(pos), strlen(get_I(pos)));
		}
		strncat(roman, "-", 1);
	}
	else {
		if (n >= 1000) {
			val = n/1000;
			strncat(roman, get_M(val), strlen(get_M(val)));
			n = n%1000;
		}
		if (n >= 100) {
			val = n/100;
			strncat(roman, get_C(val), strlen(get_C(val)));
			n = n%100;
		}
		if (n >= 10) {
			val = n/10;
			strncat(roman, get_X(val), strlen(get_X(val)));
			n = n%10;
		}
		if (n > 0) {
			strncat(roman, get_I(n), strlen(get_I(n)));
		}
	}
	return roman;
	exit(0);
}

char* get_M(int n)
{
	if (n == 1) return "M";
	if (n == 2) return "MM";
	if (n == 3) return "MMM";
	if (n == 4) return "MMMM";
	if (n == 5) return "MMMMM";
	if (n == 6) return "MMMMMM";
	if (n == 7) return "MMMMMMM";
	if (n == 8) return "MMMMMMMM";
	if (n == 9) return "MMMMMMMMM";
	else return "";
}

char* get_C(int n)
{
	if (n == 1) return "C";
	if (n == 2) return "CC";
	if (n == 3) return "CCC";
	if (n == 4) return "CD";
	if (n == 5) return "D";
	if (n == 6) return "DC";
	if (n == 7) return "DCC";
	if (n == 8) return "DCCC";
	if (n == 9) return "CM";
	else return "";
}

char* get_X(int n)
{
	if (n == 1) return "X";
	if (n == 2) return "XX";
	if (n == 3) return "XXX";
	if (n == 4) return "XL";
	if (n == 5) return "L";
	if (n == 6) return "LX";
	if (n == 7) return "LXX";
	if (n == 8) return "LXXX";
	if (n == 9) return "XC";
	else return "";
}

char* get_I(int n)
{
	if (n == 1) return "I";
	if (n == 2) return "II";
	if (n == 3) return "III";
	if (n == 4) return "IV";
	if (n == 5) return "V";
	if (n == 6) return "VI";
	if (n == 7) return "VII";
	if (n == 8) return "VIII";
	if (n == 9) return "IX";
	else return "";
}

void yyerror(char *s)
{
  printf("%s\n", s);
  exit(0);
}

int 
main()
{
  yyparse();
  return 0;
}
