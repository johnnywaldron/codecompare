%{
# include "romcalc.tab.h"

void yyerror();
int yylex();

%}

%%

"I" 	{ return I; }
"V" 	{ return V; }
"X" 	{ return X; }
"L" 	{ return L; }
"C" 	{ return C; }
"D" 	{ return D; }
"M" 	{ return M; }

"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"|" 	{ return ABS; }
"{"	{ return OPEN; }
"}"	{ return CLOSE; }


[ /t]	{ /*ignore whitespace*/ }
\n 	{ return EOL;}
.	{ yyerror();}

%%
