/* plates.l */
%{
	#define NUMBER
	#define COUNTY
	#define YEAR
	#define YEAR2
%}

NUMBER  [0-9]{1,6}
COUNTY1  C|CE|CN|CE|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK
COUNTY2	C|CE|CN|CE|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|W|WH|WX|WW|L|LK|TN|TS|WD
/*C|CE|CN|CE|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|L|LK|TN|TS|W|WD*/
YEAR1  	131|132|141|142|151|152|161|162|171|172|181|182
YEAR2  	[0-9]{2}


%%
{YEAR1}[-]{COUNTY1}[-]{NUMBER}\n 	{ 	char yr[2]; 
    						strncpy(yr, yytext, 2);
						int y = atoi(yr);
						int res = 2018-2000-y;
						printf("%d\n", res);
					}

{YEAR2}[-]{COUNTY2}[-]{NUMBER}\n	{	int res;
						char yr[2]; 
    						strncpy(yr, yytext, 2);
						int y = atoi(yr);
						if (y > 12 ) {
							int res = 2018-1900-y;	
							printf("%d\n", res);
						}
						else {
							int res = 2018-2000-y;	
							printf("%d\n", res);
						}
					}

.+					{printf("INVALID\n");}	
						
%%

int main()
{
  yylex();
  return 0;
}
