%{
 #include <stdio.h>
 #include <string.h>
 void yyerror(char *);
 int yylex(void);
 int sym[26];
%}
%token INTEGER VARIABLE
%left '+' '-' '*' '/'
%%
program:
 program statement '\n'
 | /* NULL */
 ;
statement:
 expression { 
		    char buffer[1000] = {0, };
		    char *res;
		    res = buffer;
		    int val = $1;
		    char *huns[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
		    char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
		    char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
		    int   size[] = { 0,   1,    2,     3,    2,   1,    2,     3,      4,    2};

		    //  Add 'M' until we drop below 1000.
		    if (val == 0) *res++ = 'Z';
		    while (val >= 1000) {
			*res++ = 'M';
			val -= 1000;
		    }

		    // Add each of the correct elements, adjusting as we go.

		    strcpy (res, huns[val/100]); res += size[val/100]; val = val % 100;
		    strcpy (res, tens[val/10]);  res += size[val/10];  val = val % 10;
		    strcpy (res, ones[val]);     res += size[val];

		    // Finish string off.

		    *res = '\0';
		    printf("%s\n", buffer); 
}
 | VARIABLE '=' expression { sym[$1] = $3; }
 ;
expression:
 INTEGER   
 | VARIABLE { $$ = sym[$1]; }
 | '-' expression { $$ = -$2; }
 | expression '+' expression { $$ = $1 + $3; }
 | expression '-' expression { $$ = $1 - $3; }
 | expression '*' expression { $$ = $1 * $3; }
 | expression '/' expression { $$ = $1 / $3; }
 | '{' expression '}' { $$ = $2; }
 ;
%%
void yyerror(char *s) {
 fprintf(stderr, "%s\n", s);
}
int main(void) {
 yyparse();
}
