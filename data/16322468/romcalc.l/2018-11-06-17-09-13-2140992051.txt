 /* calculator */
%{
 #include "romcalc.tab.h"
 #include <stdlib.h>
 void yyerror(char *);
%}
%%

([M]*(CM|CD|(D?C{0,3}))(XC|XL|L?X{0,3})(IX|IV|V?I{0,3}))	{
									const char *s = strdup(yytext);									
									int result = 0;
									int lastValue = 999999;
									char ch;
									for(ch = *s; ch!=0; ch = *(++s))
									{
										int value;
										switch(ch)
										{	
											case 'M': value = 1000;
											break;
											case 'D': value = 500;
											break;
											case 'C': value = 100;
											break;
											case 'L': value = 50;
											break;
											case 'X': value = 10;
											break;
											case 'V': value = 5;
											break;
											case 'I': value = 1;
											break;
											case 'Z': value = 0;
											default:
											    fprintf(stderr, "error: invalid Roman numeral letter '%c'\n", ch);
											    exit(1);
										}
										if(value > lastValue){
											result += (value - 2 * lastValue);
										}
										else{
											result += value;
										}
										lastValue = value;
									}
									yylval = result;
									return INTEGER; 		
								};
									

[a-z] {
 yylval = *yytext - 'a';
 return VARIABLE;
 }
[0-9]+ {
 yylval = atoi(yytext);
 return INTEGER;
 }
[-+()=*/\n] { return *yytext; }
[ \t] ; /* skip whitespace */
. yyerror("syntax error");

%%
int yywrap(void) {
 return 1;
}
