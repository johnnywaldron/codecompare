%{
int x=0;
%}

%%


["{"]{1} ([^\n]*|[\n])* / ["}"]{1} {x++;}
[\*]{2,}[^\n]* { x++; }


%%

int main()
{
  yylex();
  printf("\nNumber of Comments Found: %d\n", x);
  yyterminate();
}
