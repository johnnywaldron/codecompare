%{
int x=0;
%}

%%

[02468]	{ x++; }
[^02468] {; }


%%

int main()
{
  yylex();
  printf("%d\n", x);
	return 0;
}
