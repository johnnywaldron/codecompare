%{
char y[]=null;
int y1=0; //1987-1999
bool y2=false; //2000-2012
int present=2018;
int add1=1900;
int add2=2000;
int reg=0;
%}

NUMBER{1,6}  [0-9]+
COUNTY  C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MO|OY|RN|SO|T|W|WH|WX|WW-
YEAR_NEW  [1][3-8][1-2]-
YEAR_OLD1 [89][0-9]-
YEAR_OLD2 [0][0-9]-
YEAR_OLD3 [1][0-2]-
COUNTY_OLD C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW-

%%
{YEAR_NEW}{COUNTY}{NUMBER}   {memcpy(y,&yytext[0],2);
                              if(y[0]=='1'){
                                y1=atoi(y);
                                y1+=add2;
                                reg=present-y1;
                              }


                             }
{(YEAR_OLD1|YEAR_OLD2|YEAR_OLD3)}{COUNTY}{NUMBER} {memcpy(y,&yytext[0],3);
                                                   if(y[0]=='0' || y[0]=='1'){
                                                     y2=true;
                                                   }
                                                    y1=atoi(y);
                                                    y1+=add2;
                                                    reg=present-y1;
                                                   if(y[0]='8' || y[0]=='9'){
                                                      y2=false;
                                                   }
                                                    y1=atoi(y);
                                                    y1+=add1;
                                                    reg=present-y1;
                                                   }
%%

int main(){
yylex();
return 0;
}
