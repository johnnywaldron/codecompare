%{
#include <stdio.h>
#include <stdlib.h>
int yylex();
int yyparse();
void yyerror(char *s);
%}

%token NUMBER
%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND EOL

%%
/* statement: ROMAN '=' NUMBER {printf("%s = %d\n", $1, $3);} */
num  :
     /* |    num thousand hundreds tens ones EOL {printf("%d\n", $2 + $3 + $4 + $5);} */
     |    num ones EOL {printf("%d\n", $2);}
     |    num tens EOL {printf("%d\n", $2);}
     |    num tens ones EOL {printf("%d\n", $2 + $3);}
     |    num hundreds EOL {printf("%d\n", $2);}
     |    num hundreds ones EOL {printf("%d\n", $2 + $3);}
     |    num hundreds tens EOL {printf("%d\n", $2 + $3);}
     |    num hundreds tens ones EOL {printf("%d\n", $2 + $3 + $4);}
     |    num thousand EOL {printf("%d\n", $2);}
     |    num thousand ones EOL {printf("%d\n", $2 + $3);}
     |    num thousand tens EOL {printf("%d\n", $2 + $3);}
     |    num thousand tens ones EOL {printf("%d\n", $2 + $3 + $4);}
     |    num thousand hundreds EOL {printf("%d\n", $2 + $3);}
     |    num thousand hundreds tens EOL {printf("%d\n", $2 + $3 + $4);}
     |    num thousand hundreds tens ones EOL {printf("%d\n", $2 + $3 + $4 + $5);}
     ;
/* thousand :
THOUSAND : */
thousand:
     |    THOUSAND {$$ = 1000;}
     |    THOUSAND THOUSAND {$$ = 2000;}
     |    THOUSAND THOUSAND THOUSAND {$$ = 3000;}
     ;
hundreds:
     |    hundred {$$ = $1;}
     |    HUNDRED FIVEHUNDRED {$$ = 400;}
     |    FIVEHUNDRED {$$ = 500;}
     |    FIVEHUNDRED hundred {$$ = 500 + $2;}
     |    HUNDRED THOUSAND {$$ = 900;}
     ;
hundred:
     |    HUNDRED {$$ = 100;}
     |    HUNDRED HUNDRED {$$ = 200;}
     |    HUNDRED HUNDRED HUNDRED {$$ = 300;}
     ;
tens :
     |    ten {$$ = $1;}
     |    TEN FIFTY {$$ = 40;}
     |    FIFTY {$$ = 50;}
     |    FIFTY TEN {$$ = 60;}
     |    FIFTY ten {$$ = 50 + $2;}
     /* |    FIFTY TEN TEN TEN {$$ = 80;} */
     |    TEN HUNDRED {$$ = 90;}
     ;
ten  :
     |    TEN {$$ = 10;}
     |    TEN TEN {$$ = 20;}
     |    TEN TEN TEN {$$ = 30;}
     ;
/* five :
     |    ones {$$ = $1;}
     |    FIVE {$$ = 5;}
     |    ONE FIVE {$$ = 4;}
     |    FIVE one {} */
ones :
     |    one {$$ = $1;}
     |    ONE FIVE { $$ = 4; }
     |    FIVE {$$ = 5; }
     |    FIVE one {$$ = 5 + $2;}
     |    ONE TEN {$$ = 9;}
     ;
one  :
     |    ONE {$$ = 1;}
     |    ONE ONE {$$ = 2;}
     |    ONE ONE ONE {$$ = 3;}
     ;
%%

int main()
{
  /* printf("> "); */
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("%s", s);
}
