%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yylex();
int yyparse();
void yyerror(char *s);
int var[26];
int asciiVal;
%}

/* %error-verbose */
%token print ASSIGN VAR NUMBER CEOL
%nonassoc UMINUS
%token ADD SUB MUL DIV FB BB

%%
calc :
     |    calc in CEOL
     |    calc CEOL
     ;
in   :    VAR ASSIGN exp {var[$1] = $3;}
     |    print VAR      {asciiVal = var[$2]; printf("%d\n",asciiVal);}
     ;
exp  :    factor
     |    exp ADD factor { $$ = $1 + $3;}
     |    exp SUB factor { $$ = $1 - $3;}
     |    '-' exp %prec UMINUS {$$ = $2;}
     ;
factor:   term
     |    factor MUL term { $$ = $1 * $3;}
     |    factor DIV term { $$ = $1 / $3 ;}
     |    '-' factor %prec UMINUS {$$ = $2;}
     ;
term :    NUMBER
     |    VAR       {$$ = var[$1];}
     |    FB exp BB { $$ = $2;}
     |    '-' term %prec UMINUS {$$ = $2;}
     ;
%%

int main()
{
  memset(var,0,26);
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("%s\n", s);
  exit(0);
}
