%{
  #include <stdio.h>
  int count =0;
  int pass=0;
%}
open "{"
close "}"

%%
\".*\" {}
\*\*.* {}
\/\/(.*) {};
\{[^]]*\} {}
{open} {count++;}
{close} {count--; if (count<0){printf("sytax error\n"); exit(0);}}
%%

main(){
  yylex();
  return 0;
}
