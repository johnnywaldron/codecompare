%{
  int count =0;
%}
open "{"
close "}"

%%
\*\*.*    {}
\".*\"    {printf(yytext);}
\/\/(.*)  {}
\{[^]]*\} {}
{open}    {count++;}
{close}   {count--; if (count<0){printf("sytax error\n"); exit(0);}}
%%

main(){
  yylex();
  if(count==0)printf("all match");
  return 0;
}
