%{
# include "roman.tab.h"
void yyerror(char *s);
%}

%%
I         {return ONE;}
V	        {return FIVE;}
X	        {return TEN;}
L	        {return FIFTY;}
C	        {return HUNDRED;}
D	        {return FIVEHUNDRED;}
M	        {return THOUSAND;}
\n        { return EOL; }
.	        { yyerror("syntax error\n"); return 0; }
%%
