
%{
	int currentYear = 2018;
	int year = 0;
	size_t yearLen = 0;

//Various flags
	int i = 1;
	int old = 0;
	int new = 0;
	int newCounty = 0;

	int invalid = 0;

	int age = 0;
%}

NUMBER [0-9]
COUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
OLD_COUNTY L|LK|TN|TS|W|WD
INVALID T
YEAR	[0-9]

%%


{YEAR}	{
					if(i){
						year = (year * 10) + atoi(yytext);
						yearLen++;
						//printf("\nyear = %d\n", year);
						//printf("\nyearLen = %zu", yearLen);

						if(yearLen > 2){
							i = 0;
							old = 0;
							new = 1;
							if(year >= 141)
								newCounty = 1;
						}
						else if(yearLen == 2){
							old = 1;
							if(year < 13 || year > 85)
								i = 0;
						}
						
						
					}	
				}

{YEAR}-{INVALID}-{NUMBER}	{
					if(newCounty != 1){
						invalid = 1;
					}
				}

-{COUNTY}-{NUMBER} {}

{YEAR}-{OLD_COUNTY}-{NUMBER} {
					if(newCounty == 1){
						invalid = 1;
					}
				}

\n			{
					if(i == 0){
						if(invalid == 1){
							printf("INVALID\n");
						}
						else{

							//Testing prints
							//printf("Testing values {\nnew = %d\nold = %d\nyear = %d\n}\n\n", new, old, year);

							if(new){
								age = currentYear - ((year/10) + 2000);
								printf("%d\n", age);
							}
							else if(old && year < 86){
								age = currentYear - (year + 2000);
      	        printf("%d\n", age);						
							}
							else if(old && year >= 86){
								age = currentYear - (year + 1900);
              	printf("%d\n", age);
							}
						}
						//Reset all flags
	  				i = 1; year = 0; old = 0; new = 0; newCounty = 0; invalid = 0; age = 0; yearLen = 0;
					}
				}


.				{}

%%


int main(){
	yylex();
	return 0;
}

