%{

%}

%%
[/"][^/"]*[/"]			{printf("%s", yytext);}
["{"][^}]*["}"]			{}
\*{2}(.)*			{}
"{"|"}"				{printf("syntax error\n"); exit(0);}

%%

int main()
{
yylex();
return 0;
}
