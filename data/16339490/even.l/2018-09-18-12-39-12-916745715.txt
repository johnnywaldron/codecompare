/* @author Philip Bradish 16339490
 */

/* counts the number of even numbers inputted.*/
%{
int evenNum = 0;
%}

%%

[0-9]+	{ if(atoi(yytext) % 2 == 0) evenNum++; }
[\n]	{}
.		{}

%%

int main() {
	yylex();
	printf("%d\n", evenNum);
	return 0;
}
