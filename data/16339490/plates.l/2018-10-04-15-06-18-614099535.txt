/* @author Philip Bradish 16339490 */

/* Calculates the age of cars from their registration plates.*/
COUNTIES C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|W|WH|WX|WW
OLD_COUNTIES LK|TN|TS|WD
%{
const int CURRENT_YEAR = 18;
const int NUMBER_CHANGE = 13;
const int COUNTY_CHANGE = 14;
int getYear(char* regPlate);
%}

%%
[0-9]{2}-({COUNTIES})-[0-9]{1,6} { 	int age = getYear(yytext);
					if(age <= CURRENT_YEAR && age >= NUMBER_CHANGE) 
						printf("INVALID\n");
					else if(age <= CURRENT_YEAR) 
						printf("%d\n", CURRENT_YEAR-age);	
					else 
						printf("%d\n", 100-age+CURRENT_YEAR);
				}		
[0-9]{2}[12]-({COUNTIES})-[0-9]{1,6} { 	int age = getYear(yytext); 
						if(age < NUMBER_CHANGE || age > CURRENT_YEAR) 
							printf("INVALID\n"); 
						else 
							printf("%d\n", CURRENT_YEAR-age); 
					}
[0-9]{2}-({OLD_COUNTIES})-[0-9]{1,6} { 	int age = getYear(yytext);
						if(age >= COUNTY_CHANGE && age <= CURRENT_YEAR)
							printf("INVALID\n"); 
						else 
							printf("%d\n", CURRENT_YEAR-age); 
						}
[0-9]{3}-T-[0-9]{1,6} { int age = getYear(yytext);
			if(age < COUNTY_CHANGE || age > CURRENT_YEAR)
				printf("INVALID\n"); 
			else 
				printf("%d\n", CURRENT_YEAR-age); 
			}
						
						
[ \t\n]		{ /*Throw it away */ }
[^ \t\n]+ 	{ printf("INVALID"); }

%%
int getYear(char* regPlate){
	char year[3]; 
	memcpy(year, &yytext[0], 2); 
	year[2] = '\0'; 
	return atoi(year); 
}

int main() {
	yylex();
	return 0;
}
