/* @author Philip Bradish 16339490 */

/* Reads in Roman numerals.*/


%{
# include "roman.tab.h"
void yyerror(char *s);
int yylex();
%}

%%
I  					{ return ONE; }
V  					{ return FIVE; }
X  					{ return TEN; }
L					{ return FIFTY; }
C					{ return ONE_HUNDRED; }
D					{ return FIVE_HUNDRED; }
M					{ return THOUSAND; }
[ \t]*				{ }
[^ \t\nIVXLCDM]*	{ printf("syntax error"); }
[\n]				{return EOL; }
%%

//(CM|M?C{0,3})(CD|D?C{0,3})(XL|L?X{0,3})(IV|V?I{0,3}) 
