%{

%}

QUOTE	["]

%%

"}"							{printf("syntax error\n"); exit(0);}
{QUOTE}(.|\n)*{QUOTE}	{printf("%s", yytext);}
"{"[^}]*"}"					{	}
"{"							{printf("syntax error\n"); exit(0);}
"**".*\n					{printf("\n");}
.							{printf("%s", yytext);}
\n							{printf("\n");}
%%

int main(){
	yylex();
	return 0;
}
