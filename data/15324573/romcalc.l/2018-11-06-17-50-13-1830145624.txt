%{
# include "romcalc.tab.h"
%}

%%

"+"	{return ADD;}
"-"	{return SUB;}
"*"	{return MUL;}
"/"	{return DIV;}
"|" {return ABS;}
"{" {return OP;}
"}" {return CP;}
I	{return I;}
V	{return V;}
X	{return X;}
L	{return L;}
C	{return C;}
D	{return D;}
M	{return M;}
Z	{return Z;}
\n	{return EOL;}
.	{yyerror("syntax error"); exit(0);}

%%

int main (void) {
	yyparse();
    return 0;
}