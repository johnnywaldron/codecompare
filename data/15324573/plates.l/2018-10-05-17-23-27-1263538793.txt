%{
#include <string.h>
#include <stdlib.h>
%}

SEQNUMBER  [0-9]{1,6}
NEWCOUNTY  C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW	
OLDCOUNTY  C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
YEAROLD  [[8[7-9]]|[[9|0][0-9]]|[1[0-2]]]
YEARNEW  [1[3-8][1|2]]

%%
{YEARNEW}	{printf("%s", yytext);}
{YEAROLD}	{printf("%s", yytext);}
{NEWCOUNTY}	{printf("%s", yytext);}
{OLDCOUNTY}	{printf("%s", yytext);}
{SEQNUMBER}	{printf("%s", yytext);}
{YEARNEW}"-"{NEWCOUNTY}"-"{SEQNUMBER}	{char * yr = (char*) malloc(2);strncpy(yr, yytext, 2);int year = atoi(yr); year = 18-year; printf("year\n");}
{YEAROLD}"-"{OLDCOUNTY}"-"{SEQNUMBER}	{char * yr = (char*) malloc(2);strncpy(yr, yytext, 2);int year = atoi(yr); year = 18-year; printf("year\n");}
.*	{printf("INVALID");}
\n	{printf("\n");}

%%

int main(){
	yylex();
	return 0;
}
