%{
#  include <stdio.h>
int yylex();
int yyparse();

int variables[26];

%}

%output "calcwithvariables.tab.c"

/* declare tokens */
%token ADD SUB MUL DIV NUM ID
%token EOL ASSIGN PRINT

%%

calclist: {}
 | calclist inst EOL  			{ }
 ;
 
inst: ID ASSIGN exp {variables[$1] = $3;}
 | PRINT ID {printf("%d\n", variables[$2]);}
 
exp: factor {$$ = $1;}
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: factor MUL NUM { $$ = $1 * $3; }
 | factor DIV NUM { $$ = $1 / $3; }
 | unary
 ;
 
unary: SUB unary { $$ = -$2;}
 | ID {$$ = variables[$1];}
 | NUM

 
%%
void yyerror(char *s){
  printf("%s\n", s);
}