%{
#  include <stdio.h>
int yylex();
int yyparse();
%}

%output "romcalc.tab.c"

/* declare tokens */
%token ADD SUB MUL DIV ABS
%token I V X L C D M Z
%token EOL

%%

calclist: {}
 | calclist one EOL  			{ printf("\n"); }
 ;
 
one:  four	{ while($1 >= 1) {$1 = $1 - 1; printf("I");} while($1 <= -1) { $1 = $1 + 1; printf("I");} $$ = $1; }
 ;
 
four:  five	{ while($1 >= 4) {$1 = $1 - 4; printf("IV");} while($1 <= -4) { $1 = $1 + 4; printf("IV");} $$ = $1; }
 ;
 
five:  nine	{ while($1 >= 5) {$1 = $1 - 5; printf("V");} while($1 <= -5) { $1 = $1 + 5; printf("V");} $$ = $1; }
 ;
 
nine:  tens	{ while($1 >= 9) {$1 = $1 - 9; printf("IX");} while($1 <= -9) { $1 = $1 + 9; printf("IX");} $$ = $1; }
 ;
 
tens:  fourty	{ while($1 >= 10) {$1 = $1 - 10; printf("X");} while($1 <= -10) { $1 = $1 + 10; printf("X");} $$ = $1; }
 ;
 
fourty:  fifty	{ while($1 >= 40) {$1 = $1 - 40; printf("XL");} while($1 <= -40) { $1 = $1 + 40; printf("XL");} $$ = $1; }
 ;
 
fifty:  ninety	{ while($1 >= 50) {$1 = $1 - 50; printf("L");} while($1 <= -50) { $1 = $1 + 50; printf("L");} $$ = $1; }
 ;
 
ninety:  hundred	{ while($1 >= 90) {$1 = $1 - 90; printf("XC"); } while($1 <= -90) { $1 = $1 + 90; printf("XC");} $$ = $1; }
 ;
 
hundred:  fourHundo	{ while($1 >= 100) {$1 = $1 - 100; printf("C"); } while($1 <= -100) { $1 = $1 + 100; printf("C");} $$ = $1; }
 ;
 
fourHundo:  fiveHundo	{ while($1 >= 400) {$$ = $1 - 400; printf("CD"); } while($1 <= -400) { $1 = $1 + 400; printf("CD");} $$ = $1; }
 ;
 
fiveHundo:  nineHundo	{ while($1 >= 500) {$1 = $1 - 500; printf("D"); } while($1 <= -500) { $1 = $1 + 500; printf("D");} $$ = $1; }
 ;
 
nineHundo:  thousands	{ while($1 >= 900) {$1 = $1 - 900; printf("CM"); } while($1 <= -900) { $1 = $1 + 900; printf("CM");} $$ = $1; }
 ;
 
thousands:  zero	{ while($1 >= 1000) {$1 = $1 - 1000; printf("M"); } if($1 < 0) { printf("-"); } while($1 <= -1000) { $1 = $1 + 1000; printf("M"); } $$ = $1; }
 ;
 
zero:  exp	{ if($1 == 0){ printf("Z"); } $$ = $1; }
 ;
 
exp: factor {$$ = $1;}
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term {$$ = $1;}
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: romanNum {$$ = $1;}
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 ;

romanNum: thousand hundo ten digit	{$$ = $1 + $2 + $3 + $4;}
 |hundo ten digit			{$$ = $1 + $2 + $3;}
 |ten digit					{$$ = $1 + $2;}
 |digit						{$$ = $1;}
 |Z		{$$ = 0;}
 ;

thousand: {$$ = 0;}
 | thousand M {$$ = $1 + 1000;}
 ;

hundo: {$$ = 0;}
 |shundo	{$$ = $1;}
 |C D		{$$ = 400;}
 |D shundo	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;
 
shundo: {$$ = 0;}
 |C			{$$ = 100;}
 |C C		{$$ = 200;}
 |C C C		{$$ = 300;}
 ;
 
ten: {$$ = 0;}
 |sten		{$$ = $1;}
 |X L		{$$ = 40;}
 |L sten	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;
 
sten: {$$ = 0;}
 |X			{$$ = 10;}
 |X X		{$$ = 20;}
 |X X X		{$$ = 30;}
 ;
 
digit: {$$ = 0;}
 |sdigit	{$$ = $1;}
 |I V		{$$ = 4;}
 |V sdigit	{$$ = 5 + $2;}
 |I X		{$$ = 9;}
 ;
 
sdigit: {$$ = 0;}
 |I			{$$ = 1;}
 |I I		{$$ = 2;}
 |I I I		{$$ = 3;}
 ;

%%
void yyerror(char *s){
  printf("%s\n", s);
}