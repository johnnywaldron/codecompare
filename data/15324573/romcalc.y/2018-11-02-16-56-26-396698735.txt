%{
#  include <stdio.h>
int yylex();
int yyparse();
%}

%output "romcalc.tab.c"

/* declare tokens */
%token ADD SUB MUL DIV ABS
%token I V X L C D M
%token EOL

%%

calclist: {}
 |calclist romanNum EOL  			{printf("%d\n", $2);}
 ;
 
exp: factor 
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term 
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: romanNum 
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 ;

romanNum: thousand hundo ten digit	{$$ = $1 + $2 + $3 + $4;}
 |hundo ten digit			{$$ = $1 + $2 + $3;}
 |ten digit					{$$ = $1 + $2;}
 |digit						{$$ = $1;}
 ;

thousand: {$$ = 0;}
 |M {$$ = 1000;}
 |M M {$$ = 2000;}
 |M M M {$$ = 3000;}
 ;

hundo: {$$ = 0;}
 |shundo	{$$ = $1;}
 |C D		{$$ = 400;}
 |D shundo	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;
 
shundo: {$$ = 0;}
 |C			{$$ = 100;}
 |C C		{$$ = 200;}
 |C C C		{$$ = 300;}
 ;
 
ten: {$$ = 0;}
 |sten		{$$ = $1;}
 |X L		{$$ = 40;}
 |L sten	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;
 
sten: {$$ = 0;}
 |X			{$$ = 10;}
 |X X		{$$ = 20;}
 |X X X		{$$ = 30;}
 ;
 
digit: {$$ = 0;}
 |sdigit	{$$ = $1;}
 |I V		{$$ = 4;}
 |V sdigit	{$$ = 5 + $2;}
 |I X		{$$ = 9;}
 ;
 
sdigit: {$$ = 0;}
 |I			{$$ = 1;}
 |I I		{$$ = 2;}
 |I I I		{$$ = 3;}
 ;

%%
void yyerror(char *s){
  printf("%s\n", s);
}