%{
# include "calcwithvariables.tab.h"
%}

%%

"+"	{return ADD;}
"-"	{return SUB;}
"*"	{return MUL;}
"/"	{return DIV;}
";"	{return EOL;}
":=" {return ASSIGN;}
[a-z] {return ID;}
[0-9] {return NUM;}
"print" {return PRINT;}
[ \t\n] { }
.	{yyerror("syntax error"); exit(0);}

%%

int main (void) {
	yyparse();
    return 0;
}