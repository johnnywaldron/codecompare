%{
#  include <stdio.h>
int yylex();
int yyparse();
%}

%output "roman.tab.c"

/* declare tokens */
%token ERROR
%token I V X L C D M
%token EOL

%%

calclist: /* nothing */ 	{}
 | romanNum EOL 			{printf("%d\n", $1);}
 | ERROR					{printf("syntax error\n"); exit(1);}
 ;

romanNum:					{$$ = 0;}
 |thousand hundo ten digit	{$$ = $1 + $2 + $3 + $4;}
 |hundo ten digit			{$$ = $1 + $2 + $3;}
 |ten digit					{$$ = $1 + $2;}
 |digit						{$$ = $1;}
 ;

thousand:	
 |M 		{$$ = 1000;}
 |M M		{$$ = 2000;}
 |M M M		{$$ = 3000;}
 ;

hundo:		
 |shundo	{$$ = $1;}
 |C D		{$$ = 400;}
 |D shundo	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;
 
shundo:		
 |C			{$$ = 100;}
 |C C		{$$ = 200;}
 |C C C		{$$ = 300;}
 ;
 
ten:		
 |sten		{$$ = $1;}
 |X L		{$$ = 40;}
 |L sten	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;
 
sten:		
 |X			{$$ = 10;}
 |X X		{$$ = 20;}
 |X X X		{$$ = 30;}
 ;
 
digit:		
 |sdigit	{$$ = $1;}
 |X L		{$$ = 40;}
 |L sten	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;
 
sdigit:		
 |I			{$$ = 10;}
 |I I		{$$ = 20;}
 |I I I		{$$ = 30;}
 ;

%%
int main()
{
  yyparse();
  return 0;
}