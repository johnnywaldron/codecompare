%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
int TH, H, T, O, TOTAL;
%}

%token I V X L C D M EOL
%%

romanIn: /* nothing */ {}
| romanIn expr EOL { printf("%d\n", TOTAL);  }
;

expr: thous hund ten one  {TOTAL = TH + H + T + O;}
| thous hund ten { TOTAL = TH + H + T;}
| thous hund one  {TOTAL = TH + H + O;}
| thous hund  {TOTAL = TH + H;}
| thous ten one {TOTAL = TH + T + O;}
| thous ten {TOTAL = TH + T;}
| thous one { TOTAL = TH + O;}
| thous { TOTAL = TH;}
| hund ten one  { TOTAL = H + T + O;}
| hund ten  { TOTAL = H + O;}
| hund one  { TOTAL = H + O;}
| hund  { TOTAL = H;}
| ten one { TOTAL = T + O;}
| ten {TOTAL = T;}
| one {TOTAL = O;}
;

thous: M M M {TH = 3000;}
| M M {TH = 2000;}
| M {TH = 1000;}
;

hund: C M {H = 900;}
| D C C C {H = 800;}
| D C C {H = 700;}
| D C {H = 600;}
| D {H = 500;}
| C D {H = 400;}
| C C C {H = 300;}
| C C {H = 200;}
| C {H = 100;}
;

ten: X C  {T = 90;}
| L X X X {T = 80;}
| L X X {T = 70;}
| L X {T = 60;}
| L {T = 50;}
| X L {T = 40;}
| X X X {T = 30;}
| X X {T = 20;}
| X {T = 10;}
;

one: I  {O = 1;}
| I I {O = 2;}
| I I I {O = 3;}
| I V {O = 4;}
| V {O = 5;}
| V I {O = 6;}
| V I I {O = 7;}
| V I I I {O = 8;}
| I X {O = 9;}
;


%%
void yyerror(char *s)
{
  printf("error: %s\n", s);
  exit(0);
}


int
main()
{
  yyparse();
  return 0;
}
