%{

#include "romcalc.tab.h"
void yyerror(char *s);
int yylex();
%}

%%
"I" { return ONE;}
"V" { return FIVE;}
"X" { return TEN;}
"L" { return FIFTY; }
"C" { return HUND; }
"D" { return FIVE_HUND; }
"M" { return THOUSAND; }
"Z" { return ZERO; }

 /*  calculator token */
"+" { return ADD; }
"-" { return SUB; }
"*" { return MULT; }
"/" { return DIV; }
"{" { return GROUP_START; }
"}" { return GROUP_END; }
\n {return EOL;}
[ ] { }
. { yyerror("syntax error");}

%%
