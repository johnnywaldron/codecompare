%{

#include "roman.tab.h"
void yyerror(char *s);
int yylex();
%}


%%
"I" { return ONE;}
"V" { return FIVE;}
"X" { return TEN;}
"L" { return FIFTY; }
"C" { return HUND; }
"D" { return FIVE_HUND; }
"M" { return THOUSAND; }
[ ] { }
\n {return EOL;}
"exit" { return exit_cmd;}

. { ECHO; yyerror("syntax error\n");}

%%
