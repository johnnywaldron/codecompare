%{
  
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  void yyerror(char *s);
  int yylex();
%}

%start line
%token exit_cmd

%token ONE FIVE TEN FIFTY HUND FIVE_HUND THOUSAND EOL

%%
line:	
        | line numeral EOL {printf("%d\n", $2);}
        ;

numeral : thousands hundreds tens ones {$$ = $1 + $2 + $3 +$4; } //1000 + 100 + 10 + 1
| thousands hundreds tens {$$ = $1 + $2 + $3 ;} // 1000 + 100 + 10
| thousands hundreds ones {$$ = $1 + $2 + $3;} // 1000 + 100 + 1
| thousands tens ones {$$ = $1 + $2 + $3;} //1000 + 10 + 1
| hundreds tens ones {$$ = $1 + $2 + $3;} // 100 + 10 + 1
| thousands hundreds {$$ = $1 + $2;} // 1000 + 100 
| thousands tens {$$ = $1 + $2 ;} //1000 + 10
| thousands ones {$$ = $1 + $2; } // 1000 + 1//
| hundreds tens {$$ = $1 + $2;} // 100 + 10
| hundreds ones {$$ = $1 + $2;}  // 100 + 1
| tens ones {$$ = $1 + $2;} //10 + 1
| thousands {$$ = $1;} // 1000
| hundreds {$$ = $1 ;} // 100
| tens {$$ = $1 ;} //10
| ones {$$ = $1 ;} //1
;

ones: ONE TEN { $$ = 9; }
|FIVE ONE ONE ONE { $$ = 8; }
|FIVE ONE ONE { $$ = 7; }
|FIVE ONE { $$ = 6; }
|FIVE {$$ = 5;}
|ONE FIVE {$$ = 4; }
|ONE ONE ONE { $$ = 3; }
|ONE ONE { $$ = 2; }
|ONE { $$ = 1; }
;

tens: TEN HUND { $$ = 90; }
|FIFTY TEN TEN TEN { $$ = 80; }
|FIFTY TEN TEN { $$ = 70; }
|FIFTY TEN { $$ = 60; }
|FIFTY { $$ = 50; }
|TEN FIFTY { $$ = 40; }
|TEN TEN TEN { $$ = 30 ; }
|TEN TEN { $$ = 20; }
|TEN { $$ = 10; }
;

hundreds: HUND THOUSAND {$$ = 900;}
|FIVE_HUND HUND HUND HUND {$$ = 800; }
|FIVE_HUND HUND HUND { $$ = 700; }
|FIVE_HUND HUND { $$ = 600; }
|FIVE_HUND {$$ = 500; }
|HUND FIVE_HUND {$$ = 400; }
|HUND HUND HUND {$$ = 300; }
|HUND HUND { $$ = 200; }
|HUND {$$ = 100; }
;

thousands: THOUSAND THOUSAND THOUSAND thousands { $$ = 3000 + $1;}
|THOUSAND THOUSAND THOUSAND {$$ = 3000; }
|THOUSAND THOUSAND {$$ = 2000; }
|THOUSAND {$$ = 1000; }
;
%%
int main(void) {
  yyparse();
  return 0;
}
void yyerror(char * s) {
  printf("%s", s); 
  exit(0); }
