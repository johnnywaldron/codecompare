%{
int count = 0;
%}

%%
[0-9]+ {
	switch (yytext[yyleng-1]) {
	case '0': 
	case '2': 
	case '4': 
	case '6': 
	case '8':
		count++;
		break;
	}
}
%%
int main()
{
	yylex();
	printf("%8d", count);
		return 0;
}
