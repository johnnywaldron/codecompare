%{

    #define YEAR3
    #define YEAR2
    #define COUNTY1
    #define COUNTY2
    #define NUMBER
    #define END
%}

YEAR3 131|132|141|142|151|152|161|162|171|172|181|182 /*3 digit year */
YEAR2 [0-9]{2}
COUNTY1 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTY2 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NUMBER [0-9]{1,6} /*plate number can be 1-6 digits long*/
END \n|\t|" "

%%
{YEAR3}[-]{COUNTY1}[-]{NUMBER}{END}+ {
      char year[2]; /*declare year variable */
      strncpy(year, yytext, 2); /*get first two digits of year */
      int y = atoi(year); /*convert into int */
      int diff = 18-y;
      printf("%d\n", diff);

}
{YEAR2}[-]{COUNTY2}[-]{NUMBER}{END}+ {
    char year[2];
    strncpy(year, yytext, 2);
    int y = atoi(year);
    int diff;
    if (y < 87){
      diff = 2018 - (2000+y);
    } else {
      diff = 2018 - (1900+y);
    }
    return ("%d\n", diff);
}
\n    { }
.+    { printf("INVALID\n");}

%%
int main()
{
  yylex();
  return 0;
}

