%{
  #define NUMBER
  #define COUNTYN
  #define COUNTY
  #define YEAR2
  #define YEAR3
  #define END
%}

NUMBER [0-9]{1,6}
COUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTYN C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
YEAR2 [0-9]{2}
YEAR3 [1]{1}[3-8]{1}[1-2]{1}
END       [\n|\t|" "]+
%%

{YEAR3}[-]{COUNTYN}[-]{NUMBER}{END} {
                         char year[2];
                         strncpy(year, yytext, 2); /*get first two digits of year */
                         int y = atoi(year); /*convert into int */
                         int diff = 2018-2000-y;
                         printf("%d\n", diff);
}


{YEAR2}[-]{COUNTY}[-]{NUMBER}{END} {
                        char year[2];
                        strncpy(year, yytext, 2);
                        int y = atoi(year);
                        int diff;
                        if (y < 87){
                          diff = 2018 - (2000+y);
                        } else {
                          diff = 2018 - (1900+y);
                        }
                        return ("%d\n", diff);
            } 
\n    { }
.+    { printf("INVALID\n");}

%%
int main()
{
  yylex();
  return 0;
}
