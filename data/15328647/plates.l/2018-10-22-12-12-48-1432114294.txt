%{
int num;
int current_year = 2018;
int reg_year;
int reg_length;
%}

%%

[0-9]+/"-"      {
                    num = ((*(yytext) - '0') * 10) + (*(yytext+1) - '0');
                    if(num <= 18)
                    {
                        reg_year = 2000 + num;
                    }else{
                        reg_year = 1900 + num;
                    }
                    reg_length = current_year - reg_year;
                    printf("%d", reg_length);
                }
                
.               { }
%%

int main()
{
    yylex();
    return 0;
}
