%{
int decade;
int year;
%}

%%


[\n|\s| |	a]*[1]{1}[3-8]{1}[1|2]{1}[-][A-Z]{1}[A-Z]?[-][1-9]{1}[0-9]{0,5}[\n|\s| |	]* {decade = (int)(yytext[0]-'0');
decade *=10;

year = (int)(yytext[1]-'0');

year = decade + year;

if(year > 18)
{
	year += 1900;
}
else year += 2000;

int result = 2018 - year;

printf("%d\n", result);
}

[\n|\s| |	]*[0-9]{2}[-][A-Z]{1,2}[-][1-9]{1}[0-9]{0,5}[\n|\s| |	]* {decade = (int)(yytext[0]-'0');
decade *=10;

year = (int)(yytext[1]-'0');

year = decade + year;

if(year > 18)
{
	year += 1900;
}
else year += 2000;

int result = 2018 - year;

printf("%d\n", result);
}

[\n|\s| |	a]*.*[\n|\s| |	]* {printf("INVALID\n");}
[\n|\s| |	]*[1]{1}[0-3]{1}[-]["T"][-][1-9]{1}[0-9]{0,5}[\n|\s| |	]* {printf("INVALID\n");}



%%

int main()
{
yylex();
return 0;
}
