%{
int decade;
int year;
%}

%%


[1]{1}[3-8]{1}[1|2]{1}[-][A-Z]{1}[A-Z]?[-][1-9]{1}[0-9]{0,5} { decade = (int)(yytext[0]-'0');
decade *=10;

year = (int)(yytext[1]-'0');

year = decade + year;

if(year > 18)
{
	year += 1900;
}
else year += 2000;

int result = 2018 - year;

printf("%d", result);
}
[0-9]{2}[-][A-Z]{1,2}[-][1-9]{1}[0-9]{0,5}	{ }


%%

int main()
{
yylex();
return 0;
}
