%{
%}

%%

[{](^r\n.*)+[}] { }
[{].*[}] { }
[{].*[^}] { printf("syntax error\n"); exit(0);}
[^{].*[}] { printf("syntax error\n"); exit(0);}
["*"]{2}.* { }
.*["{}"].*  { }

%%

int main()
{
yylex();
return 0;
}

