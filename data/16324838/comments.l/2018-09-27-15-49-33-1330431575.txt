%{
int bal = 0;
%}

%%

[{].*[}] { }
[{](\n.*)+[}] { }
["*"]{2}.* { }
["].*["] {printf(yytext);}
[{](.*)* { bal++;}
[}](.*)* {bal--;}

%%

int main()
{
yylex();
if(bal != 0)
{
printf("syntax error\n");
exit(0);
}
return 0;
}
