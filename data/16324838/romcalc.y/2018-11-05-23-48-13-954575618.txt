/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.y,v 2.1 2009/11/08 02:53:18 johnl Exp $
 * https://www.sanfoundry.com/c-program-convert-numbers-roman/
 */
 
/* simplest version of calculator */
 
%{
#  include <stdio.h>
#  include <string.h>
void predigit(char num1, char num2); 
void postdigit(char c, int n);
void printval(int number);
int yylex();
void yyerror(char *s);
int i = 0;
char romanval[1000];
%}
 
/* declare tokens */
%token NUMBER
%token L_CURLYBOI R_CURLYBOI
%token ADD SUB MUL DIV ABS
%token I II III IV V VI VII VIII IX X XX XXX XL L LX LXX LXXX XC C CC CCC CD D DC DCC DCCC CM M
%token EOL
%%
calclist: /* nothing */
 | calclist exp EOL {
    int number = $2;
    printval(number);
};
 
exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;
 
factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;
 
term: total
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 | L_CURLYBOI exp R_CURLYBOI { $$ = $2;};
 | L_CURLYBOI exp EOL {printf("syntax error\n"); return 0;};
 ;
 
total: total numeral { if ($1 < 1000 & $1+$2 > 1000)  {printf("syntax error\n"); return 0;}
               if (($1 >= 1000 && $2 >= 1000) || $1 != $2) $$ = $1 + $2;
                       else {printf("syntax error\n"); return 0;}
                     }
 | numeral {$$ = $1;}
;
 
numeral: I {$$ = $1;}
 | II   {$$ = $1;}
 | III  {$$ = $1;}
 | IV   {$$ = $1;}
 | V    {$$ = $1;}
 | VI   {$$ = $1;}
 | VII  {$$ = $1;}
 | VIII {$$ = $1;}
 | IX   {$$ = $1;}
 | X    {$$ = $1;}
 | XX   {$$ = $1;}
 | XXX  {$$ = $1;}
 | XL   {$$ = $1;}
 | L    {$$ = $1;}
 | LX   {$$ = $1;}
 | LXX  {$$ = $1;}
 | LXXX {$$ = $1;}
 | XC   {$$ = $1;}
 | C    {$$ = $1;}
 | CC   {$$ = $1;}
 | CCC  {$$ = $1;}
 | CD   {$$ = $1;}
 | D    {$$ = $1;}
 | DC   {$$ = $1;}
 | DCC  {$$ = $1;}
 | DCCC {$$ = $1;}
 | CM   {$$ = $1;}
 | M    {$$ = $1;}
;
 
%%
int main()
{
  yyparse();
  return 0;
}
 
void yyerror(char *s)
{
 
}
 
void predigit(char num1, char num2)
 
{
    romanval[i++] = num1;
    romanval[i++] = num2;
}

void printval(int number)
{
	if(number<0){printf("-"); number= number*-1;}
    	if(number==0)
    	{
    		printf("Z\n");
    		i =0;
    		memset(romanval,0, sizeof romanval);
    	}
    	else{
       	 while (number != 0)
       	 {
       	    if (number >= 1000)
       	    {
       	     postdigit('M', number / 1000);
       	     number = number - (number / 1000) * 1000;
       	    }
       	    else if (number >= 500)
       	    {
       	     if (number < (500 + 4 * 100))
       	     {
       	         postdigit('D', number / 500);
       	         number = number - (number / 500) * 500;
       	     }
       	     else
       	     {
       	         predigit('C','M');
       	         number = number - (1000-100);
       	     }
       	    }
       	    else if (number >= 100)
       	    {
       	     if (number < (100 + 3 * 100))
       	     {
       	         postdigit('C', number / 100);
       	         number = number - (number / 100) * 100;
       	     }
       	     else
       	     {
       	         predigit('L', 'D');
       	         number = number - (500 - 100);
       	     }
       	    }
       	    else if (number >= 50 )
       	    {
       	     if (number < (50 + 4 * 10))
       	     {
       	         postdigit('L', number / 50);
       	         number = number - (number / 50) * 50;
       	     }
       	     else
       	     {
       	         predigit('X','C');
       	         number = number - (100-10);
       	     }
       	    }
       	    else if (number >= 10)
       	    {
       	     if (number < (10 + 3 * 10))
       	     {
       	         postdigit('X', number / 10);
       	         number = number - (number / 10) * 10;
       	     }
       	     else
       	     {
       	         predigit('X','L');
       	         number = number - (50 - 10);
       	     }
       	    }
       	else if (number >= 5)
           {
       	     if (number < (5 + 4 * 1))
       	     {
       	         postdigit('V', number / 5);
       	         number = number - (number / 5) * 5;
       	     }
       	     else
       	     {
       	         predigit('I', 'X');
       	         number = number - (10 - 1);
       	     }
       	    }
       	    else if (number >= 1)
       	    {
       	     if (number < 4)
       	     {
       	         postdigit('I', number / 1);
       	         number = number - (number / 1) * 1;
       	     }
       	     else
       	     {
       	         predigit('I', 'V');
       	         number = number - (5 - 1);
       	     }
       	    }
       	   }
    	printf("%s\n", romanval);
    	i=0;
    	memset(romanval,0, sizeof romanval);
	}
}
 
void postdigit(char c, int n)
{
    int j;
    for (j = 0; j < n; j++)
        romanval[i++] = c;
}
