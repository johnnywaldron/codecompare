%{
# include "calcwithvariables.tab.h"
void yyerror(char *s);
%}

VAR A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|\n

%%
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"|"     { return ABS; }
":="	{ return ASS; }
a	{ return A;   }
b	{ return B;   }
c	{ return C;   }
d	{ return D;   }
e	{ return E;   }
f	{ return F;   }
g	{ return G;   }
h	{ return H;   }
i	{ return I;   }
j	{ return J;   }
k	{ return K;   }
l	{ return L;   }
m	{ return M;   }
n	{ return N;   }
o	{ return O;   }
p	{ return P;   }
q	{ return Q;   }
r	{ return R;   }
s	{ return S;   }
t	{ return T;   }
u	{ return U;   }
v	{ return V;   }
w	{ return W;   }
x	{ return X;   }
y	{ return Y;   }
z	{ return Z;   }
[0-9]	{yylval = atoi(yytext); return NUMBER;}
	/* [0-9]+	{yyval = atoi(yytext); return NUMBER;} */


\n      { return EOL; }
[ \t]   { /* ignore white space */ }
.	{ yyerror("Mystery character\n");}
%%

