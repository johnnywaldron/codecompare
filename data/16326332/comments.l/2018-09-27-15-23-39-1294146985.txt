%{
int error = 0;
int multiLineComment = 0;
int singleLineComment = 0;
int comment = 0;
int quote = 0;
%}

%%

"{"	{if(comment==0&&quote==0){comment=1; multiLineComment=1;} 
	else if(quote==1&&error==0){printf("%s", yytext);}}
	
"}" {if(comment==0&&quote==0){error=1;}
	elseif((comment==1)&&(multiLineComment==1)){comment=0; multiLineComment=0;}
	elseif(quote==1&&error==0){printf("%s", yytext);}} 
	
"**" {if(comment==0&&quote==0){singleLineComment=1; comment=1;}
	elseif(quote==1&&error==0){printf("%s", yytext);}}
	
["] {if(quote==0&&comment==0){quote=1;}
	elseif(comment==0&&quote==1){quote=0;}
	
\n {if(singleLineComment==1&&error==0){singleLineComment=0; comment=0; printf("%s", yytext);} 
	elseif(comment==0&&error==0){printf("%s", yytext);}}
	
.  {if(comment==0&&error==0){printf("%s", yytext);}}

%%

int main()
{
  yylex();
  if(error==1)
  {
	printf("%s", yytext);
  }
  return 0;
}
