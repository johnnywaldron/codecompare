%{
# include "romcalc.tab.h"

void yyerror(char *s);
int yylex();
int result = 0;
int value = 0;
int first = 0;

%}

%%
"I" { value += 1; return I; }
"IV" { value += 4; return IV; }
"V" { value += 5; return V; }
"IX"  { value += 9; return IX; }
"X" { value += 10; return X; }
"XL" { value += 40; return XL; }
"L" { value += 50;return L; }
"XC"  { value += 90; return XC; }
"C" { value += 100; return C; }
"CD" { value += 400; return CD; }
"D" { value += 500; return D; }
"CM"  { value += 900; return CM; }
"M" { value += 1000; return M; }
"+" {if(result == 0){result = value;} value = 0; return ADD; }
"-" {if(result == 0){result = value;}value = 0; return SUB; }
"*" {if(result == 0){result = value;}value = 0; return MUL; }
"/" {if(result == 0){result = value;}value = 0; return DIV; }
"{" {return OPEN; }
"}" {return CLOSE; }
\n  { return EOL; }
.	{ yyerror("syntax error"); }
%%