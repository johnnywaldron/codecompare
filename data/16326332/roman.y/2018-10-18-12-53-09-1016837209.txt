%{ 
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
extern int value;
%}
%output "roman.tab.c"

%token I IV V IX X XL L XC C CD D CM M
%token EOL
%%

calclist: /* nothing */ {}
| calclist expr EOL { printf("%d\n", value);  }
;

expr: single | double | triple | M | M triple | M double | M single
;
 
single: I | I one | IV | V | V one | IX
;
 
one: I | I one
;

double: X | X ten | X single | XL | XL single | L | L ten | L single | XC | XC single 
;

ten: X | X ten | X single
;

triple: C | C hundred | C double | C single | CD | CD double | CD single | D | D hundred | D double | D single | CM | CM double | CM single
;

hundred: C | C hundred | C double | C single
;

%%
void yyerror(char *s)
{
  printf("error: %s\n", s);
}


int
main()
{
//  yydebug = 1;
  yyparse();
  return 0;
}