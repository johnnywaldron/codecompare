%{ 
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
extern int result;
extern int first;
extern int value;
int error = 0;
%}
%output "romcalc.tab.c"

%token I IV V IX X XL L XC C CD D CM M ADD SUB MUL DIV OPEN CLOSE
%token EOL
%%

romancalc: /*  */ {}
| romancalc expr EOL { romanconvert(result); printf("\n"); result = 0; }
;

expr: number {result = value} | number {result = value} calc
;

calc: addition | addition calc | subtraction | subtraction calc | multiplication | multiplication calc | division | division calc
;

addition: ADD number {result = result + value;}
;

subtraction: SUB number {result = result - value;}
;

multiplication: MUL number {result = result * value;}
;

division: DIV number {result = result / value;}
;

number: single | double | triple | quadruple
;
 
single: I | I one | IV | V | V one | IX
;
 
one: I | I one
;

double: X | X ten | X single | XL | XL single | L | L ten | L single | XC | XC single 
;

ten: X | X ten | X single
;

triple: C | C hundred | C double | C single | CD | CD double | CD single | D | D hundred | D double | D single | CM | CM double | CM single
;

hundred: C | C hundred | C double | C single
;

quadruple: M | M quadruple | M triple | M double | M single
;

%%
void yyerror(char *s)
{
	if(error == 0)
	{
	  printf("%s\n", s);
	  error=1;
	}
	
}

int romanconvert(int val){
	int num = val;
	if(num==0)
	{
		printf("Z");
	}
	else if(num<0)
	{
		num = num * (-1);
		printf("-");
	}
    while (num > 0) {
		if (num >= 1000) {
            printf("M");
            num = num - 1000;
        } 
		else if (num >= 500) {           
			if (num >= 900) {
                printf("CM");
                num = num - 900;
            }
			else {
                printf("D");
                num = num - 500;
            }
        } 
		else if (num >= 100) {
            if (num >= 400) {
                printf("CD");
                num = num - 400;
            } 
			else {
                printf("C");
                num = num - 100;
			}
        } 
		else if (num >= 50) {
            if (num >= 90) {
				printf("XC");
                num = num - 90;
            } 
			else {
                printf("L");
                num = num - 50;
            }
        } 
		else if (num >= 9) {
            if (num >= 40) {
				printf("XL");
                num = num - 40;
            } 
			else if (num == 9) {
				printf("IX");
                num = num - 9;
            } 
			else 
                printf("X");
                num = num - 10;
            
        } 
		else if (num >= 4) 
		{
            if (num >= 5) {
                printf("V");
                num = num - 5;
            } 
			else {
                printf("IV");
                num = num - 4;
            }
        } 
		else {
            printf("I");
            num = num - 1;
        }
    }
  }

int
main()
{
//  yydebug = 1;
  yyparse();
  return 0;
}