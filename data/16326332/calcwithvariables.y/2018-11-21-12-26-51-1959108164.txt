%{ 
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();
extern int value;
extern char ch;
int error = 0;
int i = 0;
int j = 0;
int var[26];
%}
%output "calcwithvariables.tab.c"

%token NUM EQU ADD SUB MUL DIV SEMI VAR SPACE PRINT
%token EOL
%%

romancalc: /*  */ {}
| romancalc line SEMI {}
;

line: print
| VAR {i = (int)ch - 'a'; j = i;} EQU expr {var[j] = $4;}
;

print: PRINT SPACE VAR {i = (int)ch - 'a'; printf("%d\n", var[i]);}
;


expr: factor
 | expr ADD factor { $$ = $1+$3;}
 | expr SUB factor { $$ = $1-$3;}
 ;

factor: term
 | factor MUL term { $$ = $1*$3; }
 | factor DIV term { $$ = $1/$3; }
;

term: NUM { $$ = value;}
| VAR { i = (int)ch - 'a'; $$ = var[i];}
;

%%
void yyerror(char *s)
{
	if(error == 0)
	{
	  printf("%s\n", s);
	  error=1;
	}
	
}
int
main()
{
//  yydebug = 1;
  yyparse();
  return 0;
}