%{
int year = 0;
int yearsAgo = -1;
int extraChar = 0;
%}

COUNTY14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTY87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
YEAR14 [1][4-8][1-2]
YEAR87 [1][3][1-2]|[8][7-9]|[0|9][0-9]|[1][1-2]
SEQUENCE [0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]?
%%

({YEAR14})([-])({COUNTY14})([-])({SEQUENCE}) {valid =1; year = atoi(strtok(yytext, "-")); if(year > 100){year = year/10;}yearsAgo = 18-year;}

({YEAR87})([-])({COUNTY87})([-])({SEQUENCE})  {valid =1; year = atoi(strtok(yytext, "-")); if(year > 100){year = (year/10)+2000;} else if(year>50){year+=1900;}else{year+=2000;}yearsAgo = 2018-year;}

[' '\r\t] {}

. {extraChar = 1;}

\n {if(extraChar==0 && yearsAgo>=0){printf("%d\n", yearsAgo); yearsAgo=-1;} else if(extraChar==1){printf("INVALID\n"); extraChar=0; yearsAgo=-1;}}


%%

int main()
{
  yylex();
  return 0;
}
