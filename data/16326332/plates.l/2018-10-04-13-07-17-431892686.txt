%{
int year = 0;
int yearsAgo = -1;
int valid = 0;
int nonEmptyLine = 0;
%}

COUNTY14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTY87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
YEAR14 [1][4-8][1-2]
YEAR87 [1][3][1-2]|[8][7-9]|[0|9][0-9]
SEQUENCE [0-9][0-9]?[0-9]?[0-9]?[0-9]?[0-9]?
%%

({YEAR14})([-])({COUNTY14})([-])({SEQUENCE}) {valid =1; year = atoi(strtok(yytext, "-")); if(year > 100){year = year/10;}yearsAgo = 18-year;}

({YEAR87})([-])({COUNTY87})([-])({SEQUENCE})  {valid =1; year = atoi(strtok(yytext, "-")); if(year > 100){year = (year/10)+2000;} else{year+=1900}yearsAgo = 2018-year;}

. {nonEmptyLine = 1;}

\n {if(nonEmptyLine==0 && yearsAgo>=0){printf("%s\n", yytext); yearsAgo=-1;} else if(nonEmptyLine==1){printf("INVALID\n");}}


%%

int main()
{
  yylex();
  return 0;
}
