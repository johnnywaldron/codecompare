%{
#  include <stdio.h>
int yylex();
void yyerror(char *s);
int result = 0;
%}

/* declare tokens */
%token ONE FOUR FIVE NINE TEN
%token FORTY FIFTY NINETY
%token OHUNDRED FOHUNDRED FIHUNDRED NHUNDRED
%token OTHOUSAND 
%token EOL

%%
input: /* empty */
 | input line

line: EOL
 | exp EOL { printf("%d\n", result); result = 0;}
 ; 

exp: numeral 
   | exp numeral {;}
 ;

numeral: ONE {result+=1;}
 | FOUR {result+=4;}
 | FIVE {result+=5;}
 | NINE {result+=9;}
 | TEN  {result+=10;}
 | FORTY {result+=40;}
 | FIFTY {result+=50;}
 | NINETY {result+=90;}
 | OHUNDRED {result+=100;}
 | FOHUNDRED {result+=400;}
 | FIHUNDRED {result+=500;}
 | NHUNDRED {result+=900;}
 | OTHOUSAND {result+=1000;}
 ;
%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}
