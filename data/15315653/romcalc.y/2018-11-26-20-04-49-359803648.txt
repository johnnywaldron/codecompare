%{
#  include <stdio.h>
int yylex();
void yyerror(char *s);
int result = 0;
%}

/* declare tokens */
%token ONE TWO THREE FOUR FIVE SIX SEVEN EIGHT NINE TEN
%token TWENTY THIRTY FORTY FIFTY SIXTY SEVENTY EIGHTY NINETY OHUNDRED
%token THUNDRED THHUNDRED FHUNDRED FIHUNDRED SHUNDRED SEHUNDRED EHUNDRED NHUNDRED
%token OTHOUSAND 
%token ZERO
%token OPEN CLOSE
%token ADD SUB MUL DIV
%token EOL

%%
input: /* empty */
 | input line
 ;

line: EOL
 | exp EOL {printf("%d\n", $1);}
 ; 

exp: factor
   | exp ADD factor {$$ = $1 + $3}
   | exp SUB factor {$$ = $1 - $3}
   | OPEN exp CLOSE {$$ = $2}
   ;

factor: term 
      | factor MUL term {$$ = $1 * $3}
      | factor DIV term {$$ = $1 / $3}
      | OPEN term CLOSE {$$ = $2}
      ;

term: number {$$ = result; result=0;}
    | OPEN term CLOSE {$$ = $2}
    ;

number: ZERO {result+=0;}
 |    thousands hundreds tens units 
 ;

thousands: OTHOUSAND {result+=1000;}
 | OTHOUSAND thousands {result+=1000;}
 | {;}
 ;

hundreds: OHUNDRED {result+=100;}
	| THUNDRED {result+=200;}
	| THHUNDRED {result+=300;}
	| FHUNDRED {result+=400;}
	| FIHUNDRED {result+=500;}
	| SHUNDRED {result+=600;}
	| SEHUNDRED {result+=700;}
	| EHUNDRED {result+=800;}
	| NHUNDRED {result+=900;}
	| {;}
	;

tens: TEN {result+=10;}
 | TWENTY {result+=20;}
 | THIRTY {result+=30;}
 | FORTY {result+=40;}
 | FIFTY {result+=50;}
 | SIXTY {result+=60;}
 | SEVENTY {result+=70;}
 | EIGHTY {result+=80;}
 | NINETY {result+=90;}
 | {;}
 ;

units: ONE {result+=1;}
 | TWO {result+=2;}
 | THREE {result+=3;}
 | FOUR {result+=4;}
 | FIVE {result+=5;}
 | SIX {result+=6;}
 | SEVEN {result+=7;}
 | EIGHT {result+=8;}
 | NINE {result+=9;}
 | {;}
 ;

%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
}
