/* Ultan O Morain 15315653 Assignment 4 CS3071
 * See text files for sample input
 * Read in the input
 * 
 * Takes a series of roman numerals, parses them and takes semantic actions to 
 * print the corresponding Hindu-Arabic numeral followed by a new line for each one
 *
 * Input is newline separate list of Romann numerals
 * Output is corresponding Hindu-Arabic numeral followed by a new line for each one
 * NOTE: Must check that the numeral is valid
 * 
 * NOTE: yytext is the matching
 */

%{
# include "roman.tab.h"
void yyerror(char *s);
%}

%%
I	{ return ONE; }
IV	{ return FOUR; }
V       { return FIVE; }
IX      { return NINE; }
X       { return TEN; }
XL      { return FORTY; }
L       { return FIFTY; }
XC      { return NINETY;  }
C       { return OHUNDRED; }
CD      { return FOHUNDRED; }
D       { return FIHUNDRED; }
CM      { return NHUNDRED; }
M       { return OTHOUSAND; }

\n      { return EOL; }
[ \t]   { /* ignore white space */ }
.	{ yyerror("Mystery character\n");}
%%
