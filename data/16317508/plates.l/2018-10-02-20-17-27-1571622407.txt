%{
	char* txt;
	const char* delim = "-";
	char* token;
	int isValid;
	int year;
	int isOld;
	int i;
	int j;
	char* newReg[26];
	char* oldReg[4];


%}
%%
\n					{}
\t					{}
[ ]*					{}				
[0-9]*					{}
[A-Z]*					{}
[0-9]{2,3}"-"[A-Z]{1,2}"-"[0-9]{1,6} {	txt = yytext;
					token = strtok(txt, delim);
					year = atoi(&token[0]);
					if(strlen(&token[0])==2){
						isOld = 1;
						if(year >= 87 && year <= 99){
							year += 1900;	
						}					
						else if(year >= 0 && year <= 12){
						year += 2000;
					}
					}
					else if(strlen(&token[0])==3){
						isOld = 0;
						if(year%2==1){
						year -= 1;						
						}
						else if(year%2==0){
						year -= 2;
						}
						year /= 10;
						year += 2000;
												
					}
					if(isOld == 0){
					for(i = 0; i < 26; i++){
					if(strcmp((const char*)&token[4],(const char*)&newReg[i])== 0){
					isValid = 1;
					break;
					}
					else if(i == 25){
						printf("INVALID\n");
					}
					}
					}
					else if(isOld == 1){
					for(j = 0; j < 26; j++){
					if(strcmp((const char*)&token[4],(const char*)&newReg[21])== 0){
						printf("INVALID\n");
						break;
					}
					else if(strcmp((const char*)&token[4],(const char*)&newReg[j]) == 0){	
					isValid = 1;							
					break;
					}
					}
					if(isValid != 1){
					for(j = 0; j < 4; j++){
					if(j == 3){
						printf("INVALID\n");
					}
					else if(strcmp((const char*) &token[4],(const char*) &oldReg[j]) ==0){
					isValid = 1;
					break;
					}
					}
					}
					}
					if(isValid == 1){
					year = 2018-year;
					printf("%d\n", year);
					}
					}
%%
int main(){
	newReg[0] = "C";
	newReg[1] = "CE";
	newReg[2] = "CN";
	newReg[3] = "CW";
	newReg[4] = "D";
	newReg[5] = "DL";
	newReg[6] = "G";
	newReg[7] = "KE";
	newReg[8] = "KK";
	newReg[9] = "KY";
	newReg[10] = "L";
	newReg[11] = "LD";
	newReg[12] = "LH";
	newReg[13] = "LM";
	newReg[14] = "LS";
	newReg[15] = "MH";
	newReg[16] = "MN";
	newReg[17] = "MD";
	newReg[18] = "OY";
	newReg[19] = "RN";
	newReg[20] = "SO";
	newReg[21] = "T";
	newReg[22] = "W";
	newReg[23] = "WH";
	newReg[24] = "WX";
	newReg[25] = "WW";
	oldReg[0] = "LK";
	oldReg[1] = "TN";
	oldReg[2] = "TS";
	oldReg[3] = "WD";
	yylex();
	return 0;
}
