%{
	char* txt;
	const char* delim = "-";
	char* token;
	int year;
	int isOld;
	char newRegistration[26];
%}
%%
\n					{}
\t					{}
[ ]*					{}				
[0-9]*					{}
[A-Z]*					{}
[0-9]{2,3}"-"[A-Z]{1,2}"-"[0-9]{1,6} {	txt = yytext;
					token = strtok(txt, delim);
					year = atoi(&token[0]);
					if(strlen(&token[0])==2){
						isOld = 1;
						if(year >= 87 && year <= 99){
							year += 1900;	
						}					
						else if(year >= 0 && year <= 12){
						year += 2000;
					}
					}
					else /*if(strlen(&token[0])==3)*/{
						isOld = 0;
						if(year%2==1){
						year -= 1;						
						}
						else if(year%2==0){
						year -= 2;
						}
						year /= 10;
						year += 2000;
												
					}
					year = 2018-year;
					printf("%d\n", year);
					}
%%
int main(){
	yylex();
	return 0;
}
