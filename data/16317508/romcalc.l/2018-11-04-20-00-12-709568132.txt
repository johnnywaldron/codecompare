%{
#include "romcalc.tab.h"
void yyerror(char *s);
int yylex();
int yyparse();
%}

%%
[I]	{return ONE;}
[V]	{return FIVE;}
[X]	{return TEN;}
[L]	{return FIFTY;}
[C]	{return HUNDRED;}
[D]	{return FHUNDRED;}
[M]	{return THOUSAND;}
[*] 	{return yytext[0];}
[/] 	{return yytext[0];}
[+] 	{return yytext[0];}
[-] 	{return yytext[0];}
\n	{return EOL;}
.	{yyerror("syntax error");}
%%