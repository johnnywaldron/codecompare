%{
#include <stdio.h>
#include <stdlib.h>
int yyparse();
int yyerror();
int yylex();
int number = 0;
%}

%token UN CINQ DIX CINQUANTE CENT CINQCENT MILLE EOL

%%
numeral:
| numeral hundredtothousand tentohundred onetoten EOL	{number = $1 + $2 + $3;
							if(number != 0){
							printf("%d\n",number);
							number = 0;} 
							}
;

ones: 		{$$ = 0;}
| UN		{$$ = 1;}
| UN UN  	{$$ = 2;}
| UN UN UN	{$$ = 3;} 		 
;

bfiveten: UN CINQ {$$ = 4;}
| UN DIX	{$$ = 9;}
;

onetoten: ones
| bfiveten
| CINQ ones {$$ = 5 + $2;}
;

tens:		{$$ = 0;}
| DIX		{$$ = 10;}
| DIX DIX  	{$$ = 20;}
| DIX DIX DIX 	{$$ = 30;}
;

bfiftyhundred: DIX CINQUANTE {$$ = 40;}
| DIX CENT		{$$ = 90;}
;

tentohundred: tens
| bfiftyhundred
| CINQUANTE tens {$$ = 50 + $2;}
;

hundreds:	{$$ = 0;}
| CENT		{$$ = 100;} 
| CENT CENT	{$$ = 200;}
| CENT CENT CENT {$$ = 300;}
;

bninehthousand: CENT CINQCENT {$$ = 400;}
| CENT MILLE 	{$$ = 900;}
;

hundredtothousand: hundreds
| bninehthousand
| CINQCENT hundreds {$$ = 500 + $2;}
;

%%
int main(){
	yyparse();
        return 0;
}

int yyerror(char *s)
{
  printf("%s\n", s);
  return 0;
}