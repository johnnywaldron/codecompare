%{
#include "calcwithvariables.tab.h"
void yyerror(char *s);
int yylex();
int yyparse();
%}

%%
[\n] {}
"print" {return PRINT;}
[a-z] {yylval = yytext[0]; return VAR;}
":=" {return ASSIGN;}
":="\-[0-9] {yylval = atof(yytext[2]); return NEGNUM;}
[0-9] {yylval = atof(yytext); return NUM;}
[+] {return PLUS;}
[-] {return MINUS;}
[/] {return DIV;}
[*] {return MUL;}
[;] {return SEMIC;} 
[ \t] {}
. {yyerror("syntax error");}
%%
