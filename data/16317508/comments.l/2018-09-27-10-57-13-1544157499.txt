%%
"\"".*\n*"\""                           {}
[^\"]"**".*\n[^\"]                      {yytext = "\n";}
[^\"]"{"[0-9A-z \t\n]*"}"[^\"]          {yytext = "\n";}
"{"[0-9A-z \t\n]*EOF                    {printf("syntax error\n");
                                                exit(EXIT_SUCCESS);}
[0-9A-z \t\n]*"}"                       {printf("syntax error\n");
                                                exit(EXIT_SUCCESS);}
%%

int main(){
	yylex();
        return 0;
}                
