%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yyparse();
int yyerror();
char* convertRoman();
int yylex();
int number = 0;
int runningTotal = 0;
char* result;
%}

%token ONE FIVE TEN FIFTY HUNDRED FHUNDRED THOUSAND SYMBOL NUM EOL

%%
calclist: /* nothing */
| calclist expr EOL		{runningTotal = $2; 
						if(runningTotal == 0){
						printf("Z");
						}
						else{
						result = convertRoman(runningTotal); printf("%s\n",result);}}
| calclist EOL			{}
;

expr: factor      
| expr '+' factor {$$ = $1 + $3;}
| expr '-' factor {$$ = $1 - $3;}
;

factor: term 	  
| factor '*' term {$$ = $1 * $3;}
| factor '/' term {$$ = $1 / $3;}
;

term: numeral
| '{' expr '}' {$$ = $2;}
| '-' term     {$$ = -1 * $2;}
;

numeral: athousands hundredtothousand tentohundred onetoten {$$ = $1 + $2 + $3 + $4;}

ones: 		{$$ = 0;}
| ONE		{$$ = 1;}
| ONE ONE  	{$$ = 2;}
| ONE ONE ONE	{$$ = 3;} 		 
;

bfiveten: ONE FIVE {$$ = 4;}
| ONE TEN	{$$ = 9;}
;

onetoten: ones
| bfiveten
| FIVE ones {$$ = 5 + $2;}
;

tens:		{$$ = 0;}
| TEN		{$$ = 10;}
| TEN TEN  	{$$ = 20;}
| TEN TEN TEN 	{$$ = 30;}
;

bfiftyhundred: TEN FIFTY {$$ = 40;}
| TEN HUNDRED		{$$ = 90;}
;

tentohundred: tens
| bfiftyhundred
| FIFTY tens {$$ = 50 + $2;}
;

hundreds:	{$$ = 0;}
| HUNDRED		{$$ = 100;} 
| HUNDRED HUNDRED	{$$ = 200;}
| HUNDRED HUNDRED HUNDRED {$$ = 300;}
;

bninehthousand: HUNDRED FHUNDRED {$$ = 400;}
| HUNDRED THOUSAND	{$$ = 900;}
;

hundredtothousand: hundreds
| bninehthousand
| FHUNDRED hundreds {$$ = 500 + $2;}
;

athousands: {$$ = 0;}
| THOUSAND athousands {$$ = 1000 + $2;}
;

%%
int main(){
	yyparse();
        return 0;
}
// Credit to https://stackoverflow.com/questions/23269143/c-program-that-converts-numbers-to-roman-numerals
char* convertRoman(int num){
    int del[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1}; // Key value in Roman counting
    char * sym[] = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" }; //Symbols for key values
    char* res;
    res = malloc(64);
    int i = 0;                   //
    while(num){                 //while input number is not zero
        while(num/del[i]){      //while a number contains the largest key value possible
            strcat(res, sym[i]); //append the symbol for this key value to res string
            num -= del[i];       //subtract the key value from number
        }
        i++;                     //proceed to the next key value
    }
    return res;	
}

int yyerror(char *s)
{
  printf("%s\n", s);
  return 0;
}