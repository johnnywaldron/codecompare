%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yyparse();
void yyerror();
int yylex();
int store[26];
int result = 0;
int temp = 0;
%}

%token PLUS MINUS DIV MUL SEMIC ASSIGN PRINT VAR NUM EOL
%%

calclist: /* nothing*/
| calclist VAR ASSIGN expr SEMIC {int index = $2 -97; store[index] = $4;}
| calclist PRINT VAR SEMIC {int index = $3 - 97; temp = store[index]; printf("%d\n", temp);}
;

expr: factor
| expr PLUS factor {$$ = $1 + $3;}
| expr MINUS factor {$$ = $1 - $3;}
;

factor: term
| factor MUL term {$$ = $1 * $3;}
| factor DIV term {$$ = $1 / $3;}
;

term: NUM
| VAR           {int index = $1 - 97; $$ = store[index];}
| '-' term     {$$ = -1 * $2;}
;



%%
int main(){
    memset(store,0,26);
    yyparse(); 
    return 0;
}
void yyerror(char *s)
{
	printf("%s\n", s);
	exit(EXIT_SUCCESS);
}