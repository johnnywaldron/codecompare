%{
#include "roman.tab.h"
void yyerror(char *s);
int yylex();
int yyparse();
%}

%%
"I"	{return UN;}
"V"	{return CINQ;}
"X"	{return DIX;}
"L"	{return CINQUANTE;}
"C"	{return CENT;}
"D"	{return CINQCENT;}
"M"	{return MILLE;}
\n	{return EOL;}
.	{yyerror("syntax error");}
%%