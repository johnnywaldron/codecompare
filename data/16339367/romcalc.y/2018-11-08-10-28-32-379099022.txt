%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_STRING_LEN 80

int yylex();
void yyerror(char *s);
char *toRoman(int n);
%}

/*%error-verbose*/

%token ONE FIVE TEN FIFTY ONE_HUNDRED FIVE_HUNDRED THOUSAND
%token ADD SUB MUL DIV OP CP
%token EOL FAIL

%%

calclist: /* Nothing */
 | calclist FAIL EOL { printf("syntax error\n"); }
 | calclist expr EOL {
    char *result;
    result = toRoman($2);

    printf("%s\n", result);
    free(result);
 }
 ;

expr: factor
 | expr ADD factor { $$ = $1 + $3; }
 | expr SUB factor { $$ = $1 - $3; }
 ;

factor: term
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: number 
 | OP expr CP { $$ = $2; }
 ;

number:
 thousands hundreds tens units {
    $$ = $1 + $2 + $3 + $4;  
 }
 ;

thousands: { $$ = 0; }
 | THOUSAND { $$ = 1000; }
 | THOUSAND THOUSAND { $$ = 2000; }
 | THOUSAND THOUSAND THOUSAND { $$ = 3000; }
 ;

hundreds: { $$ = 0; }
 | hundreds_0 { $$ = $1; }
 | ONE_HUNDRED FIVE_HUNDRED { $$ = 400; }
 | FIVE_HUNDRED { $$ = 500; }
 | FIVE_HUNDRED hundreds_0 { $$ = 500 + $2; }
 | ONE_HUNDRED THOUSAND { $$ = 900; }
 ;

hundreds_0:
 ONE_HUNDRED { $$ = 100; }
 | ONE_HUNDRED ONE_HUNDRED { $$ = 200; }
 | ONE_HUNDRED ONE_HUNDRED ONE_HUNDRED { $$ = 300; }
 ;

tens: { $$ = 0; }
 | tens_0 { $$ = $1; }
 | TEN FIFTY { $$ = 40; }
 | FIFTY { $$ = 50; }
 | FIFTY tens_0 { $$ = 50 + $2; }
 | TEN ONE_HUNDRED { $$ = 90; }
 ;

tens_0:
 TEN { $$ = 10; }
 | TEN TEN { $$ = 20; }
 | TEN TEN TEN { $$ = 30; }
 ;

units: { $$ = 0; }
 | units_0 { $$ = $1; }
 | ONE FIVE { $$ = 4; }
 | FIVE { $$ = 5; }
 | FIVE units_0 { $$ = 5 + $2; }
 | ONE TEN { $$ = 9; }
 ;

units_0:
 ONE { $$ = 1; }
 | ONE ONE { $$ = 2; }
 | ONE ONE ONE { $$ = 3; }
 ;
%%

int main()
{
  yyparse();
  return 0;
}

char *toRoman(int n){
    struct{
        int value;
        char romanDgts[3];
    } romanTable[] = {{1000, "M"},
                      {900, "CM"},
                      {500, "D"},
                      {400, "CD"},
                      {100, "C"},
                      {90, "XC"},
                      {50, "L"},
                      {40, "XL"},
                      {10, "X"},
                      {9, "IX"},
                      {5, "V"},
                      {4, "IV"},
                      {1, "I"}};
    char *romanNumerals;
    int i = 0;

    romanNumerals = (char *)malloc(sizeof(char)*10);

    if (n == 0) {
        romanNumerals = strcat(romanNumerals, "Z");
        return romanNumerals;
    } else if (n < 0) {
        romanNumerals = strcat(romanNumerals, "-");
        n = -n;
    }

    while (n) {
        while (n < romanTable[i].value)
            i++;
        
        while (n >= romanTable[i].value) {
            strcat(romanNumerals, romanTable[i].romanDgts);
            n -= romanTable[i].value;
        }
    }

    return romanNumerals;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
}
