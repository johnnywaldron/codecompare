%{

typedef enum {
 false = 0,
 true = 1
} bool;

char code [1024];

bool comment_flag = false;
bool string_flag = false;

bool parsing_string () {
  return string_flag;
}

bool parsing_comment () {
  return comment_flag;
}  
%}

%%
\" {
  if (!parsing_comment()) {
    string_flag ^= true;
    strcat(code, yytext);
  }
}

"{" {
  if (!parsing_string()) {
    comment_flag = true;
  } else {
    strcat(code, yytext);
  }
}

"}" {
  if (parsing_comment()) {
    comment_flag = false;
  } else {
   strcat(code, yytext);
  }
}

\*\*.+ {}

\n {
 if (!parsing_comment()) {
    strcat(code, yytext);
  }
}

. {
  if (!parsing_comment()) {
    strcat(code, yytext);
  }
}
%%

int main()
{
  yyin = fopen("input", "r");
  yylex();
  if (parsing_comment()) {
    printf("syntax error %d %d\n", comment_flag, string_flag);
    return 1;
  }

  printf("%s", code);
  return 0;
}

