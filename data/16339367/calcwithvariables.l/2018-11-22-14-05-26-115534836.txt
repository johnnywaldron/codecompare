%{
    # include "calcwithvariables.tab.h"
    void yyerror(const char *s);
%}

%%
"+"     { return ADD; }
"-"     { return SUB; }
"*"     { return MUL; }
"/"     { return DIV; }
":="    { return EQ; } 

[0-9]+  { yylval.d = atoi(yytext); return NUMBER; }
[a-z]   { yylval.s = yytext[0]; return NAME; }
"print" { return PRINT; }

\n      { return EOL; }
[ \t]   { }
.       { yyerror("Mystery character\n");}
%%
