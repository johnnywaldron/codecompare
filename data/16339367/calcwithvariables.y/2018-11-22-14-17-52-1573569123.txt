%{
# include <stdio.h>
int yylex();
void yyerror(const char *s);

int vars [26];

%}

/* %error-verbose */

%union {
    int d;
    char s;
}

%token <d> NUMBER
%token <s> NAME
%token ADD SUB MUL DIV EQ
%token PRINT
%token EOL EOS

%nonassoc PRINT
%left ADD SUB
%left MUL DIV

%type <d> exp

%start calclist

%%

calclist:
 | calclist exp EOS EOL { printf("= %d\n", $2); }
 | calclist NAME EQ exp EOS EOL {
    vars[$2 - 'a'] = $4;
 }
 | calclist PRINT NAME EOS EOL {
    printf("%d\n", vars[$3 - 'a']);
 }
 | calclist EOL {printf("syntax error\n");}
 ;

exp: NUMBER
 | exp ADD exp {$$ = $1 + $3; }
 | exp SUB exp {$$ = $1 - $3; }
 | exp MUL exp {$$ = $1 * $3; }
 | exp DIV exp {$$ = $1 / $3; }
 | NAME {$$ = vars[$1 - 'a']; }
 ;

%%

int main(){
    yyparse();
    return 0;
}

void yyerror(const char *s) {
    fprintf(stderr, "%s\n", s);
}
