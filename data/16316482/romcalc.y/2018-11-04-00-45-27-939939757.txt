%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_SIZE 1024

int yylex();
void yyerror(char *s);
char* convertToRoman(unsigned int val); 

int num = 0;
%}

%token OPEN_BRACKET
%token CLOSE_BRACKET
%token I II III
%token V VV VVV
%token X XX XXX
%token L LL LLL
%token C CC CCC
%token D DD DDD
%token M MM MMM
%token EOL
%token ADD
%token SUB
%token MUL
%token DIV

%%
calclist:
	| calclist exp EOL { 
		if ($2 >= 0) {
			char *res = convertToRoman($2); printf("%s\n", res); 
		}
	}
	;

exp: factor
	| exp ADD factor { $$ = $1 + $3; }
	| exp SUB factor { $$ = $1 - $3; }
	;

factor: numeral
	| factor MUL numeral { $$ = $1 * $3; }
	| factor DIV numeral { if ($3 != 0) { $$ = $1 / $3; } else { $$ = 0; } }
	;

numeral: 
	thousand 
	hundredDigit 
	tenDigit 
	digit 							{ $$ = $1 + $2 + $3 + $4; }
  ;

thousand:							{ $$ = 0; }
  | M           			{ $$ = 1000; }
  | MM          			{ $$ = 1000*2; }
  | MMM         			{ $$ = 1000*3; }
  ;

hundredDigit: hundred { $$ = $1; }
  | C D         			{ $$ = 400; }
  | D hundred   			{ $$ = 500 + $2; }
  | C M         			{ $$ = 900; }
  ;

hundred:							{ $$ = 0; }
  | C           			{ $$ = 100; }
  | CC          			{ $$ = 100*2; }
  | CCC         			{ $$ = 100*3; }
  ;

tenDigit: ten					{ $$ = $1; }
  | X L         			{ $$ = 40; }
  | L ten       			{ $$ = 50 + $2; }
  | X C         			{ $$ = 90; }
  ;

ten:									{ $$ = 0; }
  | X           			{ $$ = 10; }
  | XX          			{ $$ = 10*2; }
  | XXX         			{ $$ = 10*3; }
  ;

digit: one						{ $$ = $1; }
  | I V         			{ $$ = 4; }
  | V one       			{ $$ = 5 + $2; }
  | I X         			{ $$ = 9; }
  ;

one:									{ $$ = 0; }
  | I           			{ $$ = 1; }
  | II          			{ $$ = 2; }
  | III         			{ $$ = 3; }
  ;
%%

char* convertToRoman(unsigned int val) {
    char *huns[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    int   size[] = { 0,   1,    2,     3,    2,   1,    2,     3,      4,    2};

    if (val == 0) {
        return "Z";
    }

    char *res = malloc(sizeof(char) * NUM_SIZE);
    int i = 0;
    res[0] = '\0';

    // Add 'M' until the value drops below 1000.
    while (val >= 1000) {
        res[i] = 'M';
        i++;
        val -= 1000;
    }

    // Add each of the correct elements.
    strcat(res, huns[val/100]); i += size[val/100]; val = val % 100;
    strcat(res, tens[val/10]);  i += size[val/10]; val = val % 10;
    strcat(res, ones[val]); i += size[val];

    res[i] = '\0';

    return res;
}

int main()
{
  yyparse();

  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "syntax error\n");
}
