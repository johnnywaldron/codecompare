%{
#  include <stdio.h>
#include <stdbool.h>
int yylex();
void yyerror(char *s);
int variable[26];
bool error = false;
%}

/* declare tokens */
%token NUMBER
%token ADD SUB MUL DIV ABS
%token EOL
%token EQU TERMIN VAR PRINT
%%
calclist: /* nothing */
 | calclist expb TERMIN
 ; 

expb: VAR EQU exp { variable[$2-97] = $3; }
	| PRINT VAR { if (!error) printf("%d\n", variable[$2-97]); }

exp: factor 
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term 
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: NUMBER
 | VAR { $$ = variable[$1-97]; } 
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 ;
%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  error = true;
	fprintf(stderr, "%s\n", s);
}
