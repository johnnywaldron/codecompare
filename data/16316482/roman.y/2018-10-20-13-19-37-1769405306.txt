%{
#include <stdio.h>
#include <stdlib.h>

int yylex();
void yyerror(char *s);

int num = 0;
%}

%token I II III 
%token V VV VVV 
%token X XX XXX
%token L LL LLL 
%token C CC CCC 
%token D DD DDD 
%token M MM MMM
%token EOL

%%
romanNumerals: 
  | romanNumerals numeral EOL { 
    if (num != 0) { printf("%d\n", num); }; 
    num = 0;  
  }
  ;

numeral: thousand hundredDigit tenDigit digit
  ;

thousand:
  | M           { num += 1000; }
  | MM          { num += 1000*2; }
  | MMM         { num += 1000*3; }
  ;

hundredDigit: hundred
  | C D         { num += 400; }
  | D hundred   { num += 500; }
  | C M         { num += 900; }
  ;

hundred:
  | C           { num += 100; }
  | CC          { num += 100*2; }
  | CCC         { num += 100*3; }
  ;

tenDigit: ten
  | X L         { num += 40; }
  | L ten       { num += 50; }
  | X C         { num += 90; }
  ;

ten:
  | X           { num += 10; }
  | XX          { num += 10*2; }
  | XXX         { num += 10*3; }
  ;

digit: one
  | I V         { num += 4; }
  | V one       { num += 5; }
  | I X         { num += 9; }
  ;

one: 
  | I           { num++; }
  | II          { num += 2; }
  | III         { num += 3; }
  ;

%%

int main()
{
  yyparse();

  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "syntax error\n");
}