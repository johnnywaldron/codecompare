%{
int even = 0;
%}

%%
[0-9]+  {  if (atoi(yytext) % 2 == 0) even++; }
\n
.
%%

int main()
{
  yylex();
  printf("%d\n", even);

  return 0;
}
