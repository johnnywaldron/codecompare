%{
#include "romcalc.tab.h"
void yyerror(char *s);
%}

%%
"{"		{ return OPEN_BRACKET; }
"}"		{ return CLOSE_BRACKET; }

"I"   { return I; }
"II"  { return II; }
"III" { return III; }

"V"   { return V; }
"VV"  { return VV; }
"VVV" { return VVV; }

"X"   { return X; }
"XX"  { return XX; }
"XXX" { return XXX; }

"L"   { return L; }
"LL"  { return LL; }
"LLL" { return LLL; }

"C"   { return C; }
"CC"  { return CC; }
"CCC" { return CCC; }

"D"   { return D; }
"DD"  { return DD; }
"DDD" { return DDD; }

"M"   { return M; }
"MM"  { return MM; }
"MMM" { return MMM; }

"+"		{ return ADD; }
"-"		{ return SUB; }
"/"		{ return DIV; }
"*"		{ return MUL; }

\n    { return EOL; }
[ \t] { /* ignore white space */ }
.     { yyerror("Mystery character\n"); }
%%
