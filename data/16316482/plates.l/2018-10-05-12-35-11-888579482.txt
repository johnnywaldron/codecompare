%{
#define YEAR  2018

char num_char[256];
char out[36450];
int num;
%}

N       [0-9]
N_      [1-2]
C       C(E|N|W)?|D(L)?|G|K(E|K|Y)?|L(D|H|M|S)?|M(H|N|O)|OY|RN|SO|W(H|X|W)?
C_OLD   LK|T(N|S)|WD
C_NEW   T
S       [\r\n\t\f\v ]

%%
{S} {}
"\n"+ {}

{N}{2}-({C}|{C_OLD})-{N}{1,6}{S}* {
  strncpy(num_char, yytext, 2);
  num = atoi(num_char);
  if(num > 12 && num < 87) {
    printf("INVALID\n");
  } else if (num <= 12) {
    num = YEAR - (2000 + num);
    printf("%d\n", num);
  } else {
    num = YEAR - (1900 + num);
    printf("%d\n", num);
  }
}

(131|132)-({C}|{C_OLD})-{N}{1,6}{S}* {
  num = YEAR - 2013;
  printf("%d\n", num);
}

{N}{2}{N_}-({C}|{C_NEW})-{N}{1,6}{S}* {
  strncpy(num_char, yytext, 2);
  num = atoi(num_char);
  if(num < 14 || num > 18) {
    printf("INVALID\n");
  } else if (num <= 18) {
    num = YEAR - (2000 + num);
    printf("%d\n", num);
  }
}

.*{S} { printf("INVALID\n"); }

%%

int main()
{
  yylex();

  return 0;
}
