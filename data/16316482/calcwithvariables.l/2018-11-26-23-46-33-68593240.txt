%{
# include "calcwithvariables.tab.h"
void yyerror(char *s);
%}

%%
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"|"     { return ABS; }
":=" { return EQU; }
";" { return TERMIN; }
"print" { return PRINT; }
[a-zA-Z] { yylval = yytext[0]; return VAR; }
[0-9]+	{ yylval = atoi(yytext); return NUMBER; }

\n			{ /* ignore white space */ }
[ \t]   { /* ignore white space */ }
.	{ yyerror("syntax error\n");}
