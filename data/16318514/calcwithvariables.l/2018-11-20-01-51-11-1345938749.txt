%{
# include "calcwithvariables.tab.h"
void yyerror(char *s);
%}
 
%%
"{"	{ return LBRACKET; }
"}"	{ return RBRACKET; }
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"|"     { return ABS; }
":="    { return DECL; }
"print"    { return PRINT; }
[A-Z]    { printf("syntax error\n");}
[a-z]   {yylval= *yytext-'a'; return VAR; }
[0-9]   {yylval = atoi(yytext);return NUMBER;}
";\n"      { return EOL; }
[\n]{1}[a-z]{1}      { printf("syntax error\n");}
[ \t]
%%
