%{
# include "calcwithvariables.tab.h"
void yyerror(char *s);
%}
 
%%
"{"	{ return LBRACKET; }
"}"	{ return RBRACKET; }
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"|"     { return ABS; }
":="    { return DECL; }
"print"    { return PRINT; }
[a-z]   {yylval= *yytext-'a'; return VAR; }
[0-9]   {yylval = atoi(yytext);return NUMBER;}
";\n"      { return EOL; }
"\n"      { printf("syntax error");}
[ \t]
.   { yyerror("Mystery character\n");}
%%
