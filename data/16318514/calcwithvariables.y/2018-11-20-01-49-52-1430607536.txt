
%{
#  include <stdio.h>
#  include <string.h>
int yylex();
void yyerror(char *s);
long variables[26];
%}
 
/* declare tokens */
%token NUMBER
%token LBRACKET RBRACKET
%token ADD SUB MUL DIV ABS DECL VAR PRINT
%token EOL SKIP
%%
calclist: /* nothing */
 | calclist exp EOL {
    int number = $2;
    printf("%d\n", number);}
 | calclist VAR DECL SUB exp EOL {
    //char character = $2;
    int number = $5;
    int pos = $2;//character - 'a';
    variables[pos] = number*-1;
 }
 | calclist VAR DECL exp EOL {
    //char character = $2;
    int number = $4;
    int pos = $2;//character - 'a';
    variables[pos] = number;
 }
 | calclist PRINT VAR {printf("%ld\n",variables[$3]);};
;
 
exp: factor
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term 
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: total 
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 | LBRACKET exp RBRACKET { $$ = $2;};
 | LBRACKET exp EOL {printf("syntax error\n"); return 0;};
 ;

 
total: NUMBER {$$ = $1;}
 | VAR {$$ = variables[$1];}
 ;
 
%%
int main()
{
  yyparse();
  return 0;
}
 
void yyerror(char *s)
{
 
}
 
 
 
