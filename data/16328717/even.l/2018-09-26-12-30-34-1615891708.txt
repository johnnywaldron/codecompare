%{
    
%}
%%
(\".*\")  {printf(yytext);}
"**".*          {}
\{[^\}]*\}      {}
\{(^[\}]|\n)*    {printf("%c\n", "syntax error");}
(^[\{]|\n)*\}   {printf("%c\n", "syntax error");}

%%
int main(){
    yylex();
    return 0;
}
