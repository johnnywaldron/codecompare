%{
    
%}
%%
(\".*\")  {printf(yytext);}
"**".*          {}
\{[^\}]*\}      {}
\{(^[\}]|\n)*    {printf("%s\n", "syntax error"); return 1;}
(^[\{]|\n)*\}   {printf("%s\n", "syntax error"); return 1;}

%%
int main(){
    yylex();
    return 0;
}
