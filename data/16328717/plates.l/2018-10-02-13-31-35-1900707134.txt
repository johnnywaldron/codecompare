%{
int calc(char tens, char ones){
    int year = (int)(tens - '0') * 10 + (int)(ones - '0');
    int diff = 0;
    if(year > 50) diff = 18 + (100 - year);
    else diff = 18 - year;
    return diff;
}
%}
%%
1[4-9].-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)-[0-9]{0,6}[" "\n\t]*  {printf("%d\n", 8 - (int)(yytext[1] - '0'));}
(8[789]|[90][0-9]|1[0-3]).?-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW)-[0-9]{0,6}[" "\n\t]* {printf("%d\n", calc(yytext[0], yytext[1]));}
.{2,3}-.{1,2}-.{0,6}    {printf("INVALID\n");}
.   {}
%%
int main(){
    yylex();
    return 0;
}

