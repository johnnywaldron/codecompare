%option noyywrap
%{
#include "calcwithvariables.tab.h"
%}
%%
[a-z] {yylval = yytext[0]; return VAR;}
[0-9]	{yylval = yytext[0] - '0'; return VAL;}
:=		{return ASSIGNMENT;}
\+		{return ADD;}
\-		{return SUB;}
\*		{return MUL;}
\/		{return DIV;}
print {return PRINT;}
;			{return EOS;}
[" "\t\n] {}
%%

