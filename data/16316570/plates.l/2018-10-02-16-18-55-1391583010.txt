%{
#include <stdio.h>
#include <stdlib.h>
%}

plates  ([0189][0-9]|[0189][0-9][12])-[A-Z]*-[0-9]*


%%

{plates}		{
				yearFrom(yytext);
			}


%%
int yearFrom(char p[])
{
	char year[2];
	int pZero = p[0] - '0';

	if (pZero < 2)
	{
		for (int i =0; i<2; i++)
		{
			year[i] = p[i];
		}
		
		int yearFrom1 = 18 - atoi(year);
		printf("%d\n", yearFrom1);
	}
	char doubleYear[] = "1900";
	if (pZero >= 8)
	{
		for (int i =0; i<2; i++)
		{
			doubleYear[i+2] = p[i]; 		
		}
		int yearFrom2 = 2018 - atoi(doubleYear);
		printf("%d\n",yearFrom2);	
	}

}

int main()
{
	yylex();
	return 0;
}

