%{
#  include <stdio.h>
#  include <stdlib.h>
void yyerror(char *s);
int yylex();
int yyparse();

%}
%output "roman.tab.c"

%token NUM
%token EOL
%%

calclist: /* nothing */ {}
| calclist expr EOL { }
;


expr: NUM
 | expr NUM   {
                  //printf("%d\n", $$);
              }
 ;
%%
void yyerror(char *s)
{
  printf("Syntax error\n" );
}


/* int
main()
{
  yyparse();
  return 0;
} */
