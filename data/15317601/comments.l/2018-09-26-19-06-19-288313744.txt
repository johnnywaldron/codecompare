/* comments.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
int no_of_comments = 0;
%}

%%


\"[^\"]+\" { printf("%s", yytext); }
"{"[^"}"]*"}" { no_of_comments++; }
("{"|"}") { printf("syntax error\n"); exit(0); }
"**".*$ { no_of_comments++; }
\n	 { printf("%s", yytext); }
.		{ printf("%s", yytext); }

%%
int main() {
  yylex();
	return 0;
}

