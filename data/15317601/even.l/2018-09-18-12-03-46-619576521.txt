/* even.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
%}

%%

[0-9]+[0248]$ {even_nums++;}

%%

int main()
{
	return even_nums;
}
