/* even.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
%}

%%

([02468])$ {even_nums++;}
\n		{ chars++; lines++; }
.		{ chars++; }

%%
int main(const char* str) {
  //char* new_str = str;
  //const char * new_str = str;
  yy_scan_string("5 66 77 90 2");
  yylex();
  printf("%d", even_nums);
	return 0;
}

