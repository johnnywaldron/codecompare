/* even.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
%}

%%

//[0-9]+[0248]$ {even_nums++;}
[0-9]+ {even_nums++;}

%%

int main()
{
  printf("%d", even_nums);
	return 0;
}

