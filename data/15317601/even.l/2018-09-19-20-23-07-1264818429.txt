/* even.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
%option reentrant
%}

%%

(\d)*[02468]+[^\d] {even_nums++;}
\n		{ chars++; lines++; }
.		{ chars++; }

%%
int main(char* str) {
  //char* new_str = str;
  //const char * new_str = str;
  //yy_scan_string(str);
  //yylex();
  //printf("%d", even_nums);
  yyscan_t scanner;
  YY_BUFFER_STATE buf;
  yylex_init(&scanner);
  buf = yy_scan_string(str, scanner);
  yylex(scanner);
  yy_delete_buffer(buf, scanner);
  yylex_destroy(scanner);
	return 0;
}
