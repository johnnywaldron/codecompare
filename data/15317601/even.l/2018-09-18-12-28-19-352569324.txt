/* even.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
%}

%%

[0-9]+ {even_nums++;}
\n		{ chars++; lines++; }
.		{ chars++; }

%%

int main(int, char**)
{
  yylex();
  printf("%d", even_nums);
	return 0;
}
