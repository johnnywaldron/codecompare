/* even.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
%}

%%

[0-9]+ {even_nums++;}
\n		{ chars++; lines++; }
.		{ chars++; }

%%
int main() {
  // lex through the input: THIS IS A TEST... NOT MY CODE!!!
  while (yylex());
//int main()
//{
//  yylex();
  printf("%d", even_nums);
	return 0;
}

