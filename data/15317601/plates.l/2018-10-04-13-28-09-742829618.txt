/* plates.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
int no_of_comments = 0;
//\d\d[\d]+ {}
//\d\d[\d]*-[A-Z][A-Z]*-[\d]{1,6}$ {  }
//^[0-9][0-9](1|2) { if((atoi(yytext) / 10) <= 18){printf("%d\n", (18 - (atoi(yytext) / 10)));} else { printf("INVALID\n");} }
//^[0-9][0-9] { if(atoi(yytext) <= 18){printf("%d\n", (18 - atoi(yytext)));} else { printf("INVALID%s\n", yytext);} }
%}

%%

-[A-Z]+-[0-9]+ {}
[0-9][0-9](1|2) { if((atoi(yytext) / 10) <= 18){printf("%d\n", (18 - (atoi(yytext) / 10)));} else { printf("INVALID\n");} }
[0-9][0-9] { if(atoi(yytext) <= 18){printf("%d\n", (18 - atoi(yytext)));} else { printf("INVALID\n", yytext);} }
\n	 {  }
.		{  }

%%
int main() {
  yylex();
	return 0;
}

