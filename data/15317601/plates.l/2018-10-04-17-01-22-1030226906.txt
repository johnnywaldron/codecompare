/* plates.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
int no_of_comments = 0;
//\d\d[\d]+ {}
//\d\d[\d]*-[A-Z][A-Z]*-[\d]{1,6}$ {  }
//^[0-9][0-9](1|2) { if((atoi(yytext) / 10) <= 18){printf("%d\n", (18 - (atoi(yytext) / 10)));} else { printf("INVALID\n");} }
//^[0-9][0-9] { if(atoi(yytext) <= 18){printf("%d\n", (18 - atoi(yytext)));} else { printf("INVALID%s\n", yytext);} }
//[0-9]{3,} {printf("INVALID\n");}

%}
NUMBER  [0-9]{1,6}
COUNTY  KK|C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW|LK|TN|TS|WD
YY  [0-9][0-9]
YYY [0-9][0-9](1|2)
%%
{YY}/(1|2)(-{COUNTY}-{NUMBER}) {printf("%d\n", (18 - atoi(yytext)));}
{YY}/(-{COUNTY}-{NUMBER}) {printf("%d\n", (18 - atoi(yytext));}
\n	 {  }
.		{  }

%%
int main() {
  yylex();
	return 0;
}

