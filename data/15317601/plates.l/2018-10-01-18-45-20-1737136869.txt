/* plates.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
int no_of_comments = 0;
//\d\d[\d]+ {}
//\d\d[\d]*-[A-Z][A-Z]*-[\d]{1,6}$ {  }
%}

%%

[ \t]+ {}
\d\d { printf("%s\n", yytext); }
\n	 {  }
.		{  }

%%
int main() {
  yylex();
	return 0;
}

