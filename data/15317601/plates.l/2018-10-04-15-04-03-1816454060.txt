/* plates.l by Sean Raeside 15317601 */
%{
int even_nums = 0;
int chars = 0;
int lines = 0;
int no_of_comments = 0;
//\d\d[\d]+ {}
//\d\d[\d]*-[A-Z][A-Z]*-[\d]{1,6}$ {  }
//^[0-9][0-9](1|2) { if((atoi(yytext) / 10) <= 18){printf("%d\n", (18 - (atoi(yytext) / 10)));} else { printf("INVALID\n");} }
//^[0-9][0-9] { if(atoi(yytext) <= 18){printf("%d\n", (18 - atoi(yytext)));} else { printf("INVALID%s\n", yytext);} }
//[0-9]{3,} {printf("INVALID\n");}
%}

%%



-[A-Z]+-[0-9]{1,6} {}
- {printf("INVALID\n");}
[0-9][0-9](1|2) { int num = (atoi(yytext) / 10);
                  if(num >= 87) {
                    num = num + 1900;
                  }
                  else {
                    num = num + 2000;
                  }
                  printf( "%d\n", 2018 - num ); }
[0-9][0-9] { int num = (atoi(yytext));
              if(num >= 87) {
                num = num + 1900;
              }
              else {
                num = num + 2000;
              }
              printf( "%d\n", 2018 - num ); }
\n	 {  }
.		{  }

%%
int main() {
  yylex();
	return 0;
}

