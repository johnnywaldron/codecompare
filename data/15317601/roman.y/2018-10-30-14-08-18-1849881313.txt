/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.y,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

/* simplest version of calculator */

%{
#  include <stdio.h>
int yylex();
void yyerror(char *s);
int acc = 0;

%}

/* declare tokens fullnumber: sentence EOL { printf("fullnumber\n"); };*/
%token I V M C X EOL
%%

completeInput: fullLine { printf("complete: %d\n", $1); }
                | fullLine completeInput { printf("complete: %d\n", $1); }
                ;

fullLine:
        | string EOL { printf("fullLine -> %d\n", acc); acc = 0; }
        ;

string: sym { acc += $1; $$ = $1; printf("sym: %d .. %d\n", $1, $$); }
        | sym string { acc += $1; printf("string sym: %d\n", $2); }
        ;

sym: I { printf("I\n"); }
    | V { printf("V\n"); }
    | C { printf("C\n"); }
    | M { printf("M\n"); }
    | X { printf("X\n"); }
    ;

%%
int main()
{
  //printf("> ");
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}
