/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.y,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

/* simplest version of calculator */

%{
#  include <stdio.h>
int yylex();
void yyerror(char *s);
int acc = 0;
int debug = 0;
int debug_lr = 0;
int current_sym_val = 0;
int right_sym_val = 0;
int lines[100];
int lines_index = 0;

%}

/* declare tokens fullnumber: sentence EOL { printf("fullnumber\n"); };*/
%token I V M C X D L EOL
%%

completeInput: fullLine { if (debug){printf("complete: %d\n", $1);}
                          lines[lines_index] = $1;
                          printf("lines_index: %d\n", lines_index);
                          int i;
                          for (i = lines_index; i>=0; i--){
                            printf("%d\n", lines[i]);
                          }
                        }
                  | fullLine completeInput { if (debug){printf("complete: %d\n", $1);} lines[lines_index++] = $2; }
                ;

fullLine:
        | string EOL { if (debug){printf("fullLine -> %d\n", acc);} else {/*printf("%d\n", acc);*/ $$ = acc;} acc = 0; }
        ;

string: sym { acc += $1; $$ = $1; current_sym_val = $1; if (debug) {printf("sym: %d .. %d. current_symval: %d\n", $1, $$, current_sym_val);} }
        | sym string { right_sym_val = current_sym_val;
                        current_sym_val = $1;
                        if (right_sym_val <= current_sym_val) {acc += $1; if (debug_lr) {printf("add performed\n");}}
                        else {acc -= $1; if (debug_lr) {printf("sub performed\n");}}
                        if (debug) {
                          printf("string sym: %d. current_symval: %d. right_sym_val: %d\n", $1, current_sym_val, right_sym_val);
                        }
                     }
        ;

sym: I { if (debug){printf("I\n");} }
    | V { if (debug){printf("V\n");} }
    | C { if (debug){printf("C\n");} }
    | L { if (debug){printf("L\n");} }
    | M { if (debug){printf("M\n");} }
    | D { if (debug){printf("D\n");} }
    | X { if (debug){printf("X\n");} }
    ;

%%
int main()
{
  //printf("> ");
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}
