%{
    int currentyear = 2018;
    int year = 0;
    char num [2];
%}

PREVYEARS [8][7-9])|([9|0][0-9])|([1][0-2]
RECENTYEARS [1][3-8][1|2]
COUNTIESONE (C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW)
COUNTIESTWO (C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)
NUMBER [0-9]{1,6}
	
%%  
[\t\n\r' ']+
{PREVYEARS}[-]{COUNTIESONE}[-]{NUMBER}   {num[0] = yytext[0]; num[1] = yytext[1]; if (num[0]=='1'){ 
                                             year = 2000 + atoi(num);} else {year = 1900 + atoi(num);}
                                             printf("%d\n",currentyear-year);} 

{RECENTYEARS}[-]{COUNTIESTWO}[-]{NUMBER} {num[0] = yytext[0]; num[1] = yytext[1]; year = 2000 + atoi(num);
                                            printf ("%d\n", currentyear-year);;} 
%%

int main()
{
  yylex();
  return 0;
}
