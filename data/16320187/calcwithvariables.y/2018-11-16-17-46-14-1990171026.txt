%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  void yyerror(char *s);
  int yylex();
  int yyparse();

struct node{
char key;
int val;
struct node *next;
};

struct table{
int size;
struct node **list;
};

struct table *hashTable = NULL;


struct table *createTable(int size){
struct table *t = (struct table*)malloc(sizeof(struct table));
t->size = size;
t->list = (struct node**)malloc(sizeof(struct node*)*size);
int i;
for(i=0;i<size;i++)
t->list[i] = NULL;
return t;
}

int hashCode(struct table *t,char key){
if(key<0)
return -(key%t->size);
return key%t->size;
}

void insert(struct table *t,char key,int val){
int pos = hashCode(t,key);
struct node *list = t->list[pos];
struct node *newNode = (struct node*)malloc(sizeof(struct node));
struct node *temp = list;
while(temp){
if(temp->key==key){
temp->val = val;
return;
}
temp = temp->next;
}
newNode->key = key;
newNode->val = val;
newNode->next = list;
t->list[pos] = newNode;
}

int lookup(struct table *t,char key){
int pos = hashCode(t,key);
struct node *list = t->list[pos];
struct node *temp = list;
while(temp){
if(temp->key==key){
return temp->val;
}
temp = temp->next;
}
return -1;
}

%}

%output "calcwithvariables.tab.c"
%token INT VARIABLE PRINT ASSIGN SEMICOLON EOL
%union{ int intval;
        char charval;
}
%type <intval> unary expression
%type <charval> variable
%left PLUS MINUS
%left MUL DIV
%%

run:
    | run statements {}
    ;

statements: variable ASSIGN expression SEMICOLON EOL { if(hashTable == NULL){hashTable = createTable(1000);}insert(hashTable, $1, $3);}
    | PRINT variable SEMICOLON EOL {printf("%d\n",lookup(hashTable, $2));}
    ;

expression: unary
    | expression PLUS expression  { $$ = $1 + $3; }
    | expression MINUS expression { $$ = $1 - $3; }
    | expression MUL expression   { $$ = $1 * $3; }
    | expression DIV expression   { $$ = $1 / $3; }
    ;

unary: MINUS unary              { $$ = -$2; }
    | INT                       { $$ = yylval.intval;}
    | variable                  { if(hashTable == NULL){
                                hashTable = createTable(1000);
                                } $$ = lookup(hashTable, yylval.charval);}
    ;

variable: VARIABLE              {if(hashTable == NULL){
                                hashTable = createTable(1000);
                                }lookup(hashTable, yylval.charval);};
%%

void yyerror(char *s) {
    printf("syntax error\n");
}

int main(void) {
    yyparse();
}
