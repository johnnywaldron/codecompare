%{
  #include <stdio.h>
  #include <stdlib.h>
  void yyerror(char *s);
  int yylex();
  int yyparse();
%}

%output "roman.tab.c"
%token ONE
%token FIVE
%token TEN
%token FIFTY
%token HUNDRED
%token FIVEHUNDRED
%token THOUSAND
%token EOL LETTER

%%
run :
  | run Thousand EOL {printf("%d\n", $2);}
;

Thousand: FiveHundreds
| THOUSAND FiveHundreds {$$=1000+$2;}
| THOUSAND THOUSAND FiveHundreds {$$=2000+$3;}
| THOUSAND THOUSAND THOUSAND FiveHundreds {$$=3000+$4;}

;

FiveHundreds: Hundreds
  | FIVEHUNDRED Hundreds         {$$=500+$2;}
  | HUNDRED FIVEHUNDRED Fifties  {$$=400+$3;}
  | HUNDRED THOUSAND Fifties     {$$=900+$3;}
;

Hundreds: Fifties
  | HUNDRED Fifties         {$$=100+$2;}
  | HUNDRED HUNDRED Fifties {$$=200+$3;}
  | HUNDRED HUNDRED HUNDRED Fifties {$$=300+$4;}
;

Fifties: Tens
  | TEN HUNDRED Fives {$$=90+$3;}
  | FIFTY Tens        {$$=50+$2;}
  | TEN FIFTY Fives   {$$=40+$3;}
;

Tens: Fives
  | TEN TEN Fives     {$$=20+$3;}
  | TEN TEN TEN Fives {$$=30+$4;}
  | TEN Fives         {$$=10+$2;}
;

Fives: Ones
  | ONE TEN          {$$=9;}
  | ONE FIVE         {$$=4;}
  | FIVE Ones        {$$=5+$2;}
;

Ones:                {$$=0;}
  | ONE              {$$=1;}
  | ONE ONE          {$$=2;}
  | ONE ONE ONE      {$$=3;}
;

%%

int main()
{
  printf("");
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("error: %s\n", s);
}
