%{
    char code [1000];
%}

/* Rules */
%%

\*\*.* {}
\{[^\}]*\} {}
\{[^\}]* {}

\n {strcat(code, yytext);}
. {strcat(code, yytext);}

%%
/* User code */
int main()
{
  yylex();
  printf("%s", code);
	return 0;
}
