%{
#  include <stdio.h>
int yylex();
int yyparse();
void yyerror(char *s);
%}

%output "romcalc.tab.c"

/* declare tokens */
%token NUMBER
%token ADD SUB MUL DIV 
%token I V X L C D M
%token EOL

%%

calclist: /* nothing */ 	{}
 | calclist convertOne EOL 			{printf("%d\n", $2);}
 ;

 
exp: factor 
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: romanNum
 | factor MUL romanNum { $$ = $1 * $3; }
 | factor DIV romanNum { $$ = $1 / $3; }
 ;
 
 
 
romanNum: thousand hundo ten digit	{$$ = $1 + $2 + $3 + $4;}
 |hundo ten digit			{$$ = $1 + $2 + $3;}
 |ten digit					{$$ = $1 + $2;}
 |digit						{$$ = $1;}
 ;

thousand: {$$ = 0;}
 |M {$$ = 1000;}
 |M M {$$ = 2000;}
 |M M M {$$ = 3000;}
 ;

hundo:  {$$ = 0;}
 |shundo	{$$ = $1;}
 |C D		{$$ = 400;}
 |D shundo	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;
 
shundo:	 {$$ = 0;}
 |C			{$$ = 100;}
 |C C		{$$ = 200;}
 |C C C		{$$ = 300;}
 ;
 
ten:  {$$ = 0;}
 |sten		{$$ = $1;}
 |X L		{$$ = 40;}
 |L sten	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;
 
sten:  {$$ = 0;}
 |X			{$$ = 10;}
 |X X		{$$ = 20;}
 |X X X		{$$ = 30;}
 ;
 
digit:  {$$ = 0;}
 |sdigit	{$$ = $1;}
 |I V		{$$ = 4;}
 |V sdigit	{$$ = 5 + $2;}
 |I X		{$$ = 9;}
 ;
 
sdigit:  {$$ = 0;}
 |I			{$$ = 1;}
 |I I		{$$ = 2;}
 |I I I		{$$ = 3;}
 ;

 
convertOne: convertFour {while($1 > 1){printf("I"); $1 = $1-1;} while($1 < -1){ printf("I"); $1 = $1 + 1;} $$=$1; }
convertFour: convertFive{while($1 > 4){printf("IV"); $1 = $1-4;} while($1 < -4){ printf("IV"); $1 = $1 + 4;} $$=$1;}
convertFive: convertNine{while($1 > 5){printf("V"); $1 = $1-5;} while($1 < -5){ printf("V"); $1 = $1 + 5;} $$=$1; }
convertNine: convertTen	{while($1 > 9){printf("IX"); $1 = $1-9;} while($1 < -9){ printf("IX"); $1 = $1 + 9;}$$=$1;}
convertTen: convertFourty{while($1 > 10){printf("X"); $1 = $1-10;}while($1 < -10){ printf("X"); $1 = $1 + 10;}$$=$1;}
convertFourty: convertFifty{while($1 > 40){printf("XL"); $1 = $1-40;}while($1 < -40){ printf("XL"); $1 = $1 + 40;}$$=$1;}
convertFifty: convertNinety{while($1 > 50){printf("L"); $1 = $1-50;}while($1 < -50){ printf("L"); $1 = $1 + 50;}$$=$1;}
convertNinety: convertHundred{while($1 > 90){ printf("XC"); $1 = $1-90;}while($1 < -90){ printf("XC"); $1 = $1 + 90;}$$=$1;}
convertHundred: convertFourHundred{while($1 > 100){printf("C"); $1 = $1-100;}while($1 < -100){ printf("C"); $1 = $1 + 100;}$$=$1;}
convertFourHundred: convertFiveHundred {while($1 > 400){printf("CD"); $1 = $1-400;} while($1 < -400){ printf("CD"); $1 = $1 + 400;} $$=$1;}
convertFiveHundred: convertNineHundred {while($1 > 500){ printf("D"); $1 = $1-500;} while($1 < -500){ printf("D"); $1 = $1 + 500;} $$=$1;}
convertNineHundred: convertThousand {while($1 > 900){ printf("CM"); $1 = $1- 900;} while($1 < -900){ printf("CM"); $1 = $1 + 900;} $$=$1;}
convertThousand: convertZero{while($1 > 1000){ printf("M"); $1 = $1 -1000;}while($1 < -1000){ printf("M"); $1 = $1 + 1000;} $$=$1;}
convertZero: exp {if($1==0) printf("Z");if($1<0) printf("-") $$=$1;}

 
%%
int main()
{
  printf("> "); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}

 
 
%%
void yyerror(char *s){
  printf("%s\n", s);
}