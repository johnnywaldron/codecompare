
%{
  
%}

%%
"{"[^"}"]"}"	 { }
"{"[^"}"]	 {printf("syntax error\n");}
"**"*[^\n]	 	 { }
.		 {printf("%s",yytext);}
\n   		 {printf("%s",yytext);}
%%

int main()
{
  yylex();
  return 0;
}
