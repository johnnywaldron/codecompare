%{
    
%}

%%

"{"[^"}"]"}"	 { }
"{"[^"}"|\n]"}"	 { }
"**".*\n	 {printf("\n"); }
"{"[^"}"]	 {printf("syntax error\n");}
"{"	 	{printf("syntax error\n");}
"}"	 	{printf("syntax error\n");}

.		 {printf("%s",yytext);}
\n   		 {printf("%s",yytext);}
%%

int main()
{
  yylex();
  return 0;
}
