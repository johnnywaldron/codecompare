%{


  int S = 0;
  char *C = 0;
  int Y = 0;
%}

YEAR_OLD [[8[7-9]]|[9|0[0-9]|1[0-2]]]
YEAR_NEW [1[3-8][1|2]]
COUNTY_OLD C|CE|CN|CW|D|DL|G|KE|KK|Y|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW 
COUNTY_NEW L|LK|TN|TS|W|WD
NUMBER [0-9]{1-6}

%%

{YEAR_OLD}    {Y = yytext/10;}
{YEAR_NEW}    {Y = yytext + 2000;}
{COUNTY_OLD}  {validCountyOld = true; C = yytext;}
{COUNTY_NEW}  {validCountyNew = true; C = yytext;}


\n            { if(validYearOld == true&&validCountyOld == true&&ValidNumber == true){
                  
                }
                else if(validYearNew == true&&validCountyNew == true&&ValidNumber == true){
                  
                }
                else{
                  printf("INVALID\n");
                }
                
              }
%%

int main()
{
  yylex();
  return 0;
}
