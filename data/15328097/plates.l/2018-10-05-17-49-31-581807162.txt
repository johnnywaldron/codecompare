%{
  int S = 0;
  int C = 0;
  int Y = 0;
%}

YEAR_OLD [8[7-9]]|[9|0[0-9]|1[0-2]]
YEAR_NEW 1[3-8][1|2]
COUNTY_OLD C|CE|CN|CW|D|DL|G|KE|KK|Y|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW 
COUNTY_NEW L|LK|TN|TS|W|WD
NUMBER [0-9]{1,6}

%%

{YEAR_OLD}    {}
{YEAR_NEW}    {Y = 2000+atoi( yytext );}
{COUNTY_OLD}  {}
{COUNTY_NEW}  {}
{NUMBER}      {}
.		{}
\n		{if(C == S && Y > 0)
		{
			print("%d",Y);
		}
		else
		{
			printf("INVALID\n");
		}
}
%%

int main()
{
  yylex();
  return 0;
}
