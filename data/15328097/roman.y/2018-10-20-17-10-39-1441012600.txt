%{
#  include <stdio.h>
int yylex();
int yyparse();
%}

%output "roman.tab.c"

/* declare tokens */
%token I V X L C D M
%token EOL

%%

calclist: /* nothing */ 	{}
 | calclist romanNumeral EOL 			{printf("%d\n", $2);}
 ;

romanNumeral: thousand hundred ten digit	{$$ = $1 + $2 + $3 + $4;}
 |hundred ten digit			{$$ = $1 + $2 + $3;}
 |ten digit					{$$ = $1 + $2;}
 |digit						{$$ = $1;}
 ;

thousand: M {$$ = 1000;}
 |M M {$$ = 2000;}
 |M M M {$$ = 3000;}
 ;

hundred: subHundred	{$$ = $1;}
 |C D		{$$ = 400;}
 |D subHundred	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;
 
subHundred:	C			{$$ = 100;}
 |C C		{$$ = 200;}
 |C C C		{$$ = 300;}
 ;
 
ten: subTen		{$$ = $1;}
 |X L		{$$ = 40;}
 |L subTen	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;
 
subTen: X			{$$ = 10;}
 |X X		{$$ = 20;}
 |X X X		{$$ = 30;}
 ;
 
digit: subUnit	{$$ = $1;}
 |I V		{$$ = 4;}
 |V subUnit	{$$ = 5 + $2;}
 |I X		{$$ = 9;}
 ;
 
subUnit: I			{$$ = 1;}
 |I I		{$$ = 2;}
 |I I I		{$$ = 3;}
 ;

%%
void yyerror(char *s){
  printf("%s\n", s);
}