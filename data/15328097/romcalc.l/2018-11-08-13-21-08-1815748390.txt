%{
	# include "romcalc.tab.h"
	void yyerror(char *s);
%}

%%	

I	{return I;}
V	{return V;}
X	{return X;}
L	{return L;}
C	{return C;}
D	{return D;}
M	{return M;}
\n	{return EOL;}
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"{" { return OB; }
"}" { return CB; }
[0-9]+	{ yylval = atoi(yytext); return NUMBER; }
\n      { return EOL; }
[ \t]   { /* ignore white space */ }
.	{yyerror("syntax error");exit(0);}
%%

int main (void) {
	yyparse();
    return 0;
}
