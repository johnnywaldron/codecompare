%{
int count = 0;
%}

%%

[0-9]+  { count++; }
\s      { }

%%

int main() {
  yylex();
  printf("%d\n", count);
  return 0;
}
