%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yylex();
void yyerror(const char *s);

int vars[26];
%}

/* declare tokens */
%token VAR ASSIGN ADD SUB MUL DIV NUM COL PRINT
%%

full:
  | full VAR ASSIGN expr COL          { vars[$2] = $4; }
  | full PRINT VAR COL                { printf("%d\n", vars[$3]); }
  ;

expr: factor
  | expr ADD factor                    { $$ = $1 + $3; }
  | expr SUB factor                    { $$ = $1 - $3; }
  ;

factor: term
  | factor MUL term                   { $$ = $1 * $3; }
  | factor DIV term                   { $$ = $1 / $3; }
  ;

term: NUM
  | SUB NUM                           { $$ = 0 - $2; }
  | VAR                               { $$ = vars[$1]; }
  ;
%%

int main() {
  yyparse(); 
  return 0;
}

void yyerror(const char *s) {
  fprintf(stderr, "%s\n", s);
}
