%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yylex();
void yyerror(char *s);
char* printRoman(int a);

int dec[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
char *roman[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
%}

/* declare tokens */
%token ZERO
%token N1000 N900 N500 N400 N100 N90 N50 N40 N10 N9 N5 N4 N1
%token ADD SUB MUL DIV OPENPAREN CLOSEPAREN
%token EOL
%%

full:
  | full expr EOL                   { printRoman($2); }
  ;

expr: factor
  | expr ADD factor                 { $$ = $1 + $3; }
  | expr SUB factor                 { $$ = $1 - $3; }
  | OPENPAREN expr CLOSEPAREN       { $$ = $2; }
  ;

factor: numeral
  | factor MUL numeral              { $$ = $1 * $3; }
  | factor DIV numeral              { $$ = $1 / $3; }
  ;

numeral:
  | units                           
  | tens units                      { $$ = $1 + $2; }
  | hundreds tens units             { $$ = $1 + $2 + $3; }
  | thousands hundreds tens units   { $$ = $1 + $2 + $3 + $4; }
  | SUB numeral                     { $$ = 0 - $2; }
  ;

thousands:    { $$ = 0; }
  | N1000
  | N1000 N1000                     { $$ = $1 * 2; }
  | N1000 N1000 N1000               { $$ = $1 * 3; }
  ;

hundreds:   { $$ = 0; }
  | smallhundreds
  | N900
  | N500 smallhundreds              { $$ = $1 + $2; }
  | N500
  | N400
  ;

smallhundreds: N100
  | N100 N100                       { $$ = $1 * 2; }
  | N100 N100 N100                  { $$ = $1 * 3; }
  ;

tens:   { $$ = 0; }
  | smalltens
  | N90
  | N50 smalltens                   { $$ = $1 + $2; }
  | N50
  | N40
  ;

smalltens: N10
  | N10 N10                         { $$ = $1 * 2; }
  | N10 N10 N10                     { $$ = $1 * 3; }
  ;

units:  { $$ = 0; }
  | smallunits
  | N9
  | N5 smallunits                   { $$ = $1 + $2; }
  | N5
  | N4
  ;

smallunits: N1
  | N1 N1                           { $$ = $1 * 2; }
  | N1 N1 N1                        { $$ = $1 * 3; }
  ;

%%

int main() {
  yyparse();
  return 0;
}

void yyerror(char *s) {
  fprintf(stderr, "%s\n", s);
}

char* printRoman(int a) {
  if (a == 0) {
    printf("Z\n");
    return;
  }
  if (a < 0) {
    printf("-");
    a = abs(a);
  }

  int i = 0;
  while (a) {
    while (a / dec[i]) {
      printf("%s", roman[i]);
      a -= dec[i];
    }
    i++;
  }
  printf("\n");
}
