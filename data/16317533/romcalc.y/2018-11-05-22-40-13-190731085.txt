%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yylex();
void yyerror(const char *s);
void printRoman(int number);

int dec[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
char *roman[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
%}

/* declare tokens */
%token ZERO
%token N1000 N900 N500 N400 N100 N90 N50 N40 N10 N9 N5 N4 N1
%token ADD SUB MUL DIV OPENPAREN CLOSEPAREN
%token NOTNUM
%token EOL
%%

full:
  | full expr EOL                   { printRoman($2); }
  | full NOTNUM                     { printf("syntax error\n"); exit(0); }
  ;

expr: factor
  | expr ADD factor                           { $$ = $1 + $3; }
  | expr SUB factor                           { $$ = $1 - $3; }
  | OPENPAREN expr CLOSEPAREN ADD expr        { $$ = $2 + $5; }
  | OPENPAREN expr CLOSEPAREN SUB expr        { $$ = $2 - $5; }
  | OPENPAREN expr CLOSEPAREN MUL expr        { $$ = $2 * $5; }
  | OPENPAREN expr CLOSEPAREN DIV expr        { $$ = $2 / $5; }
  ;

factor: numeral
  | factor MUL numeral                        { $$ = $1 * $3; }
  | factor DIV numeral                        { $$ = $1 / $3; }
  ;

numeral:
  | units                           
  | tens units                                { $$ = $1 + $2; }
  | hundreds tens units                       { $$ = $1 + $2 + $3; }
  | thousands hundreds tens units             { $$ = $1 + $2 + $3 + $4; }
  | SUB numeral                               { $$ = 0 - $2; }
  ;

thousands:                                    { $$ = 0; }
  | N1000
  | N1000 N1000                               { $$ = $1 * 2; }
  | N1000 N1000 N1000                         { $$ = $1 * 3; }
  ;

hundreds:                                     { $$ = 0; }
  | smallhundreds
  | N900
  | N500 smallhundreds                        { $$ = $1 + $2; }
  | N500
  | N400
  ;

smallhundreds: N100
  | N100 N100                                 { $$ = $1 * 2; }
  | N100 N100 N100                            { $$ = $1 * 3; }
  ;

tens:                                         { $$ = 0; }
  | smalltens
  | N90
  | N50 smalltens                             { $$ = $1 + $2; }
  | N50
  | N40
  ;

smalltens: N10
  | N10 N10                                   { $$ = $1 * 2; }
  | N10 N10 N10                               { $$ = $1 * 3; }
  ;

units:                                        { $$ = 0; }
  | smallunits
  | N9
  | N5 smallunits                             { $$ = $1 + $2; }
  | N5
  | N4
  ;

smallunits: N1
  | N1 N1                                     { $$ = $1 * 2; }
  | N1 N1 N1                                  { $$ = $1 * 3; }
  ;

%%

int main() {
  yyparse();
  return 0;
}

void yyerror(const char *s) {
  fprintf(stderr, "%s\n", s);
}

void printRoman(int number){
  if(number == 0) {
    printf("Z");
    return;
  }
  if(number < 0) {
    printf("-");
    number = 0 - number;
  }
  while(number > 0){
    if(number >= 1000){
      printf("M");
      number -= 1000;
    } else if(number >= 900){
      printf("CM");
      number -= 900;
    } else if(number >= 500){
      printf("D");
      number -= 500;
    } else if(number >= 400){
      printf("CD");
      number -= 400;
    } else if(number >= 100){
      printf("C");
      number -= 100;
    } else if(number >= 90){
      printf("XC");
      number -= 90;
    } else if(number >= 50){
      printf("L");
      number -= 50;
    } else if(number >= 40){
      printf("XL");
      number -= 40;
    } else if(number >= 10){
      printf("X");
      number -= 10;
    } else if(number >= 9){
      printf("IX");
      number -= 9;
    } else if(number >= 5){
      printf("V");
      number -= 5;
    } else if(number >= 4){
      printf("IV");
      number -= 4;
    }else if(number >= 1){
      printf("I");
      number -= 1;
    }
  }
  printf("\n");
}
