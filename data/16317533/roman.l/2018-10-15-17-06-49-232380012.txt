%{
#include "roman.tab.h"
void yyerror(char *s);
%}

%%

M       { yylval = 1000; return NUM; }
CM      { yylval = 900; return NUM; }
D       { yylval = 500; return NUM; }
CD      { yylval = 400; return NUM; }
C       { yylval = 100; return NUM; }
XC      { yylval = 90; return NUM; }
L       { yylval = 50; return NUM; }
XL      { yylval = 40; return NUM; }
X       { yylval = 10; return NUM; }
IX      { yylval = 9; return NUM; }
V       { yylval = 5; return NUM; }
IV      { yylval = 4; return NUM; }
I       { yylval = 1; return NUM; }

\n      { return EOL; }
[ \t]   {  }
[^IVXLCDM] { return NOTNUM; }
.	      { return NOTNUM; }

%%
