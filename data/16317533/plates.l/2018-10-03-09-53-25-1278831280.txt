%{
#include <string.h>
#include <stdlib.h>
int getYear(char* string) {
  char buf[3];
  memcpy(buf, &string[0], 2);
  buf[2] = '\0';
  return atoi(buf);
}
%}

%%

1[3-9][12]-(C[ENW]?|D[L]?|G|K[EKY]|L[DHMS]?|M[HNO]|OY|RN|SO|T|W[HXW]?)-[1-9][0-9]{0,5}                { printf("%d\n", 18 - getYear(yytext)); }
(0[0-9]|1[0-2]|[89][0-9])-(C[ENW]?|D[L]?|G|K[EKY]|L[DHKMS]?|M[HNO]|OY|RN|SO|T[NS]|W[DHXW]?)-[1-9][0-9]{0,5} { int y = getYear(yytext); printf("%d\n", y < 19 ? 18 - y : 2018 - (1900 + y)); }
[0-9]*-[A-Z]*-[0-9]*                    { printf("INVALID\n"); }
[ \r\t\n]                               {}
.                                       { printf("INVALID\n"); }

%%

int main() {
  yylex();
  return 0;
}

