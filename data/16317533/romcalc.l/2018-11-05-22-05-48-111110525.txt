%{
#include "roman.tab.h"
void yyerror(char *s);
%}

%%

Z       { yylval = 0; return ZERO; }
M       { yylval = 1000; return N1000; }
CM      { yylval = 900; return N900; }
D       { yylval = 500; return N500; }
CD      { yylval = 400; return N400; }
C       { yylval = 100; return N100; }
XC      { yylval = 90; return N90; }
L       { yylval = 50; return N50; }
XL      { yylval = 40; return N40; }
X       { yylval = 10; return N10; }
IX      { yylval = 9; return N9; }
V       { yylval = 5; return N5; }
IV      { yylval = 4; return N4; }
I       { yylval = 1; return N1; }
"+"     { return ADD; }
"-"     { return SUB; }
"*"     { return MUL; }
"/"     { return DIV; }
\{      { return OPENPAREN; }
\}      { return CLOSEPAREN; }

\n      { return EOL; }
[ \t]   {  }
.	      { return NOTNUM; }

%%
