%{
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
%}

%%

\*\*.*      {}
\".*\"      { printf("%s", yytext); }
\{[^}]*\}   {}
\{[^}]      {}
\{          { printf("syntax error\n"); return 0; }
\}          { printf("syntax error\n"); return 0; }
\n          { printf("%s", yytext); }
.           { printf("%s", yytext); }

%%

int main() {
  yylex();
  return 0;
}
