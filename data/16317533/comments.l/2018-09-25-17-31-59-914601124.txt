%{
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
char code[1024];
bool openBrace = false;
bool err = false;
%}

%%

\*\*.*        {}
\".*\"        {}
\{            { openBrace = true; }
\}            { 
                if (!openBrace) {
                  err = true;
                } else { 
                  openBrace = false; 
                }
              }
\n            {
                if (!err && !openBrace) {
                  strncat(code, yytext, yyleng);
                }
              }
.             {
                if (!err && !openBrace) {
                  strncat(code, yytext, yyleng);
                }
              }

%%

int main() {
  yylex();
  if (err) {
    printf("syntax error\n");
    return 0;
  }
  printf("%s\n", code);
  return 0;
}
