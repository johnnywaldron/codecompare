%{
#include <stdio.h>
int yylex();
void yyerror(char *s);
%}

/* declare tokens */
%token NUM
%token ERR
%token EOL
%%

full:
  | full numeral EOL  { printf("%d\n", $2); }
  ;

numeral: NUM
  | numeral numeral   { $$ = $1 + $2; }
  ;

notNum: ERR
  | notNum ERR        { printf("syntax error\n"); return 0; }
  ;

%%

int main() {
  yyparse();
  return 0;
}
