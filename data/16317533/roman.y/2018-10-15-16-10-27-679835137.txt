%{
#include <stdio.h>
#include <stdlib.h>
int yylex();
void yyerror(char *s);
%}

/* declare tokens */
%token NUM
%token NOTNUM
%token EOL
%%

full:
  | full numeral EOL  { printf("%d\n", $2); }
  ;

numeral: NUM
  | numeral numeral   { $$ = $1 + $2; }
  ;

notNum: NOTNUM
  | notNum NOTNUM     { printf("syntax \n"); exit(0); }
  ;

%%

int main() {
  yyparse();
  return 0;
}

void yyerror(char *s) {
  fprintf(stderr, "%s\n", s);
}
