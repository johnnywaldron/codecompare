%{

# include "romcalc.tab.h"
void yyerror(char *s);

%}

%%

"/"		{return DIV;}
"*"		{return MULT;}
"+"		{return PLUS;}
"-"		{return MINUS;}

I		{return ONE;}
V		{return FIVE;}
X		{return TEN;}
L		{return FIFTY;}
C		{return HUNDRED;}
D		{return FIVE_HUNDRED;}
M		{return THOUSAND;}

Z		{return ZERO;}

"{"		{return LEFTB;}
"}"		{return RIGHTB;}

\n		{return EOL;}

[ \t]		{}	/*ignore whitespace*/
.		{yyerror("syntax error");return -1;}

%%
