%{

#include <stdio.h>
#include <stdlib.h>

extern int yylex();
void yyerror(char *msg);
%}

%union{
    int num;
}

%token I V X L C D M EOL ERROR
%type <num> thous fhunds hunds fifties tens fives ones

%%
number  :               
        |   number thous EOL    {printf("%d\n",$thous);}    
        |   ERROR               {printf("syntax error"); return 0;} 
        ;

thous   :   fhunds          
        |   M fhunds        {$$ = 1000 + $2;}
        |   M M fhunds      {$$ = 2000 + $3;}
        |   M M M fhunds    {$$ = 3000 + $4;}
        ;

fhunds  :   hunds       
        |   D hunds         {$$ = 500 + $2;}
        |   C D fifties     {$$ = 400 + $3;}
        |   C M fifties     {$$ = 900 + $3;}
        ;

hunds   :   fifties         
        |   C fifties       {$$ = 100 + $2;}
        |   C C fifties     {$$ = 200 + $3;}
        |   C C C fifties   {$$ = 300 + $4;}
        ;

fifties :   tens        
        |   L tens      {$$ = 50 + $2;}
        |   X L fives   {$$ = 40 + $3;}
        |   X C fives   {$$ = 90 + $3;}
        ;

tens    :   fives       
        |   X fives     {$$ = 10 + $2;}
        |   X X fives   {$$ = 20 + $3;}
        |   X X X fives {$$ = 30 + $4;}
        ;

fives   :   ones        
        |   V ones      {$$ = 5 + $2;}
        |   I V         {$$ = 4;}
        |   I X         {$$ = 9;}
        ;

ones    :               {$$ = 0;}
        |   I           {$$ = 1;}
        |   I I         {$$ = 2;}
        |   I I I       {$$ = 3;}
        ;
%%

void yyerror(char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(0);
}

int main()
{
    yyparse();
    return 0;
}