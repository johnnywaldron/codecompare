%{
#include <stdio.h>
#include <string.h>

int threeDig = 0;
int yearsSince = 0;
char *year;
char *cnty;
char *id;

int validCounty(char county[], int three)
{   
    if(!strcmp(county,"C"))         //Cork
        //3 and 2
        return 1;
    else if(!strcmp(county,"CE"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"CN"))  //
        //3 and 2
        return 1;
    else if(!strcmp(county,"CW"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"D"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"DL"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"G"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"KE"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"KK"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"KY"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"L"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"LK"))  //
        //2
        if(!three) return 1; 
        else return 0;
    else if(!strcmp(county,"LD"))  //
        //3 and 2
        return 1;
    else if(!strcmp(county,"LH"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"LM"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"LS"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"MH"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"MN"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"MO"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"OY"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"RN"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"SO"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"T"))   //
        //3 only not 2
        if(three) return 1;
        else return 0;
    else if(!strcmp(county,"TN"))   //
        //2 only not 3
        if(!three) return 1;
        else return 0;
    else if(!strcmp(county,"TS"))   //
        //2 only not 3
        if(!three) return 1;
        else return 0;
    else if(!strcmp(county,"W"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"WD"))   //
        //2 only not 3
        if(!three) return 1;
        else return 0;
    else if(!strcmp(county,"WH"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"WX"))   //
        //3 and 2
        return 1;
    else if(!strcmp(county,"WW"))   //
        //3 and 2
        return 1;
    else 
        return 0;
}

int validYear(int year)
{
    if(strlen(yytext) == 3)
    {
        if(year == 131 || year == 132 || year == 141 || year == 142 || year == 151 || year == 152 || year == 161 || year == 162 
        || year == 171 || year == 172 || year == 181 || year == 182)
        return 1;
        else 
        return 0;
    }
    else if(strlen(yytext) == 2)
    {
        if(year >= 87 && year <= 99)
            return 1;
        else if(year >= 00 && year <= 12)
            return 1;
        else    
            return 0;
    }
    else
        return 0;
}

int getNum(int year)
{
    if(year > 18)
        return (118) - year;
    else
        return 18-year;
}
%}

%%

[0-9]{2}[-][A-Z][-][0-9]{1,6}  {    char *token = strtok(yytext,"-");
                                    threeDig = 0; 
                                    int index = 0;
                                    while(token != NULL)
                                    {
                                        if(index == 0)
                                            year = token;
                                        else if(index == 1)
                                            cnty = token;
                                        else if(index == 3)
                                            id = token;

                                        token = strtok(NULL, "-");
                                        index++;
                                    }  
                                    
                                    if( (validYear(atoi(year))) != 0 && validCounty(cnty,threeDig) != 0)
                                    {   
                                        printf("%d years\n",getNum(atoi(year)));  

                                    }
                                    else printf("INVALID");
                                }

[0-9]{3}[-][A-Z]*[-][0-9]{1,6}  {   char *token = strtok(yytext,"-");
                                    int index = 0;
                                    while(token != NULL)
                                    {
                                        if(index == 0)
                                            year = token;
                                        else if(index == 1)
                                            cnty = token;
                                        else if(index == 3)
                                            id = token;

                                        token = strtok(NULL, "-");
                                        index++;
                                    }  
                                    if(((atoi(year))/10) >= 14)
                                        threeDig = 1; 
                                    else 
                                        threeDig = 0;
                                    if( (validYear(atoi(year))) != 0 && validCounty(cnty,threeDig) != 0)
                                    {   
                                        printf("%d years\n",getNum(atoi(year)/10));  

                                    }
                                    else printf("INVALID");
                                }

[\n]            {}
[.]             {}

%%

int main()
{
  yylex(); 
  return 0;
}
