%{
#  include <stdio.h>
# include <string.h>
int yylex();
void yyerror(char *s);
%}

/* declare tokens */
%token ONE
%token FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND ABS
%token EOL EL ADD SUB MUL DIV
%%
calclist: /* nothing */
 | calclist start EOL { printf("%d\n", $2); }
 | calclist ti EOL { char s[1024]; 
		     s[0] = '\0';
	             int total = $2;
			if ( total == 0) {
			strcat(s, "Z"); } 
			while (total >= 1000){ 
			strcat(s, "M"); 
			total -= 1000; }
			while (total >= 900){ 
			strcat(s, "CM"); 
			total -= 900; }
			while (total >= 500){ 
			strcat(s, "D"); 
			total -= 500; }
			while (total >= 400){ 
			strcat(s, "CD"); 
			total -= 400; }
			while (total >= 100){ 
			strcat(s, "C"); 
			total -= 100; }
		        while (total >= 90){ 
			strcat(s, "XC"); 
			total -= 90; }
			while (total >= 50){ 
			strcat(s, "L"); 
			total -= 50; }
			while (total >= 40){ 
			strcat(s, "XL"); 
			total -= 40; }
			while (total >= 10){ 
			strcat(s, "X"); 
			total -= 10; }
			while (total >= 9){ 
			strcat(s, "IX"); 
			total -= 9; }
			while (total >= 5){ 
			strcat(s, "V"); 
			total -= 5; }
			while (total >= 4){ 
			strcat(s, "IV"); 
			total -= 4; }
			while (total >= 1){ 
			strcat(s, "I"); 
			total -= 1; }
			printf("%s\n", s);} 
 ; 




ti: start
 | ti ADD ti {$$ = $1 + $3;}
 | ti SUB ti {$$ = $1 - $3;}
 | ti MUL ti {$$ = $1 * $3;}
 | ti DIV ti {$$ = $1 / $3;}
 | ti ONE {$$ = $1 + 1;}
 /* ALL CASES FOR 5 */
 | ONE FIVE {$$ = +4;}
 | ti ONE FIVE {$$ = $1 +4;}
 | ti FIVE {$$ = $1 + 5;}
 /* ALL CASES FOR 10 */
 | ONE TEN {$$ = +9;}
 | ti ONE TEN {$$ = $1 + 9;}
 | ti TEN {$$ = $1 + 10;}
 /* ALL CASES FOR 50  */
 | TEN FIFTY {$$ = +40;}
 | ti TEN FIFTY {$$ = $1 + 40;}
 | ti FIFTY {$$ = $1 + 50;}
 /* ALL CASES FOR 100 */
 | TEN HUNDRED {$$ = +90;}
 | ti TEN HUNDRED {$$ = $1 + 90;}
 | ti HUNDRED {$$ = $1 + 100;}
 /* ALL CASES FOR 500 */
 | HUNDRED FIVEHUNDRED {$$ = +400;}
 | ti HUNDRED FIVEHUNDRED {$$ = $1 + 400;}
 | ti FIVEHUNDRED { $$ = $1 + 500;}

 /*  ALL CASES FOR 1000     */
 | HUNDRED THOUSAND { $$ = +900;}
 | ti HUNDRED THOUSAND {$$ = $1 + 900;}
 | ti THOUSAND {$$ = $1 + 1000;}
 | HUNDRED THOUSAND HUNDRED HUNDRED{ yyerror("syntax error");return; }
 ;



 start: ONE | FIVE | TEN | FIFTY | HUNDRED | FIVEHUNDRED | THOUSAND 
 |ABS ONE { $$ = $2;}
 |ABS FIVE { $$ = $2;}
 |ABS TEN { $$ = $2;}
 |ABS FIFTY { $$ = $2;}
 |ABS HUNDRED { $$ = $2;}
 |ABS FIVEHUNDRED { $$ = $2;}
 |ABS THOUSAND { $$ = $2;}
 ;
%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
}








