%{
int even = 0;
int odd = 0;
int evenOrOdd = 0;
%}

%%
[0-9]+ {evenOrOdd=atoi(yytext); if(evenOrOdd%2==0) even++; else odd++;}
%%

int main()
{
  yylex();
  printf("%8d\n",even);
  return 0;
}
