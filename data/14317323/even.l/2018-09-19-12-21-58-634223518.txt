%{
int even = 0;
int odd = 0;
int evenOrOdd = 0;
%}

%%
[2469] {even++;}
[013579] {odd++;}
%%

int main()
{
  yylex();
  printf("%8d\n",even);
  return 0;
}
