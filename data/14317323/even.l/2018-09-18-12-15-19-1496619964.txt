%{ 
int evenNumbers = 0; 
int lines = 0;
int chars = 0;
int oddNumbers = 0;
%} 

%% 
[2468] { evenNumbers++;} 
\n { lines++; } 
. { chars++; } 
[013579]+ {oddNumbers++;}
%% 
int main() 
{ 
yylex(); 
printf("%8d \n", evenNumbers); 
return 0; 
} 
