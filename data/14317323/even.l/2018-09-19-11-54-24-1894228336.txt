%{
int even = 0;
int odd = 0;
%}

%%
[2468]             {even++;}
[013579]           {odd++;}
%%

int main()
{
  yylex();
  printf("%8d\n",even);
  return 0;
}
