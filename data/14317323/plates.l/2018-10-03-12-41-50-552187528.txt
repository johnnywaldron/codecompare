%{
int year = 0;
%}

%%
^([0-9]{3}?)[-] {year = atoi(yytext); printf("%d\n",year);}
%%

int main()
{
  yylex();
  return 0;
}
