%{
int years = 0;
%}
YEAR [0-9]{1,3}
INVALIDYEAR [0-9]{4,}?
DASH	-
COUNTY CE|CN|CW|DL|KE|KK|KY|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|W|WH|WX|WW|G|D|L|C
NUMBER [0-9]{1,6}?
INVALIDNUM [0-9]{7,}?
%%
{YEAR}{DASH}"LK"{DASH}{NUMBER}			{printf("Limerick City");}
{YEAR}{DASH}"TN"{DASH}{NUMBER}			{printf("North Tipperary");}
{YEAR}{DASH}"TS"{DASH}{NUMBER}			{printf("South Tipperary");}
{YEAR}{DASH}"T"{DASH}{NUMBER}			{years= atoi(yytext);
						if(years > 100) {
							years = years/10;
							years = 18-years; }
						else if(years > 18) {
							years = 100 - years;
							years = 18 + years; }
						else {
							years = 18 - years; }
						if(years <= 4) {
						printf("%d\n",years); }	
						else	{printf("INVALID\n");}			
							}
{YEAR}{DASH}"W"{DASH}{NUMBER}			{printf("Waterford City");}
{YEAR}{DASH}"WD"{DASH}{NUMBER}			{printf("County Waterford");}
{YEAR}{DASH}{COUNTY}{DASH}{NUMBER}		{years= atoi(yytext);
						if(years > 100) {
							years = years/10;
							years = 18-years; }
						else if(years > 18) {
							years = 100 - years;
							years = 18 + years; }
						else {
							years = 18 - years; }
						printf("%d\n",years);						
							}
{YEAR}{DASH}{COUNTY}{DASH}{INVALIDNUM}		{printf("INVALID\n");}
{YEAR}{DASH}[a-zA-Z]{DASH}{NUMBER}		{printf("INVALID\n");}
{INVALIDYEAR}{DASH}{COUNTY}{DASH}{NUMBER}	{printf("INVALID\n");}
^{INVALIDYEAR}					{printf("INVALID\n");}
. 						{}
\n 						{}

%%

int main()
{
  yylex();
  return 0;
}
