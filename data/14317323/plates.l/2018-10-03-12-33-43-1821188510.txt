%{
int year = 0;
%}

%%
^([0-9]{2})[-] {year = atoi(yytext); printf("%d\n",year);}
%%

int main()
{
  yylex();
  return 0;
}
