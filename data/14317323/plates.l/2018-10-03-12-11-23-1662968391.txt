%{
int year = 0;
%}

%%
[^]([0-9]+)[-] {year = atoi(yytext); year = 2018-year; printf("%d\n",year);}
%%

int main()
{
  yylex();
  printf("%d\n",year);
  return 0;
}
