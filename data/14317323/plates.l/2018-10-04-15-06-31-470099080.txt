%{
int years = 0;
%}
YEAR [0-9]{2}
SPACE [\t\n\ ]+
INVALIDYEAR [0-9]{4,}?
COUNTY [C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]
INVALIDCO ^{COUNTY}
NUMBER [0-9]{1,6}?
INVALIDNUM [0-9]{7,}?
%%


. {}
\n 		{}
^{YEAR} 	{years = atoi(yytext); if(years > 18) {years = 100-years; years = years + 18;} else {years = 18-years;} printf("%d\n",years);}

{SPACE}{YEAR} {years = atoi(yytext); if(years > 18) {years = 100-years; years = years + 18;} else {years = 18-years;} printf("%d\n",years);}

^{INVALIDYEAR}     {printf("INVALID\n");}
[-]({INVALIDCO})[-] {printf("INVALID\n");}







%%

int main()
{
  yylex();
  return 0;
}
