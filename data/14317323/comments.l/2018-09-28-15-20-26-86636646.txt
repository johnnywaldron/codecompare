%{
char c = '"';
%}
%x C_COMMENT

%%


c(.)c		{ECHO;}
"{"            { BEGIN(C_COMMENT);}
<C_COMMENT>"}" { BEGIN(INITIAL); }
<C_COMMENT>\n   {}
<C_COMMENT>.    {}
"**".*      {}
%%

int main()
{
  yylex();
  return 0;
}
