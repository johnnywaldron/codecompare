%x C_COMMENT
%x   X_COMMENT	
COM "
%%
"**".*      {}

COM            { ECHO; BEGIN(X_COMMENT); }
<X_COMMENT>COM { ECHO; BEGIN(INITIAL); }
<X_COMMENT>\n   {ECHO; }
<X_COMMENT>.    {ECHO; }

"{"            { BEGIN(C_COMMENT); }
<C_COMMENT>"}" { BEGIN(INITIAL); }
<C_COMMENT>\n   { }
<C_COMMENT>.    { }
%%

int main()
{
  yylex();
  return 0;
}
