%{
char c = '"';
%}
%x C_COMMENT
%s COMMENT
%%

c           { BEGIN(COMMENT); }
<C_COMMENT>c { BEGIN(INITIAL); }
<C_COMMENT>\n   {ECHO; }
<C_COMMENT>.    {ECHO; }

"{"            { BEGIN(C_COMMENT); }
<C_COMMENT>"}" { BEGIN(INITIAL); }
<C_COMMENT>\n   { }
<C_COMMENT>.    { }
"**".*      {}
%%

int main()
{
  yylex();
  return 0;
}
