%{

%}

COUNTY_PRE_14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
COUNTY_POST_14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW

YEAR_PRE_13 (8[7-9])|(9[0-9])|(0[0-9])|(1[0-2])
YEAR_13 13[1|2]
YEAR_POST_13 (1[4-9][1|2])

SERIAL [0-9]{1,6}

%%
{YEAR_PRE_13}{1}\-{COUNTY_PRE_14}{1}\-{SERIAL}{1}(" "|\t|\n)+    {
                                                                    char *yearString = strtok(yytext, "-");
                                                                    int year = atoi(yearString);
                                                                    if(year > 18)
                                                                      printf("%d\n", (100 - year + 18));
                                                                    else
                                                                      printf("%d\n", (18 - year));
                                                                 }

{YEAR_13}{1}\-{COUNTY_PRE_14}{1}\-{SERIAL}{1}(" "|\t|\n)+         {
                                                                    char *yearString = strtok(yytext, "-");
                                                                    char *yearSubString = malloc(2);
                                                                    strncpy(yearSubString, yearString, 2);
                                                                    int year = atoi(yearSubString);
                                                                    if(year > 18)
                                                                      printf("%d\n", (100 - year + 18));
                                                                    else
                                                                      printf("%d\n", (18 - year));
                                                                    free(yearSubString);
                                                                 }

{YEAR_POST_13}{1}\-{COUNTY_POST_14}{1}\-{SERIAL}{1}(" "|\t|\n)+   {
                                                                    char *yearString = strtok(yytext, "-");
                                                                    char *yearSubString = malloc(2);
                                                                    strncpy(yearSubString, yearString, 2);
                                                                    int year = atoi(yearSubString);
                                                                    if(year > 18)
                                                                      printf("%d\n", (100 - year + 18));
                                                                    else
                                                                      printf("%d\n", (18 - year));
                                                                    free(yearSubString);
                                                                 }

[^\t" "\n]*      {printf("INVALID\n");}
.       {}
\n      {}
%%

int main(int argc, char* argv[])
{
  yyin = fopen(argv[1], "r");
  yylex();
  fclose(yyin);
  return 0;
}

