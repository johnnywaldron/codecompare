%{
int count = 0;
%}

%%

[0-9]+	{int tmp = atoi(yytext); if((tmp % 2) == 0) {count++;}}

%%

int main()
{
  yylex();
  printf("\n%8d", count);
	return 0;
}

