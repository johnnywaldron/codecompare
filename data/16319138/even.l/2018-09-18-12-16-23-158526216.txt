%{
int count = 0;
%}

%%
[0-9]+  {int tmp = atoi(yytext); if((tmp % 2) == 0) {count++;}}
%%

int main(int argc, char* argv[])
{
  yylex();
  printf("%8d\n", count);
  return 0;
}

