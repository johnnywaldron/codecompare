%{
%}

%%
["].*\n*["]   { printf("%s", yytext); }
[*]{2}.*\n    {  }
[{]|[}]       { printf("syntax error\n"); exit(0); }
[{].*[}]      {  }
[{](\n.*)+[}] {  }
.             { printf("%s", yytext); }
\n            { printf("%s", yytext); }
%%

int main(int argc, char* argv[])
{
  yylex();
  return 0;
}

