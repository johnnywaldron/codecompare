/* plates.l  :Process number plates and outputs number of years since registration*/


%{
const int currentYear = 2018;
int regYear;
int carAge;
int digit1;
int digit2;
%}

COUNTY14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW

COUNTY87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW

SPACE [ \t\n]

%%


/*Years 1987-1999*/
((8[7-9])|(9[0-9]))"-"{COUNTY87}"-"[0-9]{1,6}{SPACE} {digit1 = yytext[0]-'0'; digit2 = yytext[1]-'0'; regYear = 1900 + (yytext[0]*10) + yytext[1]; carAge = currentYear - regYear; printf("%d\n",carAge); }

/*Years 2000-2012*/
((0[0-9])|(1[0-2]))-{COUNTY87}-[0-9]{1,6}{SPACE} {digit1 = yytext[0]-'0'; digit2 = yytext[1]-'0'; regYear = 2000 + (yytext[0]*10) + yytext[1]; carAge = currentYear - regYear; printf("%d\n",carAge); }  

/*Year 2013*/
13(1|2)-{COUNTY87}-[0-9]{1,6}{SPACE}   { printf("%d\n",5);}

/*Years 2014-2018*/
1[4-8](1|2)-{COUNTY14}-[0-9]{1,6}{SPACE} {digit1 = yytext[0]-'0'; digit2 = yytext[1]-'0'; regYear = 2000 + (yytext[0]*10) + yytext[1]; carAge = currentYear - regYear; printf("%d\n",carAge); }

/*Invalid Registrations*/
[^{SPACE}]*{SPACE}           { printf("INVALID\n"); }



%%

int main()
{
  yylex();
  return 0;
}
