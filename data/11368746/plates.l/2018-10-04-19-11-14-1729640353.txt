/* plates.l  :Process number plates and outputs number of years since registration*/
%{

int digit1;
int digit2;
int regYear;
const int currentYear = 2018;
int carAge;

%}

COUNTY87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW

SPACE [ \t\n]

%%
//Years 1987-1999
((8[7-9])|(9[0-9]))"-"{COUNTY87}"-"[0-9]{1,6}{SPACE}    {digit1 = yytext[0] - '0'; digit2 = yytext[1] - '0'; regYear = 1900 + digit1*10 + digit2; carAge = currentYear - regYear; printf("%d\n",carAge);}



%%

int main()
{
  yylex();
  return 0;
}
