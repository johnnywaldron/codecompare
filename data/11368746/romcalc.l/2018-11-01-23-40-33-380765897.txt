%{
# include "romcalc.tab.h"
void yyerror(char *s);
%}

%%
"+"	{return PLUS;}
"-"	{return MINUS;}
"*"	{return MUL;}
"/"	{return DIV;}
"{" 	{return OP;}
"}"	{return CP;}      


\n      { return EOL;}

I   	{  return ONE; }
II  	{  return TWO; }
III 	{  return THREE; }
IV  	{  return FOUR; }
V  	{  return FIVE; }
VI  	{  return SIX; }
VII  	{  return SEVEN; }
VIII  	{  return EIGHT; }
IX	{  return NINE; }

X	{ return TEN; }
XX	{ return TWENTY; }
XXX	{ return THIRTY; }
XL	{ return FORTY; }
L	{ return FIFTY; }
LX	{ return SIXTY; }
LXX	{ return SEVENTY; }
LXXX	{ return EIGHTY; }
XC	{ return NINTY; }

C	{return ONEHUNDRED;}
CC	{return TWOHUNDRED;}
CCC 	{return THREEHUNDRED;}
CD   	{return FOURHUNDRED;}
D	{return FIVEHUNDRED;}
DC	{return SIXHUNDRED;}
DCC	{return SEVENHUNDRED;}
DCCC	{return EIGHTHUNDRED;}
CM	{return NINEHUNDRED;}

M	{return ONETHOUSAND;}

.       {}

%%
