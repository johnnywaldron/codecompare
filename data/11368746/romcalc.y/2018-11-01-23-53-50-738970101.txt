%{
#  include <stdio.h>
int yylex();
void yyerror(char *s);
void printArabicToRoman(int num);
%}

%token PLUS MINUS MUL DIV EOL 
%token ONE TWO THREE FOUR FIVE SIX SEVEN EIGHT NINE
%token TEN TWENTY THIRTY FORTY FIFTY SIXTY SEVENTY EIGHTY NINTY
%token ONEHUNDRED TWOHUNDRED THREEHUNDRED FOURHUNDRED FIVEHUNDRED SIXHUNDRED SEVENHUNDRED EIGHTHUNDRED NINEHUNDRED
%token ONETHOUSAND
%token OP CP
%%
expList: expList exp EOL      {printArabicToRoman($2);}
       | exp EOL              {printArabicToRoman($1);}
;

exp: exp PLUS addpart 	      {$$ = $1 + $3;}
   | exp MINUS addpart	      {$$ = $1 - $3;} 
   | addpart		      {$$ = $1;}

addpart: addpart MUL mulpart {$$ = $1 * $3;} 
 | addpart DIV mulpart	     {$$ = $1 / $3;} 
 | mulpart 		     {$$ = $1;}
;

mulpart: numeral    {$$ = $1;}
  | OP addpart CP   {$$ = $2;}
;


numeral: thousands hundreds tens unit {$$ = $1 + $2 + $3 + $4;}    
;

unit: /*nothing*/  {$$ = 0;}
  | ONE       {$$ = 1;}
  | TWO       {$$ = 2;} 
  | THREE     {$$ = 3;}
  | FOUR      {$$ = 4;}
  | FIVE      {$$ = 5;}
  | SIX       {$$ = 6;}
  | SEVEN     {$$ = 7;}
  | EIGHT     {$$ = 8;}
  | NINE      {$$ = 9;}
;

tens: /*nothing*/ {$$ = 0;}
   |  TEN     {$$ = 10;}
   |  TWENTY  {$$ = 20;}
   |  THIRTY  {$$ = 30;}
   |  FORTY   {$$ = 40;}
   |  FIFTY   {$$ = 50;}
   |  SIXTY   {$$ = 60;}
   |  SEVENTY {$$ = 70;}
   |  EIGHTY  {$$ = 80;}
   |  NINTY   {$$ = 90;}
;

hundreds: /*nothing*/  {$$ = 0;}
  | ONEHUNDRED       {$$ = 100;}
  | TWOHUNDRED       {$$ = 200;} 
  | THREEHUNDRED     {$$ = 300;}
  | FOURHUNDRED      {$$ = 400;}
  | FIVEHUNDRED      {$$ = 500;}
  | SIXHUNDRED       {$$ = 600;}
  | SEVENHUNDRED     {$$ = 700;}
  | EIGHTHUNDRED     {$$ = 800;}
  | NINEHUNDRED      {$$ = 900;}
;


thousands: /*nothing*/      {$$ = 0;}
  | thousands ONETHOUSAND   {$$ = $1 + 1000;}
;

%%

int main()
{
  yyparse();
  return 0;
}

void printArabicToRoman(int num)
{
 
 if(num == 0){ 
   printf("Z\n");
 }else{

  int thousands = num / 1000; 
  num = num % 1000;          
  int hundreds = num / 100;   
  num = num % 100;            
  int tens = num / 10;        
  num = num % 10;             
  

  int i;
  for(i=0; i < thousands; i++){
      printf("%s","M");
  }
  
  

  if(hundreds == 1)  printf("%s","C");
  else if(hundreds == 2)  printf("%s","CC");
  else if(hundreds == 3)  printf("%s","CCC");
  else if(hundreds == 4)  printf("%s","CD");
  else if(hundreds == 5)  printf("%s","D");
  else if(hundreds == 6)  printf("%s","DC");
  else if(hundreds == 7)  printf("%s","DCC");
  else if(hundreds == 8)  printf("%s","DCCC");
  else if(hundreds == 9)  printf("%s","CM");

  

  if(tens == 1)  printf("%s","X");
  else if(tens == 2)  printf("%s","XX");
  else if(tens == 3)  printf("XXX");
  else if(tens == 4)  printf("%s","XL");
  else if(tens == 5)  printf("%s","L");
  else if(tens == 6)  printf("%s","LX");
  else if(tens == 7)  printf("%s","LXX");
  else if(tens == 8)  printf("%s","LXXX");
  else if(tens == 9)  printf("%s","XC");

  
  if(num == 1) printf("I\n");
  if(num == 2) printf("II\n");
  if(num == 3) printf("III\n");
  if(num == 4) printf("IV\n");
  if(num == 5) printf("V\n");
  if(num == 6) printf("VI\n");
  if(num == 7) printf("VII\n");
  if(num == 8) printf("VIII\n");
  if(num == 9) printf("IX\n");
  if(num == 0) printf("\n");

 }

}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
}
