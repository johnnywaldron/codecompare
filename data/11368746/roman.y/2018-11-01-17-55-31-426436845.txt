%{
#  include <stdio.h>
int yylex();
void yyerror(char *s);
%}

%token EOL 
%token ONE TWO THREE FOUR FIVE SIX SEVEN EIGHT NINE
%token TEN TWENTY THIRTY FORTY FIFTY SIXTY SEVENTY EIGHTY NINTY
%token ONEHUNDRED TWOHUNDRED THREEHUNDRED FOURHUNDRED FIVEHUNDRED SIXHUNDRED SEVENHUNDRED EIGHTHUNDRED NINEHUNDRED
%token ONETHOUSAND
%%
numList: numList numeral EOL   {if($2 != 0) {printf("%d\n",$2);}else{printf("%s\n","syntax error");}}
   | numeral EOL               {if($1 !=0)  {printf("%d\n",$1);}else{printf("%s\n","syntax error");}}          
;

numeral: thousands hundreds tens unit {$$ = $1 + $2 + $3 + $4;}    
;

unit: /*nothing*/  {$$ = 0;}
  | ONE       {$$ = 1;}
  | TWO       {$$ = 2;} 
  | THREE     {$$ = 3;}
  | FOUR      {$$ = 4;}
  | FIVE      {$$ = 5;}
  | SIX       {$$ = 6;}
  | SEVEN     {$$ = 7;}
  | EIGHT     {$$ = 8;}
  | NINE      {$$ = 9;}
;

tens: /*nothing*/ {$$ = 0;}
   |  TEN     {$$ = 10;}
   |  TWENTY  {$$ = 20;}
   |  THIRTY  {$$ = 30;}
   |  FORTY   {$$ = 40;}
   |  FIFTY   {$$ = 50;}
   |  SIXTY   {$$ = 60;}
   |  SEVENTY {$$ = 70;}
   |  EIGHTY  {$$ = 80;}
   |  NINTY   {$$ = 90;}
;

hundreds: /*nothing*/  {$$ = 0;}
  | ONEHUNDRED       {$$ = 100;}
  | TWOHUNDRED       {$$ = 200;} 
  | THREEHUNDRED     {$$ = 300;}
  | FOURHUNDRED      {$$ = 400;}
  | FIVEHUNDRED      {$$ = 500;}
  | SIXHUNDRED       {$$ = 600;}
  | SEVENHUNDRED     {$$ = 700;}
  | EIGHTHUNDRED     {$$ = 800;}
  | NINEHUNDRED      {$$ = 900;}
;


thousands: /*nothing*/      {$$ = 0;}
  | thousands ONETHOUSAND   {$$ = $1 + 1000;}
;

%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
}
