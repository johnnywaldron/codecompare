/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.l,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

/* recognize tokens for the calculator and print them out */

%{
# include "roman.tab.h"
void yyerror(char *s);
%}

%%
I   	{ return ONE; }
II  	{ return TWO; }
III 	{ return THREE; }
IV  	{ return FOUR; }
V  	    { return FIVE; }
VI  	{ return SIX; }
VII  	{ return SEVEN; }
VIII  	{ return EIGHT; }
IX	    { return NINE; }

X	    { return TEN; }
XX	    { return TWENTY; }
XXX	    { return THIRTY; }
XL	    { return FORTY; }
L	    { return FIFTY; }
LX	    { return SIXTY; }
LXX	    { return SEVENTY; }
LXXX	{ return EIGHTY; }
XC	    { return NINTY; }

C       { return ONEHUNDRED; }
CC	    { return TWOHUNDRED; }
CCC	    { return TREEHUNDRED; }
CD	    { return FOURHUNDRED; }
D	    { return FIVEHUNDRED; }
DC	    { return SIXHUNDRED; }
DCC	    { return SEVENHUNDRED; }
DCCC	{ return EIGHTHUNDRED; }
CM	    { return NINEHUNDRED; }

M	    { return ONETHOUSAND; }

\n      { return EOL; }
[ \t]   { /* ignore white space */ }
.	    { yyerror("Mystery character\n");}
%%
