%{
int countEven=0;
int countOther=0;
int no;
%}
%%
[0-9]+*[2|4|6|8]+ {no = atoi(yytext); if(no%2 == 0) ; countEven++;}
[0-9]+*[1|3|5|7|9]+ {no = atoi(yytext); if(no%2 != 0) ; countOther++;}
. {}
\n {}
%%
int main(){
yylex();
printf(""%8d%8d%\n",countEven, countOther);

return 0;
}
