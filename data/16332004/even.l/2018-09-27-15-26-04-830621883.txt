%{
int counter=0;
%}

%%
[0-9]   {counter++;}
%%

int main()
{
  yylex();
  printf("%8\n", counter);
  return 0;
}
