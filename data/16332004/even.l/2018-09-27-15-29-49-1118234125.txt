%{
int counter=0;
%}

%%
[0,2,4,6,8]+    {counter++;}
%%

int main()
{
  yylex();
  printf("%d\n", counter);
  return 0;
}
