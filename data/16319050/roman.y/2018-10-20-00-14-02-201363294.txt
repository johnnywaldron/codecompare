%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
void yyerror(char *s);
%}

/* declare tokens */
%token ONE FIVE TEN FIFTY ONEHUN FIVEHUN THOUSAND NINE FOUR NINETY FORTY FORHUN NINHUN
%token EOL
%%
calclist: /* nothing */
 | calclist number EOL { printf("%d\n", $2); }
 ; 

number: subnum {$$ = $1;}
 | romenum {$$ = $1;}
 | romenum number{$$ = $1 + $2;}
 | subnum number{if ($1 < $2*5) {yyerror(!);} else {$$ = $1 + $2;}}
 
 ;

subnum: FOUR | NINE | FORTY | NINETY | FORHUN | NINHUN {$$ = $1;}
;

romenum: ONE | FIVE | TEN | FIFTY | ONEHUN | FIVEHUN | THOUSAND {$$ = $1;}
;
 

%%
int main()
{
  printf(""); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(0);
}
