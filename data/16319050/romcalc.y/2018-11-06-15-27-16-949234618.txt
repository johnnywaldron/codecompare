%{
#  include <stdio.h>
#  include <stdlib.h>
int yylex();
void yyerror(char *s);
%}


/* declare tokens */
%token ONE FIVE TEN FIFTY ONEHUN FIVEHUN THOUSAND NINE FOUR NINETY FORTY FORHUN NINHUN
%token EOL

%%
calclist: /* nothing */
 | calclist exp EOL { int2rom($2); printf("\n"); }
 ; 

number: subnum {$$ = $1;}
 | romenum {$$ = $1;}
 | romenum number{$$ = $1 + $2;}
 | subnum number{if ($1 < $2*5) {yyerror("syntax error");} else {$$ = $1 + $2;}}
 ;

subnum: FOUR | NINE | FORTY | NINETY | FORHUN | NINHUN {$$ = $1;}
;

romenum: ONE | FIVE | TEN | FIFTY | ONEHUN | FIVEHUN | THOUSAND {$$ = $1;}
;

exp: factor
 | exp '+' factor { $$ = $1+$3; }
 | exp '-' factor { $$ = $1-$3;}
 ;

factor: term
 | factor '*' term { $$ = $1*$3; }
 | factor '/' term { $$ = $1/$3; }
 ;

term: number   { $$ = $1; }
 | '(' exp ')' { $$ = $2; }
 | '-' term    { $$ = -$2; }
 ; 

%%
int main()
{
  printf(""); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(0);
}

void int2rom(int n){
	if (n == 0) printf("Z"); 
	if (n < 0) {
		n = n*-1;
		printf("-");
	}
    int d[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
    char *s[] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
    int i = 0;
    while(n){
        while(n/d[i]){
            printf("%s",s[i]);
            n -= d[i];
        }
        i++;
    }
}