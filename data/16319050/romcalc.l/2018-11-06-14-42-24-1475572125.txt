%{
# include "romcalc.tab.h"

void yyerror(char *s);
int yylex();


%}

%%
"+" |
"-" |
"*" |
"/" |
"|" |
"(" |
")"  { return ytext[0];}	 
"CM" { yylval = 900; return NINHUN; }
"CD" { yylval = 400; return FORHUN; }
"XL" { yylval = 40; return FORTY; }
"XC" { yylval = 90; return NINETY; }
"IX" { yylval = 9; return NINE; }
"IV" { yylval = 4; return FOUR; }
"I" { yylval = 1; return ONE; }
"V" { yylval = 5; return FIVE; }
"X" { yylval = 10; return TEN; }
"L"  { yylval = 50; return FIFTY; }
"C"  { yylval = 100; return ONEHUN; }
"D"  { yylval = 500; return FIVEHUN; }
"M"  { yylval = 1000; return THOUSAND; }
\n      { return EOL; }
.	 	{ yyerror("syntax error");}
%%
