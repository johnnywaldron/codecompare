/* comment.l */

%{
#include<stdio.h>
int flag = 0;
int EOS = 0;
%}

%%
[A-Z]+ {yytext;}
\{ {flag=1;}
\} {flag=0;}
\*\*.* {flag=1;}
\n		{}
%%

#include <stdlib.h>

int main()
{
	char toc;
	while (toc=yylex()){
		if (flag==0)printf("%c", toc);		
		if (EOS==1)printf("syntax error\n");
		}
	return 0;
}
