/* comment.l */
%{
	#include<stdio.h>
	int flag = 0;
     	int error = 0;
%}

%%

\"([^\{][^\}])*.*\" ECHO;
\*\*.* ;
\{	{error=1};
\{([^}]*\n)*.*\} ;

%%
#include <stdlib.h>
    	
	int main()
{
	yylex();
	if (error==1)printf("syntax error\n");
	return 0;
}
