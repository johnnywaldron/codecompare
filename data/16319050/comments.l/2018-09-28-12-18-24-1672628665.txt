/* comment.l */
%{
#include<stdio.h>
int flag = 0;
int error = 0;
%}

%%
\"[^\"]		{yytext;}
\*\*.* ;
\{(.*\n)*.*\} ;
\{			{error=1;}
\"\{(.*\n)*.*\}\" 		{yymore();}
\".*\*\*\" 		{yymore();}

%%
#include <stdlib.h>

int main()
{
  yylex();
  printf("%8d\n", yytext);
  if (error==1)printf("syntax error\n");
  return 0;
}

int yywrap()
{
return 1;
}
