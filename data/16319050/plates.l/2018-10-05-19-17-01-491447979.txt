/* plates.l */
%{
#include "stdio.h"	
int yylval;
int values[]
%}

%%
"8"[0-9]"-"[C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]"-"[0-9]*{6} 
	{yylval = atoi(yytext[1])+1980; printf("%d", yylval);}
	
"9"[0-9]"-"[C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]"-"[0-9]*{6} 
	{yylval = atoi(yytext[1])+1990; printf("%d", yylval);}
"0"[0-9]"-"[C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]"-"[0-9]*{6} 
	{yylval = atoi(yytext[1])+2000; printf("%d", yylval);}
"1"[0-9][0-9]"-"[C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]"-"[0-9]*{6} 
	{yylval = atoi(yytext[1])+2010; printf("%d", yylval);}     
\t 		{ECHO;}
\n		{ECHO;}

%%

int main()
{
	yylex();
	return 0;
}
