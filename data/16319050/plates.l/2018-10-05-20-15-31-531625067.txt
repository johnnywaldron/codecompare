/* plates.l */
%{
#include <stdio.h>	
int yylval;
%}

COUNTY (C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)

%%
8[0-9]{1}"-"{COUNTY}"-"[0-9]{1,6} 	{	yylval = (yytext[1]+1980)-48; printf("%d\n", yylval);}
9[0-9]{1}"-"{COUNTY}"-"[0-9]{1,6} 	{	yylval = (yytext[1]+1990)-48; printf("%d\n", yylval);}
0[0-9]{1}"-"{COUNTY}"-"[0-9]{1,6} 	{	yylval = (yytext[1]+2000)-48; printf("%d\n", yylval);}
1[0-2]{1}"-"{COUNTY}"-"[0-9]{1,6} 	{	yylval = (yytext[1]+2010)-48; printf("%d\n", yylval);}
1[3-8]{1}[1-2]{1}"-"{COUNTY}"-"[0-9]{1,6} 	{	yylval = (yytext[1]+2010)-48; printf("%d\n", yylval);}         
\t 		{}
\n		{ECHO;}

%%

int main()
{
	yylex();
	return 0;
}
