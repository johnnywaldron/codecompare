/* plates.l */
%{
#include <stdio.h>	
int yylval;
int values[]
%}
COUNTY [C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]
%%
"8"[0-9]"-"{COUNTY}"-"[0-9]*{6} {const char* x=(yytext[1];	yylval = atoi(x)+1980; printf("%d", yylval);}
"9"[0-9]"-"{COUNTY}"-"[0-9]*{6} {const char* x=(yytext[1];	yylval = atoi(x)+1990; printf("%d", yylval);}
"0"[0-9]"-"{COUNTY}"-"[0-9]*{6} {const char* x=(yytext[1];	yylval = atoi(x)+2000; printf("%d", yylval);}
"1"[0-9][0-9]"-"{COUNTY}"-"[0-9]*{6} {yylval = atoi(yytext[1])+2010; printf("%d", yylval);}     
\t 		{ECHO;}
\n		{ECHO;}

%%

int main()
{
	yylex();
	return 0;
}
