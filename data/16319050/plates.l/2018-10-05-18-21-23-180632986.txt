/* plates.l */
%{
#include "stdio.h"	
int yylval;
int count;
int YEAR;
int COUNTY;
int NUMBER;
%}

%%
[0-9]? { if (yyleng==2||yyleng==3)  return YEAR;} 
(C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW) { return COUNTY;}
-	{}
[0-9]* {if (yyleng==6) return NUMBER;}
\t 		{ECHO;}
\n		{ECHO;}

%%

int main()
{
	int tok;
	while(tok=yylex()) {
	printf("%s-%s%-%s", YEAR, COUNTY, NUMBER);
	}
	return 0;
}
