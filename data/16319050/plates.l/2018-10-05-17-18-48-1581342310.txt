/* plates.l */
%{
int yylval;
int count;
YEAR;
COUNTY;
SPACE;
NUMBER;
%}

%%
[0-9]? { 2,3; yylval = atoi(yytext); count++; return YEAR;} 
(C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW) {count++; return COUNTY;}
-	{ count++;}
[0-9]* {6;  count++; return NUMBER;}
.		{}
\n		{count=0;}
%%

int main()
{
	yylex();
	if (count==5) printf("%s-%s%-%s, YEAR, COUNTY, NUMBER);
	
}
