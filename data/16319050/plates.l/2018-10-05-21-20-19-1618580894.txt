/* plates.l */
%{
#include <stdio.h>	
int yylval;
%}

NEWCOUNTY (C|CE|CN|CW|D|DL|G|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)
COUNTY (C|CE|CN|CW|D|DL|G|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW)
%%
8[0-9]{1}"-"{COUNTY}"-"[0-9]{1,6}\n 	{yylval = ((yytext[1]+1980)-(48+2018))*-1; printf("%d\n", yylval);}
9[0-9]{1}"-"{COUNTY}"-"[0-9]{1,6}\n 	{yylval = ((yytext[1]+1990)-(48+2018))*-1; printf("%d\n", yylval);}
0[0-9]{1}"-"{COUNTY}"-"[0-9]{1,6}\n 	{yylval = ((yytext[1]+2000)-(48+2018))*-1; printf("%d\n", yylval);}
1[0-2]{1}"-"{COUNTY}"-"[0-9]{1,6}\n 	{yylval = ((yytext[1]+2010)-(48+2018))*-1; printf("%d\n", yylval);}
1[3-8]{1}[1-2]{1}"-"{NEWCOUNTY}"-"[0-9]?{1,6} 	{	yylval = ((yytext[1]+2010)-(48+2018))*-1; printf("%d\n", yylval);}         
[2-9].*	{printf("INVALID\n");}
\n		{}
\t		{}
.	{}
%%

int main()
{
	yylex();
	return 0;
}
