/* FLEX FILE (.l) */
/* SCANNER */
/* ------------------------------------------------------- */
/* Recognize tokens for the roman parser */
/* Code inspired/sampled using https://github.com/ChristophBerg/postgresql-numeral */

%option noyywrap nodefault yylineno

%{
#include "roman.tab.h"
void yyerror(char *s);
%}

%%

[I]  { yylval = 1; return I; }
[V]  { yylval = 5; return V; }
[X]  { yylval = 10; return X; }
[L]  { yylval = 50; return L; }
[C]  { yylval = 100; return C; }
[D]  { yylval = 500; return D; }
[M]  { yylval = 1000; return M; }

\n      { return EOL; /* Catch new line and tab/space */}
[ \t\n]  { }

.	{ yyerror("syntax error\n");/* Catch everything else not caught and print character */}
%%

/* Mainline deleted since parser(roman.y) calls scanner(THIS FILE)*/
