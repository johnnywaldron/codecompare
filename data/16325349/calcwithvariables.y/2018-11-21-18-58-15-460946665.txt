%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yylex();
int yyparse();
void yyerror(char *s);
int arr[26];
%}

%output "calcwithvariables.tab.c"

%token ADD SUB MUL DIV
%token ASS VAR NUM
%token PRINT
%token EOL

%%
calclist: /* nothing */
 | calclist VAR ASS exp EOL {
     int index = $2 - 97;
     arr[index] = $4;
 }
 | calclist PRINT VAR EOL {
     int index = $3 - 97;
     printf("%d\n", arr[index]);
 }
 ;

 exp: factor
  | exp ADD factor { $$ = $1 + $3; }
  | exp SUB factor { $$ = $1 - $3; }
  | exp ADD VAR    { $$ = $1 + arr[$3 - 97]; }
  | VAR ADD factor { $$ = arr[$1 - 97] + $3; }
  | VAR ADD VAR    { $$ = arr[$1 - 97] + arr[$3 - 97]; }
  | exp SUB VAR    { $$ = $1 - arr[$3 - 97]; }
  | VAR SUB factor { $$ = arr[$1 - 97] - $3; }
  | VAR SUB VAR    { $$ = arr[$1 - 97] - arr[$3 - 97]; }
  ;

 factor: NUM
  | factor MUL NUM { $$ = $1 * $3; }
  | factor DIV NUM { $$ = $1 / $3; }
  | factor MUL VAR    { $$ = $1 * arr[$3 - 97]; }
  | VAR MUL NUM { $$ = arr[$1 - 97] * $3; }
  | VAR MUL VAR    { $$ = arr[$1 - 97] * arr[$3 - 97]; }
  | factor DIV VAR    { $$ = $1 / arr[$3 - 97]; }
  | VAR DIV NUM { $$ = arr[$1 - 97] / $3; }
  | VAR DIV VAR    { $$ = arr[$1 - 97] / arr[$3 - 97]; }
  ;

%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
    printf("%s\n", s);
    exit(0);
}
