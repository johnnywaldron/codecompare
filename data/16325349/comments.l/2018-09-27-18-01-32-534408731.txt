%%

\"[^\"]*\"        {printf(yytext);}
\*{2}[^\n]*       {/*do nothing*/}
\{[^\}]*\}        {/*do nothing*/}
\{                {printf("syntax error\n"); return -1;}
\}                {printf("syntax error\n"); return -1;}
\"                {printf("syntax error\n"); return -1;}

%%

int main()
{
    yylex();
    return 0;
}

