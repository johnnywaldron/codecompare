%{
#include "romcalc.tab.h"
void yyerror(char *s);
int yylex();
%}

%%
"I"     { return I; }
"V"     { return V; }
"X"     { return X; }
"L"     { return L; }
"C"     { return C; }
"D"     { return D; }
"M"     { return M; }
"+"     { return ADD; }
"-"     { return SUB; }
"*"     { return MUL; }
"/"     { return DIV; }
"{"     { return OPN; }
"}"     { return CLS; }
\n      { return EOL; }
[ \t]   { /* ignore white space */ }
.	    { yyerror("syntax error"); }
%%
