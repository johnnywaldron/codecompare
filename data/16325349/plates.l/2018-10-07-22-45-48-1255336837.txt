%{

%}

NUMBER [1-9][0-9]{0-5}
NEWCOUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
OLDCOUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NEWYEAR 1[4-8][1-2]
OLDYEAR 8[7-9]|[9|0][0-9]|1[0-2]|13[1-2]

%%

{OLDYEAR}\-{OLDCOUNTY}\-{NUMBER}|{NEWYEAR}\-{NEWCOUNTY}\-{NUMBER}   {
    char x[2] = {yytext[0],yytext[1]};
    int year = atoi(x);
    if(year>87){
        printf("%d\n", 118-year);
    }
    else{
        printf("%d\n",  18-year);
    }
}

.|\n        {}

%%

int main()
{
    yylex();
    return 0;
}

