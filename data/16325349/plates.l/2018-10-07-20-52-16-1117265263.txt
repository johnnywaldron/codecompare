%{

%}

NUMBER [1-9][0-9]*
NEWCOUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
OLDCOUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
NEWYEAR 1[4-8][1-2]
OLDYEAR 8[7-9]|[9|0][0-9]|1[0-2]|13[1-2]

%%

{OLDYEAR}\-{OLDCOUNTY}\-{NUMBER}|{NEWYEAR}\-{NEWCOUNTY}\-{NUMBER}   {
    int num1 = atoi(yytext[0]);
    int num2 = atoi(yytext[1]);
    int year = num1*10+num2;
    if(year>87){
        printf(118-year);
    }
    else{
        printf(18-year);
    }
}

%%

int main()
{
    return 0;
}

