
%{
# include "calcwithvariables.tab.h"
void yyerror(char *s);
%}

%%
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
":=" { return ASSIGN; }
"print" { return PRINT; }
[0-9]+	{ yylval = atoi(yytext); return NUMBER; }
[a-z] { yylval = yytext[0]; return VAR; }
";" { return SEMI; }
[ \t\n]   { /* ignore white space */ }
.	{ yyerror("Mystery character\n");}
%%

