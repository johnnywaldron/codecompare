%{
int comments = 0;
%}

%%
\*{2}(.)*			{/*remove single line comment*/}
["{"][^}]*["}"]		{/*remove multi-line comment*/}
\"[.]*\"			{printf("\"" + yytext + "\"";}	
%%

int main()
{
  yylex();
  //printf("%d", comments);
  return 0;
}
