%{
    #include "roman.tab.h"
    #include <stdio.h>
%}

%%
"I"     {yylval = 1; return I;}
/*"IV"    {yylval = 4; return NUMBER;}*/
"V"     {yylval = 5; return V;}
/*"IX"    {yylval = 9; return NUMBER;}*/
"X"     {yylval = 10; return X;}
/*"XL"    {yylval = 40; return NUMBER;}*/
"L"     {yylval = 50; return L;}
/*"XC"    {yylval = 90; return NUMBER;}*/
"C"     {yylval = 100; return C;}
/*CD"    {yylval = 400; return NUMBER;}*/
"D"     {yylval = 500; return D;}
/*"CM"    {yylval = 900; return NUMBER;}*/
"M"     {yylval = 1000; return M;}

\n      {return EOL;}
[ \t]   { /*ignore whitespace */ }
.       {printf("syntax error\n"); exit(0);}
%%
