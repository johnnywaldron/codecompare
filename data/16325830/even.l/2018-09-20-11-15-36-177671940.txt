%{
int evens = 0;
int lines = 0;
%}

%%

[0-9]+     {int num = atoi(yytext); if(num % 2 == 0){evens++;}}
\n		   {lines++; }
.		   {lines++; }
[ \t]   { /* ignore white space */ }
%%

int main()
{
  yylex();
  printf("%d", evens);
  return 0;
}
