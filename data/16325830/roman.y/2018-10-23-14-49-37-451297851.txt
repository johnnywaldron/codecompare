%{
    #include "roman.tab.h"
    #include <stdio.h>
    #include <stdlib.h>
    int yylex();
    void yyerror(char* s);
%}


%token EOL
%token NUMBER
%%

line:
| exp EOL { printf("%d\n", $1); }
;

exp: NUMBER
| exp NUMBER {$$ = $1 + $2; printf("%d\n", $$);}
;

%%

int main()
{
  printf("> "); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}
