
%{
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
int yylex();
int yyparse();
%}

%output "romcalc.tab.c"

/* declare tokens */
%token EOL
%token I V X L C D M
%token MULTIPLY DIVIDE ADD SUBTRACT OPEN_PARA CLOSE_PARA


%%

Initialise:	{}
 | Initialise expr EOL 			{printf("%s\n", convertToRoman($2));}
 ;
 expr: paraResult
  | paraResult ADD paraResult                { $$ = $1 + $3; }
  | paraResult SUBTRACT paraResult                { $$ = $1 - $3; }
  ;

 paraResult: paraExpr
  | paraResult MULTIPLY paraExpr          { $$ = $1 * $3; }
  | paraResult DIVIDE paraExpr          { $$ = $1 / $3; }
  ;

 paraExpr: fullNumeral
  | OPEN_PARA expr CLOSE_PARA         { $$ = $2; }
  ;

fullNumeral: thousands hundreds tens digits	{$$ = $1 + $2 + $3 + $4;}
 |hundreds tens digits			{$$ = $1 + $2 + $3;}
 |tens digits					{$$ = $1 + $2;}
 |digits						{$$ = $1;}
 ;

thousands: M {$$ = 1000;}
 |{$$ = 0;}
 |M M {$$ = 2000;}
 |M M M {$$ = 3000;}
 ;

singleHundred:	C			{$$ = 100;}
  |{$$ = 0;}
  |C C		{$$ = 200;}
  |C C C		{$$ = 300;}
  ;

hundreds: singleHundred	{$$ = $1;}
 |{$$ = 0;}
 |C D		{$$ = 400;}
 |D singleHundred	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;


singleTen: X			{$$ = 10;}
  |{$$ = 0;}
  |X X		{$$ = 20;}
  |X X X		{$$ = 30;}
  ;

tens: singleTen		{$$ = $1;}
 |{$$ = 0;}
 |X L		{$$ = 40;}
 |L singleTen	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;

singleDigit: I			{$$ = 1;}
  |{$$ = 0;}
  |I I		{$$ = 2;}
  |I I I		{$$ = 3;}
  ;

digits: singleDigit	{$$ = $1;}
 |{$$ = 0;}
 |I V		{$$ = 4;}
 |V singleDigit	{$$ = 5 + $2;}
 |I X		{$$ = 9;}
 ;



%%
void yyerror(char *result){
  printf("%s\n", result);
}

char *convertToRoman (int nonRoman){
  if(nonRoman == 0){
    return "Z";
  }

  char *roman;
  int   size[]         = {0, 1, 2, 3, 2, 1, 2, 3, 4, 2};
  char *hundreds[]     = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
  char *tens[]         = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
  char *singleDigits[] = {"","I","II","III","IV","V","VI","VII","VIII","IX"};

  while (nonRoman >= 1000) {
        *roman++ = 'M';
        nonRoman = nonRoman - 1000;
    }

  strcpy (roman, hundreds[nonRoman/100]); roman += size[nonRoman/100]; nonRoman = nonRoman % 100;
  strcpy (roman, tens[nonRoman/10]);  roman += size[nonRoman/10];  nonRoman = nonRoman % 10;
  strcpy (roman, singleDigits[nonRoman]);     roman += size[nonRoman];
  *roman = '\0';

  return roman;

}
