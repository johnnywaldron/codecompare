%{

%}

OLD_REG_NUM ([8][7-9]|[9][0-9]|[0][0-9]|[1][0-2])
NEW_REG_NUM ([1][3-8][1-2])
OLD_COUNTIES ([-]([C][ENW]|[C]|[D][L]|[D]|[G]|[K][EKY]|[M][HNO]|[O][Y]|[R][N]|[S][O]|[L][DHMSK]|[L]|[T][NS]|[W][HXWD]|[W]))
NEW_COUNTIES ([-]([C][ENW]|[C]|[D][L]|[D]|[G]|[K][EKY]|[M][HNO]|[O][Y]|[R][N]|[S][O]|[L][DHMS]|[L]|[T]|[W][HXW]|[W]))
SIX_DIGITS ([0-9]{1,6})

VALID_REG_YEAR ({OLD_REG_NUM}|{NEW_REG_NUM})


%%

{NEW_REG_NUM}{NEW_COUNTIES}{SIX_DIGITS} {printf("NEWNUM\n");}

%%

int main()
{
  return 0;
}

