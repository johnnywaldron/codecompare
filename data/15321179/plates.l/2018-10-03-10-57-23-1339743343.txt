%{
int year = 2018;
int yeary;
int res;
%}

/* float exponent */
EXP	([Ee][-+]?[0-9]+)

%%

^([8][7-9]|[1][3-8][1-2]|[9][0-9]|[0][0-9]|[1][0-2]) 	{yeary = atof(yytext); return yeary;}

^[ \t]([8][7-9]|[1][3-8][1-2]|[9][0-9]|[0][0-9]|[1][0-2]) 	{printf("VALID%s", yytext);}

[-]([C][ENW]|[C]) {printf("VALIDC");}
[-]([D][L]|[D]) {printf("VALIDD");}
[-]([G]) {printf("VALIDG");}
[-]([K][EKY]) {printf("VALIDK");}
[-]([L][DHMS]|[L]) {printf("VALIDL");}
[-]([M][HNO]) {printf("VALIDM");}
[-]([O][Y]) {printf("VALIDO");}
[-]([R][N]) {printf("VALIDR");}
[-]([S][O]) {printf("VALIDS");}
[-]([T]) {printf("VALIDT");}
[-]([W][HXW]|[W]) {printf("VALIDW");}

[-]([0-9]{1,6}) {printf("VALID%s",yytext);}


%%

int main()
{
  printf("yeary = %d", yeary);
  
  return 0;
}
