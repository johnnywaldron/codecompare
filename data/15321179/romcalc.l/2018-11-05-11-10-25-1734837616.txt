/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
 * See the README file for license conditions and contact info.
 * $Header: /home/johnl/flnb/code/RCS/fb1-5.l,v 2.1 2009/11/08 02:53:18 johnl Exp $
 */

/* recognize tokens for the calculator and print them out */
%option noyywrap nodefault yylineno

%{
#include "romcalc.tab.h"
void yyerror(char *s);

%}

%%
[I]  { yylval = 1; return I; }
[V]  { yylval = 5; return V; }
[X]  { yylval = 10; return X; }
[L]  { yylval = 50; return L; }
[C]  { yylval = 100; return C; }
[D]  { yylval = 500; return D; }
[M]  { yylval = 1000; return M; }
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
"{" { return O_BRACK; }
"}" { return C_BRACK; }

[\n] { return EOL;}

[ \s] { }

.	{ printf("syntax error\n"); exit(0);}
%%
