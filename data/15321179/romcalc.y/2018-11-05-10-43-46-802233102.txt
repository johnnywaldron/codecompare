%{
#include <stdio.h>
int yylex();
void yyerror(char *s);
#include <stdlib.h>
#include <string.h>
char* printRomanNum(int number);
char romanval[1000];
int i = 0;
%}
%token NUMBER
%token ADD SUB MUL DIV ABS O_BRACK C_BRACK
%token I
%token V
%token X
%token L
%token C
%token D
%token M
%token EOL
%%

romcalc:
| romcalc expr EOL {

  printf("%s\n");
  printRomanNum($2);

}
;

expr: factor
 | expr ADD factor { $$ = $1 + $3; }
 | expr SUB factor { $$ = $1 - $3; }
 ;

factor: inner_expr
 | factor MUL inner_expr { $$ = $1 * $3; }
 | factor DIV inner_expr { $$ = $1 / $3; }
 ;

inner_expr: num
 | O_BRACK expr C_BRACK { $$ = $2; }
 ;


expr:
  max_c M max_m { $$ = $2 - $1 + $3; }
| max_c D max_c { $$ = $2 - $1 + $3; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_m:
  %empty { $$ = 0; }
| max_c M max_m { $$ = $2 - $1 + $3; }
| max_c D max_c { $$ = $2 - $1 + $3; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_c:
  %empty { $$ = 0; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_x:
  %empty { $$ = 0; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_i:
  %empty { $$ = 0; }
|       I max_i { $$ = $1 + $2; }
;

%%
/* parse a given string and return the result via the second argument */
int main(){

  printRomanNum();

  return 0;
}

void printRomanNum(int number){

  int j;

if (number == 0)
{
    printf("Z");
    return 0;
}
if (number < 0)
{
    printf("-");
    number = abs(number);
}
while (number != 0)
{
    if (number >= 1000)
    {
        postdigit('M', number / 1000);
        number = number - (number / 1000) * 1000;
    }
    else if (number >= 500)
    {
        if (number < (500 + 4 * 100))
        {
            postdigit('D', number / 500);
            number = number - (number / 500) * 500;
        }
        else
        {
            predigit('C','M');
            number = number - (1000-100);
        }
    }
    else if (number >= 100)
    {
        if (number < (100 + 3 * 100))
        {
            postdigit('C', number / 100);
            number = number - (number / 100) * 100;
        }
        else
        {
            predigit('L', 'D');
            number = number - (500 - 100);
        }
    }
    else if (number >= 50 )
    {
        if (number < (50 + 4 * 10))
        {
            postdigit('L', number / 50);
            number = number - (number / 50) * 50;
        }
        else
        {
            predigit('X','C');
            number = number - (100-10);
        }
    }
    else if (number >= 10)
    {
        if (number < (10 + 3 * 10))
        {
            postdigit('X', number / 10);
            number = number - (number / 10) * 10;
        }
        else
        {
            predigit('X','L');
            number = number - (50 - 10);
        }
    }
    else if (number >= 5)
    {
        if (number < (5 + 4 * 1))
        {
            postdigit('V', number / 5);
            number = number - (number / 5) * 5;
        }
        else
        {
            predigit('I', 'X');
            number = number - (10 - 1);
        }
    }
    else if (number >= 1)
    {
        if (number < 4)
        {
            postdigit('I', number / 1);
            number = number - (number / 1) * 1;
        }
        else
        {
            predigit('I', 'V');
            number = number - (5 - 1);
        }
    }
}
for(j = 0; j < i; j++)
    printf("%c", romanval[j]);
return 0;
}

void predigit(char num1, char num2)
{
romanval[i++] = num1;
romanval[i++] = num2;
}

void postdigit(char c, int n)
{
int j;
for (j = 0; j < n; j++)
    romanval[i++] = c;
}

}

void yyerror(char *s){
  fprintf(stderr, "%s\n", s);
}
