%{
/*#include "numeral.h"

/* flex/bison prototypes */
/*int yyromanlex (void);
struct yyroman_buffer_state *yyroman_scan_string(char *str);
void yyroman_delete_buffer(struct yyroman_buffer_state *buffer);
void yyromanerror (char const *s);

static Roman *numeral_parse_result; /* parsing result gets stored here */
#  include <stdio.h>
int yylex();
void yyerror(char *s);
int arrayofVals[100];
int i = 0;
%}

%token I
%token V
%token X
%token L
%token C
%token D
%token M
%token EOL
%%

romanparse:
| romanparse expr EOL {printf("%d\n", $2);}
;


expr:
  max_c M max_m { $$ = $2 - $1 + $3; }
| max_c D max_c { $$ = $2 - $1 + $3; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_m:
  %empty { $$ = 0; }
| max_c M max_m { $$ = $2 - $1 + $3; }
| max_c D max_c { $$ = $2 - $1 + $3; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_c:
  %empty { $$ = 0; }
| max_x C max_c { $$ = $2 - $1 + $3; }
| max_x L max_x { $$ = $2 - $1 + $3; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_x:
  %empty { $$ = 0; }
| max_i X max_x { $$ = $2 - $1 + $3; }
| max_i V max_i { $$ = $2 - $1 + $3; }
|       I max_i { $$ = $1 + $2; }
;

max_i:
  %empty { $$ = 0; }
|       I max_i { $$ = $1 + $2; }
;

%%
/* parse a given string and return the result via the second argument */
int main(){
  yyparse();
  int j;

  for(j = 0; j < i; j++){
    printf("%d\n", arrayofVals[j]);
  }
  return 0;
}

void yyerror(char *s){
  fprintf(stderr, "%s\n", s);
}
