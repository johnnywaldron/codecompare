%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 

int yylex();
void yyerror(char *s);
void toRoman(int num);

// Previous value of token.
int prev;
%}

// Tokens
%token ONE FIVE TEN FIFTY ONEH FIVEH ONET FAIL
%token ADD SUB MUL DIV ZERO ABS OP CP
%token EOL

%%
list: /* nothing */
  | list par EOL { 
      toRoman($2);
  }
  | FAIL EOL {
    yyerror("syntax error");
 }
;

expr: factor
  | par ADD par { $$ = $1 + $3; }
  | par SUB par { $$ = $1 - $3; }
  ;

factor: terms
  | par MUL par { $$ = $1 * $3; }
  | par DIV par { $$ = $1 / $3; }
  ;

par: ZERO | expr 
  | OP expr CP  { $$ = $2; }
  ;

terms:
   units                                { $$ = $1; }
   | tenths units                       { $$ = $1 + $2; }
   | hundreths tenths units             { $$ = $1 + $2 + $3; }
   | thousands hundreths tenths units   { $$ = $1 + $2 + $3 + $4; }
;

thousands:
    ONET                  { $$ = $1; }
  | ONET ONET             { $$ = $1 + $2; }
  | ONET ONET ONET        { $$ = $1 + $2 + $2; }
;

hundreths: { $$ = 0; }
  | FIVEH | hundreds      { $$ = $1; }
  | ONEH FIVEH            { $$ = $2 - $1; }
  | ONEH ONET             { $$ = $2 - $1; }
  | FIVEH hundreds        { $$ = $1 + $2; }
  ;

hundreds:
    ONEH                  { $$ = $1; }
  | ONEH ONEH             { $$ = $1 + $2; }
  | ONEH ONEH ONEH        { $$ = $1 + $2 + $2; }
;

tenths: { $$ = 0; }
  | FIFTY | tens          { $$ = $1; }
  | TEN FIFTY             { $$ = $2 - $1; }
  | TEN ONEH              { $$ = $2 - $1; }
  | FIFTY tens            { $$ = $1 + $2; }
  ;

tens:
  TEN                     { $$ = $1; }
  | TEN TEN               { $$ = $1 + $2; }
  | TEN TEN TEN           { $$ = $1 + $2 + $2; }
;

units: { $$ = 0; }
  | FIVE |  ones          { $$ = $1; }
  | ONE FIVE              { $$ = $2 - $1; }
  | ONE TEN               { $$ = $2 - $1; }
  | FIVE ones             { $$ = $1 + $2; }
  ;

ones:
  ONE                     { $$ = $1; }
  | ONE ONE               { $$ = $1 + $2; }
  | ONE ONE ONE           { $$ = $1 + $2 + $2; }
  ;

%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(0);
}

void toRoman(int num) {
  int decimal[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
  char *roman[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
  int i = 0;
  char buff[1024];
  if (num < 0) {
    num = abs(num);
    strcat(buff, "-");
  }
  if (num == 0) strcpy(buff, "Z");
  while (num) {
    while (num / decimal[i]) {
      strcat(buff, roman[i]);
      num = num - decimal[i];
    }
    i++;
  }
  strcat(buff, "\0");
  printf("%s\n", buff);
  memset(&buff[0], 0, sizeof(buff));
}
