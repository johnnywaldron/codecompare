%{
#include <stdio.h>
int yylex();
void yyerror(char *s);

int mem[26];
%}

/* declare tokens */
%token NUMBER
%token ADD SUB MUL DIV ABS
%token ID ASSIGN MINUS PRINT
%token EOL FAIL
%%

s: /* nothing */
 | s stmt EOL { $$ = $2; }
 | s PRINT ID EOL { printf("%d\n", mem[$3 - 0x60]); }
 | FAIL { yyerror("syntax error"); }
 ; 

stmt: 
   ID ASSIGN exp { 
     // Store the number in "memory".
     int loc = $1 - 0x60;
     mem[loc] = $3;
    }
 ;

exp: factor 
 | exp ADD factor { $$ = $1 + $3; }
 | exp SUB factor { $$ = $1 - $3; }
 ;

factor: term 
 | factor MUL term { $$ = $1 * $3; }
 | factor DIV term { $$ = $1 / $3; }
 ;

term: NUMBER 
 | ABS term { $$ = $2 >= 0? $2 : - $2; }
 | SUB term { $$ = 0 - $2; }
 | ID       { $$ = mem[$1 - 0x60]; }
 ;
%%
int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "%s\n", s);
}







