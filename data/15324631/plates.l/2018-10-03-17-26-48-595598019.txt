%{
#include <stdio.h>
#include <stdlib.h>



// Country identifiers from 2014 - present
char *indent2014[26] = {
  "C", "CE", "CN", "CW", "D", "DL", "G", "KE", "KK", "KY",
  "L", "LD", "LH", "LM", "LS", "MH", "MN", "MO", "OY", "RN",
  "SO", "T", "W", "WH", "WX", "WW"};

// Country identifiers from 1987 - 2013
char *indent2013[] = {"L", "LK", "TN", "TS", "W", "WD"};

%}

%%
[A-Z]{1,2} { 
  int len = 26;
  int i;
  for (i = 0; i < len; i++) {
    int res = strcmp(indent2014[i], yytext);
    if (res != 0) {
      // Indentifier is invalid.
      printf("INVALID");
    }
  }
}

[0-9]{2,3}-[A-Z]{1,2}-[0-9]{1,6} {
  int y;
  int year[];
  int age;
  const int now = 2018;
  if (yytext[2] == '-') {
    year = {yytext[0], yytext[1]};
    y = atoi(year);
    age = now - y;
    printf(age);
  } else {
    year = {yytext[0], yytext[1], yytext[2]};
    y = atoi(year);
    age = now - y;
    printf(age);
  }
}
%%

int main() {
  yylex();
  return 0;
}
