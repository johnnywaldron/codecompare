%{
#include <stdio.h>
#include <stdlib.h>
%}

%%
^[0-9][0-9]-[A-Z]{1,2} {}
%%

int main() {
  yylex();
  return 0;
}
