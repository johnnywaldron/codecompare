%{
#include <stdio.h>
#include <stdlib.h>
#define TRUE 1
#define FALSE 0

#define NOW 2018
#define YEARS_TO_ADD_1 2000
#define YEARS_TO_ADD_2 1900


// Country identifiers from 2014 - present
char *indent2014[26] = {
  "C", "CE", "CN", "CW", "D", "DL", "G", "KE", "KK", "KY",
  "L", "LD", "LH", "LM", "LS", "MH", "MN", "MO", "OY", "RN",
  "SO", "T", "W", "WH", "WX", "WW"};

// Country identifiers from 1987 - 2013
char *indent2013[6] = {"L", "LK", "TN", "TS", "W", "WD"};

%}
IGNORE      [\n\s\t]
NUMBER      [0-9]
COUNTY2014  C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNTY2013  L|LK|TN|TS|W|WD|C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|W|WH|WX|WW
%%

{NUMBER}* { }

{NUMBER}{3}-{COUNTY2014}-{NUMBER}{1,6} { 
  char year[] = {yytext[0], yytext[1]};
  int y = atoi(year);
  y = y + YEARS_TO_ADD_1;
  y = NOW - y;
  printf("%d\n", y);
}

{NUMBER}{2}-{COUNTY2013}-{NUMBER}{1,6} { 
  char year[] = {yytext[0], yytext[1]};
  int y = atoi(year);
  if (y <= 13) {
      y = y + YEARS_TO_ADD_1;
    } else {
      y = y + YEARS_TO_ADD_2;
    }
  y = NOW - y;
  printf("%d\n", y);
}




%%

int main() {
  yylex();
  return 0;
}
