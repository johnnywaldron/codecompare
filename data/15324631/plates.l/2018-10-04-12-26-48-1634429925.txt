%{
#include <stdio.h>
#include <stdlib.h>
#define TRUE 1
#define FALSE 0


// Country identifiers from 2014 - present
char *indent2014[26] = {
  "C", "CE", "CN", "CW", "D", "DL", "G", "KE", "KK", "KY",
  "L", "LD", "LH", "LM", "LS", "MH", "MN", "MO", "OY", "RN",
  "SO", "T", "W", "WH", "WX", "WW"};

// Country identifiers from 1987 - 2013
char *indent2013[6] = {"L", "LK", "TN", "TS", "W", "WD"};

%}

%%

[^A-Z0-9\-]* { }

[A-Z]{1,2} { 
  int len14 = 26;
  int len13 = 6;
  int i;
  int valid = FALSE;

  for (i = 0; i < len14; i++) {
    int res = strcmp(indent2014[i], yytext);
    if (res == 0) {
      valid = TRUE;
      break;
    }
  }
  if (valid == FALSE) {
    printf("INVALID\n");
  }
  printf("%s", yytext);
}

[0-9]{2,3}-[A-Z]{1,2}-[0-9]{1,6} {
  int y;
  int age;
  const int now = 2018;

  int len14 = 26;
  int len13 = 6;
  int i;

  if (yytext[2] == '-') {
    // 1987 - 2013
    char year[] = {yytext[0], yytext[1]};
    y = atoi(year);
    if (y <= 13) {
      y = y + 2000;
    } else {
      y = y + 1900;
    }
    age = now - y;
    printf("%d\n", age);
  } else {
    // 2014 - present
    char year[] = {yytext[0], yytext[1]};
    y = atoi(year);
    y = y + 2000;
    age = now - y;
    printf("%d\n", age);
  }
}

.* { }

%%

int main() {
  yylex();
  return 0;
}
