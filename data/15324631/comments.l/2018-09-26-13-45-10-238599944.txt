%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buff[1024]
%}

%%

\*\*.$	    { }
\{.\}		{ }
.*  { buff = sprintf(yytext)}
\{.\Z	    { printf("Syntax error."); }

%%

int main()
{
  yylex();
  return 0;
}
