%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
int count = 0;
%}

%%

\*\*.$	    { }
\{.\}		{ }
\{.\Z	    { print("Syntax error."); }

%%

int main()
{
  yylex();
  printf("%d\n", count);
  return 0;
}
