%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buf[1024];
%}

%%


\{([^\}])*[^\}]+ { printf("syntax error\n"); return 0; }
[^\{\}\"]([^\{\}\"])*\} { printf(yytext); printf("syntax error\n"); return 0; }
\"(.)*\" { printf(yytext); }
\*\*.* { }
\{([^\}])*\} { }
\}          {printf("syntax error\n");}

%%

int main()
{
  yylex();
  return 0;
}
