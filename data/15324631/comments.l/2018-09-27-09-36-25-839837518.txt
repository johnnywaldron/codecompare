%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buf[1024];
%}

%%

\"(.)*\" { printf(yytext); }
\*\*.* { }
\{([^\}]|\n)*[^\}]+ { printf("syntax error 1\n"); return 0; }
[^\{\}\"]([^\{\}\"]|\n)*\} { printf("syntax error 2\n"); return 0; }
\{([^\}]*|\n)*\} { }

%%

int main()
{
  yylex();
  return 0;
}
