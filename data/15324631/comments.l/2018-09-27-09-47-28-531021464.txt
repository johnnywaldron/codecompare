%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buf[1024];
%}

%%

\"(.)*\" { printf(yytext); }
\*\*.* { }
\{([^\}]|\n)*[^\}]+ { printf("syntax error\n"); return 0; }
[^\{\}\"]([^\{\}\"]|\n)*\} { printf(yytext, len(yytext) - 2); printf("syntax error\n"); return 0; }
\{([^\}]*|\n)*\} { }

%%

int main()
{
  yylex();
  return 0;
}
