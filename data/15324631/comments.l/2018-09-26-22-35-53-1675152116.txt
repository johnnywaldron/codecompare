%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buf[1024];
%}

%%

\"(.)*\" { printf(yytext); }
\*\*.* { }
\{([^\}]*|\n)*\} { }
\{([^\}]|\n)*[^\}]+ { printf("syntax error\n"); }
[^\{]+([^\}]|\n)*\} { printf("syntax error\n"); }

%%

int main()
{
  yylex();
  return 0;
}
