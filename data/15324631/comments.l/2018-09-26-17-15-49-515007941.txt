%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buf[1024];
%}

%%

\*\*.* { }
\{(.|\n)*\} { }
(.|\n)*\{(.|\n)*[^\}]$ { printf("syntax error\n"); }


%%

int main()
{
  yylex();
  printf(buf);
  return 0;
}
