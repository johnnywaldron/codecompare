%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buf[1024];
%}

%%

\*\*.* { }
\{(.|\n)*\} { }


%%

int main()
{
  yylex();
  printf(buf);
  return 0;
}
