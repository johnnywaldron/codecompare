%{
#include <stdio.h>
#include <stdlib.h>
%}

%{
  char buf[1024];
%}

%%

^[\*\*].$ { buf = strcpy(buf, yytext); }


%%

int main()
{
  yylex();
  printf(buf);
  return 0;
}
