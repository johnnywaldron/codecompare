%{
#include "romcalc.tab.h"
#include <stdio.h>
#include <stdlib.h>

void yyerror(char *s);
%}

%%
I     { yylval = 1; return ONE; }
V     { yylval = 5; return FIVE; }
X     { yylval = 10; return TEN; }
L     { yylval = 50; return FIFTY; }
C     { yylval = 100; return ONEH; }
D     { yylval = 500; return FIVEH; }
M     { yylval = 1000; return ONET; }

"+"   { return ADD; }
"-"   { return SUB; }
"*"   { return MUL; }
"/"   { return DIV; }
"|"   { return ABS; }
"Z"   { return ZERO; }

"{"   { return OP; }
"}"   { return CP; }

\n    { return EOL; }
[ \t] { }
.     { return FAIL; }

%%