%{
#include "romcalc.tab.h"
%}

%%

 /* roman numeral "component" */
"I"   { return I; } /*all of these are tokens defined in the bison file */
"V"   { return V; }
"X"   { return X; }
"L"   { return L; }
"C"   { return C; }
"D"   { return D; }
"M"   { return M; }
"Z"   { return Z; }

 /* characters and operators */
"{"   |
"}"   |
"+"   |
"-"   |
"*"   |
"/"   { return yytext[0]; }

 /* NewLine */
\n    { return EOL; }

 /* Unineterested in any tabs or spaces */
[ \t]* { /*do nothing*/}

.     { yyerror("syntax error"); }

%%
