%{

%}

%%

"\"".*"\""    { printf("%s", yytext); } /* highest precedence */

"**".*        { }

"{"[^}]*"}"   { }

"{"[^}]*      { printf("syntax error\n"); exit(0); }

[^{]*"}"      { printf("syntax error\n"); exit(0); }

%%

int main()
{
  yylex();
  return 0;
}

