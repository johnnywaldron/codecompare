%{

%}

%%

"\"".*"\""    { printf("%s", yytext); } /* highest precedence */

"**".*        { }

"{"[^}]*"}"   { }

"{"           { printf("syntax error\n"); exit(0); } /* matches only if previous doesn't match*/

"}"           { printf("syntax error\n"); exit(0); } /* matches only if the third pattern doesn't match*/

%%

int main()
{
  yylex();
  return 0;
}

