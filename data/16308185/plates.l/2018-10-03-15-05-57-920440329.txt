%{
/*int chars = 0;
int words = 0;
int lines = 0;*/
int CURR_UPPER_BOUND = 18; /* current year = 18 */
int CURR_LOWER_BOUND = 13; /* new format issued at year 2013 */
int PAST_UPPER_BOUND = 12; /* latest year with old format in year 2012 */
int PAST_LOWER_BOUND = 87; /* 1987 last two digits = 87 */
%}
/*definitions section*/
/*TODO:make a county general and concat to differences*/

COUNTY_PRESENT C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW

COUNTY_PAST C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WH|WX|WW|WD

CURR_YEAR [0-9]{2}[1-2]
/* TODO: concat PAST_YEAR to make CURR_YEAR */
PAST_YEAR [0-9]{2}

DIG [0-9]{6}

SPACE [ \t\n]

%%

{CURR_YEAR}[-]{COUNTY_PRESENT}[-]{DIG}{SPACE} { char subStr[3];
                                         memcpy(subStr, yytext, 2);
                                         subStr[2] = '\0';
                                         int year = atoi(subStr);
                                         if(year >= CURR_LOWER_BOUND && year <= CURR_UPPER_BOUND)
                                         {   int diff = CURR_UPPER_BOUND - year;
                                             printf("%d\n", diff);
                                         }
                                         else
                                         {
                                             printf("INVALID\n");
                                         }
                                       }

{PAST_YEAR}[-]{COUNTY_PAST}[-]{DIG}{SPACE}  { char subStr[3];
                                       memcpy(subStr, yytext, 2);
                                       subStr[2] = '\0';
                                       int year = atoi(subStr);
                                       if(year >= PAST_LOWER_BOUND)
                                       {   int diff = CURR_UPPER_BOUND + (100 - year);
                                           printf("%d\n", diff);
                                       }
                                       else if( year <= PAST_UPPER_BOUND)
                                       {
                                         int diff = CURR_UPPER_BOUND - year;
                                             printf("%d\n", diff);
                                       }
                                       else
                                       {
                                           printf("INVALID\n");
                                       }
                                     }

.		{ printf("INVALID\n"); }

%%

int main()
{
  yylex();
  return 0;
}

