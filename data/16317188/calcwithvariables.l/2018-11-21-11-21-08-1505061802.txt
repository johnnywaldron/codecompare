%{
# include "calcwithvariables.tab.h"
void yyerror(char *s);
%}

%%
"+"	{ return ADD; }
"-"	{ return SUB; }
"*"	{ return MUL; }
"/"	{ return DIV; }
":=" {return ASS;}
"print" {return PRINT;}
";" {return SEMI;}
[0-9]+	{ yylval = atoi(yytext); return NUMBER; }
[a-z] {yylval = *yytext; return VAR;}
\n      { return EOL; }
[ \t]   { /* ignore white space */ }
.	{ yyerror("syntax error\n");}
%%


