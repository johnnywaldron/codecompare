%{


%}

COUNT14 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
COUNT87 C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW


YR14-18 [1][4-8][1-2]
YR13 [1][3][1-2]
YR00-12 [0][0-9]|[1][0-1]
YR87-99 [8|9][0-9]


NUM [1-9][0-9]{0,5}


%%


{YR14-18}\-{COUNT14}\-{NUM} {char x[2] = {yytext[0],yytext[1]}; int v = 2018 - (2000 + atoi(x)); printf("%d\n", v);}

{YR13}\-{COUNT87}\-{NUM} {char x[2] = {yytext[0],yytext[1]}; int v = 2018 - (2000 + atoi(x)); printf("%d\n", v);}

{YR00-12}\-{COUNT87}\-{NUM} {char x[2] = {yytext[0],yytext[1]}; int v = 2018 - (2000 + atoi(x)); printf("%d\n", v);}

{YR87-99}\-{COUNT87}\-{NUM} {char x[2] = {yytext[0],yytext[1]}; int v = 2018 - (1900 + atoi(x)); printf("%d\n", v);}

[^\r^\t^\n ] {printf("INVALID\n"); return 1;}

. {} 


%%




int main()
{
  yylex();	
  return 0;
}
