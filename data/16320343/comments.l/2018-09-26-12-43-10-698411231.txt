%{
	int bracketCounter;
%}


%%
\{		{bracketCounter++;}
\}		{bracketCounter--;}
\n		{}
.		{}
%%


int main()
{
	yylex();
	printf("%d\n", bracketCounter);
	return 0;
}
