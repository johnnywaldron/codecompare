%{
	int bracketCounter = 0;
%}
     
     	
%%
[A-z0-9\.\'\;\:\=\(\)\,\+\- ]*[{]		{bracketCounter++;if(bracketCounter==1){int lastChar = strlen(yytext)-1; yytext[lastChar]='\0';printf(yytext);}}
[}][A-z0-9\.\'\;\:\=\(\)\,\+\- ]*		{bracketCounter--;;printf(yytext+1);}
[A-z0-9\.\'\;\:\=\(\)\,\+\- ]*[{][A-z0-9\.\'\;\:\=\(\)\,\+\-\{\} ]* {bracketCounter++;}
[A-z0-9\.\'\;\:\=\(\)\,\+\-\{\} ]*[}]							{bracketCounter--;}
[{][A-z0-9 ]+\n*[}]	{/*printf("nice");*/}
\*\*[A-z0-9 ]+		{/*printf("Single Line Comment");*/}
[A-z0-9\.\'\;\:\=\(\)\,\+\-\{\} ]+			{if(bracketCounter==0)printf("%s",yytext);}
\n				{if(bracketCounter==0)printf("%s",yytext);}
.				{}
%%
    		
    
int main()
{
	yylex();
	if(bracketCounter != 0)
		printf("syntax error\n");
//printf("%d\n", bracketCounter);
	return 0;
}
