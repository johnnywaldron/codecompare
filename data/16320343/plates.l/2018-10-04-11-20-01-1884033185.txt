%{

%}
NUMBER [0-9]
COUNTY C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
YEAR [0-9]
%%
{YEAR}{COUNTY}{NUMBER}[ ]*      {printf("%s", yytext);}
.     {printf("INVALID");}
%%

int main()
{
  yylex();

  return 0;
}
