%{
#include <stdio.h>
#include <stdlib.h>
#include "calcwithvariables.tab.h"
	
void yyerror();
int yylex();
%}

%%
[a-z] { yylval = yytext[0]; return VAR; }
[0-9] { yylval = atoi(yytext); return NUM; }
"+"	  { return ADD; }
"-"	  { return SUB; }
"*"	  { return MUL; }
"/"	  { return DIV; }
":="  { return ASSIGN; }
";"	  { return END; }
"\n"  { ; }
"print" { return PRINT; }
[	\s\t\r ]* ;
.	{  yyerror(); }

%%
