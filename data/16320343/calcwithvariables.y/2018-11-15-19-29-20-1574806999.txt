%{
#include <stdio.h>
#include <stdlib.h>

int yylex();
int yyparse();
void yyerror();
void dictAdd(char key, int value);
int dictGet(char key);

# define DICT_S 26
typedef struct key_value{
	
	char key;
	int value;

} pair;

pair dict[DICT_S];

%}

// declare tokens

%token VAR NUM ASSIGN PRINT END
%token ADD SUB MUL DIV
%%

calclist:  /*none*/
| calclist stmt  { ; }

stmt: expr END ;
	| PRINT VAR END { int rc = dictGet($2); printf("%i\n", rc); } 
	;
	
	
expr: factor
 | VAR  			{ $$ = dictGet($1); }	
 | VAR ASSIGN expr  { dictAdd($1, $3); }
 | expr ADD factor 	{ $$ = $1 + $3; /*printf("Adding %i and %i\n", $1, $3);*/ }
 | expr SUB factor 	{ $$ = $1 - $3; /*printf("$$(%i)=%i-%i\n",$$,$2,$3 ); */ }
 ;

factor: term 
 | factor MUL term	{ $$ = $1 * $3; }
 | factor DIV term  { $$ = $1 / $3; }
 ;

term: VAR 			{ $$ = dictGet($1); }
 | NUM 				{ $$ = $1; }
 | SUB term 		{ $$ = -$1; }
 ;

%%

int dictGet(char key){
	int i;
	for( i=0; i< DICT_S; i++){	
		if(dict[i].key == key){
			//printf("Used key %c to get val %i\n", dict[i].key, dict[i].value);
			return dict[i].value;	
		}
	}
	return -1;
}

void dictAdd(char key, int value){
		//adding values
	int i;
	for(i=0 ; i < DICT_S ; i++){
		if(dict[i].key == NULL){
			dict[i].key = key;
			dict[i].value = value;
		//	printf("Added %c and %i\n", key, value);
			break;
		}
		//updating values
		else if( dict[i].key == key){
			dict[i].value = value;
		//	printf("Updated %c and %i\n", key, value);
			break;
		}
	}		
}

int main()
{ 
 	yyparse();
  	return 0;
}

void yyerror()
{
	printf("syntax error\n");
	exit(0);
}

