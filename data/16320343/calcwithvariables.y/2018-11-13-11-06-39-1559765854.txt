%{
#include <stdio.h>
#include <stdlib.h>

int yylex();
int yyparse();
void yyerror();
void dictAdd(char key, int value);
int dictGet(char key);

# define DICT_S 26
typedef struct key_value{
	
	char key;
	int value;

} pair;

pair dict[DICT_S];

%}

// declare tokens

%token VAR NUM ASSIGN PRINT EOL END
%token ADD SUB MUL DIV
%%

calclist:  /*none*/
| calclist stmt EOL { ; }

stmt: expr END ;
	| PRINT VAR END { int rc = dictGet($2); printf("%i\n", rc); } 

expr: factor
	| expr ADD factor { $$ = $1 + $3; }
	| expr SUB factor { $$ = $1 - $3; }
	;	

factor: term
	| factor MUL term { $$ = $1 * $3; }	
	| factor DIV term { $$ = $1 / $3; }
	
term: NUM|VAR
	| VAR ASSIGN VAR { dictAdd($1, dictGet($3)); }
	| VAR ASSIGN NUM { dictAdd($1, $3); }
;	

%%

int dictGet(char key){
	int i;
	for( i=0; i< DICT_S; i++){	
		if(dict[i].key == key){
			return dict[i].value;	
		}
	}

}

void dictAdd(char key, int value){
		//adding values
	int i;
	for(i=0 ; i < DICT_S ; i++){
		if(dict[i].key == NULL){
			dict[i].key = key;
			dict[i].value = value;
			break;
		}
		//updating values
		else if( dict[i].key == key){
			dict[i].value = value;
			break;
		}
	}		
}

int main()
{ 
 	yyparse();
  	return 0;
}

void yyerror()
{
	printf("syntax error\n");
	exit(0);
}

