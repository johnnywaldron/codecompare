%{
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

void yyerror(const char *s);
int yylex();
int yyparse();

int array [26];
//printf("%d \n",array[$2]);
%}
%token ID NUMBER PRINT DIV MUL SUB ADD ASSIGN SPACE SEMI EOL
%error-verbose
%%
loop:
| stmt loop {return 0;}
;
stmt: 
| stmt ID ASSIGN expr SEMI EOL { array[$2] = $4;}
| stmt PRINT ID SEMI EOL {printf("%d\n",array[$3]);}
;

expr: unary 
| expr MUL expr {$$ = $1 * $3;}
| expr DIV expr {$$ = $1 / $3;}
| expr SUB expr {$$ = $1 - $3;}
| expr ADD expr {$$ = $1 + $3;}
;
unary: SUB unary {$$ = $2 * (-1);}
| NUMBER 
| ID { $$ = array[$1];}
;
%%
int main()
{
	yyparse();
	return 0;
}
void yyerror(const char *s)
{
    printf("syntax error\n");
}

