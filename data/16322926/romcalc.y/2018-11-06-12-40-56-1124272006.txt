%{
#include <stdio.h>
void yyerror(char *s);
int yylex();
void convertToRoman(int ans);
//decimal2roman($2);
//%error-verbose

%}

%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND EOL 
%token ADD SUB MUL DIV BRACKET1 BRACKET2

%%
calclist: 
| calclist exp EOL{convertToRoman($2);}
| calclist EOL
;

exp: factor
| exp ADD factor { $$ = $1 + $3;}
| exp SUB factor { $$ = $1 - $3;}
;

factor: term
| factor MUL term {$$ = $1 * $3; }
| factor DIV term {$$ = $1 / $3; }
;

term: oneT
| tenT oneT {$$ = $1 + $2;}
| hundredT tenT oneT {$$ = $1 + $2 + $3;}
| thousand hundredT tenT oneT {$$ = $1 + $2 + $3 + $4;}
| BRACKET1 exp BRACKET2 { $$ = $2; }
 ;

thousand: {$$ = 0;}
| THOUSAND {$$ = 1000;}
| THOUSAND thousand {$$ = 1000 + $2;}
;
hundredT: {$$ = 0;}
| hundred {$$ = $1;}
| HUNDRED FIVEHUNDRED {$$ = 400;}
| FIVEHUNDRED {$$ = 500;}
| FIVEHUNDRED hundred {$$ = 500 + $2;}
| HUNDRED THOUSAND {$$ = 1000 - 100;}
;
hundred: HUNDRED {$$ = 100;}
| HUNDRED HUNDRED {$$ = 200;}
| HUNDRED HUNDRED HUNDRED {$$ = 300;}
;
tenT: {$$ = 0;}
| ten {$$ = $1;}
| FIFTY {$$ = 50;}
| TEN FIFTY {$$ = 50 - 10;}
| TEN HUNDRED {$$ = 100 - 10;}
| FIFTY ten {$$ = 50 + $2;}
;
ten: TEN {$$ = 10;}
| TEN TEN {$$ = 20;}
| TEN TEN TEN {$$ = 30;}
;
oneT: {$$ = 0;}
| one {$$ = $1;}
| FIVE {$$ = 5;}
| ONE FIVE {$$ = 5 - 1;}
| ONE TEN {$$ = 10 - 1;}
| FIVE one {$$ = $2 + 5;}
;
one : ONE {$$ = 1;}
| ONE ONE {$$ = 2;}
| ONE ONE ONE {$$ = 3;}
;

%%
int main()
{
	yyparse();
	return 0;
}
void yyerror(char *s)
{
    printf("%s\n", s);
}
//Code was adapted form the link below but changed to suit this program: 
//https://www.thecrazyprogrammer.com/2017/09/convert-decimal-number-roman-numeral-c-c.html?fbclid=IwAR2VZNCskcBOpl-k6cMCZo8avx8OYzozEHNRbAU1p1RLZH1vY8B2r6k3EKQ

void convertToRoman(int number){
    int numberArray[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};                     
    char *romanNumeral[] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};   
    int i = 0;
    if(number == 0){
        printf("Z");
    }
    else{
        if(number < 0)
        {
            number = number * -1;
            printf("-");
        }
        while(number){                           
            while(number/numberArray[i]){           
                printf("%s",romanNumeral[i]);       
                number -= numberArray[i];            
                }
        i++;                              
        }
    }

    printf("\n");
}


