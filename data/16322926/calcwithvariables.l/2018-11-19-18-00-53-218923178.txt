
%{
#include "calcwithvariables.tab.h"
void yyerror(char *s);
int yylex();
int yyparse();
%}
%%
":=" 	{ return ASSIGN; }
"print" { return PRINT;}
";"		{ return SEMI; }
"/" 	{ return DIV; }
"*" 	{ return MUL; }
"-" 	{ return SUB; }
"+" 	{ return ADD; }
[a-z] 	{ yylval = yytext[0] - 'a'; return ID; }
[0-9]+ 	{ yylval = atoi(yytext); return NUMBER; }
\n 		{return EOL; }
.		{}
%%
