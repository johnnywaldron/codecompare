
%{
#include "calcwithvariables.tab.h"
%}
%%
":=" 	{ return ASSIGN; }
"print" { return PRINT;}
";"		{ return SEMI; }
"/" 	{ return DIV; }
"*" 	{ return MUL; }
"-" 	{ return SUB; }
"+" 	{ return ADD; }
[a-z] 	{ yylval = yytext[0] - 'a'; return ID; }
[0-9]+ 	{ yylval = atoi(yytext); return NUMBER; }
[A-Z]   {printf("syntax error\n");}
\n 		{return EOL; }
.		{}
%%
