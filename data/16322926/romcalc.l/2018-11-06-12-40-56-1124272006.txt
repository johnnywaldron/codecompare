%{
#include "romcalc.tab.h"

void yyerror(char *s);
int yylex();
int yyparse();
//\{.+?\} {return yytext[0];} 
%}

%%


I {return ONE;}
V {return FIVE;}
X {return TEN;}
L {return FIFTY;}
C {return HUNDRED;}
D {return FIVEHUNDRED;}
M {return THOUSAND;}

\+ {return ADD;}
\- {return SUB;}
\* {return MUL;}
\/ {return DIV;}
\{ {return BRACKET1;}
\} {return BRACKET2;}

\n  {return EOL;}
[ \t]   { /* ignore white space */ }
.      {yyerror("syntax error");return 0;}
%%

