%{
int currentYear = 2018;
int olderYear = 1900;
int newYear = 2000;
int reg = 0;

NUMBER      ([1-9]{1,6})
NEWCOUNTY   "C"|"CE"|"CN"|"CW"|"D"|"DL"|"G"|"KE"|"KK"|"KY"|"L"|"LD"|"LH"|"LM"|"LS"|"MH"|"MN"|"MO"|"OY"|"RN"|"SO"|"T"|"W"|"WH"|"WX"|"WW"
OLDCOUNTY   C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
OLDYEAR     ([89][0-9]-|[0][0-9]-|[1][0-2]-)
NEWYEAR     ([1][3-8][1-2]-)

%}

%%
{OLDYEAR}{OLDCOUNTY}{NUMBER}   {
                                  char year[];
                                  int year1;
                                  bool new = false;
                                  int fullYear = 0;
                                  memcpy( year, &yytext[0], 2 );
                                  if(year[0] == '0' || year[0] == '1')
                                  {
                                    new = true;
                                   }
                                  year1 = atoi(year);

                                  if(new)
                                  {
                                    fullYear = year1 + newYear;
                                    reg = currentYear - fullYear;
                                  }
                                  else{
                                    fullYear = year1 + olderYear;
                                    reg = currentYear - fullYear;
                                  }
                                  printf(reg);

                                  }

  {NEWYEAR}{NEWCOUNTY}{NUMBER}{
                                  char year[];
                                  int year1;
                                  //bool firstHalf = false;
                                  int fullYear = 0;
                                  memcpy( year, &yytext[0], 2 );
                                  /* if(year[2] == '1')
                                  {
                                    firstHalf = true;
                                  } */
                                  year1 = atoi(year);
                                  fullYear = year1 + newYear;
                                  /* if(firstHalf)
                                  { */
                                    reg = currentYear - fullYear;
                                  /* } */
                                  //else{
                                //    reg = currentYear - fullYear;
                                //  }
                                  printf(reg);
                                }

\n                              {}
.                               {printf("INVALID");}

%%
	int main()
{
	  yylex();
		return 0;
}

