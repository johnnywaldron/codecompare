%{
int currentYear = 2018;
int olderYear = 1900;
int newYear = 2000;
int reg = 0;
%}

%%
([89][0-9]|[0][0-9]|[1][0-2])-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW)-[0-9]{1,6}[^0-9] {
                                  char year[3];
                                  int year1;
                                  int new = 0;
                                  int fullYear = 0;
                                  memcpy( year, &yytext[0], 2 );
                                  year[2] = '\0';
                                  if(year[0] == '0' || year[0] == '1')
                                  {
                                    new = 1;
                                   }
                                  year1 = atoi(year);

                                  if(new == 1)
                                  {
                                    fullYear = year1 + newYear;
                                    reg = currentYear - fullYear;
                                  }
                                  else{
                                    fullYear = year1 + olderYear;
                                    reg = currentYear - fullYear;
                                  }
                                  printf("%d\n",reg);

                                  }
([1][3-8][12])-(C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW)-[0-9]{1,6}[^0-9]    {
                                  char year[3];
                                  int year1;
                                  int fullYear = 0;
                                  memcpy( year, &yytext[0], 2 );
                                  year[2] = '\0';
                                  year1 = atoi(year);
                                  fullYear = year1 + newYear;

                                  reg = currentYear - fullYear;

                                  printf("%d\n",reg);
                                }
([0-9]*-*[A-z]*-*[0-9]*)*       {printf("INVALID \n");}
[\ \t\n]*                   {}

.                               {}

%%
	int main()
{
	  yylex();
		return 0;
}

