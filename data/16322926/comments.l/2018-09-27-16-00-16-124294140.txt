
		/* Companion source code for "flex & bison", published by O'Reilly
  	 * Media, ISBN 978-0-596-15597-1
  	 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
  	 * Author: Irene Ann Tony
		*/


%{
  int pair = 0;
%}

%%
\*\*.*        {}
\".*\"        {printf(yytext)}
\{([^]]*\}    {}
\{[^}\w]      {printf("syntax error\n")}
\{            {pair++;}
\}            {pair--;}

%%

	int main()
{
	  yylex();
  	if(pair != 0){printf("syntax error\n");}
		return 0;
}

