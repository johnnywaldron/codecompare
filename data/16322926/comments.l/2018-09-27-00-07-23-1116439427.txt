
		/* Companion source code for "flex & bison", published by O'Reilly
  	 * Media, ISBN 978-0-596-15597-1
  	 * Copyright (c) 2009, Taughannock Networks. All rights reserved.
  	 * Author: Irene Ann Tony
		*/


%{
  int pair = 0;

%}

%%

\*\*.*	       {}                 //dont print after **
\{(.|\n)*?\}   {}                 //dont print between { and }
\{             {pair++;}          //check even number of { as }
\}             {pair--;}          //check even number if { as }
\"(.|\n)*?\"   {printf(yytext);}  //print between ""
\n	           {printf(yytext);}  //print all the new lines
.	             {printf(yytext);}  //print everything except the new line
%%

	int main()
{
	  yylex();
  	if(pair != 0){printf("syntax error\n");}
		return 0;
}

