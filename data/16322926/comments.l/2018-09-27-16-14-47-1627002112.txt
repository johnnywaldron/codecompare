%{
  int pair = 0;
%}

%%
\*\*.*        {}
\".*\"        {printf(yytext);}
\{[^]]*\}     {}
\{            {pair++; if(pair > 0){printf("syntax error\n"); exit(0);}}
\}            {pair--; if(pair < 0){printf("syntax error\n"); exit(0);}}

%%

	int main()
{
	  yylex();
		return 0;
}

