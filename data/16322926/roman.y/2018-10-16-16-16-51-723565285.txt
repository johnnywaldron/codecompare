%{
#include <stdio.h>
void yyerror(char *s);
int yylex();
int yyparse();
// thousand:
// fiveHundred: THOUSAND FIVECENT FIVE TEN FIFTY CENT FIVECENT THOUSAND  hundred fifty tenSub ten five oneSub + $3 + $4 + $5
%}

%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND EOL

%%
number :
| number five fifty hundred fiveHundred thousand EOL {printf("%d\n", $2 + $3 + $4 + $5 + $6);}
;
thousand: fiveHundred
| THOUSAND {$$ = 1000;}
| THOUSAND THOUSAND {$$ = 2000;}
| THOUSAND THOUSAND THOUSAND {$$ = 3000;}
| thousand fiveHundred {$$ = $1 + $2;}
;
fiveHundred: hundred   {}
| FIVEHUNDRED hundred {$$ = 500 + $2;}
| FIVEHUNDRED THOUSAND {$$ = 1000 - 500;}
;
hundred: fifty
| HUNDRED FIVEHUNDRED {$$ = 400;}
| HUNDRED THOUSAND {$$ = 1000 - 100;}
| HUNDRED {$$ = 100;}
| HUNDRED HUNDRED {$$ = 200;}
| HUNDRED HUNDRED HUNDRED {$$ = 300;}
| HUNDRED fifty {$$ = 100 + $2;}
;
fifty: ten
| tenSub
| FIFTY ten {$$ = 50 + $2;}
;
tenSub: TEN FIFTY {$$ = 50 - 10;}
| TEN HUNDRED {$$ = 100 - 10;}
;
ten: five
| TEN {$$ = 10;}
| TEN TEN {$$ = 20;}
| TEN TEN TEN {$$ = 30;}
| TEN five {$$ = 10 + $2;}
;
five: one
| subone
| FIVE one {$$ = $2 + 5;}
;
subone : ONE FIVE {$$ = 5 - 1;}
| ONE TEN {$$ = 10 - 1;}
;
one : /* empty */{$$ = 0;}
| ONE {$$ = 1;}
| ONE ONE {$$ = 2;}
| ONE ONE ONE {$$ = 3;}
;


%%
	int main()
{
	  yyparse();
		return 0;
}
    void yyerror(char *s)
{
    fprintf(stderr, "error: %s\n", s);
}


