/* Companion source code for "flex & bison", published by O'Reilly
 * Media, ISBN 978-0-596-15597-1
 * Copyright (c) 2009, Taughannock Networks. All rights reserved.

 * Author: Irene Ann Tony
 */


%{
int even = 0;

%}

%%

[0-9]+	{ 
		
			if(atoi(yytext) % 2 == 0)
			{
				even++;
			}
	}

%%

int main()
{
  yylex();
  printf("%8d\n", even);
	return 0;
}
