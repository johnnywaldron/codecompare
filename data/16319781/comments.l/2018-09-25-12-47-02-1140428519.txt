/* */
%{
int openBracketCount = 0;
%}

%%
"{"	    { openBracketCount++; }
"}"	    { if (openBracketCount>0) { openBracketCount--; } }
"**".*  {/*comment */} 
[a-zA-Z]+	{ if(openBracketCount==0) { printf("<%s>\n", yytext); } }
\n      {}
.       {if(openBracketCount==0) { printf("<%s>\n", yytext); } }
%%

int main()
{
  yylex();
  return 0;
}
