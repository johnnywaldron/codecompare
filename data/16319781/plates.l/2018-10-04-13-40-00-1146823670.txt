/* */
%{
  int currentYear=2018;
  int workingNo=0;
  int lengthOfYear=1;
  int digit3=1;     //true
  int yearOpen=1;
%}

YEAR    [0-9]
COUNTY  [C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]
OLDC    [LK|TN|TS|T|W|WD]
NUMBER  [0-9] 
%%
{YEAR}  { if(yearOpen==1)
          {
            
            workingNo= workingNo*10+atoi(yytext);
            if(lengthOfYear==1&&atoi(yytext)!=1){digit3=0;}
            if(lengthOfYear==2&& digit3==1)
            { if(atoi(yytext)>=3){digit3=1;}
              else{digit3=0;}        
            }
            
            if(lengthOfYear>=2&&digit3==0){yearOpen=0;}
            if(lengthOfYear>=3&&digit3==1){yearOpen=0;}
            if(yearOpen==1){
            lengthOfYear++;
            printf(" %s:year, %d, %d ", yytext, digit3, lengthOfYear);}
          }
        }
-{COUNTY}-{NUMBER}	{
//printf("%d, %d", lengthOfYear, workingNo );
}
\n      { workingNo=0; lengthOfYear=0; printf("\n"); yearOpen=1;}
.       {}
%%

int main()
{  
  yylex();
 
  return 0;
}
