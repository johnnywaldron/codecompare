/* */
%{
  int currentYear=2018;
  int workingNo=0;
  int lengthOfYear=0;
%}

YEAR    [0-9]
COUNTY  [C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW]
OLDC    [LK|TN|TS|T|W|WD]
NUMBER  [0-9] 
%%
{YEAR}  { workingNo= atoi(yytext);
          int notFinished=1; int tempNo=workingNo;
          while (notFinished)
          { if(tempNo/10>=1)
            {
              tempNo=tempNo/10;
              lengthOfYear++;
            }
            else
            {
              notFinished=0; 
            }
          }
        }
-{COUNTY}-{NUMBER}	{printf("%d, %d", lengthOfYear, workingNo );}
\n      { workingNo=0; lengthOfYear=0; printf("\n");}
.       {}
%%

int main()
{  
  yylex();
 
  return 0;
}
