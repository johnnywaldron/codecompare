/* */
%{
  int currentYear=2018;
  int workingNo=0;
  int lengthOfYear=0;
%}

NUMBER  [0-9]
COUNTY  C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
YEAR    [0-9] 
%%
{YEAR}+  { workingNo= workingNo*10+atoi(yytext);
          lengthOfYear++;
        }
{YEAR}-{COUNTY}-{NUMBER}	{printf("%d", workingNo);}
\n      { printf("\n");}
.       {}
%%

int main()
{  
  yylex();
 
  return 0;
}
