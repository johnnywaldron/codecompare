/* */
%{
  void yyerror(char *s);
  int yylex();
%}


%%
"I"   { return 1;}
"V"   { return 5;}
"X"   { return 10;}
"L"   { return 50;}
"C"   { return 100;}
"D"   { return 500;}
"M"   { return 1000;}
\n    { }
[ \s\t\r] { /* ignore white space */ }
. 	  { yyerror("Mystery character\n"); }
%%

