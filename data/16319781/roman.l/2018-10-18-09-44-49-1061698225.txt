/* */
%{
  #include <stdio.h>
  void yyerror(char *s);
  int yylex();
%}
  

%%
<<EOF>> {
		  at_end = 1;
		  return END;
		  }

"I"	 { return I; }
"IV" { return IV; }
"V"	 { return V; }
"IX" { return IX; }
"X"	 { return X; }
"XL" { return XL; }
"L"	 { return L; }
"XC" { return XC; }
"C"	 { return C; }
"CD" { return CD; }
"D"	 { return D; }
"CM" { return CM; }
"M"	 { return M; }
[ \s\t\r]	|
\n	 { return EOL; }
. { yyerror("syntax error"); }
%%

