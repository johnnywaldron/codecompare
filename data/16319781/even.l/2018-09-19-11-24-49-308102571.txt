/* */
%{
int allEvens = 0;
int i;
%}

DIGIT [0-9]

%%

[DIGIT]+	{i = atoi(yytext); if(i%2==0)allEvens++;}

%%

int main()
{
  yylex();
  printf("%8d\n", allEvens);
  return 0;
}
