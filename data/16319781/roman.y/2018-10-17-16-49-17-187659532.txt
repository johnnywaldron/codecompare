/* simplest version of calculator */

%{
#  include <stdio.h>
int yylex (YYSTYPE * yylval_param ,yyscan_t yyscanner);
void yyerror(char *s);
%}

/* declare tokens */
%token <d> TOTAL
%token EOL

%%

input: %empty
| input line
;

term: TOTAL 
 | exp TOTAL { printf("= %d\n> ", $1); }
 ;

%%
int main()
{
  printf("> "); 
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("error: %s\n", s);
}


