
%{
#  include <stdio.h>
int yylex ();
void yyerror(char *s);
int error=0;
int total=0;
int end=0;
int prev=0;
%}

/* declare tokens */
%token I IV V IX X XL L XC C CD D CM M
%token EOL
%start start1
%%

start1: 
 | exp { printf("%d\n> ", total); total=0; }
 | err EOL {total=0; }
 | EOL { YYACCEPT; }
 ; 


exp: 
 | I  { total += 1; prev=1;}
 | IV { if(prev==1){error=1;}
        else{total += 4; prev=4; } }
 | V  { if(prev==4){error=1;}
        else{total += 5; prev=5; } }
 | IX { if(prev==5){error=1;}
        else{total += 9; prev=9; } }
 | X  { if(prev==9){error=1;}
        else{total += 10; prev=10; } }
 | XL { if(prev==10){error=1;}
        else{total += 40; prev=40; } }
 | L  { if(prev==40){error=1;}
        else{total += 50; prev=50; } }
 | XC { if(prev==50){error=1;}
        else{total += 90; prev=90; } }
 | C  { if(prev==90){error=1;}
        else{total += 100; prev=100; } }
 | CD { if(prev==100){error=1;}
        else{total += 400; prev=400; } }
 | D  { if(prev==400){error=1;}
        else{total += 500; prev=500; } }
 | CM { if(prev==500){error=1;}
        else{total += 900; prev=900; } }
 | M  { if(prev==900){error=1;}
        else{total += 1000; prev=1000; } }
 ;
 
%%
int main()
{
  while (!error&&!end)
  {
	  yyparse();
	}
	if (error)
	{
	  yyerror("syntax error");
	}
  return 0;
}

void yyerror(char *s)
{
  printf("%s\n", s);
}



