
%{
#include <stdio.h>
#include <stdlib.h>
int yylex ();
void yyerror(char *s);
void intToRoman(int number);
int digit(char ch, int n, int i, char *c);
int sub_digit(char num1, char num2, int i, char *c); 
int error=0;
int p=0;  //prev 
%}
%output "romcalc.tab.c"
/* declare tokens */
%token I IV V IX X XL L XC C CD D CM M
%token ADD SUB MUL DIV OPEN CLOSE EOL
%start start1
%%

start1: 
 | start1 expr EOL{ if(!error){ intToRoman($2); 
 //printf("%d\n", $2);
 } }
 ; 


expr: factor
 | expr ADD factor { $$ = $1 + $3; }
 | expr SUB factor { $$ = $1 - $3; }
 ;

factor: term
 | factor '*' term { $$ = $1 * $3; }
 | factor '/' term { $$ = $1 / $3; }
 ;

term: no
 | term no { $$ = $1 + $2; }
 | '{' expr '}' { $$ = $2; }
 //| '-' term    { $$ = newast('M', $2, NULL); }
 ;

no:  
 | I  { $$ = 1; p=1;}
 | IV { if(p==1){error=1;}
        else{$$ = 4; p=4; } }
 | V  { if(p==4){error=1;}
        else{$$ = 5; p=5; } }
 | IX { if(p==5){error=1;}
        else{$$ = 9; p=9; } }
 | X  { if(p==9){error=1; }
        else{$$ = 10; p=10; } }
 | XL { if(p==10){error=1; }
        else{$$ = 40; p=40; } }
 | L  { if(p==40){error=1; }
        else{$$ = 50; p=50; } }
 | XC { if(p==50){error=1; }
        else{$$ = 90; p=90; } }
 | C  { if(p==90||p==900){error=1; }
        else{$$ = 100; p=100; } }
 | CD { if(p==100){error=1; }
        else{$$ = 400; p=400; } }
 | D  { if(p==400){error=1; }
        else{$$ = 500; p=500; } }
 | CM { if(p==500){error=1; }
        else{$$ = 900; p=900; } }
 | M  { if(p==900){error=1; }
        else{$$ = 1000; p=1000; } }       
 ;
 
%%
int main()
{
  if (!error)
  {
	  yyparse();
	}
	if (error)
	{
	  yyerror("syntax error");
	}
  return 0;
}
void intToRoman(int number)
{
  char c[10001]; 
  int i = 0; 
  int nFlag =0;  
    // If number entered is not valid 
  if (number == 0) 
  { 
    printf("Z"); 
    return; 
  }
  if (number < 0) 
  { 
    nFlag=1;
    number= abs(number); 
  }  
  
    // TO convert decimal number to roman numerals 
    while (number != 0) 
    { 
        // If base value of number is greater than 1000 
        if (number >= 1000) 
        { 
            // Add 'M' number/1000 times after index i 
            i = digit('M', number/1000, i, c); 
            number = number%1000; 
        } 
  
        // If base value of number is greater than or 
        // equal to 500 
        else if (number >= 500) 
        { 
            // To add base symbol to the character array 
            if (number < 900) 
            { 
               // Add 'D' number/1000 times after index i 
               i = digit('D', number/500, i, c); 
               number = number%500; 
             printf("Roman numeral is: "); } 
  
            // To handle subtractive notation in case of number 
            // having digit as 9 and adding corresponding base 
            // symbol 
            else
            { 
                // Add C and M after index i/. 
                i = sub_digit('C', 'M', i, c); 
                number = number%100 ; 
            } 
        } 
  
        // If base value of number is greater than or equal to 100 
        else if (number >= 100) 
        { 
            // To add base symbol to the character array 
            if (number < 400) 
            { 
                i = digit('C', number/100, i, c); 
                number = number%100; 
            } 
  
            // To handle subtractive notation in case of number 
            // having digit as 4 and adding corresponding base 
            // symbol 
            else
            { 
                i = sub_digit('C','D',i,c); 
                number = number%100; 
            } 
        } 
  
        // If base value of number is greater than or equal to 50 
        else if (number >= 50 ) 
        { 
            // To add base symbol to the character array 
            if (number < 90) 
            { 
                i = digit('L', number/50,i,c); 
                number = number%50; 
            } 
  
            // To handle subtractive notation in case of number 
            // having digit as 9 and adding corresponding base 
            // symbol 
            else
            { 
                i = sub_digit('X','C',i,c); 
                number = number%10; 
            } 
        } 
        // If base value of number is greater than or equal to 10 
        else if (number >= 10) 
        { 
            // To add base symbol to the character array 
            if (number < 40) 
            { 
                i = digit('X', number/10,i,c); 
                number = number%10; 
            } 
  
            // To handle subtractive notation in case of 
            // number having digit as 4 and adding 
            // corresponding base symbol 
            else
            { 
                i = sub_digit('X','L',i,c); 
                number = number%10; 
            } 
        } 
  
        // If base value of number is greater than or equal to 5 
        else if (number >= 5) 
        { 
            if (number < 9) 
            { 
                i = digit('V', number/5,i,c); 
                number = number%5; 
            } 
  
            // To handle subtractive notation in case of number 
            // having digit as 9 and adding corresponding base 
            // symbol 
            else
            { 
                i = sub_digit('I','X',i,c); 
                number = 0; 
            } 
        } 
  
        // If base value of number is greater than or equal to 1 
        else if (number >= 1) 
        { 
            if (number < 4) 
            { 
                i = digit('I', number,i,c); 
                number = 0; 
            } 
  
            // To handle subtractive notation in case of 
            // number having digit as 4 and adding corresponding 
            // base symbol 
            else
            { 
                i = sub_digit('I', 'V', i, c); 
                number = 0; 
            } 
        } 
    } 
    if(nFlag==1)
    { 
      printf("-"); 
    }
    int j;
    for (j = 0; j < i; j++) 
      printf("%c", c[j]); 
    printf("\n");
}
int sub_digit(char num1, char num2, int i, char *c) 
{ 
    c[i++] = num1; 
    c[i++] = num2; 
    return i; 
} 
  
// To add symbol 'ch' n times after index i in c[] 
int digit(char ch, int n, int i, char *c) 
{ 
    int j;
    for ( j = 0; j < n; j++) 
        c[i++] = ch; 
    return i; 
} 
void yyerror(char *s)
{
  printf("%s\n", s);
  error=1;
}
