%{
#include <stdio.h>
#include <stdlib.h>
%}


counties1 (DL)|(CW)|(CN)|(CE)|(C)|(D)|(G)|(KE)|(KK)|(KY)|(LD)|(LH)|(LM)|(LS)|(L)|(MH)|(MN)|(MO)|(OY)|(RN)|(SO)|(T)|(WH)|(WX)|(WW)|(W)
counties2 (DL)|(CW)|(CN)|(CE)|(C)|(D)|(G)|(KE)|(KK)|(KY)|(LD)|(LH)|(LK)|(LM)|(LS)|(L)|(MH)|(MN)|(MO)|(OY)|(RN)|(SO)|(TN)|(TS)|(WH)|(WD)|(WX)|(WW)|(W)


%%
[\r\n\t\f\v ] {}
"\n"+	{}
([1][0-2]|[0][0-9]|[1][3-9][12])-{counties1}-([0-9]{1,6})	{year(yytext);}
([8][7-9]|[09][0-9]|[1]([0-2]|[3][1-2]))-{counties2}-([0-9]{1,6})	{year(yytext);}
%%

int year(char str[])
{
  char year[2];
  if(str[0] == '0' | str[0] == '1')
  {
    year[0] = str[0];
    year[1] = str[1];
    int year1 = atoi(year);
    year1 = 18 - year1;
    printf("%d\n", year1);
  }

  if(str[0] == '8' | str[0] == '9')
  {
    year[0] = str[0];
    year[1] = str[1];
    int year2 = atoi(year);
    year2 = 118 - year2;
    printf("%d\n", year2);
  }
}

int main()
{
  yylex();
	return 0;
}

