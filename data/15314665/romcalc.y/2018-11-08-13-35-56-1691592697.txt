
%{
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
int yylex();
int yyparse();
char* toRom(int hinduArab);

%}

%output "romcalc.tab.c"

/* declare tokens */
%token endLine
%token I V X L C D M
%token MULT DIV ADD SUB 
%token PERENTH_OPEN PERENTH_CLOSE


%%

BeginPrint:	{}
 | BeginPrint expr endLine 			{printf("%s\n", toRom($2));}
 ;

 expr: pararomanNumult
  | expr ADD pararomanNumult                { $$ = $1 + $3; }
  | expr SUB pararomanNumult                { $$ = $1 - $3; }
  ;

 pararomanNumult: paraExpr
  | pararomanNumult MULT paraExpr          { $$ = $1 * $3; }
  | pararomanNumult DIV paraExpr          { $$ = $1 / $3; }
  ;

 paraExpr: fullNumeral
  | PERENTH_OPEN expr PERENTH_CLOSE         { $$ = $2; }
  ;

fullNumeral: thous hunds tens eyes	{$$ = $1 + $2 + $3 + $4;}
 |hunds tens eyes			{$$ = $1 + $2 + $3;}
 |tens eyes					{$$ = $1 + $2;}
 |eyes						{$$ = $1;}
 ;

thous: M {$$ = 1000;}
 |{$$ = 0;}
 |M M {$$ = 2000;}
 |M M M {$$ = 3000;}
 ;

oneHund:	C			{$$ = 100;}
  |{$$ = 0;}
  |C C		{$$ = 200;}
  |C C C		{$$ = 300;}
  ;

hunds: oneHund	{$$ = $1;}
 |{$$ = 0;}
 |C D		{$$ = 400;}
 |D oneHund	{$$ = 500 + $2;}
 |C M		{$$ = 900;}
 ;


ten: X			{$$ = 10;}
  |{$$ = 0;}
  |X X		{$$ = 20;}
  |X X X		{$$ = 30;}
  ;

tens: ten		{$$ = $1;}
 |{$$ = 0;}
 |X L		{$$ = 40;}
 |L ten	{$$ = 50 + $2;}
 |X C		{$$ = 90;}
 ;

oneI: I			{$$ = 1;}
  |{$$ = 0;}
  |I I		{$$ = 2;}
  |I I I		{$$ = 3;}
  ;

eyes: oneI	{$$ = $1;}
 |{$$ = 0;}
 |I V		{$$ = 4;}
 |V oneI	{$$ = 5 + $2;}
 |I X		{$$ = 9;}
 ;



%%
void yyerror(char *romanNumult){
  printf("%s\n", romanNumult);
  exit(0);
}

char *toRom (int hinduArab){
 char *romanNum;
  if(hinduArab == 0){
     *romanNum = 'Z';
	 return romanNum;
  }

 
  int   size[]         = {0, 1, 2, 3, 2, 1, 2, 3, 4, 2};
  char *hunds[]     = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
  char *tens[]         = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
  char *eyes[] = {"","I","II","III","IV","V","VI","VII","VIII","IX"};

  if (hinduArab < 0) {
     *romanNum++ = '-';
	 hinduArab = -hinduArab;
    }

  while (hinduArab >= 1000) {
        *romanNum++ = 'M';
        hinduArab -= 1000;
    }
	
   strcpy (romanNum, hunds[hinduArab/100]); romanNum += size[hinduArab/100]; hinduArab = hinduArab % 100;
    strcpy (romanNum, tens[hinduArab/10]);  romanNum += size[hinduArab/10];  hinduArab = hinduArab % 10;
    strcpy (romanNum, eyes[hinduArab]);     romanNum += size[hinduArab];

*romanNum = '\0';

return romanNum;
 

  }
