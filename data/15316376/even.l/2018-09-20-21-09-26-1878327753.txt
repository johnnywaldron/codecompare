%{
int count = 0;
int i = 0;
%}

%%

d*[02468]	{ count++; }
\n      { /* ignore new line */ }
[ \t]   { /* ignore white space */ }
.	{ /* ignore everything else */ }

%%

int main()
{
  yylex();
  printf("%d\n", count);
  return 0;
}
