%{
        enum yytexttype {
                COMMENT = 1,
                CODE = 2
        };

        char* yylval;
%}

cmnt1           "{"[^}]*|\n*"}"
cmnt2           "**".*
str             ["]+[.]*+["]

%%
{str}           { return CODE; }

{cmnt1}         { return COMMENT; }

{cmnt2}         { return COMMENT; }

"{"             { printf("syntax error\n"); exit(0); }

"}"             { printf("syntax error\n"); exit(0); }

.               { yylval = yytext; return CODE; }
%%

int main() {
        int text;
        while(text = yylex()) {
                if(text == CODE) {
                        printf("%s", yylval);
                }
        }
        return 0;
}
