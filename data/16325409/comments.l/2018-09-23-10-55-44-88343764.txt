%{
        enum yytokentype {
                COMMENT = 1,
                CODE = 2
        };

        char* yylval;
        int comment1_flag = 0;
        int comment2_flag = 0;
%}

%%
"**"            { if(!comment1_flag) {comment1_flag = 1;} }
\n              { if(comment1_flag) {comment1_flag = 0;} else {yylval = "\n"; return CODE;} }
"{"             { if(!comment2_flag) {comment2_flag = 1;} }
"}"             { if(comment2_flag) {comment2_flag = 0;} }
.               { if(comment1_flag || comment2_flag) {return COMMENT;} else {yylval = yytext; return CODE;} }
%%

int main() {
        int tok;

        while(tok = yylex()) {
                if(tok == CODE) {
                        printf("%s", yylval);
                }
        }
        return 0;
}

