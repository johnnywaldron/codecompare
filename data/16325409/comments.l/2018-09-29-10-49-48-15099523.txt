%{
        enum yytexttype {
                COMMENT = 1,
                CODE = 2
        };

        char* yylval;
%}

comment1        "{".*|\n*"}"
comment2        "**".*
string          ["]+[.]*+["]

%%
{string}        { yylval = yytext; return CODE; }

{comment1}      { return COMMENT; }

{comment2}      { return COMMENT; }

.               { yylval = yytext; return CODE; }
%%

int main() {
        int text;
        while(text = yylex()) {
                if(text == CODE) {
                        printf("%s", yylval);
                }
        }
        return 0;
}  
