%{
	enum yytokentype {
		ONE = 0,
		FIVE = 1,
		TEN = 2,
		FIFTY = 3,
		HUNDRED = 4,
		FIVEHUNDRED = 5,
		THOUSAND = 6,
		ADD = 7,
		SUB = 8,
		MUL = 9,
		DIV = 10,
		O_PAR = 11,
		C_PAR = 12,
		EOL = 13
	};
%}

%%
I		{ return ONE; }
V		{ return FIVE; }
X		{ return TEN; }
L		{ return FIFTY; }
C		{ return HUNDRED; }
D		{ return FIVEHUNDRED; }
M		{ return THOUSAND; }

"+"		{ return ADD; }
"-"		{ return SUB; }
"*"		{ return MUL; }
"/"		{ return DIV; }
"{"		{ return O_PAR; }
"}"		{ return C_PAR; }
\n		{ return EOL; }

[ \t]*		{ /*ignore*/ }
.		{ yyerror();  }
%%
