%{
#include <stdio.h>
int yylex();
void yyerror();
void intToRoman(int x, char* result);
%}


%token ONE FIVE TEN FIFTY HUNDRED FIVEHUNDRED THOUSAND
%token ADD SUB MUL DIV
%token O_PAR C_PAR
%token EOL

%%
calclist: 	/* nothing */
		| exp EOL 					{ char romNum[50]; intToRoman($1, romNum); printf("%s\n", romNum); };

exp: 		factor
    		| exp ADD factor 				{ $$ = $1 + $3; }
 		| exp SUB factor 				{ $$ = $1 - $3; };

factor: 	term
       		| factor MUL term 				{ $$ = $1 * $3; }
 		| factor DIV term 				{ $$ = $1 / $3; };

term: 		number   					{ $$ = $1; }
 		| O_PAR exp C_PAR 				{ $$ = $2; }
 		| SUB term    					{ $$ = - $2; };

number: 	thousands EOL 					{ };

thousands: 	THOUSAND fivehundreds				{ $$ = 1000 + $2; }
	 	| THOUSAND THOUSAND fivehundreds		{ $$ = 2000 + $3; }
		| THOUSAND THOUSAND THOUSAND fivehundreds	{ $$ = 3000 + $4; }
		| fivehundreds					{ $$ = $1; };

fivehundreds: 	FIVEHUNDRED hundreds				{ $$ = 500 + $2; }
	    	| HUNDRED FIVEHUNDRED fifties			{ $$ = 400 + $3; }
		| HUNDRED THOUSAND fifties			{ $$ = 900 + $3; }
		| hundreds					{ $$ = $1; };

hundreds: 	HUNDRED fifties					{ $$ = 100 + $2; }
		| HUNDRED HUNDRED fifties			{ $$ = 200 + $3; }
		| HUNDRED HUNDRED HUNDRED fifties		{ $$ = 300 + $4; }
		| fifties					{ $$ = $1; };

fifties: 	FIFTY tens					{ $$ = 50 + $2; }
       		| TEN FIFTY fives				{ $$ = 40 + $3; }
		| TEN HUNDRED fives				{ $$ = 90 + $3; }
		| tens						{ $$ = $1; };

tens: 		TEN fives					{ $$ = 10 + $2; }
    		| TEN TEN fives					{ $$ = 20 + $3; }
		| TEN TEN TEN fives				{ $$ = 30 + $4; }
		| fives						{ $$ = $1; };

fives: 		FIVE ones					{ $$ = 5 + $2; }
     		| ONE FIVE					{ $$ = 4; }
		| ONE TEN					{ $$ = 9; }
		| ones						{ $$ = $1; };

ones: 		/* NOTHING */					{ $$ = 0; }
    		| ONE						{ $$ = 1; }
		| ONE ONE					{ $$ = 2; }
		| ONE ONE ONE					{ $$ = 3; };
%%

int main() {
	yyparse();
	return 0;
}

void intToRoman(int x, char* result) {
	if ( x == 0 ) {
		*result++ = 'Z';
		return;
	}
	if ( x < 0 ) {
		*result++ = '-';
		x *= -1;
	}

	char *huns[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
	char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
	char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
	int   size[] = { 0,   1,    2,     3,    2,   1,    2,     3,      4,    2};

	while (x >= 1000) {
		*result++ = 'M';
		x -= 1000;
	}

	strcpy (result, huns[x/100]); result += size[x/100]; x = x % 100;
	strcpy (result, tens[x/10]);  result += size[x/10];  x = x % 10;
	strcpy (result, ones[x]);     result += size[x];

	*result++ = '\0';
}

void yyerror() {
	printf("syntax error\n");
}
