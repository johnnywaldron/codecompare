%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  void yyerror(char *s);
  extern int yylex();
  extern int yyparse();

  int variables[26];

  void str_var(char c, int val) {
    variables[c-'a'] = val;
  }
  int ld_var(char c) {
    return variables[c-'a'];
  }

%}

%union {
  int ival;
  char cval;
}

/* declare tokens */
%token EOL
%right ASSIGN
%left ADD SUB           /* %left makes these left associative */
%left MUL DIV
%token SEMIC PRINT
%type <ival> expr
%type <ival> unary
%token <ival> NUM
%token <cval> VAR

%%

run:
| line run                      {return 0;}
;

line: VAR ASSIGN expr SEMIC EOL {str_var($1, $3);}
| PRINT VAR SEMIC EOL           {printf("%d\n", ld_var($2));}
;

expr: expr ADD expr            {$$ = $1 + $3;}
| expr SUB expr                {$$ = $1 - $3;}
| expr MUL expr                {$$ = $1 * $3;}
| expr DIV expr                {$$ = $1 / $3;}
| unary                         {$$ = $1;}
;

unary: SUB unary                {$$ = - $2;}
| NUM                           {$$ = $1;}
| VAR                           {$$ = ld_var($1);}
;

%%

int main() {
  yyparse();
  return 0;
}

void yyerror(char *s) {
  printf("syntax error\n");
}
