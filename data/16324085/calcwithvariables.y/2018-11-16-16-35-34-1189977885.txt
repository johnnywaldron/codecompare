%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  void yyerror(char *s);
  extern int yylex();
  extern int yyparse();

  int variables[26];

  void str_var(char c, int val) {
    variables[c-'a'] = val;
  }

  int ld_var(char c) {
    return variables[c-'a'];
  }

%}

%union {
  int ival;
  char cval;
}

/* declare tokens */
%token EOL
%left ADD SUB           /* %left makes these left associative */
%left MUL DIV
%left ASSIGN
%token SEMIC PRINT
%type <ival> expr
%type <ival> unary
%token <ival> NUM
%token <cval> VAR

%%

run: full run
| full

full: line full
| PRINT VAR SEMIC EOL           {printf("%d\n", ld_var($2));}

line: VAR ASSIGN expr SEMIC EOL {str_var($1, $3);}

expr: unary ADD expr            {$$ = $1 + $3;}
| unary SUB expr                {$$ = $1 - $3;}
| unary MUL expr                {$$ = $1 * $3;}
| unary DIV expr                {$$ = $1 / $3;}
| unary                         {$$ = $1;}

unary: SUB unary                {$$ = - $2;}
| NUM                           {$$ = $1;}
| VAR                           {$$ = ld_var($1);}

%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("syntax error\n");
}
