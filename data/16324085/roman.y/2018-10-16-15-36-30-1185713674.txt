%{
  #  include <stdio.h>
  #  include <stdlib.h>

  void yyerror(char *s);
  extern int yylex();
  extern int yyparse();
%}

/* declare tokens */
%token ONE FIVE TEN FIFTY HUND FIVEHUN THOUS EOL
/* I V X L C D M */

%%

fullnum: /* nothing */
| thousand              {printf("%d\n", $1);}
;

thousand: fivehund
| THOUS                       {$$ = 1000;}
| THOUS THOUS                 {$$ = 2000;}
| THOUS THOUS THOUS           {$$ = 3000;}
| THOUS fivehund              {$$ = 1000 + $2;}
| THOUS THOUS fivehund        {$$ = 2000 + $3;}
| THOUS THOUS THOUS fivehund  {$$ = 3000 + $4;}


fivehund: hundreds
| FIVEHUN               {$$ = 500;}
| FIVEHUN hundreds      {$$ = 500 + $2;}
| HUND FIVEHUN          {$$ = 400;}     /* maybe don't need */
| HUND FIVEHUN fifty    {$$ = 400 + $3;}
;

hundreds: fifty
| HUND                  {$$ = 100;}
| HUND HUND             {$$ = 200;}
| HUND HUND HUND        {$$ = 300;}
| HUND fifty            {$$ = 100 + $2;}
| HUND HUND fifty       {$$ = 200 + $3;}
| HUND HUND HUND fifty  {$$ = 300 + $4;}
;

 fifty: tens
| FIFTY                 {$$ = 50;}
| FIFTY tens            {$$ = 50 + $2;}
| TEN FIFTY             {$$ = 40;}    /* maybe don't need */
| TEN FIFTY five        {$$ = 40 + $3;}
;

tens: five
| TEN                   {$$ = 10;}
| TEN TEN               {$$ = 20;}
| TEN TEN TEN           {$$ = 30;}
| TEN five              {$$ = 10 + $2;}
| TEN TEN five          {$$ = 20 + $3;}
| TEN TEN TEN five      {$$ = 30 + $4;}
;

five: ones
| FIVE                  {$$ = 5;}
| FIVE ones             {$$ = 5 + $2;}
| ONE FIVE              {$$ = 4;}
;

ones: EOL               {$$ = 0;}
| ONE EOL               {$$ = 1;}
| ONE ONE EOL           {$$ = 2;}
| ONE ONE ONE EOL       {$$ = 3;}
;

%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  fprintf(stderr, "error: %s\n", s);
}
