%{
#include "roman.tab.h"
%}

%%
"I"   { return I; }
"V"   { return V; }
"X"   { return X; }
"L"   { return L; }
"C"   { return C; }
"D"   { return D; }
"M"   { return M; }
\n    { return EOL; }
[^I|V|X|L|C|D|M|\n| ] { return ERR; }

%%
