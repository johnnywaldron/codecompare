%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  void yyerror(char *s);
  extern int yylex();
  extern int yyparse();

  void dec_to_roman(int num)
  {
    if(num == 0)
    printf("Z");
    char *sign = "";
    if(num < 0) {
      printf("-");
      num = num * -1;
    }
    char rom_num[100];
    int dec[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
    char *sym[] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
    int i = 0;
    while(num){
      while(num/dec[i]){
        printf("%s",sym[i]);
        num -= dec[i];
      }
      i++;
    }
    printf("\n");
  }

%}

/* declare tokens */
%token EOL ERR
%left ADD SUB           /* %left makes these left associative */
%left MUL DIV
%left OPENPAR CLOSPAR
%token I V X L C D M
/* I V X L C D M */

%%

print:
| print expr EOL        {dec_to_roman($2);}
;

expr: thousands
| expr ADD expr         {$$ = $1 + $3;}
| expr SUB expr         {$$ = $1 - $3;}
| expr MUL expr         {$$ = $1 * $3;}
| expr DIV expr         {$$ = $1 / $3;}
| OPENPAR expr CLOSPAR  {$$ = $2;}
;

thousands: fivehund
| M thousands           {$$ = 1000 + $2;}
;

fivehund: hundreds
| D hundreds            {$$ = 500 + $2;}
| C D fifty             {$$ = 400 + $3;}
| C M fifty             {$$ = 900 + $3;}
;

hundreds: fifty
| C fifty               {$$ = 100 + $2;}
| C C  fifty            {$$ = 200 + $3;}
| C C C fifty           {$$ = 300 + $4;}
;
 fifty: tens
| L tens                {$$ = 50 + $2;}
| X L five              {$$ = 40 + $3;}
| X C five              {$$ = 90 + $3;}
;

tens: five
| X five                {$$ = 10 + $2;}
| X X five              {$$ = 20 + $3;}
| X X X five            {$$ = 30 + $4;}
;

five: ones
| V ones                {$$ = 5 + $2;}
| I V                   {$$ = 4;}
| I X                   {$$ = 9;}
;

ones:                   {$$ = 0;}
| I                     {$$ = 1;}
| I I                   {$$ = 2;}
| I I I                 {$$ = 3;}
;

%%

int main()
{
  yyparse();
  return 0;
}

void yyerror(char *s)
{
  printf("syntax error\n");
}
