%{
#include "calcwithvariables.tab.h"

%}

%%
[a-z]     { yylval.cval = yytext[0]; return VAR;}
[0-9]+    { yylval.ival = atoi(yytext); return NUM; }
":="      { return ASSIGN; }
"+"       { return ADD; }
"-"       { return SUB; }
"*"       { return MUL; }
"/"       { return DIV; }
";"       { return SEMIC; }
"print "  { return PRINT; }
\n        { return EOL; }
.         { }
%%
