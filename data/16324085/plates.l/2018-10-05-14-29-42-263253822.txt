%{
int year = 0;
%}

YEAR3       [1][3-8][12]
YEAR2       ([8][7-9])|([09][0-9])|([1][0-2])
COUNTYNEW   (C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|O|T|W|WH|WX|WW)
COUNTYOLD   (C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|O|TN|TS|W||WD|WH|WX|WW)
NUMBER      [1-9][0-9]{1,5}

%%

{YEAR3}[ \t]*{COUNTYNEW}[ \t]*{NUMBER}  { year = atoi(YEAR3);
                                          printf("%d\n", (182 - year)); }
{YEAR2}[ \t]*{COUNTYOLD}[ \t]*{NUMBER}  { year = atoi(YEAR2);
                                          if(year < 13)
                                            printf("%d", (18 - year));
                                          else
                                            printf("%d", (2018 - (1900 + year)));}
.                                       {printf("INVALID\n");}

%%

int main()
{
  yylex();
  return 0;
}

