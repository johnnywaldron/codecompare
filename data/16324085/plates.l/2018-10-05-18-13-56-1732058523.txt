%{

int year = 0;
%}
/*2013 - 2018 (131 - 182)*/
YEAR3       [1][3-8][1|2][-]
/*1987 - 2012*/
YEAR2       ([8][7-9])|([9|0][0-9])|([1][0-2])[-]
/*2015 - 2018 (151 - 182)*/
COUNTYNEW   (C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|O|T|W|WH|WX|WW)[-]
/*1987 - 2014*/
COUNTYOLD   (C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LK|LD|LH|LM|LS|MH|MN|MO|OY|RN|O|TN|TS|W|WD|WH|WX|WW)[-]
NUMBER      [0-9]{1,6}

%%

{YEAR3}{COUNTYNEW}{NUMBER}  {char yearStr[3]; yearStr[2] = '\0';
                                  yearStr[0] = yytext[0]; yearStr[1] = yytext[1];
                                  year = atoi(yearStr);
                                  printf("%d\n", (18 - year));}
{YEAR2}{COUNTYOLD}{NUMBER}  {char yearStr[3]; yearStr[2] = '\0';
                                  yearStr[0] = yytext[0]; yearStr[1] = yytext[1];
                                  year = atoi(yearStr);
                                  if(year < 13)
                                    printf("%d\n", (18 - year));
                                  else
                                    printf("%d\n", (2018 - (1900 + year)));}
[ \tab\n]                         /*do nothing*/
[.]                               {printf("INVALID\n");}

%%

int main()
{
  yylex();
  return 0;
}

