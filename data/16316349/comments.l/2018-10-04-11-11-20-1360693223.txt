/*
 * https://www.scss.tcd.ie/John.Waldron/3071/3071.html
 * author: sastaffo@tcd.ie, student no. 16316349
 * extension to 5th October
 */

/*comments.l removes properly formatted comments from the input*/
%{
int singleLineComment = 0;
int commentOpen = 0;
char* program = "";
%}

%%

"**"    { singleLineComment = 1;}
"{"     { commentOpen = 1; }
"}"     { if (commentOpen) commentOpen = 0; else { printf("syntax error\n"); return;}}
\n      { if (singleLineComment) singleLineComment = 0; }
.*      { if (!singleLineComment && !commentOpen) program.concat(program, yytext);}

<<EOF>> { if (commentOpen) printf("syntax error\n"); return;}

%%

int main()
{
  yylex();
  printf("%d\n", program);
	return 0;
}

