%{
   int currYear = 18;
%}

old      {yrOld}{dash}{cntyOld}{dash}{seqNum}
thirteen {yrT}{dash}{cntyOld}{dash}{seqNum}
new      {yrNew}{dash}{cntyNew}{dash}{seqNum}

dash    [-]
yrOld   [0-1,8-9][0-9]
yrT     [1][3][1-2]
yrNew   [1][0-9][1-2]
cntyNew C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
cntyOld C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LK|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
seqNum  [1-9][0-9]{0,5}

%%

{old} {/*finds all plates from years 1987-2012*/
            char yearS[3];
            memcpy(yearS, yytext, 2);
            yearS[2]='\0';
            int year = atoi(yearS);
            int age;
            if (year>=00&&year<=12) {
                age = currYear-year;
                printf("%d\n",age);
            } else if (year>=87&&year<=99) {
                age = (100+currYear)-year;
                printf("%d\n",age);
            } else printf("INVALID - year old\n");
        }
{thirteen} { /*finds all plates from year 2013*/
            int age = currYear-13;
            printf("%d\n",age);
        }
{new} {/*finds all plates from years 2014-present*/
            char yearS[3];
            memcpy(yearS, yytext, 2);
            yearS[2]='\0';
            int year = atoi(yearS);
            if (year>=14&&year<=currYear) {
                int age = currYear-year;
                printf("%d\n",age);
            } else printf("INVALID - year new \n");
        }
\t|\n|\s      { /*ignores whitespace */}
.*      {/*prints INVALID for all other forms*/
            printf("INVALID - match [%s]\n", yytext);
        }
%%

