%{
   int currYear = 18;
   char* inv = "INVALID\n";
   char* str = "%d\n";
%}

dash    "-"
yrOld   [0-9]{[2]}
yr13    "131"|"132"
yrNew   [0-9]{[2]}[1-2]
cntyNew C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LM|LS|MH|MN|MO|OY|RN|SO|T|W|WH|WX|WW
cntyOld C|CE|CN|CW|D|DL|G|KE|KK|KY|L|LD|LH|LK|LM|LS|MH|MN|MO|OY|RN|SO|TN|TS|W|WD|WH|WX|WW
seqNum  [1-9][0-9]{0,[5]}

old      {yrOld}"-"{cntyOld}"-"{seqNum}
thirteen "131"|"132""-"{cntyOld}"-"{seqNum}
new      {yrNew}"-"{cntyNew}"-"{seqNum}
%%
old  {/*finds all plates from years 1987-2012*/
            char* yearChar = {yytext[0], yytext[1]};
            int year = atoi(yearChar);
            int age;
            if (year>=00&&year<=12) {
                age = currYear-year;
                printf(str,age);
            } else if (year>=87&&year<=99) {
                age = (100+currYear)-year;
                printf(str,age);
            } else printf(inv);
        }
thirteen { /*finds all plates from year 2013*/
            int age = currYear-13;
        }
new {/*finds all plates from years 2014-present*/
            char* yearChar = {yytext[0], yytext[1]};
            int year = atoi(yearChar);
            if (year>=14&&year<=currYear) {
                int age = currYear-year;
                printf(str,age);
            } else printf(inv);
        }

.*      {/*prints INVALID all other forms*/
            printf(inv);
        }
%%

