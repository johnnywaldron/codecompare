#!/usr/bin/python3
'''
    Test harness to build and run a front-end on a Python test-suite.
    Default is to measure all fornt-ends against all libraries.
    Prints a table of pass rates (no of files passed / total no of files)
'''

import os
import codecs
from datetime import datetime

from difflib import SequenceMatcher

from tokenise_bison import Processor

# Where the data files live:
_THIS_DIR = os.path.dirname(__file__)
DATAROOT = os.path.join(_THIS_DIR, '..', 'data')
RESULTS = os.path.join(_THIS_DIR, '..', 'results')

PROMISCUITY_FILE = os.path.join(_THIS_DIR, 'one-total-sims.dat')
PAIRS_FILE = os.path.join(_THIS_DIR, 'two-total-sims.dat')

# N.B. these are in chronological order:
PROJECTS = 'even comments plates roman romcalc calcwithvariables'.split()

PROGRAMS = \
    'calcwithvariables.l comments.l even.l plates.l \
    roman.l romcalc.l calcwithvariables.y roman.y romcalc.y'.split()

# prefix format of JW's filename:
FILENAME_FORMAT = 'YYYY-MM-DD-HH-MM-SS'

# This is the earliest  date that a project could have been submitted:
FIRST_DAY = datetime(2018, 9, 17)
SEC_IN_DAY = 24*60*60  # Used to convert no. of seconds to no. of days

# File suffix for any data I write:
DATA_SUFFIX = '.dat'

# Set this is you want the bison files pre-processed
BISON_PROCESS = os.path.join(os.path.dirname(__file__), '..', 'bisonpp')
BISON_PROCESS = None


def read_jw_data(project):
    '''Read in the similiary results from JW's file'''
    get_student = lambda l: l.split('/')[1]  # The student id
    REP_SUFFIX = '.rep'
    metrics = []
    repfile = os.path.join(RESULTS, project+REP_SUFFIX)
    with open(repfile, 'r') as fh:
        for line in fh:
            fp1, fp2, c, m = line.strip().split()
            [s1, s2] = [get_student(f) for f in [fp1, fp2]]
            if s1 < s2:  # Symmetric realtion, so only record once
                metrics.append((s1, s2, round(float(m)*100)))
    return sorted(metrics, key=lambda t: t[0]+t[1])


def get_students(dataroot=DATAROOT):
    '''Return the list of directory names - i.e. student ids'''
    _, dirs, _ = next(os.walk(dataroot))
    return sorted(dirs)


def get_txt_files(student, project):
    '''Return a list of the txt files, sorted in order of date/time'''
    dirname = os.path.join(DATAROOT, student, project)
    files = []
    if os.path.isdir(dirname):
        _, _, files = next(os.walk(dirname))
    files = [f for f in files if f.endswith('.txt')]
    prefix_len = len(FILENAME_FORMAT)
    files.sort(key=lambda f: f[:prefix_len])
    return files


def _read_file_process(line, level=4):
    '''How do you want a program line processed? Default=delete spaces'''
    if level == 1:  # Strip white spaces on right
        return line.rstrip()
    elif level == 2:  # Strip white spaces on left and right
        return line.strip()
    elif level == 3:  # Strip left/right, replace all other spaces with one
        return ' '.join(line.strip().split())
    elif level == 4:  # Delete all whitespace in the line
        return ''.join(line.strip().split())
    else:  # Do no processing at all:
        return line


def read_file(student, project, file):
    '''Return the contents of a program file as a list of non-blank lines'''
    filename = os.path.join(DATAROOT, student, project, file)
    # Do some preprocessing if it's a bison file:
    if project.endswith('.y') and BISON_PROCESS:
        tempfile = os.path.join(BISON_PROCESS, student+'-'+project)
        if not os.path.isfile(tempfile):
            p = Processor(True)
            with open(tempfile, 'w') as ofh:
                for line in p.read_bison_file(filename):
                    ofh.write(line+'\n')
        filename = tempfile
    with codecs.open(filename, 'r', encoding='utf-8', errors='ignore') as fh:
        raw_lines = [line for line in fh]
    lines = [_read_file_process(line) for line in raw_lines]
    lines = [nb for nb in lines if len(nb) > 0]
    return lines


def read_latest_file(student, project):
    '''Return the lines from the most recent file for this student'''
    files = get_txt_files(student, project)
    if files:
        return read_file(student, project, files[-1])


def get_timestamps(dataroot=DATAROOT):
    ''' Calculate the time of the last submission for each project/student.
        The time is calculated as days since FIRST_DAY, 1 decimal place.
    '''
    DEC_PLACES = 2  # No of decimal places in time (days)
    times = {}
    for student_id in get_students():
        times[student_id] = {}
        for program in PROGRAMS:
            files = get_txt_files(student_id, program)
            if (not files) or len(files) == 0:
                continue
            # Get the date and time of the student's last submission:
            latest = files[-1]
            prefix_len = len(FILENAME_FORMAT)
            dt = [int(y) for y in latest[:prefix_len].split('-')]
            # Now convert this to seconds and days, and store:
            diff_sec = (datetime(*dt) - FIRST_DAY).total_seconds()
            diff_day = round(diff_sec / SEC_IN_DAY, DEC_PLACES)
            times[student_id][program] = diff_day
    return times


def _line_is_junk(s):
    '''Ignore these lines in every file (for SequenceMatcher)'''
    return len(s.strip()) == 0


def sm_compare(f1, f2):
    '''Compare two files using a SequenceMatcher object'''
    sm = SequenceMatcher(_line_is_junk, f1, f2)
    return sm.ratio()


def jaccard(f1, f2):
    '''Calculate the Jaccard distance betwen two lists (as sets)'''
    s1 = set(f1)
    s2 = set(f2)
    num_common = len(s1 & s2)
    return num_common / (len(s1) + len(s2) - num_common)


def difference(f1, f2):
    '''Calculate the distance betwen two lists as a prop of the first'''
    s1 = set(f1)
    s2 = set(f2)
    num_common = len(s1 & s2)
    return num_common / len(s1)


def count_common(f1, f2):
    '''Return the number of lines these files have in common'''
    s1 = set(f1)
    s2 = set(f2)
    num_common = len(s1 & s2)
    return num_common


def compare_latest(program, compare):
    ''' Compare the latest submission for a program for all students
        Do a fully-symmetric comparison, just in case.
        REturn a list of triples: (student1, student2, measure)
    '''
    students = get_students()
    metrics = []
    for i, s1 in enumerate(students):
        f1 = read_latest_file(s1, program)
        if (not f1) or len(f1) == 0:
            continue
        for s2 in students:  # not symmetric, so not students[i+1:]:
            if s1 == s2:
                continue
            f2 = read_latest_file(s2, program)
            if (not f2) or len(f2) == 0:
                continue
            sim = round(compare(f1, f2) * 100)
            metrics.append((s1, s2, sim))
    return sorted(metrics, key=lambda t: t[0]+t[1])


def compare_list(student, project, compare):
    '''Compare each file for this student against the previous one'''
    files = get_txt_files(student, project)
    metrics = []
    before = read_file(student, project, files[0])
    for i in range(1, len(files)):
        after = read_file(student, project, files[i])
        sim = round(compare(before, after) * 100)
        metrics.append(sim)
        before = after
    return metrics


def count_files(rootdir, suffix='.txt'):
    '''Count files with this suffix in a dir and its subdirectories'''
    count = 0
    for _, _, files in os.walk(rootdir):
        files = [filename for filename in files
                 if filename.endswith(suffix)]
        count += len(files)
    return count


def count_activity(student_id):
    '''Return the total number of .txt for this student'''
    dirname = os.path.join(DATAROOT, student_id)
    return count_files(dirname, '.txt')


def count_project_activity(student_id, project):
    '''Return the total nuymber of .txt files for this student/project'''
    studir = os.path.join(DATAROOT, student_id)
    _, pfiles, _ = next(os.walk(studir))  # All projects files for this student
    pcounts = [count_files(os.path.join(studir, pfile))
               for pfile in pfiles if pfile.startswith(project)]
    return sum(pcounts)


def count_programs_per_project(programs=PROGRAMS):
    '''Return a dict with the no. of .l/.y files per project'''
    projects = {}
    for prog in programs:
        proj = prefix(prog)
        projects[proj] = projects.get(proj, 0) + 1
    return projects


def count_similar(metrics, threshold=75):
    '''Count those student pairs with a similarity over the given threshold'''
    sims = [m for (_, _, m) in metrics if m > threshold]
    return len(sims)


def print_similar(programs, threshold=75, filename='over'+DATA_SUFFIX):
    '''Print those student-pairs with a similarity over the given threshold'''
    filename = '{}-{}'.format(threshold, filename)
    with open(filename, 'w') as fh:
        for program in programs:
            for s1, s2, m in compare_latest(program, difference):
                if m > threshold:
                    fh.write('{} {} {} {}\n'.format(program, s1, s2, m))
    print('Written', filename, flush=True)


def print_totals(students, programs):
    '''Print the total sim count for each student and each student-pair'''
    done_program = {s: set() for s in students}
    one_totals = {s: 0 for s in students}  # Totals per student
    two_totals = {}  # Totals per student-pair
    for program in programs:
        # For this program, collect all the sim results for each student:
        one_totals_prog = {s: [] for s in students}  # Totals per student
        for s1, s2, simval in compare_latest(program, difference):
            if not (s1 in students and s2 in students):
                continue
            done_program[s1].add(program)
            done_program[s2].add(program)
            one_totals_prog[s1].append(simval)
            one_totals_prog[s2].append(simval)
            two_totals[(s1, s2)] = simval + two_totals.get((s1, s2), 0)
        # Process all for this program, so now add in to overall totals:
        for s, simlist in one_totals_prog.items():
            if len(simlist) > 0:
                one_totals[s] += max(simlist)
    # Print promiscuity averages for each student (biggest first):
    with open(PROMISCUITY_FILE, 'w') as fh:
        for s, sim_tot in sorted(one_totals.items(), key=lambda p: -p[1]):
            prog_count = len(done_program[s])
            sim_avg = round(sim_tot/prog_count)
            fh.write('{} {} {} {}\n'.format(s, sim_tot, prog_count, sim_avg))
    print('Written', PROMISCUITY_FILE, flush=True)
    # Print totals for each student-pair (biggest first):
    with open(PAIRS_FILE, 'w') as fh:
        for ((s1, s2), m) in sorted(two_totals.items(), key=lambda p: -p[1]):
            fh.write('{} {} {}\n'.format(s1, s2, m))
    print('Written', PAIRS_FILE, flush=True)


def prefix(filename):
    '''Snip off dot-suffix from a filename'''
    return filename[:filename.rfind('.')]


def suffix(filename):
    '''Return off dot-suffix from a filename'''
    return filename[filename.rfind('.')+1:]


def dot_print_similar(programs, threshold=75, highlight=[]):
    ''' Print those student pairs with a similarity over the given threshold
        One file for each project (use the file prefix).
        Highlight any student in the given list (color node red)
    '''
    # Get metrics for each project by grouping files:
    projects = set(prefix(p) for p in programs)
    pmetrics = {p: [] for p in projects}
    for proj_file in programs:
        fmetrics = [(proj_file, s1, s2, m)
                    for (s1, s2, m) in compare_latest(proj_file, difference)
                    if m > threshold]
        pmetrics[prefix(proj_file)].extend(fmetrics)
    total_activity = {s: count_activity(s) for s in get_students()}
    times = get_timestamps()
    # Now print for each project:
    for proj, metrics in pmetrics.items():
        dotfile = '{}-{}.dot'.format(proj, threshold)
        proj_activity = {s: count_project_activity(s, proj)
                         for s in get_students()}
        with open(dotfile, 'w') as fh:
            fh.write('digraph ' + proj + ' {\n')
            fh.write('node [fontsize=30]\n')
            students = set()
            # Write edges
            for proj_file, s1, s2, m in metrics:
                if s1 == s2:
                    continue
                fh.write('n{} -> n{} [label="{}={}\%"]\n'
                         .format(s1, s2, suffix(proj_file), m))
                students.add(s1)
                students.add(s2)
            # Write nodes
            for s in students:
                fillcolor = ''
                if s in highlight:  # Then color the node red:
                    fillcolor = 'fillcolor=red, style=filled, '
                # Get the times of all files for this project:
                stimes = [t for (prog, t) in times[s].items()
                          if prog.startswith(proj)]
                fh.write('n{} [{}label="{} / {} @ {}"]\n'
                         .format(s, fillcolor,
                                 total_activity[s], proj_activity[s],
                                 max(stimes)))
            fh.write('}\n')
        print('Written', dotfile, flush=True)


HIGHLIGHT = []

if __name__ == '__main__':
    students = get_students()
    #print_similar(PROGRAMS, 75)
    #print_totals(students, PROGRAMS)
    #get_timestamps()
    highlight_ids = [s for s,_ in HIGHLIGHT]
    dot_print_similar(PROGRAMS, 75, highlight_ids)
