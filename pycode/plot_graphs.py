#!/usr/bin/python3
'''
    Test harness to build and run a front-end on a Python test-suite.
    Default is to measure all fornt-ends against all libraries.
    Prints a table of pass rates (no of files passed / total no of files)
'''

import os
from math import sqrt

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import mannwhitneyu

import compare

# Make sure we only use Type 1 fonts:
plt.rc('text', usetex=True)
plt.rc('font', family='serif')


SUMMATIVE_FILE = os.path.join(compare.RESULTS, 'summative.csv')
FORMATIVE_FILE = os.path.join(compare.RESULTS, 'summativewithnum.txt')

METRICS_FILE = os.path.join(compare._THIS_DIR, 'all-metrics.dat')
COMPARE_FILE = os.path.join(compare._THIS_DIR, 'all-compare-values.dat')
SCATTER_FILE = 'scatterplot'
SIMHIST_FILE = 'similarity'

LABEL_SIZE = 16


class Metrics:
    def __init__(self, student_id):
        self.student_id = student_id
        self.update(0, 0, 0, 0, 0)

    def update(self, summative, formative, programs, promiscuity, activity):
        self.summative = summative
        self.formative = formative
        self.programs = programs
        self.promiscuity = promiscuity
        self.activity = activity
        self.calc_suspicion()

    def calc_suspicion(self, s_target=0, f_target=100):
        ''' Return the Euclidean distance from the given points
            for the summative and formative marks.
        '''
        s_diff = self.summative - s_target
        f_diff = self.formative - f_target
        dist = sqrt(s_diff*s_diff + f_diff*f_diff)
        self.suspicion = round(dist, 2)

    def __str__(self):
        return '{} {} {} {} {}'.format(self.summative, self.formative,
                                       self.programs,
                                       self.promiscuity, self.activity)


def read_marks(marks):
    # First read the summative marks:
    with open(SUMMATIVE_FILE, 'r') as fh:
        for line in fh:
            student_id, summative = line.strip().split(',')
            if student_id in marks:
                marks[student_id].summative = int(summative)
    # Now the formative marks
    with open(FORMATIVE_FILE, 'r') as fh:
        for line in fh:
            (s, f, student_id) = line.strip().split()
            if student_id not in marks:  # Did MCQs but not programs
                continue
            mk = marks[student_id]
            assert mk.summative == int(s), \
                'error with {}: {} vs {}'.format(student_id, mk, s)
            mk.formative = int(f)


def count_all_activity(marks):
    '''Return a count of all the .txt files for each student'''
    for student_id in marks.keys():
        marks[student_id].activity = compare.count_activity(student_id)


def read_promiscuity(marks, pfile=compare.PROMISCUITY_FILE):
    with open(pfile, 'r') as fh:
        for line in fh:
            (student_id, _,  pdone, pavg) = line.strip().split()
            marks[student_id].programs = int(pdone)
            marks[student_id].promiscuity = int(pavg)


def get_project_activity(programs=compare.PROGRAMS):
    ''' Figure out how many files (.l/.y) for each project'''
    students = compare.get_students()
    projects = compare.count_programs_per_project(programs)
    activity = {}
    for proj, num_files in projects.items():
        counts = [compare.count_project_activity(student_id, proj)
                  for student_id in students]
        activity[proj] = [c / num_files for c in counts]
    return activity


def adjacent_values(vals, q1, q3):
    '''Outliers: return the bounds for +/- 1.5 times the IQR'''
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])
    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value


def plot_activity_violin(programs=compare.PROGRAMS,
                         save_as='violin-activity.pdf'):
    ''' Violin plots showing no. of attempts for each project.
        For each violin, show dot for median, bar for the IQR (Q1 to Q3),
        and whiskers for 1.5*IQR.
    '''
    activity = get_project_activity(programs)
    projects = compare.PROJECTS
    data = [activity[p] for p in projects]
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12, 6), sharey=True)
    plt.rc('axes', labelsize=LABEL_SIZE)    # fontsize of the x and y labels
    parts = ax.violinplot(data, showmeans=False, showmedians=False,
                          showextrema=False)
    # Set the colours for the violin plots:
    for pc in parts['bodies']:
        pc.set_facecolor('#4f90d9')
        pc.set_edgecolor('black')
        pc.set_alpha(1)
    # Do the quartiles:
    quartile1, medians, quartile3 = np.percentile(data, [25, 50, 75], axis=1)
    # Print quartile data to screen, just for confirmation
    for v, q1, q2, q3 in zip(projects, quartile1, medians, quartile3):
        print('\t{} Q1={:3.0f}, Q2={:3.0f}, Q3={:3.0f}'.format(v, q1, q2, q3))
    whiskers = np.array([
        adjacent_values(sorted_array, q1, q3)
        for sorted_array, q1, q3 in zip(data, quartile1, quartile3)])
    whiskersMin, whiskersMax = whiskers[:, 0], whiskers[:, 1]
    # Now draw the meadian, IQR and whiskers:
    inds = np.arange(1, len(medians) + 1)
    ax.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
    ax.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
    ax.vlines(inds, whiskersMin, whiskersMax, color='k', linestyle='-', lw=1)

    # set style for the axes
    ax.set_xlabel('Projects')
    ax.set_xticklabels([''] + projects)
    ax.set_ylabel('Number of attempts')
    ax.set_ylim(0, 225)
    ax.yaxis.grid(True, linestyle='--', which='major', color='grey', alpha=.25)
    if save_as:
        fig.set_size_inches(12, 6, forward=True)
        plt.savefig(save_as, bbox_inches='tight')
    else:
        plt.show()


def pos_to_highlight(metrics, topmost=10):
    ''' Return a list of indicies into metrics for the top n
    '''
    # Sort the metrics by suspicion, and get the topmost
    ometrics = sorted(metrics, key=lambda m: m.suspicion)
    ids = [m.student_id for m in ometrics[-topmost:]]
    print(ids)
    # Now list the positions of those in ids:
    highlight = []
    for i, m in enumerate(metrics):
        if m.student_id in ids:
            highlight.append(i)
    return highlight


def print_stats_mw(diff, label, data, barrier=0):
    '''Print the Mann-Whitney rank test for activity, promiscuity'''
    lower, higher = [], []
    for i, v in enumerate(diff):
        if v <= barrier:
            lower.append(data[i])
        else:
            higher.append(data[i])
    U, pval = mannwhitneyu(lower, higher, alternative='less')
    auc = round(U / (len(lower)*len(higher)), 2)
    print('{:20}: U={}, AUC={:4.2f}, p={}'.format(label, U, auc, pval))


def plot_one_graph(xlabel, ylabel, x, y, highlight, ax):
    # ax.set_yticks(np.arange(220, 300, 15))
    ax.scatter(x[highlight:], y[highlight:], marker='x', color='blue')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    # Now highlight the topmost ones:
    if highlight > 0:
        ax.scatter(x[:highlight], y[:highlight], marker="+", color='blue')
    if min(x) >= 0 and max(x) <= 100:
        ax.set_xlim(xmax=101)
    else:
        plt.axvline(x=0, color='red', linestyle='--')
    ax.set_ylim(ymax=101)


def plot_scatter(metrics, save_as=None):
    ''' Plot all of the scatter plots on one figure.
        Four variables: summative, formative, activity, promiscuity.
    '''
    INACT_STR = 'InActivity'
    COLLAB_STR = 'Collaboration'
    highlight = 0  # highlight the top 10
    # Only plot if attempts>4; put in list to keep the order:
    metrics = [m for m in metrics.values() if m.programs > 4]
    metrics.sort(key=lambda m: m.suspicion)
    print([(m.student_id, m.suspicion) for m in metrics[:highlight]])

    # Round formative/summative marks up to a minimum value:
    MIN_MARK = 40
    form = [max(m.formative, MIN_MARK) for m in metrics]
    summ = [max(m.summative, MIN_MARK) for m in metrics]
    # Plot IN-activity, as a percentage of the max:
    act = [m.activity for m in metrics]
    max_activity = max(act)
    act = [round((max_activity-a)*100/max_activity, 0) for a in act]
    # Promiscuity as-is:
    prom = [m.promiscuity for m in metrics]
    # Difference between formative and summative:
    diff = [f-s for (f, s) in zip(form, summ)]

    print_stats_mw(diff, INACT_STR, act)
    print_stats_mw(diff, COLLAB_STR, prom)
    # fig.set_size_inches(12, 24, forward=True)
    # fig.tight_layout()
    plt.subplots_adjust(left=0.13, bottom=0.20, wspace=0.5, hspace=0.5)
    plt.rc('axes', labelsize=LABEL_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=12)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=12)    # fontsize of the tick labels
    PLOTS_PER_LINE = 2
    for i, (xlabel, ylabel, x, y) in enumerate([
        ('Formative', 'Summative', form, summ),
        ('InActivity', COLLAB_STR, act, prom),
        ('Formative', INACT_STR, form, act),
        ('Formative', COLLAB_STR, form, prom),
        ('Summative', INACT_STR, summ, act),
        ('Summative', COLLAB_STR, summ, prom),
        ('Formative - Summative', INACT_STR, diff, act),
        ('Formative - Summative', COLLAB_STR, diff, prom),
    ]):
        fig, ax = plt.subplots()
        row = i // PLOTS_PER_LINE
        col = i % PLOTS_PER_LINE
        plot_one_graph(xlabel, ylabel, x, y, highlight, ax)
        if save_as:
            filename = '{}-{}-{}.pdf'.format(save_as, row, col)
            plt.savefig(filename, bbox_inches='tight')
            print('Written', filename, flush=True)
        plt.close(fig)


def write_metrics_file(filename, metrics):
    '''Write out all the metrics, one line per student'''
    ometrics = sorted(metrics.values(), key=lambda m: m.suspicion)
    with open(filename, 'w') as fh:
        for m in ometrics:
            fh.write('{} {} {:}\n'.format(m.student_id, m, m.suspicion))


def read_metrics_file(filename):
    '''Read the metrics from a file and return a dict indexed by student_id'''
    metrics = {}
    with open(filename, 'r') as fh:
        for line in fh:
            data = line.strip().split()
            student_id = data[0]
            nos = [int(m) for m in data[1:-1]]
            metrics[student_id] = Metrics(student_id)
            metrics[student_id].update(*nos)
    return metrics


def get_metrics(file=METRICS_FILE):
    '''Return the metrics: read from file, or generate if no file'''
    if os.path.isfile(file):
        metrics = read_metrics_file(file)
    else:
        metrics = {s: Metrics(s) for s in compare.get_students()}
        read_marks(metrics)
        count_all_activity(metrics)
        read_promiscuity(metrics)
        for m in metrics.values():
            m.calc_suspicion()
        write_metrics_file(file, metrics)
    return metrics


def get_and_plot_metrics(save_as=SCATTER_FILE):
    metrics = get_metrics()
    plot_scatter(metrics, save_as)


##########################################################################

def multi_compare_latest(programs, compare_funcs):
    ''' Compare the latest submission for a program for all students.
        Use all the given comparison funcs, collect results for each.
        Retrurn a list of tuples, oneelement for each compare_func
    '''
    results = []
    students = compare.get_students()
    for program in programs:
        for s1 in students:
            f1 = compare.read_latest_file(s1, program)
            if (not f1) or len(f1) == 0:
                continue
            for s2 in students:  # not symmetric, so not students[i+1:]:
                if s1 == s2:
                    continue
                f2 = compare.read_latest_file(s2, program)
                if (not f2) or len(f2) == 0:
                    continue
                sims = [func(f1, f2) for func in compare_funcs]
                for i in range(1, len(sims)):  # All but first are percents:
                    sims[i] = round(sims[i] * 100)
                data = [s1, s2, program] + sims
                results.append(tuple(data))
    return results


def similarity_histograms(programs=compare.PROGRAMS, save_as=SIMHIST_FILE):
    def save_fig(fig, desc):
        if save_as:
            filename = '{}-{}.pdf'.format(save_as, desc)
            plt.savefig(filename, bbox_inches='tight')
            print('Written', filename, flush=True)
        plt.close(fig)
    # First, get the similiarity values:
    results = []
    if os.path.isfile(COMPARE_FILE):
        with open(COMPARE_FILE, 'r') as fh:
            for line in fh:
                entries = line.strip().split()
                sims = [s for s in entries[:3]] + [int(s) for s in entries[3:]]
                results.append(sims)
    else:  # Must regenerate data:
        funcs = [compare.count_common, compare.jaccard, compare.difference]
        results = multi_compare_latest(programs, funcs)
        with open(COMPARE_FILE, 'w') as fh:
            for sims in results:
                line = ' '.join([str(r) for r in sims])
                fh.write(line + '\n')
    # Now let's plot the histograms:
    want_normed = False
    ylabel = 'Pairwise comparisons'
    plt.rc('axes', labelsize=LABEL_SIZE)
    fig, ax = plt.subplots()
    plt.hist([c[3] for c in results], range(0, 150, 5), density=want_normed)
    plt.xticks(range(0, 150, 10))
    plt.ylabel(ylabel)
    plt.xlabel('No. of shared lines')
    save_fig(fig, 'lines')
    fig, ax = plt.subplots()
    prange = list(range(0, 100, 5)) + [101]
    plt.hist([c[4] for c in results], prange, density=want_normed)
    plt.xticks(range(0, 110, 10))
    plt.ylabel(ylabel)
    plt.xlabel('Jaccard Index')
    save_fig(fig, 'jaccard')
    fig, ax = plt.subplots()
    plt.hist([c[5] for c in results], prange, density=want_normed)
    plt.xticks(range(0, 110, 10))
    plt.ylabel(ylabel)
    plt.xlabel('Tversky Index')
    save_fig(fig, 'tversky')


if __name__ == '__main__':
    graphs = 1
    if graphs == 1:  # similarity lines/jaccard/tversky
        similarity_histograms()
    elif graphs == 2:  # Violin plot
        plot_activity_violin()
    else:  # 8 scatterplots
        get_and_plot_metrics()
