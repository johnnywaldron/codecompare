#!/usr/bin/python3
'''
    Test harness to build and run a front-end on a Python test-suite.
    Default is to measure all fornt-ends against all libraries.
    Prints a table of pass rates (no of files passed / total no of files)
'''

import os
import numpy as np
import matplotlib.pyplot as plt

import compare


def file_len(fname, root=compare.DATAROOT):
    fname = os.path.join(root, fname)
    i = -1
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


class ProjInfo():
    '''Record counts for a single assignment'''
    def __init__(self):
        self.programs = 0      # Typically 1 (flex only) or 2 (flex/bison)
        self.total_files = 0   # All files for all students
        self.students_did = 0  # No. of students making at least 1 attempt
        self.total_lines = 0   # a 'wc -l' over all files

    def __str__(self):
        # No. of attempts per student (divide by no. of programs):
        files_per_student = self.total_files / self.students_did
        attempts_per_student = round(files_per_student / self.programs)
        # Avg LOC for assignment (multiply by no. of programs):
        lines_per_file = round(self.total_lines / self.total_files)
        lines_per_assignment = lines_per_file * self.programs
        return '{} {} {} {} {}'\
            .format(self.students_did,
                    self.total_files, attempts_per_student,
                    self.total_lines, lines_per_assignment)


def calc_basic_stats(projects=compare.PROJECTS):
    '''Count files, LOC per assignment for the whole set of students'''
    students = compare.get_students()
    proj_info = {p: ProjInfo() for p in projects}
    for proj in projects:
        proj_programs = [p for p in compare.PROGRAMS if p.startswith(proj)]
        proj_info[proj].programs = len(proj_programs)
        for student in students:
            sp_files = []
            for program in proj_programs:
                fnames = [os.path.join(student, program, fname)
                          for fname in compare.get_txt_files(student, program)]
                sp_files.extend(fnames)
            proj_info[proj].total_files += len(sp_files)
            if len(sp_files) > 0:
                proj_info[proj].students_did += 1
            line_count = sum([file_len(fname) for fname in sp_files])
            proj_info[proj].total_lines += line_count
    for i, p in enumerate(projects):
        pstr = ' & '.join(str(proj_info[p]).split())
        print('{} & {:20s} & {}\\\\'.format(i+1, p, pstr))
    print('Total files:', sum([p.total_files for p in proj_info.values()]))
    print('Total LOC:', sum([p.total_lines for p in proj_info.values()]))

if __name__ == '__main__':
    calc_basic_stats()
