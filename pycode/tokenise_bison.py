#!/usr/bin/python3
'''
    Test harness to build and run a front-end on a Python test-suite.
    Default is to measure all fornt-ends against all libraries.
    Prints a table of pass rates (no of files passed / total no of files)
'''

import os
import sys
import subprocess
import shutil


import codecs


BISON = '/media/jpower/passport/bigApps/parsing/flexbison/local/bin/bison'

TEST_LOG = os.path.join(os.getcwd(), 'test.log')


ACCEPTING = '--accepting rule at line '


def shell_run(cmd, dir=os.path.dirname(__file__)):
    return subprocess.call(cmd, cwd=dir, shell=True)


class Processor:
    def __init__(self, discard_comments=False):
        self.reset_file(None)
        self.discard_comments = discard_comments
        self.debug = False

    def reset_file(self, filename):
        self.filename = filename
        self.in_comment = False
        self.section = 1

    def process_token(self, token, lexeme):
        if self.debug:
            print(token, lexeme)
            return
        if self.discard_comments:
            if token in [191, 686]:  # /*
                self.in_comment = True
                return None
            elif token in [483, 495]:  # */
                self.in_comment = False
                return None
            elif token == 190:  # //  one-line comment
                return None
            elif self.in_comment:
                return None
        # Otherwise, proceed as normal:
        if token in [650, 662, 680, 791, 316]:  # code-char, quotes, open-br
            return lexeme
        elif token == 792:  # Newline
            if self.section in [1, 3]:
                return ' \n'
        elif token in [189, 403]:  # space-in-rules, Special chars
            return None
        elif lexeme == '%%':
            self.section += 1
            return ('\n'+lexeme+'\n')
        elif lexeme == ';' and token == 282:
            return ('\n'+lexeme+'\n')
        elif lexeme == '%empty':
            return (lexeme+' ')
        elif lexeme in ['%}', '%{']:
            return ('\n'+lexeme+'\n')
        elif lexeme in ['|', ':'] or lexeme.startswith('%'):  # %token, Bar
            return ('\n'+lexeme+' ')
        else:
            return (lexeme+' ')

    def process_line(self, line):
        if not line.startswith(ACCEPTING):
            return
        line = line[len(ACCEPTING):]
        token, lexeme = line.split(maxsplit=1)
        # Strip parentheses and then quotes (if there):
        if lexeme[0] == '(' and lexeme[-1] == ')':
            lexeme = lexeme[1:-1]
        if lexeme[0] == '"' and lexeme[-1] == '"':
            lexeme = lexeme[1:-1]
        return self.process_token(int(token), lexeme)

    def read_bison_file(self, yfile):
        self.reset_file(yfile)
        toExec = '{} {} > {} 2>&1'.format(BISON, yfile, TEST_LOG)
        retcode = shell_run(toExec)
        if retcode > 0:
            print('Failed with', yfile)
            return
        buffer = ''
        with codecs.open(TEST_LOG, 'r', encoding='utf-8', errors='ignore') as fh:
            prevline = fh.readline().strip()
            for line in fh:
                line = line.strip()
                if line == '")':
                    prevline += line
                    line = ''
                tok = self.process_line(prevline)
                if tok:
                    buffer += tok
                prevline = line
        return buffer.split('\n')


if __name__ == '__main__':
    for yfile in sys.argv[1:]:
        p = Processor(True)
        for line in p.read_bison_file(yfile):
            print(line)
