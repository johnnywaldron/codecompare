BASIC=basic.txt
printf "a\nb\nc\n" > $BASIC

printf "a\nb\nc\n" > same.txt
printf "a\n1\nb\n2\nc\n" > plustwo.txt
printf "b\nc\n" > lessone.txt
printf "b\n" > lesstwo.txt
printf "c\nb\na\n" > difforder.txt
printf "d\ne\nf\n" > alldiff.txt

DIFFOPTS="--suppress-common-lines --ignore-all-space --ignore-blank-lines"

OTHERS=`ls *.txt | grep -v basic.txt`

for other in $OTHERS; do
    echo '---' $BASIC $other
    sdiff -B -s $BASIC $other | wc -l
    diff $DIFFOPTS $BASIC $other | grep '[\<\>]' | wc -l
done



